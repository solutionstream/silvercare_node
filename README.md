## Initial Setup

### .env
The very first thing that needs to be done is to install a `.env` file.  The project ships with a `.env.sample` file that can be used as a starting point.

`$ cp .env.sample .env`

Edit the file according to your needs.

## Run Server (Development)

Install development dependencies:

`$ yarn install --development`

Run the development server:

`$ npm run server:dev`

## Run Server (Docker)

### builder
The raw javascript code can be built by using the `builder` docker container.
This container has all of the tools required to process the sourcecode without needing to install anything on the host machine.
The output of this container is written to the `build` folder.

To build for development:
`$ NODE_ENV=development docker-compose up --build builder`

To build production:
`$ docker-compose up --build builder`

### server
Next run the `server` container.
This will contain only the production dependencies and files required to run the server.

To run for development:
`$ docker-compose up --build server`

To run for production:
`$ NODE_ENV=production docker-compose up --build server`

### workers
It is possible to run the server without the workers container but scheduled tasks will not be execute.
This includes but is not limited to thumbnail generation, reminders, alerts, and overdue checks.

To run workers:
`$ docker-compose up --build workers`

## Run Tests

### mongodb
To run the unit tests you will need to setup a mongodb server.
Configure `.env` to point to your mongo instance whether localhost or remote.

### yarn
The environment where the tests are run needs to have all development dependencies installed.
This can be easily achieved using:

`$ yarn install --development`
or
`$ npm install --development`

### test:runner
Running the unit tests without coverage is as simple as:

`$ npm run test:runner`

### test:coverage
Running the tests and collecting code coverage is also easy:

`$ npm run test:coverage`

Test coverage will be written to the `coverage` directory.
