#!/bin/bash

ENV=${NODE_ENV:-"production"}

if [ $ENV == "production" ]; then
    npm run clean
    npm run compile:prod
else
    npm run clean
    npm run compile:dev
fi
