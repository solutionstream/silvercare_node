import * as dotenv from "dotenv";
import { URLOptions } from "./database";


// Load from .env
dotenv.config();

export const app = {
    environment: process.env.NODE_ENV || "development",
    get debug() {
        return this.environment === "development";
    },
    get production() {
        return this.environment === "production";
    },
};

export const auth = {
    // The number of rounds to pass the password through
    bcrypt_rounds: +process.env.BCRYPT_ROUNDS || 10,

    // Number of failed login attempts before locking the account
    failed_logins_max: +process.env.FAILED_LOGINS_MAX || 10,

    // How long the account should be locked for
    account_lockout_minutes: +process.env.ACCOUNT_LOCKOUT_MINS,
};

export const aws = {
    s3: {
        bucket: process.env.S3_BUCKET,
        region: process.env.S3_REGION,
        access: process.env.AWS_ACCESS_KEY,
        secret: process.env.AWS_SECRET_KEY,
    },
    ses: {
        region: process.env.AWS_DEFAULT_REGION,
        access: process.env.AWS_ACCESS_KEY,
        secret: process.env.AWS_SECRET_KEY,
    },
    sns: {
        region: process.env.AWS_DEFAULT_REGION,
        access: process.env.AWS_ACCESS_KEY,
        secret: process.env.AWS_SECRET_KEY,
    },
    sqs: {
        region: process.env.AWS_DEFAULT_REGION,
        access: process.env.AWS_ACCESS_KEY,
        secret: process.env.AWS_SECRET_KEY,
    },
};


interface DatabaseConfig {
    app: URLOptions;
    test: URLOptions;
}

export const db: DatabaseConfig = {
    app: {
        host: process.env.DB_HOST,
        name: process.env.DB_NAME,
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        mongo: { ssl: !!process.env.DB_SSL },
    },
    test: {
        host: process.env.DB_TEST_HOST,
        name: process.env.DB_TEST_NAME,
        username: process.env.DB_TEST_USER,
        password: process.env.DB_TEST_PASS,
        mongo: { ssl: !!process.env.DB_TEST_SSL },
    },
};

export const dashboard = {
    // URL where the admin dashboard can be found
    base_url: process.env.DASHBOARD_BASE_URL,
};

export const email = {
    from: process.env.FROM_EMAIL,
};

export const freeus = {
    // API Endpoint
    endpoint: process.env.FREEUS_ENDPOINT,

    // API Key
    api_key: process.env.FREEUS_API_KEY,
};

export const media = {
    // Maximum file size a user can upload (0 to disable)
    per_file_size: +process.env.PER_FILE_QUOTA,

    // Maximum space usage for a user (0 to disable)
    per_user_size: +process.env.PER_USER_QUOTE,
};

export const task = {
    // Grace period after the task's due date before considering it overdue
    overdue_after_mins: +process.env.OVERDUE_AFTER_MINS,

    // The number of minutes prior to the due date to send a reminder
    remind_before_mins: +process.env.REMIND_BEFORE_MINS,
};

export const session = {
    // Used to sign session tokens
    secret: process.env.SESSION_SECRET,

    // Session token origin - Always verified against tokens
    issuer: process.env.SESSION_ISSUER,
};

export const super_user = {
    username: process.env.SUPER_USER_EMAIL,
};

export const fixtures = {
    conditions: "conditions",
};
