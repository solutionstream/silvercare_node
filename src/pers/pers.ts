import * as request from "request-promise-native";
import { freeus } from "../config";
import { exhaustiveCheck } from "../utils";


export class PERS {

    static async post(url: string, data: object) {
        return request.post(url, {
            json: true,
            body: data,
            headers: {
                "Content-Type": "application/json",
                "X-ApiKey": freeus.api_key,
            },
        });
    }

    static action(message: PERS.Message): PERS.Action {
        const { action, vibrate } = message;

        switch (action) {
        case "PLAY1":
            return vibrate ? "PLAY1VIBRATE" : "PLAY1";
        case "PLAY2":
            return vibrate ? "PLAY2VIBRATE" : "PLAY2";
        case "PLAY3":
            return vibrate ? "PLAY3VIBRATE" : "PLAY3";
        default:
            return exhaustiveCheck(action);
        }
    }

    static date(date: Date = new Date()): string {
        const yyyy = date.getUTCFullYear();
        const MM = ("0" + (date.getUTCMonth() + 1)).slice(-2);
        const dd = ("0" + date.getUTCDate()).slice(-2);
        const hh = ("0" + date.getUTCHours()).slice(-2);
        const mm = ("0" + date.getUTCMinutes()).slice(-2);
        const ss = ("0" + date.getUTCSeconds()).slice(-2);

        return `${yyyy}-${MM}-${dd}T${hh}:${mm}:${ss}Z`;
    }

    // Send an alert event notification
    static async alertevent(options: PERS.SendOptions) {
        return PERS.post(`${freeus.endpoint}/alertevent`, {
            "AlarmNum": 12345679,
            "OpAct": PERS.action(options.message),
            "UtcDate": PERS.date(),
            "Xmit": options.imei,
        });
    }

}

export namespace PERS {

    // Acceptable PERS actions
    export namespace Action {
        export const SIGBEGIN = "SigBegin";
        export const SIGCLEAR = "SigClear";
        export const TESTHOME = "TestHome";
        export const PLAY1 = "PLAY1";
        export const PLAY2 = "PLAY2";
        export const PLAY3 = "PLAY3";
        export const PLAY1VIBRATE = "PLAY1VIBRATE";
        export const PLAY2VIBRATE = "PLAY2VIBRATE";
        export const PLAY3VIBRATE = "PLAY3VIBRATE";
        export const VIBRATE = "VIBRATE";
    }

    // Action union
    export type Action =
        typeof Action.SIGBEGIN
        | typeof Action.SIGCLEAR
        | typeof Action.TESTHOME
        | typeof Action.PLAY1
        | typeof Action.PLAY2
        | typeof Action.PLAY3
        | typeof Action.PLAY1VIBRATE
        | typeof Action.PLAY2VIBRATE
        | typeof Action.PLAY3VIBRATE
        | typeof Action.VIBRATE;

    export namespace Message {
        export const GENERIC = Action.PLAY1;
        export const TRYING_TO_REACH_YOU = Action.PLAY2;
        export const TAKE_MEDICATION = Action.PLAY3;
    }

    export interface Message {
        action: typeof Message.GENERIC | typeof Message.TRYING_TO_REACH_YOU | typeof Message.TAKE_MEDICATION;
        vibrate: boolean;
    }

    export type IMEI = string;
    export interface SendOptions {
        imei: IMEI;
        message: Message;
    }
}
