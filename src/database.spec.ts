import "mocha";
import * as database from "./database";
import {expect} from "./testing";


describe("Database Helpers", async () => {

    it("should build database urls properly", async () => {
        const host = "www.example.com:1337";
        const username = "user";
        const password = "password";
        const name = "databasename";
        const mongo = { ssl: true };

        expect(database.url({ host })).to.equal(`mongodb://${host}/`);
        expect(database.url({ host, name })).to.equal(`mongodb://${host}/${name}`);
        expect(database.url({ host, username })).to.equal(`mongodb://${username}@${host}/`);
        expect(database.url({ host, password })).to.equal(`mongodb://${host}/`);
        expect(database.url({ host, username, password })).to.equal(`mongodb://${username}:${password}@${host}/`);
        expect(database.url({ host, name, username, password })).to.equal(`mongodb://${username}:${password}@${host}/${name}`);
        expect(database.url({ host, name, username, password, mongo })).to.equal(`mongodb://${username}:${password}@${host}/${name}?ssl=true`);
    });

});
