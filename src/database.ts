import * as debug from "debug";
import * as mongo from "mongodb";
import * as query from "querystring";
import {URL} from "url";
import * as uuid from "uuid";
import * as errors from "./errors";
import * as migrations from "./migrations";
import * as models from "./models";


// Conditionally log to console.
const log = debug("database");

// Remove requirement to import this from mongodb
export {Db, MongoError, ObjectID} from "mongodb";

export function id(): mongo.ObjectID {
    return new mongo.ObjectID();
}

export function version(): string {
    return uuid.v4();
}

// Parse a string into an object id
export function parseObjectID(object: string) {
    try {
        return mongo.ObjectID.createFromHexString(object);
    } catch (_) { return undefined; }
}

// Type guard: determine whether we are dealing with an object id
export function isObjectID(object: any): object is mongo.ObjectID {
    return object instanceof mongo.ObjectID;
}

export function assert(condition: boolean, error?: string | Error) {
    if (!condition) {
        if (error instanceof Error) {
            throw error;
        } else {
            throw new errors.InvalidDocument(error);
        }
    }
}

// Flatten an object one level to allow partial calls to $set
export function $set(object: { [key: string]: { [key: string]: any; } }): object {
    const flattened: any = {};

    for (const namespace in object) {
        if (!object.hasOwnProperty(namespace)) { continue; }
        for (const key in object[namespace]) {
            if (!object[namespace].hasOwnProperty(key)) { continue; }
            flattened[`${namespace}.${key}`] = object[namespace][key];
        }
    }

    return flattened;
}

// Can either be a database ObjectID or a type containing { _id: ObjectID }
export interface Identifiable { _id: mongo.ObjectID; }
export type IDConvertible<T extends Identifiable> = T | mongo.ObjectID;

// Given any IDConvertible object return the ObjectID
export function toID<T extends Identifiable>(convertible: IDConvertible<T>): mongo.ObjectID {
    if (isObjectID(convertible)) {
        return convertible;
    } else {
        return convertible._id;
    }
}

// Given a collection of Identifiable objects, remove any objects matching the given id/identifiable
export function without<T extends Identifiable>(array: T[], exclude: IDConvertible<T>): T[] {
    const exclude_id = toID(exclude);
    return array.filter((v) => !v._id.equals(exclude_id));
}

export interface URLOptions {
    host: string;       // localhost:1337
    name?: string;      // databasename
    mongo?: object;     // additional mongo options
    username?: string;
    password?: string;
}

// Create a url from a set of URL options
// This allows us to configure different database connections in each environment
export function url(options: URLOptions) {
    const url = new URL("mongodb://www.example.com");

    url.host = options.host;
    url.pathname = options.name || "/";
    url.search = query.stringify(options.mongo);

    if (options.username) {
        url.username = options.username || "";
        url.password = options.password || "";
    }

    return url.href;
}

// Connect to a random database
// This is useful in unit tests
export async function random(options: URLOptions) {
    return connect({ ...options, name: uuid.v4() });
}

// Create a database connection
export async function connect(options: URLOptions) {
    const db_url = url(options);
    log("connecting", db_url);
    const db = await mongo.MongoClient.connect(db_url);
    return db;
}

export async function drop(db: mongo.Db) {
    await db.dropDatabase();
}


// Perform any necessary database migrations
export async function migrate(db: mongo.Db) {
    // TODO: more robust migration engine, currently just runs these scripts
    await migrations.create_super_user(db);
    await migrations.conditions(db);
}

// Perform setup for all collections
export async function setupCollections(db: mongo.Db) {
    await Promise.all([
        models.ActivityLog.setupCollection(db),
        models.CareGiver.setupCollection(db),
        models.DeviceList.setupCollection(db),
        models.Document.setupCollection(db),
        models.Job.setupCollection(db),
        models.MediaLibrary.setupCollection(db),
        models.PAC.setupCollection(db),
        models.Patient.setupCollection(db),
        models.TaskList.setupCollection(db),
        models.User.setupCollection(db),
    ]);
}

// Load documents into the database to create a known initial state
export async function fixture(db: mongo.Db, collections: {[key: string]: object[]}) {
    const promises = Object.keys(collections).map((key) => {
        return db.collection(key).insertMany(collections[key]);
    });

    await Promise.all(promises);
}

// Destroy the database with impunity.
export async function destroy(db: mongo.Db) {
    await db.dropDatabase();
    await db.close();
}
