import * as jwt from "jsonwebtoken";


export interface Claims {
    iss: string;
    sub: string;
    aud: string;
    exp?: number;
    nbf?: number;
    jti?: string;
    [key: string]: any;
}

export interface VerifyOptions {
    audience?: string;
    issuer?: string;
    subject?: string;
    ignoreExpiration?: boolean;
    ignoreNotBefore?: boolean;
    jwtId?: string;
}

export class JWT {

    static timestamp(seconds: number = 0) {
        return Math.floor(Date.now() / 1000) + seconds;
    }

    static expired(payload: Claims, skew: number = 5 * 60) {
        const timestamp = JWT.timestamp();
        const expiration = payload.exp || JWT.timestamp();
        return (expiration + skew) <= timestamp;
    }

    // Sign some claims using a secret string
    static async sign(secret: string, claims: Claims) {
        return new Promise<string>((resolve, reject) => {
            jwt.sign(claims, secret, { algorithm: "HS256" }, (error, encoded) => {
                if (error) { reject(error); }
                else { resolve(encoded); }
            });
        });
    }

    // Verify the payload of a token
    static async verify(secret: string, token: string, verify: VerifyOptions = {}) {
        const options: jwt.VerifyOptions = {
            ...verify,
            algorithms: ["HS256"],
            clockTolerance: 5 * 60,
        };

        return new Promise<Claims>((resolve, reject) => {
            jwt.verify(token, secret, options, (error, claims) => {
                if (error) { reject(error); }
                else { resolve(claims as Claims); }
            });
        });
    }

}

export namespace JWT {
    export type Error = jwt.JsonWebTokenError;
    export const Error = jwt.JsonWebTokenError;

    export type TokenExpiredError = jwt.TokenExpiredError;
    export const TokenExpiredError = jwt.TokenExpiredError;

    export type NotBeforeError = jwt.NotBeforeError;
    export const NotBeforeError = jwt.NotBeforeError;
}
