import * as AWS from "aws-sdk";
import * as nodemailer from "nodemailer";
import * as configuration from "../config";


// Get the SES configuration object
const { aws: { ses: config } } = configuration;

// Create an SES instance
export const SES = new AWS.SES({
    accessKeyId: config.access,
    secretAccessKey: config.secret,
    region: config.region,
});

// Create our mailer instance
export const mailer = nodemailer.createTransport({ SES } as any);

export { AttachmentObject } from "nodemailer";
