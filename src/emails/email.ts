import * as _ from "lodash";
import { TemplateDirectory } from "../templating";
import { AttachmentObject, mailer } from "./mailer";


// Thin wrapper around nodemailer to handle templating
export class Email {

    // Determine whether a template is rendered
    static isUnrendered(object: any): object is Email.UnrenderedTemplate {
        return typeof object.template !== "undefined";
    }

    // Render a template
    static async render(template: string, context: object) {
        const directory = TemplateDirectory.default(template);
        return directory.render(context);
    }

    // Send an email
    static async send(options: Email.Options) {
        let content = { ...options.content };
        delete options.content;

        // Get fields by rendering a template
        if (Email.isUnrendered(content)) {
            const context = { ...options, ...content.context };
            content = await this.render(content.template, context);
        }

        // Apply any content options to the options
        return await mailer.sendMail(_.merge(options, content));
    }
}

export namespace Email {

    export type Content = RenderedTemplate | UnrenderedTemplate;

    // Defines a template that needs to be rendered
    export interface UnrenderedTemplate {
        template: string;
        context?: object;
    }

    // Defines a template that has already been rendered
    export interface RenderedTemplate {
        subject?: string;
        html?: string;
        text?: string;
    }

    // Options for sending an email
    export interface Options {
        from: string;
        to?: string | string[];
        cc?: string | string[];
        bcc?: string | string[];
        replyTo?: string;
        inReplyTo?: string;
        references?: string | string[];
        headers?: any;
        attachments?: AttachmentObject[];
        alternatives?: AttachmentObject[];
        messageId?: string;
        date?: Date;
        encoding?: string;
        content: Content;
    }
}
