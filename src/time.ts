interface DateConstructor {
    current(): Date;
}

// Creates a method that can be safely mocked in tests
Date.current = () => {
    return new Date();
};
