import "mocha";
import * as config from "../config";
import * as database from "../database";
import { Bucket, BucketURL } from "../media";
import { APIUser } from "../middleware";
import { Activity, Media, MediaLibrary, Patient, User } from "../models";
import { router } from "../routes";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";


describe("MediaLibrary Routes", () => {
    let db: database.Db;
    let user: User;
    let api_user: Stubbed<APIUser>;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;
    let user_id: database.ObjectID;
    let file_id: database.ObjectID;
    let now: Date;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await database.setupCollections(db);

        now = new Date();
        user_id = database.id();
        file_id = database.id();
        user = User.factory({ _id: user_id, username: "Test" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });
    });

    afterEach(async () => {
        sandbox.restore();
        await database.drop(db);
    });

    describe("file extensions", () => {
        it("should correctly get extension", async () => {
            const simple = Media.File.extension("file.jpeg");
            expect(simple).to.equal(".jpeg");

            const multiple = Media.File.extension("file.tar.gz");
            expect(multiple).to.equal(".tar.gz");

            const leading = Media.File.extension(".htaccess");
            expect(leading).to.equal("");

            const hidden = Media.File.extension(".config.json");
            expect(hidden).to.equal(".json");
        });
    });

    describe("request upload", () => {
        let size: number;
        let mimetype: string;
        let filename: string;
        let tags: string[];
        let urlSpy: sinon.SinonSpy;

        beforeEach(async () => {
            size = 1483584;
            mimetype = "image/jpg";
            filename = "image.jpeg";
            tags = ["special"];

            urlSpy = sandbox.spy(BucketURL.prototype, "post");
            config.media.per_file_size = 0;
            config.media.per_user_size = 0;

            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if missing params", async () => {
            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if per file quota exceeded", async () => {
            // Ensure we will go over quota
            config.media.per_file_size = size - 1;

            helper.body({ size, mimetype, filename, tags });
            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "OutOfSpace",
            });
        });

        it("should fail if per user quota exceeded", async () => {
            // Ensure we will go over quota
            config.media.per_user_size = size - 1;

            helper.body({ size, mimetype, filename, tags });
            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "OutOfSpace",
            });
        });

        it("should correctly build URL", async () => {
            helper.body({ size, mimetype, filename, tags });
            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.CREATED);

            // Ensure URL generation uses correct data
            expect(urlSpy).to.have.callCount(1);
            expect(urlSpy).to.have.been.calledWith({
                filename: `medialibrary/${user_id}/${file_id}.jpeg`,
                mimetype,
                meta: {
                    original_filename: filename,
                },
            });
        });

        it("should add file to new medialibrary", async () => {
            // Ensure that user does not have a medialibrary
            const existing_library = await MediaLibrary.fetch(db, { user_id });
            expect(existing_library).to.equal(undefined);

            const file = {
                _id: file_id,
                basedir: `medialibrary/${user_id}`,
                filename: `${file_id}.jpeg`,
                filepath: `medialibrary/${user_id}/${file_id}.jpeg`,
                original_filename: filename,
                mimetype,
                size,
                tags,
                storage: "s3",
                created: now,
                updated: now,
            };

            helper.body({ size, mimetype, filename, tags });
            await helper.put(`/user/${user_id}/media/${file_id}/request`, router);

            // Ensure media library was created and file was inserted
            const medialibrary = (await MediaLibrary.fetch(db, { user_id }))!;
            expect(medialibrary.pending).to.deep.equal([file]);
        });

        it("should add file to existing medialibrary", async () => {
            // Create empty medialibrary for user
            const medialibrary = (await MediaLibrary.create(db, { user_id }))!;
            expect(medialibrary.pending).to.deep.equal([]);

            const file = {
                _id: file_id,
                basedir: `medialibrary/${user_id}`,
                filename: `${file_id}.jpeg`,
                filepath: `medialibrary/${user_id}/${file_id}.jpeg`,
                original_filename: filename,
                mimetype,
                size,
                tags,
                storage: "s3",
                created: now,
                updated: now,
            };

            helper.body({ size, mimetype, filename, tags });
            await helper.put(`/user/${user_id}/media/${file_id}/request`, router);

            // Ensure file was inserted into existing medialibrary
            await medialibrary.refresh(db);
            expect(medialibrary.pending).to.deep.equal([file]);
        });

        it("should return successfully", async () => {
            const file = {
                _id: file_id,
                basedir: `medialibrary/${user_id}`,
                filename: `${file_id}.jpeg`,
                filepath: `medialibrary/${user_id}/${file_id}.jpeg`,
                original_filename: filename,
                mimetype,
                tags,
                size,
                storage: "s3",
                created: now,
                updated: now,
            };

            helper.body({ size, mimetype, filename, tags });
            const context = await helper.put(`/user/${user_id}/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                post: urlSpy.returnValues[0],
                file,
            });
        });

        it("should return succesfully using 'me' route", async () => {
            const file = {
                _id: file_id,
                basedir: `medialibrary/${user_id}`,
                filename: `${file_id}.jpeg`,
                filepath: `medialibrary/${user_id}/${file_id}.jpeg`,
                original_filename: filename,
                mimetype,
                tags,
                size,
                storage: "s3",
                created: now,
                updated: now,
            };

            helper.body({ size, mimetype, filename, tags });
            const context = await helper.put(`/user/me/media/${file_id}/request`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                post: urlSpy.returnValues[0],
                file,
            });
        });
    });

    describe("confirm upload", () => {
        let size: number;
        let mimetype: string;
        let filename: string;
        let urlStub: sinon.SinonStub;
        let author: Activity.Author;

        beforeEach(async () => {
            size = 1483584;
            mimetype = "image/jpg";
            filename = "image.jpeg";
            urlStub = sandbox.stub(Bucket.prototype, "head");
            author = Activity.Author.factory({
                _id: database.id(),
                type: "admin",
                full_name: "Administrator",
            });

            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);

            await Patient.create(db, { user_id, full_name: "Patient" });
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/user/${user_id}/media/${file_id}/confirm`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if medialibrary does not exist", async () => {
            const context = await helper.put(`/user/${user_id}/media/${file_id}/confirm`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if there is no pending file", async () => {
            // Create an empty medialibrary
            await MediaLibrary.create(db, { user_id });

            const context = await helper.put(`/user/${user_id}/media/${file_id}/confirm`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if bucket data does not match file", async () => {
            // Bucket will report different file
            urlStub.returns({
                ContentType: "image/jpeg",
                ContentLength: 123456789,
                Metadata: {
                    original_filename: "other.jpeg",
                },
            });

            // Create file object
            const file = Media.File.factory(user_id, {
                _id: file_id,
                original_filename: filename,
                mimetype,
                size,
            });

            // Create medialibrary and add pending file
            const medialibrary = await MediaLibrary.create(db, { user_id });
            medialibrary.add_pending(file);
            await medialibrary.save(db);

            // Call endpoint and verify results
            const context = await helper.put(`/user/${user_id}/media/${file_id}/confirm`, router);
            expect(urlStub).to.have.callCount(1);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "Conflict",
            });
        });

        it("should move file from pending to confirmed", async () => {
            // Bucket will report same file
            urlStub.returns({
                ContentType: mimetype,
                ContentLength: size,
                Metadata: {
                    original_filename: filename,
                },
            });

            // Create a file object
            const file = Media.File.factory(user_id, {
                _id: file_id,
                original_filename: filename,
                mimetype,
                size,
            });

            // Create medialibrary and add pending file
            const medialibrary = await MediaLibrary.create(db, { user_id });
            medialibrary.add_pending(file);
            await medialibrary.save(db);

            // Call endpoint and verify results
            const context = await helper.put(`/user/${user_id}/media/${file_id}/confirm`, router);
            expect(urlStub).to.have.callCount(1);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal(file);

            // Ensure file is moved from pending to confirmed
            await medialibrary.refresh(db);
            expect(medialibrary.pending).to.deep.equal([]);
            expect(medialibrary.files).to.deep.equal([file]);
        });
    });

    describe("update file", () => {
        let file: Media.File;
        let file_id: database.ObjectID;

        beforeEach(async () => {
            file_id = database.id();
            file = Media.File.factory(user_id, {
                _id: file_id,
                original_filename: "test.jpeg",
                size: 100,
                mimetype: "image/jpeg",
            });

            api_user.sv_admin.returns(true);
        });

        it("should fail if medialibrary doesn't exist", async () => {
            const context = await helper.post(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if file doesn't exist", async () => {
            await MediaLibrary.create(db, { user_id });
            const context = await helper.post(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.post(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should update file", async () => {
            await MediaLibrary.create(db, { user_id, files: [file] });

            const filename = "test2.png";
            const tags = ["test"];

            helper.body({ filename, tags });

            const context = await helper.post(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                file: { ...file, original_filename: filename, tags },
            });
        });
    });

    describe("get file", () => {
        let size: number;
        let mimetype: string;
        let md5sum: string;
        let original_filename: string;
        let file: Media.File;
        let urlSpy: sinon.SinonSpy;

        beforeEach(async () => {
            size = 1483584;
            mimetype = "image/jpg";
            md5sum = "23MnPK8bIHUXAThPkdhS6w==";
            original_filename = "image.jpeg";
            file = Media.File.factory(user_id, {
                _id: file_id,
                original_filename,
                size,
                mimetype,
            });

            urlSpy = sandbox.spy(BucketURL.prototype, "get");
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const medialibrary = await MediaLibrary.create(db, { user_id, files: [file] });
            expect(medialibrary.files).to.deep.equal([file]);

            const context = await helper.get(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if medialibrary does not exist", async () => {
            const context = await helper.get(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if file does not exist", async () => {
            const medialibrary = await MediaLibrary.create(db, { user_id });
            expect(medialibrary.files).to.deep.equal([]);

            const context = await helper.get(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should get file and url", async () => {
            const medialibrary = await MediaLibrary.create(db, { user_id, files: [file] });
            expect(medialibrary.files).to.deep.equal([file]);

            const context = await helper.get(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                get_url: urlSpy.returnValues[0],
                file,
            });

            expect(urlSpy).to.have.callCount(1);
            expect(urlSpy).to.have.been.calledWith({
                filename: file.filepath,
            });
        });
    });

    describe("delete file", () => {
        let size: number;
        let mimetype: string;
        let md5sum: string;
        let original_filename: string;
        let file: Media.File;
        let bucketStub: sinon.SinonStub;

        beforeEach(async () => {
            size = 1483584;
            mimetype = "image/jpg";
            md5sum = "23MnPK8bIHUXAThPkdhS6w==";
            original_filename = "image.jpeg";
            file = Media.File.factory(user_id, {
                _id: file_id,
                original_filename,
                size,
                mimetype,
            });

            bucketStub = sandbox.stub(Bucket.prototype);
            api_user.sv_admin.returns(true);
        });

        it("should fail if medialibrary doesn't exist", async () => {
            const context = await helper.delete(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if file does not exist in database", async () => {
            const medialibrary = await MediaLibrary.create(db, { user_id });
            expect(medialibrary.files).to.deep.equal([]);

            const context = await helper.delete(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.delete(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should delete file from bucket and database", async () => {
            const medialibrary = await MediaLibrary.create(db, { user_id, files: [file] });
            expect(medialibrary.files).to.deep.equal([file]);

            // Verify the output from the API
            const context = await helper.delete(`/user/${user_id}/media/${file_id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Verify that delete is called on the bucket
            expect(Bucket.prototype.delete).to.have.callCount(1);
            expect(Bucket.prototype.delete).to.have.been.calledWith({
                path: file.filepath,
            });

            // Verify that the medialibrary no longer has the file
            await medialibrary.refresh(db);
            expect(medialibrary.files).to.deep.equal([]);
        });
    });

    describe("search library", () => {
        let pdf_1: Media.File;
        let jpg_1: Media.File;
        let jpg_2: Media.File;

        beforeEach(async () => {
            pdf_1 = Media.File.factory(user_id, {
                original_filename: "file.pdf",
                size: 12345,
                mimetype: "application/pdf",
                tags: ["A"],
            });

            jpg_1 = Media.File.factory(user_id, {
                original_filename: "image1.jpg",
                size: 23444,
                mimetype: "image/jpeg",
                tags: ["B"],
            });

            jpg_2 = Media.File.factory(user_id, {
                original_filename: "image2.jpeg",
                size: 34556,
                mimetype: "image/jpeg",
                tags: ["B", "C"],
            });

            // Allow access to the route
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/user/${user_id}/media`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should return empty array for missing medialibrary", async () => {
            const context = await helper.get(`/user/${user_id}/media`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                files: [],
                pending: [],
            });
        });

        it("should return all files when no search criteria", async () => {
            await MediaLibrary.create(db, { user_id, files: [pdf_1, jpg_1], pending: [jpg_2] });

            const context = await helper.get(`/user/${user_id}/media`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                files: [pdf_1, jpg_1],
                pending: [jpg_2],
            });
        });

        it("should return all files matching tag", async () => {
            await MediaLibrary.create(db, { user_id, files: [pdf_1, jpg_1], pending: [jpg_2] });

            helper.query({ tags: "A" });
            const context_1 = await helper.get(`/user/${user_id}/media`, router);
            expect(context_1.status).to.equal(status.OK);
            expect(context_1.body).to.json.equal({
                files: [pdf_1],
                pending: [],
            });

            helper.query({ tags: ["A", "C"] });
            const context_2 = await helper.get(`/user/${user_id}/media`, router);
            expect(context_2.status).to.equal(status.OK);
            expect(context_2.body).to.json.equal({
                files: [pdf_1],
                pending: [jpg_2],
            });
        });

        it("should return all files matching extension", async () => {
            await MediaLibrary.create(db, { user_id, files: [pdf_1, jpg_1], pending: [jpg_2] });

            helper.query({ extensions: ".pdf" });
            const context_1 = await helper.get(`/user/${user_id}/media`, router);
            expect(context_1.status).to.equal(status.OK);
            expect(context_1.body).to.json.equal({
                files: [pdf_1],
                pending: [],
            });

            helper.query({ extensions: [".jpg", ".jpeg"] });
            const context_2 = await helper.get(`/user/${user_id}/media`, router);
            expect(context_2.status).to.equal(status.OK);
            expect(context_2.body).to.json.equal({
                files: [jpg_1],
                pending: [jpg_2],
            });
        });
    });

});
