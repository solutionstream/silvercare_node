import "koa";
import "mocha";
import * as config from "../config";
import * as database from "../database";
import { APIUser } from "../middleware";
import { User } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("User Routes", async () => {
    let db: database.Db;
    let api_user: Stubbed<APIUser>;
    let user_id: database.ObjectID;
    let existing_user: User;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;
    const username = "test@example.com";
    const password = "secret";
    const roles = ["caregiver"];

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await database.setupCollections(db);
        user_id = database.id();
        existing_user = await User.create(db, { _id: user_id, username: "username" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(existing_user) as any;
        helper = new RouteHelper({ state: { db, api_user, user: existing_user } });
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    describe("create user", () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should fail to create user without username and password", async () => {
            const user_id = database.id();
            const context = await helper.put(`/user/${user_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should create user without password or roles", async () => {
            const user_id = database.id();
            helper.body({ username });

            const context = await helper.put(`/user/${user_id}`, router);
            const user = await db.collection("user").findOne({ username });
            expect(user.password).to.equal(null);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: user_id,
                username,
                roles: [],
                read_tacos: false,
            });
        });

        it("should create user with password and roles", async () => {
            const user_id = database.id();
            helper.body({ username, password, roles });

            const context = await helper.put(`/user/${user_id}`, router);
            const user = await db.collection("user").findOne({ username });
            expect(user.password).to.not.equal(password); // encrypted
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: user_id,
                roles,
                username,
                read_tacos: false,
            });
        });
    });

    describe("update user", () => {
        let username: string;
        let password: string;
        let read_tacos: boolean;

        beforeEach(async () => {
            username = "test2@example.com";
            password = "hunter2";
            read_tacos = true;
            api_user.sv_admin.returns(true);
        });

        it("should fail for invalid params", async () => {
            const context = await helper.post(`/user/${user_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({ error: "InvalidParams" });
        });

        it("should fail to update user if user doesn't exist", async () => {
            helper.body({ username, password, read_tacos });

            const context = await helper.post(`/user/${database.id()}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({ error: "Not Found" });
        });

        it("should update user", async () => {
            helper.body({ username, password, read_tacos });

            const context = await helper.post(`/user/${user_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: user_id,
                roles: existing_user.roles,
                username,
                read_tacos,
            });
        });
    });
});
