import * as status from "http-status";
import * as Router from "koa-router";
import * as database from "../database";
import * as errors from "../errors";
import { Agenda } from "../jobs";
import { APIUser } from "../middleware";
import { IContact, Media, MediaLibrary, PAC } from "../models";
import { undef, Variadic } from "../utils";
import { assertEmpty, parseId } from "./utils";


interface ContextState {
    db: database.Db;
    api_user: APIUser;
    pac_id: database.ObjectID;
    user_id: database.ObjectID;
}


const router = new Router();

// Parse url parameters
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));
router.param("pac_id", parseId("pac_id"));

// Routes
router.put("/user/:user_id/pac/:pac_id", createPAC());

router.get("/pac", searchPACS());
router.get("/pac/:pac_id", getPAC());
router.put("/pac/:pac_id", updatePAC());
router.put("/pac/:pac_id/invite", invitePAC());

export default router;


interface CreatePACBody {
    full_name?: string;
    addresses?: Variadic<IContact.PhysicalAddress.JSON>;
    phones?: Variadic<IContact.PhoneNumber.JSON>;
    emails?: Variadic<IContact.EmailAddress.JSON>;
    theme?: PAC.Theme.JSON;
}

export function createPAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, pac_id } = ctx.state as ContextState;
        const {
            full_name,
            addresses: addressJSON,
            phones: phoneJSON,
            emails: emailJSON,
            theme: themeJSON,
            ...unexpected,
        } = ctx.request.body as CreatePACBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!full_name) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: "Missing required field",
            });
        }

        // Parse params
        const addresses = undef.map(addressJSON, IContact.PhysicalAddress.fromJSON);
        const phones = undef.map(phoneJSON, IContact.PhoneNumber.fromJSON);
        const emails = undef.map(emailJSON, IContact.EmailAddress.fromJSON);
        const theme = undef.apply(themeJSON, PAC.Theme.fromJSON);

        // Set the user's profile photo
        let photo: undefined | Media.Reference;
        const medialibrary = await MediaLibrary.fetch(db, { user_id });
        if (medialibrary) {
            photo = medialibrary.profile_reference(user_id);
        }

        // Create a PAC object
        const pac = PAC.factory({
            _id: pac_id,
            user_id,
            full_name,
            photo,
            addresses,
            phones,
            emails,
            theme,
        });

        try {
            PAC.validate(pac);
        } catch (error) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: `Validation Error: ${error.message}`,
            });
        }

        await PAC.insert(db, pac);
        ctx.status = status.CREATED;
        ctx.body = pac;
    };
}

interface UpdatePACBody {
    full_name?: string;
    addresses: Variadic<IContact.PhysicalAddress.JSON>;
    phones: Variadic<IContact.PhoneNumber.JSON>;
    emails: Variadic<IContact.EmailAddress.JSON>;
    theme?: PAC.Theme.JSON;
}

export function updatePAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, pac_id } = ctx.state as ContextState;
        const {
            full_name,
            addresses: addressJSON,
            phones: phoneJSON,
            emails: emailJSON,
            theme: themeJSON,
            ...unexpected,
        } = ctx.request.body as UpdatePACBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.pac_self(pac_id) && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Parse params
        const addresses = undef.map(addressJSON, IContact.PhysicalAddress.fromJSON);
        const phones = undef.map(phoneJSON, IContact.PhoneNumber.fromJSON);
        const emails = undef.map(emailJSON, IContact.EmailAddress.fromJSON);
        const theme = undef.apply(themeJSON, PAC.Theme.fromJSON);

        // Lookup the patient object
        const pac = await PAC.fetch(db, { _id: pac_id });
        if (!pac) {
            throw errors.create(status.NOT_FOUND);
        }

        // Set the user's profile photo
        let photo: undefined | Media.Reference;
        const medialibrary = await MediaLibrary.fetch(db, { user_id: pac.user_id });
        if (medialibrary) {
            photo = medialibrary.profile_reference(pac.user_id);
        }

        // Update the patient values
        pac.update({
            full_name,
            photo,
            addresses,
            phones,
            emails,
            theme,
        });

        try {
            PAC.validate(pac);
        } catch (error) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: `Validation Error: ${error.message}`,
            });
        }

        await pac.save(db);
        ctx.status = status.OK;
        ctx.body = pac;
    };
}

export function getPAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, pac_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const pac = await PAC.fetch(db, { _id: pac_id });
        if (!pac) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we have access to this route
        if (!api_user.patient_of(pac)
            && !api_user.caregiver_of(pac)
            && !api_user.pac_self(pac)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        ctx.status = status.OK;
        ctx.body = pac;
    };
}

interface SearchPACQuery {
    limit?: string;
    full_name?: string;
    address?: string;
    phone?: string;
    email?: string;
}

export function searchPACS(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db } = ctx.state as ContextState;
        const {
            limit: limitJSON,
            full_name,
            address,
            phone,
            email,
            ...unexpected,
        } = ctx.request.query as SearchPACQuery;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we have access to the route
        if (!api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!limitJSON) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: "Missing required fields",
            });
        }

        const pacs = await PAC.search(db, {
            limit: parseInt(limitJSON, 10),
            full_name,
            address,
            phone,
            email,
        });

        ctx.status = status.OK;
        ctx.body = { pacs };
    };
}

export function invitePAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, pac_id } = ctx.state as ContextState;

        // Ensure that we are allowed to call this route
        if (!api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Get the PAC object
        const pac = await PAC.fetch(db, { _id: pac_id });
        if (!pac) {
            throw errors.create(status.NOT_FOUND);
        }

        // Schedule invitation email for PAC
        await Agenda.invite_pac(db, { user_id: pac.user_id });
        ctx.status = status.NO_CONTENT;
    };
}
