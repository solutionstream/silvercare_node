import "koa";
import "mocha";
import * as config from "../config";
import * as database from "../database";
import { APIUser } from "../middleware";
import { Media, PAC, User } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("PAC Routes", () => {
    let db: database.Db;
    let user: User;
    let user_id: database.ObjectID;
    let api_user: Stubbed<APIUser>;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await database.setupCollections(db);
        user = await User.create(db, { username: "user" });
        user_id = user._id;

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });
    });

    afterEach(async () => {
        sandbox.restore();
        await database.drop(db);
    });


    describe("create pac", () => {
        let pac_id: database.ObjectID;

        beforeEach(async () => {
            pac_id = database.id();
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail for invalid params", async () => {
            // Call without params
            const context_1 = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context_1.status).to.equal(status.BAD_REQUEST);
            expect(context_1.body).to.json.equal({
                error: "InvalidParams",
            });

            // Call with invalid params
            helper.body({ full_name: "" });
            const context_2 = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context_2.status).to.equal(status.BAD_REQUEST);
            expect(context_2.body).to.json.equal({
                error: "InvalidParams",
            });

            // Call with unknown params
            helper.body({ unknown: 1 });
            const context_3 = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context_3.status).to.equal(status.BAD_REQUEST);
            expect(context_3.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if pac already exists", async () => {
            await PAC.create(db, { _id: pac_id, full_name: "PAC", user_id });
            helper.body({ full_name: "PAC" });

            const context = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.INTERNAL_SERVER_ERROR);
            expect(context.body).to.json.equal({
                error: "MongoError",
            });
        });

        it("should create pac for user id", async () => {
            const pac = PAC.factory({
                _id: pac_id,
                user_id,
                full_name: "PAC",
                theme: {
                    icon: new Media.Reference(database.id(), database.id()),
                    primary_color: "#CC00CC",
                },
                addresses: [{ type: "home", address: "123 Fake St." }],
                phones: [{ type: "mobile", number: "+15551234567" }],
                emails: [{ type: "home", email: "user@pac.com" }],
            });

            helper.body({
                full_name: pac.full_name,
                theme: pac.theme,
                addresses: pac.addresses,
                phones: pac.phones,
                emails: pac.emails,
            });

            // Everything but the version string should match
            delete pac.version;

            const context = await helper.put(`/user/${user_id}/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.shallow.json.equal(pac);
        });

        it("should create pac for current user", async () => {
            const pac = PAC.factory({
                _id: pac_id,
                user_id,
                full_name: "PAC",
            });

            helper.body({
                full_name: pac.full_name,
            });

            // Everything but the version string should match
            delete pac.version;

            const context = await helper.put(`/user/me/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.shallow.json.equal(pac);
        });
    });

    describe("search pacs", () => {
        it("should fail for invalid params");
        it("should search pacs by name");
        it("should search pacs by address");
        it("should search pacs by phone number");
        it("should search pacs by email address");
    });

    describe("get pac", () => {

        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            const pac = await PAC.create(db, { full_name: "PAC", user_id });
            api_user.patient_of.returns(false);
            api_user.caregiver_of.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.patient_of).to.have.callCount(1);
            expect(api_user.caregiver_of).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if pac does not exist", async () => {
            const pac_id = database.id();
            const context = await helper.get(`/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should return pac matching id", async () => {
            const pac = await PAC.create(db, { full_name: "PAC", user_id });
            const context = await helper.get(`/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal(pac);
        });
    });

    describe("update pac", () => {
        let pac: PAC;
        let pac_id: database.ObjectID;

        beforeEach(async () => {
            pac_id = database.id();
            pac = await PAC.create(db, {
                _id: pac_id,
                user_id,
                full_name: "PAC",
                theme: {
                    icon: new Media.Reference(database.id(), database.id()),
                    primary_color: "#CC00CC",
                },
                addresses: [{ type: "home", address: "123 Fake St." }],
                phones: [{ type: "mobile", number: "+15551234567" }],
                emails: [{ type: "home", email: "user@pac.com" }],
            });

            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.pac_self.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.pac_self).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail for invalid params", async () => {
            // Call with invalid params
            helper.body({ full_name: "" });
            const context_1 = await helper.put(`/pac/${pac_id}`, router);
            expect(context_1.status).to.equal(status.BAD_REQUEST);
            expect(context_1.body).to.json.equal({
                error: "InvalidParams",
            });

            // Call with unknown params
            helper.body({ unknown: 1 });
            const context_2 = await helper.put(`/user/${user._id}/pac/${pac_id}`, router);
            expect(context_2.status).to.equal(status.BAD_REQUEST);
            expect(context_2.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if pac does not exist", async () => {
            const pac_id = database.id();
            const context = await helper.put(`/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should update pac values", async () => {
            pac.update({
                full_name: "PAC_2",
                theme: { icon: null, primary_color: null },
                addresses: [],
                phones: [],
                emails: [],
            });

            helper.body({
                full_name: pac.full_name,
                theme: pac.theme,
                addresses: pac.addresses,
                phones: pac.phones,
                emails: pac.emails,
            });

            // Everything but version should match
            delete pac.version;

            const context = await helper.put(`/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.shallow.json.equal(pac);
        });

        it("should not modify pac values that are not specified", async () => {
            // Everything but the version should remain the same
            delete pac.version;

            const context = await helper.put(`/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.shallow.json.equal(pac);
        });
    });

    describe("invite pac", () => {

    });
});
