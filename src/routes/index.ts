import * as Router from "koa-router";
import * as database from "../database";
import activity from "./activity_routes";
import caregiver from "./caregiver_routes";
import device from "./device_routes";
import medialibrary from "./medialibrary_routes";
import medication from "./medication_routes";
import pac from "./pac_routes";
import patient from "./patient_routes";
import task from "./task_routes";
import {router as user, unauthenticated as user_noauth} from "./user_routes";


// Collapse routes into single router
export const router = new Router();
router.use("", activity.routes(), activity.allowedMethods());
router.use("", caregiver.routes(), caregiver.allowedMethods());
router.use("", device.routes(), device.allowedMethods());
router.use("", medialibrary.routes(), medialibrary.allowedMethods());
router.use("", medication.routes(), medication.allowedMethods());
router.use("", pac.routes(), pac.allowedMethods());
router.use("", patient.routes(), patient.allowedMethods());
router.use("", task.routes(), task.allowedMethods());
router.use("", user.routes(), task.allowedMethods());
router.get("/id", async (ctx) => {
    ctx.status = 200;
    ctx.body = { id: database.id() };
});

// Collapse unathenticated routes into single router
export const noauth = new Router();
noauth.use("", user_noauth.routes(), user_noauth.allowedMethods());
