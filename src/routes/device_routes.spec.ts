import "koa";
import "mocha";
import * as config from "../config";
import * as database from "../database";
import { APIUser, IAPIUserMethods } from "../middleware";
import { Device, DeviceList, User } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("Device Routes", async () => {
    let db: database.Db;
    let now: Date;
    let api_user: Stubbed<APIUser, keyof IAPIUserMethods>;
    let user: User;
    let user_id: database.ObjectID;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await db.dropDatabase();
        await database.setupCollections(db);

        now = new Date();
        user_id = database.id();
        user = User.factory({ _id: user_id, username: "api-user" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);

        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    describe("search devices", async () => {
        let mobile: Device.Mobile;

        beforeEach(async () => {
            mobile = {
                _id: database.id(),
                type: "ios",
                version: { major: 0, minor: 0, patch: 1 },
                push_token: "Exponent[asf]",
                created: new Date(),
            };

            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/user/${user_id}/device`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should get list of devices", async () => {
            await DeviceList.create(db, { user_id, devices: [mobile] });
            const context = await helper.get(`/user/${user_id}/device`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                devices: [mobile],
            });
        });
    });

    describe("register", async () => {
        let device_id: database.ObjectID;

        beforeEach(async () => {
            device_id = database.id();
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if there is no device", async () => {
            const context = await helper.put(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "Bad Request",
            });
        });

        it("should automatically create devicelist", async () => {
            const device = {
                type: "email",
                email_address: "mmouse@aol.com",
            };

            helper.body({ device });
            await helper.put(`/user/${user_id}/device/${device_id}`, router);

            const devicelist = await DeviceList.fetch(db, { user_id });
            expect(devicelist!.devices).to.deep.equal([
                {
                    _id: device_id,
                    created: now,
                    ...device,
                },
            ]);
        });

        it("should modify existing devicelist", async () => {
            const devicelist = await DeviceList.create(db, { user_id });
            const device = {
                type: "sms",
                phone_number: "+1-555-867-5309",
            };

            helper.body({ device });
            await helper.put(`/user/${user_id}/device/${device_id}`, router);

            await devicelist.refresh(db);
            expect(devicelist.devices).to.deep.equal([
                {
                    _id: device_id,
                    created: now,
                    ...device,
                },
            ]);
        });

        it("should create device without token", async () => {
            const device = {
                type: "ios",
                version: { major: 0, minor: 0, patch: 0 },
                push_token: null,
            };

            helper.body({ device });
            const context = await helper.put(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.equal(undefined);

            const devicelist = await DeviceList.fetch(db, { user_id });
            expect(devicelist!.devices).to.deep.equal([
                {
                    _id: device_id,
                    created: now,
                    ...device,
                },
            ]);
        });

        it("should create device with token", async () => {
            const device = {
                type: "android",
                version: { major: 0, minor: 0, patch: 0 },
                push_token: "0df980dfdf098df",
            };

            helper.body({ device });
            const context = await helper.put(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.equal(undefined);

            const devicelist = await DeviceList.fetch(db, { user_id });
            expect(devicelist!.devices).to.deep.equal([
                {
                    _id: device_id,
                    created: now,
                    ...device,
                },
            ]);
        });

        it("should update existing device", async () => {
            const device: Device = {
                _id: device_id,
                created: new Date(),
                type: "ios",
                version: { major: 0, minor: 0, patch: 0 },
                push_token: null,
            };

            const devicelist = await DeviceList.create(db, { user_id, devices: [device] });
            const updated_device = {
                ...device,
                version: { major: 0, minor: 1, patch: 0 },
                push_token: "sd1fasd5foiuas8df",
            };

            helper.body({ device: updated_device });
            const context = await helper.put(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.equal(undefined);

            await devicelist.refresh(db);
            expect(devicelist.devices).to.deep.equal([
                {
                    ...device,
                    created: now,
                    version: { major: 0, minor: 1, patch: 0 },
                    push_token: "sd1fasd5foiuas8df",
                },
            ]);
        });
    });

    describe("unregister", async () => {

        let device_id: database.ObjectID;
        let device: Device.SMS;

        beforeEach(async () => {
            device_id = database.id();
            device = {
                _id: device_id,
                type: "sms",
                phone_number: "+15558675309",
                created: new Date(),
            };

            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.delete(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(2);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should remove device", async () => {
            const devicelist = await DeviceList.create(db, { user_id, devices: [device] });
            const context = await helper.delete(`/user/${user_id}/device/${device_id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            await devicelist.refresh(db);
            expect(devicelist.devices).to.deep.equal([]);
        });
    });

    describe("ping pers", async () => {

        beforeEach(async () => {
            sandbox.stub(DeviceList.prototype, "pers");
            api_user.sv_admin.returns(true);
        });

        it("should fail if user has no pers", async () => {
            const context = await helper.put(`/user/${user_id}/device/pers/ping`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should should enforce access controls", async () => {
            api_user.user_self.returns(false);
            api_user.caregiver_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/user/${user_id}/device/pers/ping`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.user_self).to.have.callCount(1);
            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should ping pers", async () => {
            await DeviceList.create(db, {
                user_id,
                devices: [{
                    _id: database.id(),
                    created: new Date(),
                    type: "pers",
                    identifier: "098sdf098dsf",
                }],
            });

            const context = await helper.put(`/user/${user_id}/device/pers/ping`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);
            expect(DeviceList.prototype.pers).to.have.callCount(1);
            expect(DeviceList.prototype.pers).to.have.been.calledWith({
                message: {
                    action: "PLAY2",
                    vibrate: true,
                },
            });
        });
    });
});
