import * as status from "http-status";
import * as Router from "koa-router";
import * as moment from "moment";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { Activity, ActivityLog, Patient } from "../models";
import { undef } from "../utils";
import { assertEmpty, parseId } from "./utils";


interface ContextState {
    db: database.Db;
    api_user: APIUser;
    patient_id: database.ObjectID;
    journal_id: database.ObjectID;
}

const router = new Router();

// Parse url parameters
router.param("patient_id", parseId("patient_id"));
router.param("journal_id", parseId("journal_id"));

// Routes
router.put("/patient/:patient_id/journal/:journal_id", createJournal());
router.get("/patient/:patient_id/activities", searchActivities());

export default router;


interface CreateJournalBody {
    title?: string;
    body?: string;
}

function createJournal(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, db, patient_id, journal_id } = ctx.state as ContextState;
            const { title, body, ...unexpected } = ctx.request.body as CreateJournalBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get the patient matching the id
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient", {
                    debug: "Patient with id not found",
                });
            }

            // Only caregivers can access this route
            if (!api_user.caregiver_to(patient)) {
                throw errors.create(status.FORBIDDEN);
            }

            if (!title || !body) {
                throw errors.create(status.BAD_REQUEST, "MissingParams", {
                    debug: "title and body are required fields",
                });
            }

            // Create the journal entry using supplied params
            const author = api_user.author_for(patient);
            const journal = await ActivityLog.create(db, {
                _id: journal_id,
                patient_id,
                type: "journal",
                title,
                meta: { body },
                author,
            });

            // Output created journal
            ctx.status = status.CREATED;
            ctx.body = journal;

        } catch (error) {
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, error.message);
            }

            throw error;
        }
    };
}

interface SearchActivitiesParams {
    type?: string;
    term?: string;
    start?: string;
    until?: string;
    limit?: string;
    author_id?: string;
}

function searchActivities(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, db, patient_id } = ctx.state as ContextState;
            const {
                term,
                type: type_string,
                start: start_string,
                until: until_string,
                limit: limit_string,
                author_id: author_id_string,
                ...unexpected,
            } = ctx.query as SearchActivitiesParams;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get the patient matching the patient id
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient", {
                    debug: "Patient with id not found",
                });
            }

            // Ensure that we are allowed to access this route
            if (!api_user.caregiver_to(patient) && !api_user.pac_to(patient) && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            const author_id = undef.apply(author_id_string, database.parseObjectID);
            const start = undef.apply(start_string, (str) => moment(str).toDate());
            const until = undef.apply(until_string, (str) => moment(str).toDate());
            const limit = undef.apply(limit_string, (str) => parseInt(str, 10));
            let type: undefined|Activity.Type;

            // Date range is required
            if (!start) {
                throw errors.create(status.BAD_REQUEST, "InvalidDateRange", {
                    debug: "start is a required field",
                });
            }

            if (!limit && !until) {
                throw errors.create(status.BAD_REQUEST, "InvalidDateRange", {
                    debug: "either limit or until must be provided",
                });
            }

            // Parse the caregiver id string if provided
            if (author_id_string && !author_id) {
                throw errors.create(status.BAD_REQUEST, "InvalidID", {
                    debug: "CareGiver id is not valid",
                });
            }

            // Ensure type string is one of the activity types
            if (type_string) {
                if (!ActivityLog.is_type(type_string)) {
                    throw errors.create(status.BAD_REQUEST, "InvalidType", {
                        debug: `Valid type values are: ${Object.keys(Activity.Type)}`,
                    });
                }

                type = type_string;
            }

            // Search activities matching parameters
            const activities = await ActivityLog.search(db, {
                patient_id,
                type,
                term,
                start,
                until,
                limit,
                author_id,
            });

            ctx.status = status.OK;
            ctx.body = { activities };

        } catch (error) {
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, error.message);
            }

            throw error;
        }
    };
}
