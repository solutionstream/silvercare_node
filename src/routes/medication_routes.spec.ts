import "mocha";
import * as moment from "moment";
import * as config from "../config";
import * as database from "../database";
import { APIUser } from "../middleware";
import { Activity, Media, Medication, Note, Patient, TaskList, User } from "../models";
import { router } from "../routes";
import { Schedule } from "../scheduling";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";


describe("Medication Routes", async () => {
    let counter: number;
    let db: database.Db;
    let api_user: Stubbed<APIUser>;
    let sandbox: sinon.SinonSandbox;
    let helper: RouteHelper;
    let user_id: database.ObjectID;
    let user: User;
    let patient: Patient;
    let name: string;
    let dosage: string;
    let rx_num: string;
    let next_refill: null | Date;
    let pharmacy: string;
    let prescriber: string;
    let purpose: string;
    let instructions: string;
    let appearance: string;
    let photo: Media.Reference;
    let notes: Note[];
    let attachments: Media.Reference[];

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        counter = 0;
        name = "Fukitol";
        dosage = "10mg";
        rx_num = "RX1233456";
        next_refill = null;
        pharmacy = "Walgreens";
        prescriber = "Tom's Drugs";
        purpose = "Existential Dread";
        instructions = "Take it... or don't";
        appearance = "";
        photo = createMediaReference();
        notes = [ createNote() ];
        attachments = [ createMediaReference() ];

        await database.setupCollections(db);

        user_id = database.id();
        user = await User.create(db, { _id: user_id, username: "Test" });
        patient = await Patient.create(db, { user_id, full_name: "Patient" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });
    });

    afterEach(async () => {
        sandbox.restore();
        await database.drop(db);
    });

    interface CreateMedication {
        schedule: Schedule;
        _id?: database.ObjectID;
        pillpack_id?: database.ObjectID;
        name?: string;
        dosage?: string;
        rx_num?: string;
        next_refill?: null | Date;
        pharmacy?: string;
        prescriber?: string;
        purpose?: string;
        appearance?: string;
        instructions?: string;
        photo?: Media.Reference;
        notes?: Note[];
        attachments?: Media.Reference[];
    }

    // Create a medication object
    async function createMedication(patient: Patient, options: CreateMedication) {
        const medication = Medication.factory({
            _id: options._id || database.id(),
            pillpack_id: options.pillpack_id || database.id(),
            name: options.name || `[${counter++}] ${name}`,
            dosage: options.dosage || `[${counter++}] ${dosage}`,
            rx_num: options.rx_num || `[${counter++}] ${rx_num}`,
            next_refill: options.next_refill,
            pharmacy: options.pharmacy || `[${counter++}] ${pharmacy}`,
            prescriber: options.prescriber || `[${counter++}] ${prescriber}`,
            purpose: options.purpose || `[${counter++}] ${purpose}`,
            appearance: options.appearance || `[${counter++}] ${appearance}`,
            instructions: `[${counter++}] ${instructions}`,
            schedule: options.schedule,
            photo: options.photo || photo,
            notes: options.notes || notes,
            attachments: options.attachments || attachments,
        });

        await patient.add_medication(db, medication);
        return medication;
    }

    function createMediaReference() {
        return new Media.Reference(
            database.id(),
            database.id(),
        );
    }

    function createNote(options: Partial<Note> = {}) {
        const note: Note = {
            author: options.author || `[${counter++}] Author`,
            content: options.content || `[${counter++}] Content`,
            date: options.date || new Date(),
        };

        return note;
    }

    describe("get medication", async () => {
        let now: Date;
        let schedule: Schedule;

        beforeEach(async () => {
            now = moment().toDate();
            schedule = Schedule.hourly(now, 8);
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/patient/${patient._id}/medications`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient does not exist", async () => {
            const patient_id = database.id();
            const context = await helper.get(`/patient/${patient_id}/medications`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if medication does not exist", async () => {
            const medication_id = database.id();
            const context = await helper.get(`/patient/${patient._id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should get specific medication", async () => {
            await createMedication(patient, { schedule });
            const medication = await createMedication(patient, { schedule });

            const context = await helper.get(`/patient/${patient._id}/medications/${medication._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({ medication });
        });

        it("should get all medications", async () => {
            const medication_1 = await createMedication(patient, { schedule });
            const medication_2 = await createMedication(patient, { schedule });

            const context = await helper.get(`/patient/${patient._id}/medications`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                medications: [ medication_1, medication_2 ],
            });
        });
    });

    describe("create route", async () => {
        let now: Date;
        let schedule: Schedule;
        let medication_id: database.ObjectID;
        let author: Activity.Author;

        beforeEach(async () => {
            now = moment().toDate();
            schedule = Schedule.hourly(now, 8);
            medication_id = database.id();
            author = Activity.Author.factory({
                _id: database.id(),
                type: "admin",
                full_name: "Administrator",
            });

            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/patient/${patient._id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if params are missing", async () => {
            const context = await helper.put(`/patient/${patient._id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if the patient does not exist", async () => {
            const patient_id = database.id();
            const context = await helper.put(`/patient/${patient_id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should create associated tasklist", async () => {
            helper.body({
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                instructions,
                appearance,
                schedule,
            });

            // Try to create a medication for an existing medication
            const context = await helper.put(`/patient/${patient._id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: medication_id,
                pillpack_id: null,
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                instructions,
                appearance,
                schedule,
                photo: null,
                notes: [],
                attachments: [],
            });

            // Ensure that a tasklist is created for the medication
            const tasklist = await db.collection("tasklist").findOne({ medication_id });
            expect(tasklist).to.shallow.equal({
                medication_id,
                patient_id: patient._id,
                sequences: [ { schedule } ],
                shared: {
                    title: name,
                    type: "medication",
                    description: instructions,
                    notes: [],
                    attachments: [],
                },
            });
        });
    });

    describe("update route", async () => {
        let now: Date;
        let schedule: Schedule;
        let medication: Medication;
        let tasklist: TaskList;
        let author: Activity.Author;

        beforeEach(async () => {
            now = moment().toDate();
            schedule = Schedule.hourly(now, 8);
            medication = await createMedication(patient, { schedule });
            tasklist = await TaskList.insert(db, TaskList.fromMedication(patient, medication));
            author = Activity.Author.factory({
                _id: database.id(),
                type: "admin",
                full_name: "Administrator",
            });

            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.post(`/patient/${patient._id}/medications/${medication._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient not found", async () => {
            const patient_id = database.id();
            const medication_id = database.id();

            const context = await helper.post(`/patient/${patient_id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchPatient",
            });
        });

        it("should fail if medication not found", async () => {
            const medication_id = database.id();

            const context = await helper.post(`/patient/${patient._id}/medications/${medication_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchMedication",
            });
        });

        it("should edit the related tasklist", async () => {
            const pillpack_id = database.id();
            const name = "New";
            const dosage = "100g";
            const rx_num = "RX0";
            const next_refill = null;
            const pharmacy = "Walgreens";
            const prescriber = "";
            const purpose = "Pain";
            const instructions = "As needed";
            const appearance = "Large gel capsule";
            const photo = createMediaReference();
            const notes = [ createNote({}) ];
            const attachments = [ createMediaReference() ];

            helper.body({
                pillpack_id,
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                instructions,
                appearance,
                photo,
                notes,
                attachments,
            });

            const context = await helper.post(`/patient/${patient._id}/medications/${medication._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                _id: medication._id,
                pillpack_id,
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                instructions,
                appearance,
                photo,
                notes,
                attachments,
                schedule: medication.schedule,
            });

            await tasklist.refresh(db);
            expect(tasklist.medication_id).to.deep.equal(medication._id);
            expect(tasklist.patient_id).to.deep.equal(patient._id);
            expect(tasklist.sequences).to.shallow.equal([ { schedule: medication.schedule } ]);
            expect(tasklist.shared).to.deep.equal({
                type: "medication",
                title: medication.name, // Original name
                description: medication.instructions, // Original instructions
                notes,
                attachments,
            });
        });
    });

    describe("update schedule route", async () => {
        let now: Date;
        let schedule: Schedule;
        let medication: Medication;
        let tasklist: TaskList;
        let author: Activity.Author;

        beforeEach(async () => {
            now = moment().toDate();
            schedule = Schedule.hourly(now, 8);
            medication = await createMedication(patient, { schedule });
            tasklist = await TaskList.insert(db, TaskList.fromMedication(patient, medication));
            author = Activity.Author.factory({
                _id: database.id(),
                type: "admin",
                full_name: "Administrator",
            });

            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.post(`/patient/${patient._id}/medications/${medication._id}/schedule`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if schedule is missing", async () => {
            const context = await helper.post(`/patient/${patient._id}/medications/${medication._id}/schedule`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should edit tasklist schedule", async () => {
            const new_date = moment(now).add({ m: 1 }).toDate();
            const new_until = moment(new_date).add({ ms: -1 }).toDate();
            const schedule = Schedule.hourly(new_date, 12);
            helper.body({ schedule });

            const context = await helper.post(`/patient/${patient._id}/medications/${medication._id}/schedule`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                ...medication,
                schedule,
            });

            await tasklist.refresh(db);
            expect(tasklist.medication_id).to.deep.equal(medication._id);
            expect(tasklist.patient_id).to.deep.equal(patient._id);
            expect(tasklist.sequences).to.shallow.equal([
                { schedule: { ...medication.schedule, until: new_until }},
                { schedule },
            ]);
            expect(tasklist.shared).to.deep.equal({
                type: "medication",
                title: medication.name,
                description: medication.instructions,
                notes: medication.notes,
                attachments: medication.attachments,
            });
        });
    });

    describe("delete medication", async () => {
        let now: Date;
        let sandbox: sinon.SinonSandbox;
        let schedule: Schedule;
        let medication: Medication;
        let tasklist: TaskList;
        let author: Activity.Author;

        beforeEach(async () => {
            sandbox = sinon.sandbox.create();

            now = moment().toDate();
            schedule = Schedule.hourly(now, 8);
            medication = await createMedication(patient, { schedule });
            tasklist = await TaskList.insert(db, TaskList.fromMedication(patient, medication));
            author = Activity.Author.factory({
                _id: database.id(),
                type: "admin",
                full_name: "Administrator",
            });

            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        afterEach(async () => {
            sandbox.restore();
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.delete(`/patient/${patient._id}/medications/${medication._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should keep tasklist", async () => {
            // Force date to be expected time
            const current = moment(now).add({ m: 30 }).toDate();
            const until_date = moment(current).add({ ms: -1 }).toDate();
            sandbox.stub(Date, "current").returns(current);

            // Run the route
            const context = await helper.delete(`/patient/${patient._id}/medications/${medication._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Expect tasklist to be updated
            await tasklist.refresh(db);
            expect(tasklist.medication_id).to.deep.equal(medication._id);
            expect(tasklist.patient_id).to.deep.equal(patient._id);
            expect(tasklist.sequences.length).to.equal(1);
            expect(tasklist.sequences).to.shallow.equal([
                { schedule: { ...medication.schedule, until: until_date } },
            ]);
            expect(tasklist.shared).to.deep.equal({
                type: "medication",
                title: medication.name,
                description: medication.instructions,
                notes: medication.notes,
                attachments: medication.attachments,
            });
        });
    });

});
