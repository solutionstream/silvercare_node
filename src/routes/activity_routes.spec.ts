import "mocha";
import * as moment from "moment";
import * as config from "../config";
import * as database from "../database";
import { APIUser, IAPIUserMethods } from "../middleware";
import { Activity, ActivityLog, Patient, User } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("Activity Routes", async () => {
    let db: database.Db;
    let user: User;
    let api_user: Stubbed<APIUser, keyof IAPIUserMethods>;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await database.setupCollections(db);
        user = User.factory({ username: "mmouse@example.com" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, api_user });
    });

    afterEach(async () => {
        await database.drop(db);
        sandbox.restore();
    });

    describe("create journal", async () => {
        let now: Date;
        let user_id: database.ObjectID;
        let patient_id: database.ObjectID;
        let journal_id: database.ObjectID;
        let title: string;
        let body: string;
        let author: Activity.Author;

        beforeEach(async () => {
            now = new Date();
            user_id = database.id();
            patient_id = database.id();
            journal_id = database.id();
            title = "Journal Entry";
            body = "Journal Body";
            author = Activity.Author.factory({
                _id: database.id(),
                full_name: "Mickey Mouse",
                type: "caregiver",
            });

            await Patient.create(db, {
                _id: patient_id,
                user_id,
                full_name: "Patient",
            });

            // Allow access to the route
            api_user.caregiver_to.returns(true);
            api_user.author_for.returns(author);
        });

        it("should fail if user is not the patient's caregiver", async () => {
            api_user.caregiver_to.returns(false);
            const context = await helper.put(`/patient/${patient_id}/journal/${journal_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
        });

        it("should fail if required params not provided", async () => {
            const context = await helper.put(`/patient/${patient_id}/journal/${journal_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "MissingParams",
            });
        });

        it("should fail if patient does not exist", async () => {
            const patient_id = database.id();
            helper.body({ title, body });

            const context = await helper.put(`/patient/${patient_id}/journal/${journal_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchPatient",
            });
        });

        it("should create journal entry", async () => {
            sandbox.stub(Date, "current").returns(now);

            helper.body({ title, body });

            const context = await helper.put(`/patient/${patient_id}/journal/${journal_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: journal_id,
                type: "journal",
                patient_id,
                title,
                meta: { body },
                created: now,
                updated: now,
                author,
            });
        });
    });

    describe("search activites", async () => {
        let counter: number;
        let before_start: Date;
        let start: Date;
        let now: Date;
        let until: Date;
        let after_until: Date;
        let user_id: database.ObjectID;
        let patient_id: database.ObjectID;
        let journal_id: database.ObjectID;
        let title: string;
        let body: string;
        let author: Activity.Author;

        beforeEach(async () => {
            counter = 0;
            before_start = moment("2017-10-14T23:59:59.999").toDate();
            start = moment("2017-10-15T00:00:00.000").toDate();
            now = moment("2017-11-01T10:00:00.000").toDate();
            until = moment("2017-11-15T00:00:00.000").toDate();
            after_until = moment("2017-11-15T00:00:00.001").toDate();
            user_id = database.id();
            patient_id = database.id();
            journal_id = database.id();
            title = "Journal Entry";
            body = "Journal Body";

            const patient = await Patient.create(db, { _id: patient_id, user_id, full_name: "Patient" });
            author = Activity.Author.factory(patient);

            sandbox.stub(Date, "current").returns(now);

            // Allow access to the route for tests
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        // Create an activity in the database
        async function createActivity(opts: Partial<Activity.FactoryOptions>): Promise<Activity> {
            return ActivityLog.create(db, {
                _id: opts._id,
                created: opts.created,
                updated: opts.updated,
                type: opts.type || "other" as any,
                title: opts.title || `Title #${counter++}`,
                meta: opts.meta || {} as any,
                author: opts.author || author,
                patient_id: opts.patient_id || patient_id,
            });
        }

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.sv_admin.returns(false);
            api_user.pac_to.returns(false);

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail without date range", async () => {
            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidDateRange",
            });
        });

        it("should fail for invalid activity type", async () => {
            const type = "invalid";
            helper.query({ start, until, type });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidType",
            });
        });

        it("should fail when patient does not exist", async () => {
            const patient_id = database.id();
            helper.query({ start, until });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchPatient",
            });
        });

        it("should only get results for patient", async () => {
            const activity1 = await createActivity({ patient_id });
            const activity2 = await createActivity({ patient_id });
            await createActivity({ patient_id: database.id() });
            helper.query({ start, until });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                activities: [activity1, activity2],
            });
        });

        it("should filter by date range", async () => {
            await createActivity({ created: before_start });
            const activity2 = await createActivity({ created: start });
            const activity3 = await createActivity({ created: now });
            const activity4 = await createActivity({ created: until });
            await createActivity({ created: after_until });
            helper.query({ start, until });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                activities: [activity2, activity3, activity4],
            });
        });

        it("should filter results by type", async () => {
            const activity1 = await createActivity({ type: "journal" });
            await createActivity({ type: "other" });
            const activity3 = await createActivity({ type: "journal" });
            helper.query({ start, until, type: "journal" });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                activities: [activity1, activity3],
            });
       });

        it("should perform full-text search", async () => {
            const activity1 = await createActivity({ meta: { body: "a test"} });
            const activity2 = await createActivity({ meta: { body: "testing"} });
            const activity3 = await createActivity({ title: "another test" });
            const activity4 = await createActivity({ title: "testing" });
            await createActivity({ meta: {body: "protest"} });
            await createActivity({ title: "protest" });
            await createActivity({ meta: {body: "te st" }});
            await createActivity({ meta: {body: "unrelated" }});
            helper.query({ start, until, term: "test" });

            const context = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                activities: [ activity1, activity2, activity3, activity4 ],
            });
        });

        it("should filter by author", async () => {
            const caregiver = Activity.Author.factory({ _id: database.id(), full_name: "Mickey Mouse", type: "caregiver" });
            const patient = Activity.Author.factory({ _id: database.id(), full_name: "Donald Duck", type: "patient" });

            const activity1 = await createActivity({ author: caregiver });
            const activity2 = await createActivity({ author: caregiver });
            const activity3 = await createActivity({ author: patient });

            helper.query({ start, until, author_id: caregiver._id });
            const context_1 = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context_1.status).to.equal(status.OK);
            expect(context_1.body).to.json.equal({
                activities: [activity1, activity2],
            });

            helper.query({ start, until, author_id: patient._id });
            const context_2 = await helper.get(`/patient/${patient_id}/activities`, router);
            expect(context_2.status).to.equal(status.OK);
            expect(context_2.body).to.json.equal({
                activities: [activity3],
            });
        });
    });
});
