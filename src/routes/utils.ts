import * as status from "http-status";
import * as Router from "koa-router";
import * as config from "../config";
import * as database from "../database";
import * as errors from "../errors";


export namespace parseId {
    export interface Callback { (ctx: Router.IRouterContext): Promise<database.ObjectID>; }
    export interface Options { [key: string]: Callback; }
}

// Parse the parameter to get an object id
export function parseId(key: string, opts?: parseId.Options): Router.IParamMiddleware {
    return async (param, ctx, next) => {
        if (opts && typeof opts[param] === "function") {
            ctx.state[key] = await opts[param](ctx);
        } else {
            ctx.state[key] = database.parseObjectID(param);
        }

        // Throw an error if the parse fails
        if (!ctx.state[key]) {
            throw errors.create(status.BAD_REQUEST, "InvalidURL", {
                debug: `${ctx.params[key]} is not a valid id`,
            });
        }

        await next();
    };
}

export function assertEmpty(object: object, ...error: Array<string|number>) {
    if (config.app.production) { return; }

    const unhandled_keys = Object.keys(object);
    if (unhandled_keys.length) {
        throw errors.create(...error, {
            debug: `Unexpected properties: ${unhandled_keys}`,
        });
    }
}
