import * as status from "http-status";
import * as Router from "koa-router";
import * as database from "../database";
import * as errors from "../errors";
import { Agenda } from "../jobs";
import { APIUser } from "../middleware";
import { CareGiver, PAC, Patient, User } from "../models";
import { assert, toArray, undef, Variadic } from "../utils";
import { assertEmpty, parseId } from "./utils";


interface ContextState {
    db: database.Db;
    api_user: APIUser;
    user_id: database.ObjectID;
    token: string;
}


// Authenticated router
export const router = new Router();
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));
router.put("/user/:user_id", createUser());
router.post("/user/:user_id", updateUser());
router.get("/user/:user_id/info", userInfo());
router.get("/user", searchUsers());

// Unauthenticated router
export const unauthenticated = new Router();
unauthenticated.param("user_id", parseId("user_id"));
unauthenticated.put("/user/:user_id/forgot", resetPassword());


interface SearchUserQuery {
    username?: string;
    limit?: number;
}

export function searchUsers(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db } = ctx.state as ContextState;
        const { username, limit, ...unexpected } = ctx.request.query as SearchUserQuery;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.pac && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!limit || !username) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: "limit & username are required fields",
            });
        }

        // Search users
        const users = await User.search(db, { username, limit: limit | 0 });

        // TODO: If the api user is a PAC they can only search users they are assigned to

        const safe = users.map(({ _id, username, roles, read_tacos }) => ({ _id, username, roles, read_tacos }));

        ctx.status = status.OK;
        ctx.body = { users: safe };
    };
}

export function userInfo(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id } = ctx.state as ContextState;

        // Fetch data about the user
        const [caregiver, pac, patient, user] = await Promise.all([
            CareGiver.fetch(db, { user_id }),
            PAC.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
            User.get(db, { _id: user_id }),
        ]);

        if (!user) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.user_self(user)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        ctx.status = status.OK;
        ctx.body = {
            _id: user._id,
            username: user.username,
            roles: user.roles,
            patient_id: patient && patient._id,
            caregiver_id: caregiver && caregiver._id,
            pac_id: pac ? pac._id : (caregiver ? caregiver.pac_id : (patient ? patient.pac_id : undefined)),
            read_tacos: user.read_tacos,
        };
    };
}

interface CreateUserBody {
    username?: string;
    password?: string;
    roles?: Variadic<string>;
}

// Create a user account
export function createUser(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, user_id, db } = ctx.state as ContextState;
            const {
                username,
                password,
                roles: rolesJSON,
                ...unexpected,
            } = ctx.request.body as CreateUserBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Ensure that we are allowed to access this route
            if (!api_user.pac && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            if (!username) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams");
            }

            // Only email addresses can be usernames
            assert.email(username);

            // Create user with desired credentials
            const roles = undef.apply(rolesJSON, toArray);
            const user = await User.create(db, {
                _id: user_id,
                roles: roles as User.Role[],
                username,
                password,
            });

            // Output user data to client
            ctx.status = status.CREATED;
            ctx.body = {
                _id: user._id,
                roles: user.roles,
                username: user.username,
                read_tacos: user.read_tacos,
            };
        } catch (error) {
            // Creation failed due to a validation error
            if (error instanceof errors.InvalidDocument) {
                ctx.throw(status.BAD_REQUEST, error.message);
            }

            // Failed for another reason
            throw error;
        }
    };
}

interface UpdateUserBody {
    username?: string;
    password?: string;
    read_tacos?: boolean;
}

export function updateUser(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, user_id, db } = ctx.state as ContextState;
        const {
            username,
            password,
            read_tacos,
            ...unexpected,
        } = ctx.request.body as UpdateUserBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.user_self(user_id) && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (typeof username !== "string" || typeof password !== "string") {
            throw errors.create(status.BAD_REQUEST, "InvalidParams");
        }

        // Enforce that only email addresses can be usernames
        assert.email(username);

        // Find the exiting user record
        const user = await User.get(db, { _id: user_id });
        if (!user) {
            throw errors.create(status.NOT_FOUND);
        }

        // Update the user account
        user.update({
            ...user,
            username,
            password,
            read_tacos,
        });
        await user.save(db);

        // Output user data to client
        ctx.status = status.CREATED;
        ctx.body = {
            _id: user._id,
            roles: user.roles,
            username: user.username,
            read_tacos: user.read_tacos,
        };
    };
}

export function resetPassword(): Router.IMiddleware {
    return async (ctx) => {
        const { db, user_id } = ctx.state as ContextState;

        // Schedule a password reset email
        await Agenda.reset_password(db, { user_id });

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}
