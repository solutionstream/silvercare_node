import "koa";
import "mocha";
import * as config from "../config";
import * as database from "../database";
import { APIUser, IAPIUserMethods } from "../middleware";
import { User } from "../models";
import { CareGiver, IContact, Patient } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("CareGiver Routes", async () => {
    let db: database.Db;
    let user: User;
    let user_id: database.ObjectID;
    let api_user: Stubbed<APIUser, keyof IAPIUserMethods>;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        user_id = database.id();
        user = User.factory({ _id: user_id, username: "mmouse@example.com" });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });

        await db.dropDatabase();
        await database.setupCollections(db);
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    describe("get caregiver", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should fail if caregiver doesn't exist", async () => {
            const context = await helper.get(`/caregiver/me`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should enforce access control", async () => {
            api_user.caregiver_self.returns(false);
            api_user.patient_of.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            // Ensure that we resolve "me" caregiver to api_user object instance
            api_user.caregiver = await CareGiver.create(db, { user_id, full_name: "Test" });

            const context = await helper.get(`/caregiver/me`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_self).to.have.callCount(1);
            expect(api_user.patient_of).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should get caregiver data", async () => {
            const caregiver = await CareGiver.create(db, { user_id, full_name: "Test" });
            const context = await helper.get(`/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({ caregiver });
        });
    });

    describe("create caregiver", async () => {
        let caregiver_id: database.ObjectID;
        let pac_id: database.ObjectID;
        let full_name: string;
        let addresses: IContact.PhysicalAddress[];
        let phones: IContact.PhoneNumber[];
        let emails: IContact.EmailAddress[];

        beforeEach(async () => {
            caregiver_id = database.id();
            pac_id = database.id();
            full_name = "Mickey Mouse";
            addresses = [{ type: "home", address: "123 Fake St" }];
            phones = [{ type: "home", number: "+15558675309" }];
            emails = [{ type: "home", email: "mmouse@example.com" }];

            api_user.sv_admin.returns(true);
        });

        it("should fail with invalid params", async () => {
            const context = await helper.put(`/user/${user_id}/caregiver/${caregiver_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "Bad Request",
            });
        });

        it("should enforce access controls", async () => {
            api_user.pac_self.returns(false);
            api_user.sv_admin.returns(false);

            helper.body({ full_name });

            const context = await helper.put(`/user/${user_id}/caregiver/me`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.pac_self).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should create caregiver for user", async () => {
            const caregiver = CareGiver.factory({
                _id: caregiver_id,
                user_id,
                pac_id,
                full_name,
                addresses,
                phones,
                emails,
            });

            helper.body({
                pac_id: caregiver.pac_id,
                full_name: caregiver.full_name,
                address: caregiver.addresses[0],
                phone: caregiver.phones[0],
                email: caregiver.emails[0],
            });

            // This is the only property we don't expect to match
            delete caregiver.version;

            const context = await helper.put(`/user/${user_id}/caregiver/${caregiver_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.shallow.json.equal({ caregiver });
        });
    });

    describe("patient list", async () => {

        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should fail if caregiver doesn't exist", async () => {
            const context = await helper.get(`/caregiver/me/patients`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_self.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            // Ensure that we resolve "me" to api user caregiver object
            api_user.caregiver = await CareGiver.create(db, { user_id, full_name: "Test" });

            const context = await helper.get(`/caregiver/me/patients`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "Forbidden",
            });

            expect(api_user.caregiver_self).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if caregiver has no patient", async () => {
            const caregiver = await CareGiver.create(db, { user_id, full_name: "Test" });

            const context = await helper.get(`/caregiver/${caregiver._id}/patients`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should return patients for caregiver", async () => {
            // User is their own caregiver
            const patient_1 = await Patient.create(db, { user_id, full_name: "Patient" });
            const caregiver = (await CareGiver.fetch(db, { user_id }))!;

            // Another patient's caregiver
            const patient_2 = await Patient.create(db, {
                user_id: database.id(),
                full_name: "Patient",
                carecircle: {
                    caregivers: [caregiver.contact()],
                    contacts: [],
                },
            });

            const context = await helper.get(`/caregiver/${caregiver._id}/patients`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal([patient_1, patient_2]);
        });

    });

});
