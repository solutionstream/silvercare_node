import * as status from "http-status";
import * as Router from "koa-router";
import * as _ from "lodash";
import * as moment from "moment";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { ActivityLog } from "../models/activitylog";
import { Media } from "../models/medialibrary";
import { Note } from "../models/note";
import { Patient } from "../models/patient";
import { Task, TaskID, TaskIDJSON, TaskList } from "../models/tasklist";
import { IScheduleJSON, Schedule } from "../scheduling";
import { undef } from "../utils";
import { assertEmpty, parseId } from "./utils";


// This is what our routes expect to find in the context
// This is not guaranteed, it's just here for type inference
interface ContextState {
    db: database.Db;
    api_user: APIUser;
    patient_id: database.ObjectID;
    task_id: database.ObjectID;
}

const router = new Router();

// Parse url parameters
router.param("patient_id", parseId("patient_id"));
router.param("task_id", parseId("task_id"));

// Change the schedule of a task sequence
// This will influence the individual task instances and so invalidate any client data
router.put(`/patient/:patient_id/tasks/sequence/:task_id/schedule`, rescheduleTasklist());

// Update everything that can be updated about a task sequence
// This will change the default values to be applied to all future tasks in the sequence
router.put(`/patient/:patient_id/tasks/sequence/:task_id`, updateTasklist());

// List all of the tasks for a patient
router.get(`/patient/:patient_id/tasks/sequence/:task_id`, getTasklist());
router.get(`/patient/:patient_id/tasks/sequence`, getTasklist());

// Create a task sequence
router.post(`/patient/:patient_id/tasks/sequence/:task_id`, createTaskList());

// Update everything that can be updated about a task instance
router.put(`/patient/:patient_id/tasks/instance`, updateInstance());


export default router;

// Returns the following values:
// - undefined: no id was provided
// - null: caregiver is being unassigned (explicit null)
// - caregiver reference: caregiver being assigned
function validateCaregiverId(patient: Patient, id: undefined | null | string) {
    if (id === null) { return null; }
    if (id === undefined) { return undefined; }

    const object_id = database.parseObjectID(id);
    if (!object_id) {
        throw errors.create(status.BAD_REQUEST, "InvalidID");
    }

    const caregiver_ref = patient.caregiver_specific(object_id);
    if (!caregiver_ref) {
        throw errors.create(status.CONFLICT, "CannotAssign", {
            debug: "Caregiver is not in patient's care circle",
        });
    }

    return caregiver_ref.reference();
}

export interface CreateTaskBody {
    title?: string;
    type?: Task.Type;
    schedule?: IScheduleJSON;
    caregiver_id?: string;
    description?: string;
}

// Create a new task in the database
export function createTaskList(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, task_id, db } = ctx.state as ContextState;
            const {
                title,
                type,
                schedule,
                caregiver_id,
                description,
                ...unexpected,
            } = ctx.request.body as CreateTaskBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get the patient whose tasks we're modifying
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient");
            }

            // Ensure that we are allowed to access this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            if (!title || !type) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "Missing required fields",
                });
            }

            // Not allowed to create a medication task from this endpoint
            // Medication tasks are created automatically when a medication is created
            if (type === "medication") {
                throw errors.create(status.BAD_REQUEST, "InvalidType", {
                    debug: "Cannot directly create medication tasks.  Create a medication instead.",
                });
            }

            // Validate the caregiver id string
            const caregiver_ref = validateCaregiverId(patient, caregiver_id);
            const new_schedule = undef.apply(schedule, Schedule.fromJSON);

            if (!new_schedule) {
                throw errors.create(status.BAD_REQUEST, {
                    debug: "Schedule is invalid",
                });
            }

            // Append the desired action to the patient record
            const tasklist = await TaskList.create(db, {
                _id: task_id,
                patient_id,
                schedule: new_schedule,
                defaults: {
                    caregiver: caregiver_ref || null,
                },
                shared: {
                    title,
                    type,
                    description,
                },
            });

            // Log the creation of the tasklist
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "tasklist_create",
                title: "Task Created",
                author: api_user.author_for(patient),
                meta: tasklist,
            });

            ctx.status = status.CREATED;
            ctx.body = {
                _id: tasklist._id,
                defaults: tasklist.defaults,
                shared: tasklist.shared,
            };
        } catch (error) {

            // Validations errors are because of bad request data
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            // Rethrow errors that are HttpError as these are ready for output
            if (error instanceof errors.HttpError) {
                throw error;
            }

            // Everything else is treated as a server fault
            throw errors.create(status.INTERNAL_SERVER_ERROR, {
                debug: error.message,
            });

        }
    };
}

export interface GetTasklistBody {
    start?: number;
    until?: number;
}

// Get all tasks for a given patient
export function getTasklist(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, task_id, db } = ctx.state as ContextState;
            const {
                start,
                until,
                ...unexpected,
            } = ctx.query as GetTasklistBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            const patient = await Patient.fetch(db, { _id: patient_id });

            // Ensure that we are allowed to access this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Ensure that we have required fields
            if (!start || !until) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "start and until are required fields",
                });
            }

            // Set a limit on how large the date range can be
            const range_duration = moment.duration(moment(until).diff(start));
            if (range_duration.asMonths() > 1) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "date range cannot be greater than one month",
                });
            }

            // Get all tasklists whose schedule falls within our date range
            // It should be safe to fetch them all as there is not much data stored
            const all_tasklists = await TaskList.fetch(db, patient_id, task_id);
            const tasklists = all_tasklists.filter(
                (list) => list.in_range(start, until));

            if (!tasklists.length) {
                ctx.status = status.NO_CONTENT;
            } else {
                ctx.status = status.OK;
                ctx.body = _.map(tasklists, (list) => {
                    return {
                        _id: list._id,
                        defaults: list.defaults,
                        shared: list.shared,
                        schedules: _.map(list.sequences, (seq) => seq.outputSchedule()),
                        tasks: list.between(start, until),
                    };
                });
            }

        } catch (error) {

            // Validations errors are because of bad request data
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            // Rethrow errors that are HttpError as these are ready for output
            if (error instanceof errors.HttpError) {
                throw error;
            }

            // Everything else is treated as a server fault
            throw errors.create(status.INTERNAL_SERVER_ERROR, {
                debug: error.message,
            });

        }
    };
}


export interface UpdateInstanceBody {
    address?: TaskIDJSON;
    new_date?: number;
    cancelled?: boolean;
    completed?: boolean;
    caregiver_id?: string;
}

// Update a specific instance of a task
export function updateInstance(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, db } = ctx.state as ContextState;
            const {
                address,
                new_date,
                cancelled,
                completed,
                caregiver_id,
                ...unexpected,
            } = ctx.request.body as UpdateInstanceBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get Patient containing this task
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient");
            }

            // Ensure that we have access to this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Validate the caregiver id string
            const caregiver_ref = validateCaregiverId(patient, caregiver_id);
            const task_id = undef.apply(address, TaskID.fromJSON);
            if (!task_id) {
                throw errors.create(status.BAD_REQUEST, "InvalidID", {
                    debug: "Unable to parse task id",
                });
            }

            // Get the tasklist associated with this task
            const tasklist = await TaskList.fetchOne(db, patient_id, task_id.tasklist_id);
            if (!tasklist) {
                throw errors.create(status.NOT_FOUND, "NoSuchTask");
            }

            // Get the specific task instance being modified
            const task = tasklist.instance(task_id);
            if (!task) {
                throw errors.create(status.NOT_FOUND, "NoSuchTask");
            }

            // Make required changes to the tasklist
            const updated = await tasklist.updateOne(db, task, {
                cancelled,
                completed,
                date: new_date ? new Date(new_date) : undefined,
                caregiver: caregiver_ref,
            });

            // Log cancellation of the task
            if (!updated) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "task_cancel",
                    title: "Cancelled Task",
                    author: api_user.author_for(patient),
                    meta: task,
                });
            }

            // Log unassignement of a task
            else if (!updated.caregiver && task.caregiver) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "task_unassign",
                    title: "Unassigned Task",
                    author: api_user.author_for(patient),
                    meta: updated,
                });
            }

            // Log assignment of a task
            else if (updated.caregiver && (!task.caregiver || updated.caregiver._id.equals(task.caregiver._id))) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "task_assign",
                    title: "Assigned Task",
                    author: api_user.author_for(patient),
                    meta: updated,
                });
            }

            // Log completion of task
            if (updated && updated.completed && !task.completed) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "task_complete",
                    title: "Completed Task",
                    author: api_user.author_for(patient),
                    meta: updated,
                });
            }


            if (updated) {
                ctx.status = status.OK;
                ctx.body = updated;
            } else if (cancelled) {
                ctx.status = status.NO_CONTENT;
            } else {
                throw errors.create(status.INTERNAL_SERVER_ERROR, {
                    debug: "Update appears successful but could not locate updated task",
                });
            }

        } catch (error) {

            // Validations errors are because of bad request data
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            // Rethrow errors that are HttpError as these are ready for output
            if (error instanceof errors.HttpError) {
                throw error;
            }

            // Everything else is treated as a server fault
            throw errors.create(status.INTERNAL_SERVER_ERROR, {
                debug: error.message,
            });

        }
    };
}

export interface RescheduleTasklistBody {
    schedule?: IScheduleJSON;
}

// Change the schedule for a task list
export function rescheduleTasklist(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, db, patient_id, task_id } = ctx.state as ContextState;
            const {
                schedule,
                ...unexpected,
            } = ctx.request.body as RescheduleTasklistBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");
            const patient = await Patient.fetch(db, { _id: patient_id });

            // Ensure that we are allowed to access this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Parse the JSON schedule
            const new_schedule = undef.apply(schedule, Schedule.fromJSON);
            if (!new_schedule) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "schedule is a required field",
                });
            }

            // Get the tasklist being modified
            const tasklist = await TaskList.fetchOne(db, patient_id, task_id);
            if (!tasklist) {
                throw errors.create(status.NOT_FOUND, "NoSuchTask");
            }

            // Update the tasklist schedule
            await tasklist.updateSchedule(db, new_schedule);

            // Log tasklist reschedule
            if (patient) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "tasklist_reschedule",
                    title: "Change Task Schedule",
                    author: api_user.author_for(patient),
                    meta: tasklist,
                });
            }

            ctx.status = status.NO_CONTENT;

        } catch (error) {
            // Validations errors are because of bad request data
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            // Rethrow errors that are HttpError as these are ready for output
            if (error instanceof errors.HttpError) {
                throw error;
            }

            // Everything else is treated as a server fault
            throw errors.create(status.INTERNAL_SERVER_ERROR, {
                debug: error.message,
            });
        }
    };
}

interface UpdateSequenceBody {
    title?: string;
    type?: Task.Type;
    description?: string;
    caregiver_id?: string;
    notes?: Note.JSON[];
    attachments?: Media.ReferenceJSON[];
}

// Update the entire sequence of tasks
export function updateTasklist(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, task_id, db } = ctx.state as ContextState;
            const {
                title,
                type,
                description,
                caregiver_id: caregiver_id_string,
                notes: notesJSON,
                attachments: attachmentsJSON,
                ...unexpected,
            } = ctx.request.body as UpdateSequenceBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get the patient whose tasks we're modifying
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient");
            }

            // Ensure we are allowed to access to this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Validate the caregiver id string
            // This is the default caregiver for a task
            const caregiver_id = validateCaregiverId(patient, caregiver_id_string);

            // Update timestamps in notes to be dates
            let notes: undefined|Note[];
            if (notesJSON) {
                notes = notesJSON.map(Note.fromJSON);
            }

            let attachments: undefined|Media.Reference[];
            if (attachmentsJSON) {
                attachments = _.compact(attachmentsJSON.map(Media.Reference.fromJSON));
            }

            // Get a hold of the existing tasklist
            const tasklist = await TaskList.fetchOne(db, patient_id, task_id);
            if (!tasklist) {
                throw errors.create(status.NOT_FOUND, "NoSuchTask");
            }

            // Update linked medication
            if (tasklist.medication_id) {
                await patient.update_medication(db, {
                    _id: tasklist.medication_id,
                    notes,
                    attachments,
                });
            }

            // Update the data for the tasklist
            const updated = await tasklist.updateAll(db, {
                title,
                type,
                description,
                caregiver: caregiver_id,
                notes,
                attachments,
            });

            const tasklist_caregiver = tasklist.defaults.caregiver;
            const updated_caregiver = updated.defaults.caregiver;

            // Log unassign
            if (tasklist_caregiver && !updated_caregiver) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "tasklist_unassign",
                    title: "Unassigned Task",
                    author: api_user.author_for(patient),
                    meta: updated,
                });
            }

            // Log assignement
            else if (updated_caregiver
                && (!tasklist_caregiver || tasklist_caregiver._id.equals(updated_caregiver._id))) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "tasklist_assign",
                    title: "Assigned Task",
                    author: api_user.author_for(patient),
                    meta: updated,
                });
            }

            ctx.status = status.OK;
            ctx.body = {
                _id: updated._id,
                defaults: updated.defaults,
                shared: updated.shared,
            };

        } catch (error) {

            // Validations errors are because of bad request data
            if (error instanceof errors.InvalidDocument) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            // Rethrow errors that are HttpError as these are ready for output
            if (error instanceof errors.HttpError) {
                throw error;
            }

            // Everything else is treated as a server fault
            throw errors.create(status.INTERNAL_SERVER_ERROR, {
                debug: error.message,
            });
        }
    };
}
