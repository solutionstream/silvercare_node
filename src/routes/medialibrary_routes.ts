import * as status from "http-status";
import * as Router from "koa-router";
import * as _ from "lodash";
import * as config from "../config";
import * as database from "../database";
import * as errors from "../errors";
import { Bucket, S3 } from "../media";
import { APIUser } from "../middleware";
import { ActivityLog, CareGiver, Media, MediaLibrary, Patient } from "../models";
import { toArray, undef, Variadic } from "../utils";
import { assertEmpty, parseId } from "./utils";


export interface ContextState {
    db: database.Db;
    api_user: APIUser;
    user_id: database.ObjectID;
    file_id: database.ObjectID;
}


const router = new Router();

// Parse url parameters
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));

// Determine the file id
// Handle special "profile" id as an alias for the file with the "profile" tag
router.param("file_id", parseId("file_id", {
    "profile": async (ctx) => {
        const { db, user_id } = ctx.state as ContextState;

        // Get the medialibrary for the user
        let medialibrary = await MediaLibrary.fetch(db, { user_id });
        if (!medialibrary) {
            medialibrary = await MediaLibrary.create(db, { user_id });
        }

        // Get the id for the current profile photo
        // If there is no profile photo then generate an id
        const profile = medialibrary.profile_photo();
        return profile ? profile._id : database.id();
    },
}));

// Determine the user id for a given patient
router.param("patient_id", async (id, ctx, next) => {
    const { db } = ctx.state as ContextState;

    // Parse the patient id
    const patient_id = database.parseObjectID(id);
    if (!patient_id) {
        throw errors.create(status.NOT_FOUND);
    }

    // Get the patient being referenced
    const patient = await Patient.fetch(db, { _id: patient_id });
    if (!patient) {
        throw errors.create(status.NOT_FOUND);
    }

    // Add the patient's user id to the context
    ctx.state.user_id = patient.user_id;
    await next();
});

// Routes
router.get(`/patient/:patient_id/media`, searchMedia());
router.get(`/patient/:patient_id/media/:file_id`, getMedia());
router.delete(`/patient/:patient_id/media/:file_id`, deleteMedia());
router.post(`/patient/:patient_id/media/:file_id`, updateMedia());
router.put(`/patient/:patient_id/media/:file_id/request`, requestUpload());
router.put(`/patient/:patient_id/media/:file_id/confirm`, confirmUpload());

router.get(`/user/:user_id/media`, searchMedia());
router.get(`/user/:user_id/media/:file_id`, getMedia());
router.delete(`/user/:user_id/media/:file_id`, deleteMedia());
router.post(`/user/:user_id/media/:file_id`, updateMedia());
router.put(`/user/:user_id/media/:file_id/request`, requestUpload());
router.put(`/user/:user_id/media/:file_id/confirm`, confirmUpload());


export default router;


interface SearchMediaQuery {
    tags?: Variadic<string>;
    extensions?: Variadic<string>;
}

export function searchMedia(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id } = ctx.state as ContextState;
        const {
            tags,
            extensions,
            ...unexpected,
        } = ctx.query as SearchMediaQuery;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [medialibrary, patient] = await Promise.all([
            MediaLibrary.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        let files: Media.File[] = [];
        let pending: Media.File[] = [];

        if (medialibrary) {
            files = medialibrary.files;
            pending = medialibrary.pending;

            if (tags) {
                files = _.filter(files, (file) => Media.File.matches_tag(file, tags));
                pending = _.filter(pending, (file) => Media.File.matches_tag(file, tags));
            }

            if (extensions) {
                files = _.filter(files, (file) => Media.File.matches_extension(file, extensions));
                pending = _.filter(pending, (file) => Media.File.matches_extension(file, extensions));
            }
        }

        ctx.status = status.OK;
        ctx.body = { files, pending };
    };
}

export function getMedia(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, file_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [medialibrary, caregiver, patient] = await Promise.all([
            MediaLibrary.fetch(db, { user_id }),
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        if (!medialibrary) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not exist",
            });
        }

        const file = medialibrary.file(file_id);
        if (!file) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not contain file with id",
            });
        }

        // Ensure that we have access to this route
        if (file.tags.indexOf("profile") === -1
            && !api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Get url for retrieving the file
        const bucket = new Bucket(S3, config.aws.s3.bucket);
        const get_url = bucket.url.get({ filename: file.filepath });

        ctx.status = status.OK;
        ctx.body = { get_url, file };
    };
}

interface UpdateMediaBody {
    filename?: string;
    tags?: Variadic<string>;
}

export function updateMedia(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, file_id } = ctx.state as ContextState;
        const { filename, tags: tags_v, ...unexpected } = ctx.request.body as UpdateMediaBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [medialibrary, caregiver, patient] = await Promise.all([
            MediaLibrary.fetch(db, { user_id }),
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!medialibrary) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not exist",
            });
        }

        const file = medialibrary.file(file_id);
        if (!file) {
            throw errors.create(status.NOT_FOUND, {
                debug: "File not found",
            });
        }

        // Update file tags
        if (tags_v != null) {
            file.tags = toArray(tags_v);
            file.updated = Date.current();
        }

        // Update filename
        if (filename) {
            file.original_filename = filename;
            file.updated = Date.current();
        }

        // Write the file back into the medialibrary
        medialibrary.add_file(file);
        await medialibrary.save(db);

        // Log the updating of a file for patient
        if (patient) {
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "media_update",
                title: "Updated File",
                author: api_user.author_for(patient),
                meta: { ...file, user_id },
            });
        }

        ctx.status = status.OK;
        ctx.body = { file };
    };
}

export function deleteMedia(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, file_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [medialibrary, caregiver, patient] = await Promise.all([
            MediaLibrary.fetch(db, { user_id }),
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!medialibrary) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not exist",
            });
        }

        const file = medialibrary.remove_file(file_id) || medialibrary.remove_pending(file_id);
        if (!file) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not contain file with id",
            });
        }

        // Delete file from bucket
        const bucket = new Bucket(S3, config.aws.s3.bucket);
        await bucket.delete({ path: file.filepath });

        // Delete file from database
        await medialibrary.save(db);

        // Log deletion of files for patients
        if (patient) {
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "media_delete",
                title: "Deleted File",
                author: api_user.author_for(patient),
                meta: { ...file, user_id },
            });
        }

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}

export interface RequestUploadBody {
    mimetype?: string;
    size?: number;
    filename?: string;
    tags?: Variadic<string>;
}

export function requestUpload(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, file_id } = ctx.state as ContextState;
        const {
            tags: tagsJSON,
            size,
            mimetype,
            filename: original_filename,
            ...unexpected,
        } = ctx.request.body as RequestUploadBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [caregiver, patient] = await Promise.all([
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Ensure we have required fields
        if (!size || !mimetype || !original_filename) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: "Missing required field",
            });
        }

        // Get or create the medialibrary for this user
        let medialibrary = await MediaLibrary.fetch(db, { user_id });
        if (!medialibrary) {
            medialibrary = await MediaLibrary.create(db, { user_id });
        }

        // Check desired upload size against quotas
        if (!medialibrary.check_quotas(size)) {
            throw errors.create(status.CONFLICT, "OutOfSpace", {
                debug: "Uploading file would exceed space quotas",
            });
        }

        // Create a file record matching the desired file
        const tags = undef.apply(tagsJSON, toArray);
        const file = Media.File.factory(user_id, {
            _id: file_id,
            original_filename,
            mimetype,
            size: size | 0, // Coerce to number
            tags,
        });

        // Create an authorization to upload the file
        const bucket = new Bucket(S3, config.aws.s3.bucket);
        const post = bucket.url.post({
            filename: file.filepath,
            mimetype: file.mimetype,
            meta: {
                original_filename,
            },
        });

        // Add record as pending
        medialibrary.add_pending(file);
        await medialibrary.save(db);

        ctx.status = status.CREATED;
        ctx.body = { post, file };
    };
}

export function confirmUpload(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, file_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const [caregiver, patient] = await Promise.all([
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Get the user's medialibrary
        const medialibrary = await MediaLibrary.fetch(db, { user_id });
        if (!medialibrary) {
            throw errors.create(status.NOT_FOUND, {
                debug: "MediaLibrary does not exist",
            });
        }

        // Ensure that we have a pending file
        const file = medialibrary.remove_pending(file_id);
        if (!file) {
            throw errors.create(status.NOT_FOUND, {
                debug: "Pending file with id not found",
            });
        }

        // Confirm that the file has been uploaded
        const bucket = new Bucket(S3, config.aws.s3.bucket);
        const meta = await bucket.head({ path: file.filepath });

        // Verify that metadata matches
        if (!medialibrary.verify(file, meta)) {
            throw errors.create(status.CONFLICT, {
                debug: "MediaLibrary file does not match file in s3",
            });
        }

        // Add file as confirmed
        medialibrary.add_file(file);
        await medialibrary.save(db);

        // Log the creation of a file for patients
        if (patient) {
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "media_create",
                title: "Uploaded File",
                author: api_user.author_for(patient),
                meta: { ...file, user_id },
            });
        }

        ctx.status = status.OK;
        ctx.body = file;
    };
}
