import * as status from "http-status";
import * as Router from "koa-router";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { ActivityLog } from "../models/activitylog";
import { Media } from "../models/medialibrary";
import { Note } from "../models/note";
import { Medication, Patient } from "../models/patient";
import { TaskList } from "../models/tasklist";
import { assertEmpty, parseId } from "../routes/utils";
import { IScheduleJSON, Schedule } from "../scheduling";
import { assert, parseDate, undef, Variadic } from "../utils";


// This is what our routes expect to find in the context
interface ContextState {
    db: database.Db;
    api_user: APIUser;
    medication_id: database.ObjectID;
    patient_id: database.ObjectID;
}


const router = new Router();

// Parse url params
router.param("patient_id", parseId("patient_id"));
router.param("medication_id", parseId("medication_id"));

// Routes
router.get("/patient/:patient_id/medications", getMedication());
router.get("/patient/:patient_id/medications/:medication_id", getMedication());
router.put("/patient/:patient_id/medications/:medication_id", createMedication());
router.post("/patient/:patient_id/medications/:medication_id", updateMedication());
router.post("/patient/:patient_id/medications/:medication_id/schedule", updateSchedule());
router.delete("/patient/:patient_id/medications/:medication_id", deleteMedication());

export default router;


export function getMedication(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, medication_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Get the patient
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access the route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Return all medications
        if (!medication_id) {
            ctx.status = status.OK;
            ctx.body = { medications: patient.medications };
            return;
        }

        // Get medication by id
        const medication = patient.medication(medication_id);
        if (!medication) {
            throw errors.create(status.NOT_FOUND);
        }

        ctx.status = status.OK;
        ctx.body = {medication};

    };
}

// Body params expected for creating medications
export interface CreateMedicationBody {
    pillpack_id?: string;
    name?: string;
    dosage?: string;
    rx_num?: string;
    next_refill?: number;
    pharmacy?: string;
    prescriber?: string;
    purpose?: string;
    appearance?: string;
    instructions?: string;
    schedule?: IScheduleJSON;
    photo?: Media.ReferenceJSON;
    notes?: Variadic<Note.JSON>;
    attachments?: Variadic<Media.ReferenceJSON>;
}

export function createMedication(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, medication_id, db} = ctx.state as ContextState;
            const {
                name,
                dosage,
                rx_num,
                pharmacy,
                prescriber,
                purpose,
                appearance,
                instructions,
                next_refill: next_refill_ts,
                pillpack_id: pillpack_id_string,
                schedule: scheduleJSON,
                photo: photoJSON,
                notes: notesJSON,
                attachments: attachmentsJSON,
                ...unexpected,
            } = ctx.request.body as CreateMedicationBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get the specified patient
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, {
                    debug: "Patient could not be found",
                });
            }

            // Ensure that we are allowed to access the route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Ensure that required params are included
            if (!name || !dosage || !scheduleJSON) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "Required params are missing",
                });
            }

            // Parse the schedule
            const schedule = undef.apply(scheduleJSON, Schedule.fromJSON);
            if (!schedule) {
                throw errors.create(status.BAD_REQUEST, {
                    debug: "Schedule is not valid",
                });
            }

            const photo = undef.apply(photoJSON, Media.Reference.fromJSON);
            const pillpack_id = undef.apply(pillpack_id_string, database.parseObjectID);
            const next_refill = undef.apply(next_refill_ts, (date) => date !== null ? parseDate(date) : null);
            const notes = undef.map(notesJSON, Note.fromJSON);
            const attachments = undef.map(attachmentsJSON, Media.Reference.fromJSON);

            const medication = Medication.factory({
                _id: medication_id,
                pillpack_id,
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                appearance,
                instructions,
                schedule,
                photo,
                notes,
                attachments,
            });

            // Add medication to patient
            await patient.add_medication(db, medication);

            // Create a tasklist for this medication
            const tasklist = TaskList.fromMedication(patient, medication);
            await TaskList.insert(db, tasklist);

            // Log the creation of the medication
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "medication_create",
                title: "Medication Added",
                author: api_user.author_for(patient),
                meta: patient.medication(medication)!,
            });

            ctx.status = status.CREATED;
            ctx.body = medication;

        } catch (error) {
            if (error instanceof assert.AssertionError) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            throw error;
        }
    };
}

export interface UpdateMedicationBody {
    pillpack_id?: string;
    name?: string;
    dosage?: string;
    rx_num?: string;
    next_refill?: number;
    pharmacy?: string;
    prescriber?: string;
    purpose?: string;
    appearance?: string;
    instructions?: string;
    schedule?: IScheduleJSON;
    photo?: Media.ReferenceJSON;
    notes?: Note.JSON[];
    attachments?: Media.ReferenceJSON[];
}

export function updateMedication(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, medication_id, db } = ctx.state as ContextState;
            const {
                pillpack_id: pillpack_id_string,
                name,
                dosage,
                rx_num,
                pharmacy,
                prescriber,
                purpose,
                appearance,
                instructions,
                next_refill: next_refill_ts,
                photo: photoJSON,
                notes: notesJSON,
                attachments: attachmentsJSON,
                ...unexpected,
            } = ctx.request.body as UpdateMedicationBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Patient is required
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient", {
                    debug: "Patient not found",
                });
            }

            // Ensure that we are allowed to access the route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            const medication = patient.medication(medication_id);
            if (!medication) {
                throw errors.create(status.NOT_FOUND, "NoSuchMedication", {
                    debug: "Medication not found",
                });
            }

            // Parse JSON values
            const photo = undef.apply(photoJSON, Media.Reference.fromJSON);
            const pillpack_id = undef.apply(pillpack_id_string, database.parseObjectID);
            const next_refill = undef.apply(next_refill_ts, (date) => date !== null ? parseDate(date) : null);
            const notes = undef.map(notesJSON, Note.fromJSON);
            const attachments = undef.map(attachmentsJSON, Media.Reference.fromJSON);

            // Update linked tasklist if one exists
            const tasklist = await TaskList.find(db, { medication_id });
            if (tasklist) {
                await tasklist.updateAll(db, {
                    notes,
                    attachments,
                });
            }

            // Perform medication update
            await patient.update_medication(db, {
                _id: medication_id,
                pillpack_id,
                name,
                dosage,
                rx_num,
                next_refill,
                pharmacy,
                prescriber,
                purpose,
                appearance,
                instructions,
                photo,
                notes,
                attachments,
            });

            // Log the changes to the medication
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "medication_update",
                title: "Changed Medication",
                author: api_user.author_for(patient),
                meta: patient.medication(medication_id)!,
            });

            ctx.status = status.OK;
            ctx.body = patient.medication(medication_id);

        } catch (error) {
            if (error instanceof assert.AssertionError) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            throw error;
        }
    };
}

export interface UpdateScheduleBody {
    schedule?: IScheduleJSON;
}

export function updateSchedule(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, medication_id, db } = ctx.state as ContextState;
            const { schedule: scheduleJSON, ...unexpected } = ctx.request.body as UpdateScheduleBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient", {
                    debug: "Patient not found",
                });
            }

            // Ensure that we are allowed to access the route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            const medication = patient.medication(medication_id);
            if (!medication) {
                throw errors.create(status.NOT_FOUND, "NoSuchMedication", {
                    debug: "Medication not found",
                });
            }

            const schedule = undef.apply(scheduleJSON, Schedule.fromJSON);
            if (!schedule) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "Schedule is a required property",
                });
            }

            // Update tasklist schedule
            const tasklist = await TaskList.find(db, { medication_id });
            if (tasklist) {
                await tasklist.updateSchedule(db, schedule);
            }

            await patient.update_medication_schedule(db, {
                _id: medication_id,
                schedule,
            });

            // Log the changes to the medication
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "medication_reschedule",
                title: "Changed Medication Schedule",
                author: api_user.author_for(patient),
                meta: patient.medication(medication_id)!,
            });

            ctx.status = status.OK;
            ctx.body = patient.medication(medication_id);

        } catch (error) {
            if (error instanceof assert.AssertionError) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            throw error;
        }
    };
}

export function deleteMedication(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, patient_id, medication_id, db } = ctx.state as ContextState;
            const { ...unexpected } = ctx.request.body;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Patient is required
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND, "NoSuchPatient", {
                    debug: "Patient not found",
                });
            }

            // Ensure that we are allowed to access the route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            // Medication must exist
            const medication = patient.medication(medication_id);
            if (!medication) {
                throw errors.create(status.NOT_FOUND, "NoSuchMedication", {
                    debug: "Medication not found",
                });
            }

            // Remove medication from patient
            await patient.remove_medication(db, medication_id);

            // End the current task schedule
            const tasklist = await TaskList.find(db, { patient_id, medication_id });
            if (tasklist) {
                const schedule = Schedule.once(Date.current());
                await tasklist.updateSchedule(db, schedule);
            }

            // Log the changes to the medication
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "medication_delete",
                title: "Deleted Medication",
                author: api_user.author_for(patient),
                meta: medication,
            });

            ctx.status = status.NO_CONTENT;
            ctx.body = undefined;

        } catch (error) {
            if (error instanceof assert.AssertionError) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            throw error;
        }
    };
}
