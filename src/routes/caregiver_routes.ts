import * as status from "http-status";
import * as Router from "koa-router";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { CareGiver, IContact, Patient } from "../models";
import { undef } from "../utils";
import { assertEmpty, parseId } from "./utils";


interface ContextState {
    db: database.Db;
    api_user: APIUser;
    user_id: database.ObjectID;
    caregiver_id: database.ObjectID;
}

// Get a caregiver id from the API user
export function caregiverMe(): parseId.Callback {
    return async (ctx) => {
        const { api_user } = ctx.state as ContextState;
        return api_user.caregiver ? api_user.caregiver._id : database.id();
    };
}

const router = new Router();

// Parse URL parameters
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));
router.param("caregiver_id", parseId("caregiver_id", { "me": caregiverMe() }));

// Routes
router.put("/user/:user_id/caregiver/:caregiver_id", createCaregiver());
router.get("/caregiver/:caregiver_id", getCareGiver());
router.get("/caregiver/:caregiver_id/patients", getPatientList());

export default router;


export function getPatientList(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, caregiver_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Get the caregiver's patients
        const caregiver = await CareGiver.fetch(db, { _id: caregiver_id });
        if (!caregiver) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_self(caregiver) && !api_user.pac_to(caregiver) && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Search all patients assigned to this caregiver
        const patients = await Patient.search(db, { caregivers: [caregiver._id], limit: 100 });
        if (!patients.length) {
            throw errors.create(status.NOT_FOUND);
        }

        ctx.status = 200;
        ctx.body = patients;
    };
}

export function getCareGiver(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, caregiver_id } = ctx.state as ContextState;

        const caregiver = await CareGiver.fetch(db, { _id: caregiver_id });
        if (!caregiver) {
            throw errors.create(status.NOT_FOUND);
        }

        if (!api_user.caregiver_self(caregiver)
            && !api_user.patient_of(caregiver)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        ctx.status = status.OK;
        ctx.body = { caregiver };
    };
}

interface CreateCareGiverBody {
    pac_id?: string;
    full_name?: string;
    address?: IContact.PhysicalAddress.JSON;
    phone?: IContact.PhoneNumber.JSON;
    email?: IContact.EmailAddress.JSON;
}

export function createCaregiver(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, caregiver_id } = ctx.state as ContextState;
        const {
            pac_id: pac_id_str,
            full_name,
            address: address_json,
            phone: phone_json,
            email: email_json,
            ...unexpected,
        } = ctx.request.body as CreateCareGiverBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        if (!full_name) {
            throw errors.create(status.BAD_REQUEST, {
                debug: "full_name is a required field",
            });
        }

        const pac_id = undef.apply(pac_id_str, database.parseObjectID);
        const addresses = undef.map(address_json, IContact.PhysicalAddress.fromJSON);
        const phones = undef.map(phone_json, IContact.PhoneNumber.fromJSON);
        const emails = undef.map(email_json, IContact.EmailAddress.fromJSON);

        // Ensure that we have access to this record
        if (!api_user.pac_self(pac_id) && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Create caregiver object
        const caregiver = await CareGiver.create(db, {
            _id: caregiver_id,
            user_id,
            pac_id,
            full_name,
            addresses,
            phones,
            emails,
        });

        ctx.status = status.CREATED;
        ctx.body = { caregiver };
    };
}
