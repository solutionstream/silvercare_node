// import * as status from "http-status";
import * as status from "http-status";
import * as Router from "koa-router";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { ActivityLog, CareGiver, Device, DeviceList, Patient } from "../models";
import { PERS } from "../pers";
import { assertEmpty, parseId } from "./utils";


interface ContextState {
    db: database.Db;
    api_user: APIUser;
    user_id: database.ObjectID;
    device_id: database.ObjectID;
}

const router = new Router();

// Parse URL params
router.param("device_id", parseId("device_id"));
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));

// Routes
router.get(`/user/:user_id/device`, searchDevice());
router.put(`/user/:user_id/device/:device_id`, registerDevice());
router.delete(`/user/:user_id/device/:device_id`, deleteDevice());
router.put(`/user/:user_id/device/pers/ping`, pingPERS());


export default router;

export function pingPERS(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id } = ctx.state as ContextState;
        const [devicelist, patient] = await Promise.all([
            DeviceList.get(db, user_id),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.caregiver_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Ensure that the user has a PERS device
        const pers = devicelist.devices.find((device) => device.type === "pers");
        if (!pers) {
            throw errors.create(status.NOT_FOUND);
        }

        // Send notification to all PERS devices
        await devicelist.pers({
            message: {
                action: PERS.Message.TRYING_TO_REACH_YOU,
                vibrate: true,
            },
        });

        // Write to activity log if patient's PERs is pinged
        if (patient) {
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "pers_ping",
                title: "Called PERS",
                author: api_user.author_for(patient),
                meta: {},
            });
        }

        ctx.status = status.NO_CONTENT;
    };
}

export function searchDevice(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id } = ctx.state as ContextState;
        const [devicelist, caregiver, patient] = await Promise.all([
            DeviceList.get(db, user_id),
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure we are allowed to access this route
        if (!api_user.user_self(user_id)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        ctx.status = status.OK;
        ctx.body = { devices: devicelist.devices };
    };
}

interface RegisterBody {
    device?: any;
}

export function registerDevice(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, db, device_id, user_id } = ctx.state as ContextState;
            const {
                device: device_json,
                ...unexpected,
            } = ctx.request.body as RegisterBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            const [devicelist, caregiver, patient] = await Promise.all([
                DeviceList.get(db, user_id),
                CareGiver.fetch(db, { user_id }),
                Patient.fetch(db, { user_id }),
            ]);

            // Ensure that we have access to this route
            if (!api_user.user_self(user_id)
                && !api_user.pac_to(patient)
                && !api_user.pac_to(caregiver)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            if (!device_json) {
                throw errors.create(status.BAD_REQUEST, {
                    debug: "Device is a required field",
                });
            }

            // From this point forward device is considered valid
            const device: Device = Object.assign(device_json, {
                _id: device_id,
                created: Date.current(),
            });

            // Parse json values for device and validate the object
            DeviceList.validateDevice(device);

            // Add the device
            if (!devicelist.contains(device)) {
                await devicelist.insert(db, device);
                ctx.status = status.CREATED;
            } else {
                await devicelist.insert(db, device);
                ctx.status = status.OK;
            }

        } catch (error) {
            // Creation failed due to a validation error
            if (error instanceof errors.InvalidDocument) {
                ctx.throw(status.BAD_REQUEST, error.message);
            }

            // Failed for another reason
            throw error;
        }
    };
}

export function deleteDevice(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, user_id, device_id } = ctx.state as ContextState;

        const [devicelist, caregiver, patient] = await Promise.all([
            DeviceList.get(db, user_id),
            CareGiver.fetch(db, { user_id }),
            Patient.fetch(db, { user_id }),
        ]);

        // Ensure that we have access to this route
        if (!api_user.user_self(user_id)
            && !api_user.pac_to(patient)
            && !api_user.pac_to(caregiver)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        await devicelist.remove(db, device_id);
        ctx.status = status.NO_CONTENT;
    };
}
