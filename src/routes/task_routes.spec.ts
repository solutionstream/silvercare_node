import "koa";
import "mocha";
import * as moment from "moment";
import * as config from "../config";
import * as database from "../database";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { User } from "../models";
import { ITask, Task, TaskID, TaskList } from "../models";
import { Activity, CareGiver, Medication, Patient } from "../models";
import { Event, Schedule } from "../scheduling";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("Task Routes", async () => {
    let db: database.Db;
    let today: Date;
    let tomorrow: Date;
    let two_days: Date;
    let description: string;
    let patient: Patient;
    let patient_id: database.ObjectID;
    let user: User;
    let user_id: database.ObjectID;
    let caregiver: CareGiver;
    let caregiver_id: database.ObjectID;
    let other_caregiver: CareGiver;
    let other_caregiver_id: database.ObjectID;
    let api_user: Stubbed<APIUser>;
    let author: Activity.Author;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        today = moment().add({ h: 1 }).toDate();
        tomorrow = moment(today).add({ d: 1 }).toDate();
        two_days = moment(today).add({ d: 2 }).toDate();
        description = "Take over the world!";
        author = Activity.Author.factory({
            _id: database.id(),
            type: "admin",
            full_name: "Administrator",
        });

        // Caregiver belonging to a different user than the patient
        caregiver_id = database.id();
        caregiver = CareGiver.factory({
            _id: caregiver_id,
            user_id: database.id(),
            full_name: "Donald Duck",
        });

        // Create a second care giver in the care circle
        other_caregiver_id = database.id();
        other_caregiver = CareGiver.factory({
            _id: other_caregiver_id,
            user_id: database.id(),
            full_name: "Goofy",
        });

        // Patient reference with caregiver in care circle
        user_id = database.id();
        user = User.factory({ _id: user_id, username: "mmouse" });
        patient_id = database.id();
        patient = Patient.factory({
            _id: patient_id,
            user_id,
            full_name: "Patient",
            carecircle: {
                caregivers: [
                    caregiver.contact({ relationship: "Friend", notes: [] }),
                    other_caregiver.contact({ relationship: "Friend", notes: [] }),
                ],
                contacts: [],
            },
         });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;
        helper = new RouteHelper({ state: { db, api_user } });

        await database.setupCollections(db);
        await database.fixture(db, {
            user: [user],
            caregiver: [caregiver, other_caregiver],
            patient: [patient],
        });
    });

    afterEach(async () => {
        await db.dropDatabase();
        sandbox.restore();
    });

    // Easily create a tasklist in the database for testing
    async function createTasklist(options: { type: Task.Type, title: string, schedule: Schedule, caregiver: CareGiver.Reference, [key: string]: any }) {
        const { caregiver, schedule, medication_id, ...shared } = options;
        const tasklist = await TaskList.create(db, {
            _id: database.id(),
            medication_id,
            patient_id,
            schedule,
            defaults: { caregiver },
            shared,
        });

        // Add matching medication for medication types
        if (shared.type === "medication") {
            const medication = Medication.factory({
                _id: medication_id,
                pillpack_id: null,
                name: shared.title,
                dosage: "10mg",
                purpose: "Pain",
                instructions: "Take as needed",
                prescriber: "Someone",
                schedule,
                photo: null,
                notes: shared.notes,
                attachments: shared.attachments,
            });

            patient.medications.push(medication);
            await patient.save(db);
        }

        return tasklist;
    }

    function tasklistSchedules(list: TaskList) {
        return list.sequences.map((seq) => seq.outputSchedule());
    }

    // Easily create a task instance
    // This deals with some of the weirdness surrounding the task id
    function createTask(tasklist: TaskList, options: ITask & Event.Base): Task {
        const { position, date, out_of, caregiver } = options;
        return new Task({
            tasklist_id: tasklist._id,
            sequence_id: tasklist.sequences[0]._id,
            position,
            original_date: date,
        }, { date, position, out_of, caregiver, completed: false, due: date });
    }

    describe("create task sequence", async () => {
        const task_id = database.id();
        const title = "something";
        const type = "appointment";
        const description = "Go to this thing";
        let schedule: Schedule;

        beforeEach(async () => {
            schedule = Schedule.once(today);
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should append task template to patient record", async () => {
            helper.body({
                title,
                type,
                schedule,
                description,
                caregiver_id,
            });

            // Insert the task at the given id
            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                _id: task_id,
                defaults: {
                    caregiver: caregiver.reference(),
                },
                shared: {
                    title,
                    type,
                    description,
                    notes: [],
                    attachments: [],
                },
            });
        });

        it("should validate the task to be inserted", async () => {
            // Ensure that the validation fails
            const error = new errors.InvalidDocument("Forced Failure");
            const validate = sandbox.stub(TaskList, "validate").throws(error);

            // Add data to request body
            helper.body({
                title,
                type,
                schedule,
                description,
                caregiver_id,
            });

            // Insert the task at the given id
            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(validate).to.have.callCount(1);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if task is medication", async () => {
            helper.body({
                title,
                type: "medication",
                schedule,
                description,
                caregiver_id,
            });

            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidType",
            });
        });

        it("should fail if caregiver id is invalid", async () => {
            helper.body({
                title,
                type,
                schedule,
                description,
                caregiver_id: "invalid id string",
            });

            // Insert the task at the given id
            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidID",
            });
        });

        it("should fail if caregiver is not in care circle", async () => {
            const other_caregiver_id = database.id();
            helper.body({
                title,
                type,
                schedule,
                description,
                caregiver_id: other_caregiver_id,
            });

            // Insert the task at the given id
            const context = await helper.post(`/patient/${patient_id}/tasks/sequence/${task_id}`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "CannotAssign",
            });
        });

        it("should schedule proactive jobs to send task reminders");
        it("should schedule proactive jobs to check for task status changes");
    });

    describe("get tasks for patient", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/patient/${patient_id}/tasks/sequence/`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if start or until not provided", async () => {
            helper.query({ start: today });
            const context_start = await helper.get(`/patient/${patient_id}/tasks/sequence/`, router);
            expect(context_start.status).to.equal(status.BAD_REQUEST);
            expect(context_start.body).to.deep.equal({
                error: "InvalidParams",
            });

            helper.query({ until: tomorrow });
            const context_until = await helper.get(`/patient/${patient_id}/tasks/sequence/`, router);
            expect(context_until.status).to.equal(status.BAD_REQUEST);
            expect(context_until.body).to.deep.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if the date range is too large", async () => {
            const until = moment(today).add({ d: 31 }).toDate();
            helper.query({ start: today, until });

            const context = await helper.get(`/patient/${patient_id}/tasks/sequence`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should return no content when there are no tasklists", async () => {
            helper.query({ start: today, until: tomorrow });

            const context = await helper.get(`/patient/${patient_id}/tasks/sequence`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);
        });

        it("should return no content when no tasks appear during range", async () => {
            await createTasklist({
                title: "something",
                type: "appointment",
                schedule: Schedule.daily({ start: today, until: tomorrow }),
                caregiver: caregiver.reference(),
            });

            const late_start = moment(tomorrow).add({ d: 1 }).toDate();
            const late_until = moment(late_start).add({ d: 1 }).toDate();
            helper.query({ start: late_start, until: late_until });

            const context = await helper.get(`/patient/${patient_id}/tasks/sequence`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);
        });

        it("should return all tasks for specific tasklist", async () => {
            // Create task list form start to until
            await createTasklist({
                title: "something",
                type: "appointment",
                schedule: Schedule.daily({ start: today, until: tomorrow }),
                caregiver: caregiver.reference(),
            });

            // Create another task list for the same date range
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                schedule: Schedule.daily({ start: today, until: tomorrow }),
                caregiver: caregiver.reference(),
            });

            const range_start = moment(today).subtract({ d: 7 }).toDate();
            const range_until = moment(tomorrow).add({ d: 7 }).toDate();
            helper.query({ start: range_start, until: range_until });

            // Specify the tasklist id in the URL
            const context = await helper.get(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal([
                {
                    _id: tasklist._id,
                    defaults: { caregiver: caregiver.reference() },
                    shared: {
                        title: "something",
                        type: "other",
                        description: null,
                        notes: [],
                        attachments: [],
                    },
                    schedules: tasklistSchedules(tasklist),
                    tasks: [
                        createTask(tasklist, {
                            date: today,
                            position: 1,
                            out_of: 1,
                            caregiver: caregiver.reference(),
                        }),
                        createTask(tasklist, {
                            date: tomorrow,
                            position: 1,
                            out_of: 1,
                            caregiver: caregiver.reference(),
                        }),
                    ],
                },
            ]);
        });

        it("should return all tasks for patient", async () => {
            // Create task list form start to until
            const tasklist_1 = await createTasklist({
                title: "something_1",
                type: "appointment",
                schedule: Schedule.daily({ start: today, until: tomorrow }),
                caregiver: caregiver.reference(),
            });

            // Create another task list for the same date range
            const tasklist_2 = await createTasklist({
                title: "something_2",
                type: "other",
                schedule: Schedule.daily({ start: today, until: tomorrow }),
                caregiver: other_caregiver.reference(),
            });

            const range_start = moment(today).subtract({ d: 7 }).toDate();
            const range_until = moment(today).add({ d: 7 }).toDate();
            helper.query({ start: range_start, until: range_until });

            // No tasklist in the URL so it searching all tasklists
            const context = await helper.get(`/patient/${patient_id}/tasks/sequence/`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal([
                {
                    _id: tasklist_1._id,
                    defaults: { caregiver: caregiver.reference() },
                    shared: {
                        title: "something_1",
                        type: "appointment",
                        description: null,
                        notes: [],
                        attachments: [],
                    },
                    schedules: tasklistSchedules(tasklist_1),
                    tasks: [
                        createTask(tasklist_1, {
                            date: today,
                            position: 1,
                            out_of: 1,
                            caregiver: caregiver.reference(),
                        }),
                        createTask(tasklist_1, {
                            date: tomorrow,
                            position: 1,
                            out_of: 1,
                            caregiver: caregiver.reference(),
                        }),
                    ],
                },
                {
                    _id: tasklist_2._id,
                    defaults: { caregiver: other_caregiver.reference() },
                    shared: {
                        title: "something_2",
                        type: "other",
                        description: null,
                        notes: [],
                        attachments: [],
                    },
                    schedules: tasklistSchedules(tasklist_2),
                    tasks: [
                        createTask(tasklist_2, {
                            date: today,
                            position: 1,
                            out_of: 1,
                            caregiver: other_caregiver.reference(),
                        }),
                        createTask(tasklist_2, {
                            date: tomorrow,
                            position: 1,
                            out_of: 1,
                            caregiver: other_caregiver.reference(),
                        }),
                    ],
                },
            ]);
        });
    });

    describe("update task instance", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access control", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail on invalid task id", async () => {
            await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Send an invalid task id
            helper.body({ address: "invalid task id" });

            // Run the route
            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidID",
            });
        });

        it("should fail if the task is not found", async () => {
            // Create a task id for a non-existent task
            const task_id = TaskID.factory({
                tasklist_id: database.id(),
                sequence_id: database.id(),
                original_date: new Date(),
                position: 1,
            });

            helper.body({ address: task_id });

            // Run the route
            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchTask",
            });
        });

        it("should only update task with matching position", async () => {
            const end_of_today = moment(today).endOf("day").toDate();
            const tasklist = await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.daily({ start: today, until: tomorrow, instances: 2, relative: true }),
                caregiver: caregiver.reference(),
            });

            // Verify the number of tasks in the tasklist
            const first = tasklist.first()!;

            // Unsassign the first task
            helper.body({
                address: first.address,
                caregiver_id: null,
            });

            // Run the route
            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                address: first.address,
                date: today,
                due: end_of_today,
                position: 1,
                out_of: 2,
                caregiver: null,
                completed: false,
            });

            // Verify the tasks in the tasklist
            const updated_tasklist = await TaskList.fetchOne(db, patient, tasklist);
            const updated_tasks = updated_tasklist!.all();
            expect(updated_tasks[0].caregiver).to.equal(null);
            expect(updated_tasks[1].caregiver).to.deep.equal(caregiver.reference());
        });

        it("should unassign task", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Reassign the task
            const task = tasklist.first()!;
            helper.body({
                address: task.address,
                caregiver_id: null,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                address: task.address,
                date: today,
                due: today,
                position: 1,
                out_of: 1,
                caregiver: null,
                completed: false,
            });
        });

        it("should cancel task", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Cancel the task instance
            const task = tasklist.first()!;
            helper.body({
                address: task.address,
                cancelled: true,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
        });

        it("should complete task", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Mark as completed
            const task = tasklist.first()!;
            helper.body({
                address: task.address,
                completed: true,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/instance`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                address: task.address,
                date: today,
                due: today,
                position: 1,
                out_of: 1,
                caregiver: caregiver.reference(),
                completed: true,
            });
        });

        it("should uncomplete completed task", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Mark task as completed
            const task = tasklist.first()!;
            await tasklist.updateOne(db, task, { completed: true });

            // Try marking the task as incomplete
            helper.body({
                address: task.address,
                completed: false,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/instance`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                address: task.address,
                date: today,
                due: today,
                position: 1,
                out_of: 1,
                caregiver: caregiver.reference(),
                completed: false,
            });
        });

        it("should update task details", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
                notes: [
                    { author: "mmouse", date: new Date(), content: "This is a note" },
                ],
            });

            // Change everything that we can about the task
            const task = tasklist.first()!;
            helper.body({
                address: task.address,
                new_date: tomorrow,
                caregiver_id: other_caregiver._id,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/instance/`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                address: task.address,
                date: tomorrow,
                due: tomorrow,
                position: 1,
                out_of: 1,
                caregiver: other_caregiver.reference(),
                completed: false,
            });
        });
    });

    describe("update task sequence", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${database.id()}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if updating type from medication", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "medication",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
                medication_id: database.id(), // required for medications
            });

            // Change type from medication to something else
            helper.body({
                type: "other",
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidType",
            });
        });

        it("should fail if updating type to medication", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "appointment",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Change type to medication
            helper.body({
                type: "medication",
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidType",
            });
        });

        it("should fail if there is no such tasklist", async () => {
            const tasklist_id = database.id();
            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "NoSuchTask",
            });
        });

        it("should fail if caregiver is not in care circle", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                description,
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            // Update with a caregiver that is not in the care circle
            const other_caregiver_id = database.id();
            helper.body({
                caregiver_id: other_caregiver_id,
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "CannotAssign",
            });
        });

        it("should update the task sequence", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
            });

            const title = "something_2";
            const type = "appointment";
            const description = "Go to this";
            const note = { author: "Mickey Mouse", date: new Date(), content: "Comment" };

            // Change all available fields
            helper.body({
                title,
                type,
                description,
                caregiver_id: other_caregiver._id,
                notes: [note],
            });

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                _id: tasklist._id,
                defaults: { caregiver: other_caregiver.reference() },
                shared: {
                    title,
                    type,
                    description,
                    notes: [note],
                    attachments: [],
                },
            });
        });

        it("should update associated medication", async () => {
            const medication_id = database.id();
            const tasklist = await createTasklist({
                title: "something",
                type: "medication",
                description: "description",
                schedule: Schedule.once(today),
                caregiver: caregiver.reference(),
                medication_id,
            });

            const title = "something_2";
            const description = "Go to this";
            const note = { author: "Mickey Mouse", date: new Date(), content: "Comment" };
            const attachment = { user: database.id(), file: database.id() };

            // Change all available fields
            helper.body({
                title,
                description,
                caregiver_id: other_caregiver._id,
                notes: [note],
                attachments: [attachment],
            });

            await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}`, router);
            await patient.refresh(db);
            expect(patient.medications[0]).to.shallow.equal({
                _id: medication_id,
                notes: [note],
                attachments: [attachment],
            });
        });
    });

    describe("reschedule task sequence", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access control", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${database.id()}/schedule`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should end sequence without losing past data", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                schedule: Schedule.daily({ start: today, until: two_days }),
                caregiver: caregiver.reference(),
            });

            // Replace the schedule with one that ends sooner than the current
            const schedule = Schedule.daily({ start: tomorrow, until: tomorrow });
            helper.body({ schedule });

            // Replace the schedule
            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}/schedule`, router);
            expect(context.status).to.equal(status.NO_CONTENT);

            // Verify that the existing schedule is modified to end
            const updated = await TaskList.fetchOne(db, tasklist.patient_id, tasklist);
            expect(updated!.sequences.length).to.equal(1);
            expect(updated!.sequences[0].schedule).to.shallow.equal({
                start: today,
                until: moment(tomorrow).subtract({ ms: 1 }).toDate(),
            });
        });

        it("should create new sequence with different schedule", async () => {
            const tasklist = await createTasklist({
                title: "something",
                type: "other",
                schedule: Schedule.daily({ start: today, until: two_days }),
                caregiver: caregiver.reference(),
            });

            // Replace the schedule with a completely different thing
            const schedule = Schedule.daily({ start: tomorrow, until: two_days, instances: 2 });
            helper.body({ schedule });

            // Replace the schedule
            const context = await helper.put(`/patient/${patient_id}/tasks/sequence/${tasklist._id}/schedule`, router);
            expect(context.status).to.equal(status.NO_CONTENT);

            const updated = await TaskList.fetchOne(db, tasklist.patient_id, tasklist);
            expect(updated!.sequences.length).to.equal(2);
            expect(updated!.sequences[1].schedule).to.deep.equal(schedule);
            expect(updated!.sequences[0].schedule).to.shallow.equal({
                start: today,
                until: moment(tomorrow).add({ ms: -1 }).toDate(),
            });
        });

    });

    describe("attach media to task sequence", async () => {

    });

});
