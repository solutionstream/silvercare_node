import "koa";
import "mocha";
import * as config from "../config";
import * as database from "../database";
import { APIUser } from "../middleware";
import { User } from "../models";
import { Activity, CareCircle, CareGiver, Media, PAC, Patient } from "../models";
import { expect, RouteHelper, sinon, status, Stubbed } from "../testing";
import { router } from "./";


describe("Patient Routes", async () => {
    let db: database.Db;
    let user: User;
    let user_id: database.ObjectID;
    let api_user: Stubbed<APIUser>;
    let author: Activity.Author;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        user = User.factory({ username: "username" });
        user_id = user._id;
        author = Activity.Author.factory({
            _id: database.id(),
            type: "admin",
            full_name: "Administrator",
        });

        sandbox = sinon.sandbox.create();
        sandbox.stub(APIUser.prototype);
        api_user = new APIUser(user) as any;

        helper = new RouteHelper();
        helper.state({ db, user, api_user });

        await database.setupCollections(db);
        await database.fixture(db, { user: [user] });
    });

    afterEach(async () => {
        await db.dropDatabase();
        sandbox.restore();
    });


    describe("create patient", async () => {
        let patient_id: database.ObjectID;

        beforeEach(async () => {
            patient_id = database.id();
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.sv_admin.returns(false);
            const context = await helper.put(`/user/${user_id}/patient/${patient_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail for invalid params", async () => {
            helper.body({ phone: { type: "home", number: "invalid" } });
            const context = await helper.put(`/user/${user_id}/patient/${patient_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should create patient for current user", async () => {
            // Create a new patient object using our desired values
            const patient = Patient.factory({
                _id: patient_id,
                user_id,
                pac_id: database.id(),
                full_name: "Patient Zero",
                preferred_name: "Zero",
                date_of_birth: new Date(),
                gender: "Zombie",
                blood_type: "Yes",
                faith: "Zombie",
                instructions: "Brains...",
                conditions: [ "Stiffness" ],
                allergies: [ "Shotgun Shells" ],
                addresses: [ { type: "home", address: "123 Fake St." } ],
                phones: [ { type: "mobile", number: "+15551234567" } ],
                emails: [ { type: "home", email: "zero@zomb.io" } ],
            });

            helper.body({
                pac_id: patient.pac_id,
                full_name: patient.full_name,
                preferred_name: patient.preferred_name,
                date_of_birth: patient.date_of_birth,
                gender: patient.gender,
                blood_type: patient.blood_type,
                faith: patient.faith,
                instructions: patient.instructions,
                conditions: patient.conditions,
                allergies: patient.allergies,
                address: patient.addresses[0],
                phone: patient.phones[0],
                email: patient.emails[0],
            });

            // This value will be changed by the route
            // Everything else should match
            delete patient.version;

            const context = await helper.put(`/user/me/patient/${patient_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.shallow.json.equal(patient);
        });

        it("should only create one patient per user");
    });

    describe("update patient", async () => {
        let patient: Patient;

        beforeEach(async () => {
            patient = await Patient.create(db, { user_id, full_name: "Patient" });
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.put(`/patient/${patient._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail for invalid params", async () => {
            helper.body({ phone: { type: "home", number: "invalid" } });
            const context = await helper.put(`/patient/${patient._id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should fail if patient does not exist", async () => {
            const context = await helper.put(`/patient/${database.id()}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should update patient values", async () => {
            patient.update({
                pac_id: database.id(),
                full_name: "Patient Zero",
                preferred_name: "Zero",
                date_of_birth: new Date(),
                gender: "Zombie",
                blood_type: "Yes",
                faith: "Zombie",
                instructions: "Brains...",
                conditions: ["Stiffness"],
                allergies: ["Shotgun Shells"],
                addresses: [{ type: "home", address: "123 Fake St." }],
                phones: [{ type: "mobile", number: "+15551234567" }],
                emails: [{ type: "home", email: "zero@zomb.io" }],
            });

            helper.body({
                pac_id: patient.pac_id,
                full_name: patient.full_name,
                preferred_name: patient.preferred_name,
                date_of_birth: patient.date_of_birth,
                gender: patient.gender,
                blood_type: patient.blood_type,
                faith: patient.faith,
                instructions: patient.instructions,
                conditions: patient.conditions,
                allergies: patient.allergies,
                address: patient.addresses[0],
                phone: patient.phones[0],
                email: patient.emails[0],
            });

            // Route will update this value
            // Everything else will be compared
            delete patient.version;

            const context = await helper.put(`/patient/${patient._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.shallow.json.equal(patient);
        });
    });

    describe("get patient by id", async () => {
        let patient: Patient;

        beforeEach(async () => {
            patient = await Patient.create(db, { user_id, full_name: "Patient" });
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const context = await helper.get(`/patient/${patient._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail for patient that doesn't exist", async () => {
            const patient_id = database.id();
            const context = await helper.get(`/patient/${patient_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should return patient with matching id", async () => {
            const context = await helper.get(`/patient/${patient._id}`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal(patient);
        });
    });

    describe("get patient carecircle", async () => {

        beforeEach(async () => {
            api_user.sv_admin.returns(true);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.sv_admin.returns(false);

            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const context = await helper.get(`/patient/${patient._id}/carecircle`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail when patient doesn't exist", async () => {
            const patient_id = database.id();
            const context = await helper.get(`/patient/${patient_id}/carecircle`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should get empty carecircle", async () => {
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const patient_c = CareGiver.factory(patient).contact();

            const context = await helper.get(`/patient/${patient._id}/carecircle`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                caregivers: [patient_c],
                contacts: [],
                providers: [],
            });
        });

        it("should get full carecircle", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const caregiver_c = caregiver.contact({ relationship: "Friend" });
            const contact = CareCircle.Miscellaneous.factory({ full_name: "Contact", relationship: "Friend" });
            const pac = await PAC.create(db, { full_name: "PAC", user_id: database.id() });
            const patient = await Patient.create(db, {
                user_id,
                pac_id: pac._id,
                full_name: "Patient",
                carecircle: {
                    caregivers: [caregiver_c],
                    contacts: [contact],
                },
            });

            const patient_c = CareGiver.factory(patient).contact();
            const context = await helper.get(`/patient/${patient._id}/carecircle`, router);
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.json.equal({
                contacts: [contact],
                caregivers: [caregiver_c, patient_c],
                providers: [pac],
            });
        });
    });

    describe("add caregiver", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "Test" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient", carecircle: { caregivers: [caregiver.contact()], contacts: [] }});
            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient doesn't exist", async () => {
            const patient_id = database.id();
            const caregiver_id = database.id();

            const context = await helper.put(`/patient/${patient_id}/carecircle/caregiver/${caregiver_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if caregiver doesn't exist", async () => {
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const caregiver_id = database.id();

            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should add caregiver to patient care circle", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const patient_c = CareCircle.CareGiver.factory(patient);

            // Test route output
            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Test change to patient record
            await patient.refresh(db);
            expect(patient.carecircle).to.deep.equal({
                contacts: [],
                caregivers: [
                    patient_c,
                    {
                        _id: caregiver._id,
                        full_name: caregiver.full_name,
                        relationship: "",
                        notes: [],
                        read_discharge_docs: false,
                        notifications: {},
                    },
                ],
            });
        });

        it("should update caregiver in patient circle", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const caregiver_c = caregiver.contact({ relationship: "Friend" });
            const patient = await Patient.create(db, {
                user_id,
                full_name: "Patient",
                carecircle: {
                    caregivers: [ caregiver_c ],
                    contacts: [],
                },
            });

            // Update the caregiver name
            caregiver.update({ full_name: "Updated" });
            await caregiver.save(db);

            // Change the caregiver's relationship and notes
            const relationship = "Best Friend";
            const notes = [{ author: "Mickey Mouse", date: new Date(), content: "..." }];
            const read_discharge_docs = true;
            helper.body({ relationship, notes, read_discharge_docs  });

            // Test route output
            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Test change to patient record
            await patient.refresh(db);
            const patient_c = CareCircle.CareGiver.factory(patient);

            expect(patient.carecircle).to.deep.equal({
                contacts: [],
                caregivers: [
                    patient_c,
                    {
                        _id: caregiver._id,
                        full_name: caregiver.full_name,
                        relationship,
                        notes,
                        read_discharge_docs,
                        notifications: {
                            enabled: true,
                            tasks: {},
                        },
                    },
                ],
            });
        });

        it("should set primary caregiver", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });

            helper.body({ primary: true });

            // Test route output
            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Test change to patient record
            await patient.refresh(db);
            const patient_c = CareCircle.CareGiver.factory(patient);

            expect(patient.primary_caregiver).to.deep.equal(caregiver._id);
            expect(patient.carecircle).to.deep.equal({
                contacts: [],
                caregivers: [
                    patient_c,
                    {
                        _id: caregiver._id,
                        full_name: caregiver.full_name,
                        relationship: "",
                        notes: [],
                        read_discharge_docs: false,
                        notifications: {},
                    },
                ],
            });
        });

        it("should unset primary caregiver", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient", primary_caregiver: caregiver._id });

            helper.body({ primary: false });

            // Test route output
            const context = await helper.put(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            // Test change to patient record
            await patient.refresh(db);
            const patient_c = CareCircle.CareGiver.factory(patient);

            expect(patient.primary_caregiver).to.equal(null);
            expect(patient.carecircle).to.deep.equal({
                contacts: [],
                caregivers: [
                    patient_c,
                    {
                        _id: caregiver._id,
                        full_name: caregiver.full_name,
                        relationship: "",
                        notes: [],
                        read_discharge_docs: false,
                        notifications: {},
                    },
                ],
            });
        });
    });

    describe("remove caregiver", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient", carecircle: { caregivers: [caregiver.contact()], contacts: [] }});
            const context = await helper.delete(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient does not exist", async () => {
            const patient_id = database.id();
            const caregiver_id = database.id();

            const context = await helper.delete(`/patient/${patient_id}/carecircle/caregiver/${caregiver_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should remove caregiver from carecircle", async () => {
            const caregiver = await CareGiver.create(db, { user_id: database.id(), full_name: "CareGiver" });
            const caregiver_c = caregiver.contact({ relationship: "Friend" });
            const patient = await Patient.create(db, {
                user_id,
                full_name: "Patient",
                carecircle: {
                    caregivers: [caregiver_c],
                    contacts: [],
                },
            });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/caregiver/${caregiver._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            await patient.refresh(db);
            const patient_c = CareCircle.CareGiver.factory(patient);
            expect(patient.carecircle).to.deep.equal({
                caregivers: [patient_c],
                contacts: [],
            });
        });

        it("should unassign all caregiver tasks");
    });

    describe("add contact", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const contact_id = database.id();
            const context = await helper.put(`/patient/${patient._id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient doesn't exist", async () => {
            const contact_id = database.id();
            const patient_id = database.id();

            const context = await helper.put(`/patient/${patient_id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail for invalid contact details", async () => {
            const contact_id = database.id();
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });

            const context = await helper.put(`/patient/${patient._id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.BAD_REQUEST);
            expect(context.body).to.json.equal({
                error: "InvalidParams",
            });
        });

        it("should add contact to contactlist", async () => {
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const contact_id = database.id();

            const full_name = "Contact";
            const relationship = "Friend";
            const photo = new Media.Reference(database.id(), database.id());
            const addresses = [{ type: "home", address: "123 Fake St." }];
            const emails = [{ type: "work", email: "mmouse@aol.com" }];
            const phones = [{ type: "mobile", number: "+15551234567" }];
            const notes = [{ author: "Mickey Mouse", date: new Date(), content: "..." }];

            helper.body({
                full_name,
                relationship,
                photo,
                addresses,
                emails,
                phones,
                notes,
            });

            const context = await helper.put(`/patient/${patient._id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.CREATED);
            expect(context.body).to.json.equal({
                version: context.body.version,
                _id: contact_id,
                full_name,
                relationship,
                photo,
                addresses,
                emails,
                phones,
                notes,
            });
        });
    });

    describe("remove contact", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access control", async () => {
            api_user.caregiver_to.returns(false);
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const contact = CareCircle.Miscellaneous.factory({ full_name: "Contact", relationship: "Friend" });
            const patient = await Patient.create(db, { user_id, full_name: "Patient", carecircle: { caregivers: [], contacts: [contact] }});
            const context = await helper.delete(`/patient/${patient._id}/carecircle/contact/${contact._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.caregiver_to).to.have.callCount(1);
            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient doesn't exist", async () => {
            const contact_id = database.id();
            const patient_id = database.id();

            const context = await helper.delete(`/patient/${patient_id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if contact does not exist", async () => {
            const contact_id = database.id();
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/contact/${contact_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should remove contact", async () => {
            const contact = CareCircle.Miscellaneous.factory({ full_name: "Contact", relationship: "Friend" });
            const patient = await Patient.create(db, {
                user_id,
                full_name: "Patient",
                carecircle: {
                    caregivers: [],
                    contacts: [contact],
                },
            });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/contact/${contact._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);
        });
    });

    describe("add pac", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access control", async () => {
            api_user.sv_admin.returns(false);

            const pac = await PAC.create(db, { full_name: "PAC", user_id: database.id() });
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const context = await helper.put(`/patient/${patient._id}/carecircle/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient does not exist", async () => {
            const patient_id = database.id();
            const pac_id = database.id();

            const context = await helper.put(`/patient/${patient_id}/carecircle/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if pac does not exist", async () => {
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });
            const pac_id = database.id();

            const context = await helper.put(`/patient/${patient._id}/carecircle/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should set pac id", async () => {
            const pac = await PAC.create(db, { full_name: "PAC", user_id: database.id() });
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });

            const context = await helper.put(`/patient/${patient._id}/carecircle/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            await patient.refresh(db);
            expect(patient.pac_id).to.deep.equal(pac._id);
        });
    });

    describe("remove pac", async () => {
        beforeEach(async () => {
            api_user.sv_admin.returns(true);
            api_user.author_for.returns(author);
        });

        it("should enforce access controls", async () => {
            api_user.pac_to.returns(false);
            api_user.sv_admin.returns(false);

            const pac = await PAC.create(db, { full_name: "PAC", user_id: database.id() });
            const patient = await Patient.create(db, { user_id, pac_id: pac._id, full_name: "Patient" });
            const context = await helper.delete(`/patient/${patient._id}/carecircle/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({ error: "Forbidden" });

            expect(api_user.pac_to).to.have.callCount(1);
            expect(api_user.sv_admin).to.have.callCount(1);
        });

        it("should fail if patient does not exist", async () => {
            const patient_id = database.id();
            const pac_id = database.id();

            const context = await helper.delete(`/patient/${patient_id}/carecircle/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.NOT_FOUND);
            expect(context.body).to.json.equal({
                error: "Not Found",
            });
        });

        it("should fail if patient has no pac", async () => {
            const pac_id = database.id();
            const patient = await Patient.create(db, { user_id, full_name: "Patient" });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "Conflict",
            });
        });

        it("should fail if patient pac doesn't match", async () => {
            const pac_id = database.id();
            const patient = await Patient.create(db, { user_id, pac_id: database.id(), full_name: "Patient" });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/pac/${pac_id}`, router);
            expect(context.status).to.equal(status.CONFLICT);
            expect(context.body).to.json.equal({
                error: "Conflict",
            });
        });

        it("should remove patient pac", async () => {
            const pac = await PAC.create(db, { full_name: "PAC", user_id: database.id() });
            const patient = await Patient.create(db, { user_id, pac_id: pac._id, full_name: "Patient" });

            const context = await helper.delete(`/patient/${patient._id}/carecircle/pac/${pac._id}`, router);
            expect(context.status).to.equal(status.NO_CONTENT);
            expect(context.body).to.equal(undefined);

            await patient.refresh(db);
            expect(patient.pac_id).to.equal(null);
        });
    });
});
