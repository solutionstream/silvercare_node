import * as status from "http-status";
import * as Router from "koa-router";
import * as _ from "lodash";
import * as moment from "moment";
import { email, fixtures } from "../config";
import { Db, ObjectID, parseObjectID } from "../database";
import { Email } from "../emails";
import * as errors from "../errors";
import { APIUser } from "../middleware";
import { ActivityLog, CareGiver, Document, IContact, Media, MediaLibrary, Note, PAC, Patient, TaskList, User } from "../models";
import { assertEmpty, parseId } from "../routes/utils";
import { assert, Dashboard, parseDate, toArray, undef, Variadic } from "../utils";


// This is what our routes expect to find in the context
interface ContextState {
    db: Db;
    api_user: APIUser;
    user_id: ObjectID;
    patient_id: ObjectID;
    caregiver_id: ObjectID;
    pac_id: ObjectID;
    contact_id: ObjectID;
}


const router = new Router();

// Parse url parameters
router.param("patient_id", parseId("patient_id"));
router.param("caregiver_id", parseId("caregiver_id"));
router.param("contact_id", parseId("contact_id"));
router.param("pac_id", parseId("pac_id"));
router.param("user_id", parseId("user_id", { "me": (ctx) => ctx.state.user._id }));

// Routes
router.put("/user/:user_id/patient/:patient_id", createPatient());

router.get("/patient", searchPatients());
router.get("/patient/conditions", searchConditions());
router.get("/patient/:patient_id", getPatient());
router.put("/patient/:patient_id", updatePatient());
router.get("/patient/:patient_id/carecircle", getCareCircle());
router.put("/patient/:patient_id/carecircle/caregiver/invite", inviteCareGiver());
router.put("/patient/:patient_id/carecircle/caregiver/accept", acceptInvitation());
router.put("/patient/:patient_id/carecircle/caregiver/:caregiver_id", updateCareGiver());
router.delete("/patient/:patient_id/carecircle/caregiver/:caregiver_id", removeCareGiver());
router.put("/patient/:patient_id/carecircle/contact/:contact_id", updateContact());
router.delete("/patient/:patient_id/carecircle/contact/:contact_id", removeContact());
router.put("/patient/:patient_id/carecircle/pac/:pac_id", updatePAC());
router.delete("/patient/:patient_id/carecircle/pac/:pac_id", removePAC());

export default router;


// Body params expected for creating a patient
interface CreatePatientBody {
    pac_id?: string;
    full_name?: string;
    preferred_name?: string;
    date_of_birth?: number;
    gender?: string;
    blood_type?: string;
    faith?: string;
    instructions?: string;
    conditions?: Variadic<string>;
    allergies?: Variadic<string>;
    address?: IContact.PhysicalAddress.JSON;
    phone?: IContact.PhoneNumber.JSON;
    email?: IContact.EmailAddress.JSON;
}

// Create a new patient record
export function createPatient(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, user_id, db, patient_id } = ctx.state as ContextState;
        const {
            pac_id: pac_id_str,
            full_name,
            preferred_name,
            date_of_birth: date_of_birth_num,
            gender,
            blood_type,
            faith,
            instructions,
            conditions: conditions_array,
            allergies: allergies_array,
            address: address_json,
            phone: phone_json,
            email: email_json,
            ...unexpected,
        } = ctx.request.body as CreatePatientBody;

        if (!api_user.pac && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        if (!full_name) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams");
        }

        // Parse all of the supplied parameters
        const pac_id = api_user.pac ? api_user.pac._id : undef.apply(pac_id_str, ObjectID.createFromHexString);
        const date_of_birth = undef.apply(date_of_birth_num, parseDate);
        const conditions = undef.apply(conditions_array, toArray);
        const allergies = undef.apply(allergies_array, toArray);
        const addresses = undef.map(address_json, IContact.PhysicalAddress.fromJSON);
        const phones = undef.map(phone_json, IContact.PhoneNumber.fromJSON);
        const emails = undef.map(email_json, IContact.EmailAddress.fromJSON);

        // Set the user's profile photo
        let photo: undefined | Media.Reference;
        const medialibrary = await MediaLibrary.fetch(db, { user_id });
        if (medialibrary) {
            photo = medialibrary.profile_reference(user_id);
        }

        // Create a patient object
        const patient = Patient.factory({
            _id: patient_id,
            user_id,
            pac_id,
            full_name,
            preferred_name,
            date_of_birth,
            gender,
            blood_type,
            faith,
            instructions,
            conditions,
            allergies,
            photo,
            addresses,
            phones,
            emails,
        });

        try {
            Patient.validate(patient);
        } catch (error) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: `Validation error: ${error.message}`,
            });
        }

        // Insert valid patient into db
        await Patient.insert(db, patient);

        // Log the creation of the patient record
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "patient_create",
            title: "Patient Record Created",
            author: api_user.author_for(patient),
            meta: patient,
        });

        ctx.status = status.CREATED;
        ctx.body = patient;
    };
}


interface UpdatePatientBody {
    pac_id?: string;
    full_name?: string;
    preferred_name?: string;
    date_of_birth?: number;
    gender?: string;
    blood_type?: string;
    faith?: string;
    instructions?: string;
    conditions?: Variadic<string>;
    allergies?: Variadic<string>;
    address?: IContact.PhysicalAddress.JSON;
    phone?: IContact.PhoneNumber.JSON;
    email?: IContact.EmailAddress.JSON;
}

export function updatePatient(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id } = ctx.state as ContextState;
        const {
            pac_id: pac_id_str,
            full_name,
            preferred_name,
            date_of_birth: date_of_birth_num,
            gender,
            blood_type,
            faith,
            instructions,
            conditions: conditions_array,
            allergies: allergies_array,
            address: address_json,
            phone: phone_json,
            email: email_json,
            ...unexpected,
        } = ctx.request.body as UpdatePatientBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Get the patient to be updated
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Parse all of the supplied parameters
        const pac_id = undef.apply(pac_id_str, parseObjectID);
        const date_of_birth = undef.apply(date_of_birth_num, parseDate);
        const conditions = undef.apply(conditions_array, toArray);
        const allergies = undef.apply(allergies_array, toArray);
        const addresses = undef.map(address_json, IContact.PhysicalAddress.fromJSON);
        const phones = undef.map(phone_json, IContact.PhoneNumber.fromJSON);
        const emails = undef.map(email_json, IContact.EmailAddress.fromJSON);

        // Set the user's profile photo
        let photo: undefined | Media.Reference;
        const medialibrary = await MediaLibrary.fetch(db, { user_id: patient.user_id });
        if (medialibrary) {
            photo = medialibrary.profile_reference(patient.user_id);
        }

        // Update the values of the patient object
        patient.update({
            pac_id,
            full_name,
            preferred_name,
            date_of_birth,
            gender,
            blood_type,
            faith,
            instructions,
            conditions,
            allergies,
            photo,
            addresses,
            phones,
            emails,
        });

        try {
            Patient.validate(patient);
        } catch (error) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: `Validation Error: ${error.message}`,
            });
        }

        await patient.save(db);
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "patient_update",
            title: "Patient Record Updated",
            author: api_user.author_for(patient),
            meta: patient,
        });

        ctx.status = status.OK;
        ctx.body = patient;

    };
}

// Get a patient record by id
export function getPatient(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        ctx.status = status.OK;
        ctx.body = patient;
    };
}

interface SearchPatientQuery {
    caregivers?: Variadic<string>;
    pacs?: Variadic<string>;
    name?: string;
    dob?: number;
    address?: string;
    phone?: string;
    email?: string;
    limit?: number;
}

export function searchPatients(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db } = ctx.state as ContextState;
        const {
            caregivers: caregivers_r,
            pacs: pacs_r,
            dob: dob_n,
            name,
            address,
            phone,
            email,
            limit,
            ...unexpected,
        } = ctx.request.query as SearchPatientQuery;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.pac && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!limit) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                debug: "limit is a required field",
            });
        }

        // Parse caregiver ids
        let caregivers: undefined | ObjectID[];
        if (caregivers_r) {
            const array = Array.isArray(caregivers_r) ? caregivers_r : [caregivers_r];
            caregivers = _.compact(array.map(parseObjectID));
            if (caregivers.length !== array.length) {
                throw errors.create(status.BAD_REQUEST, {
                    debug: "Invalid CareGiver id",
                });
            }
        }

        // Parse pac ids
        let pacs: undefined | ObjectID[];

        // If the API user is a PAC they are limited to searching by their PAC
        if (api_user.pac) {
            pacs = [api_user.pac._id];
        }
        // Otherwise allow the user to specify the list of pacs to search
        else if (pacs_r || api_user.pac) {
            const array = Array.isArray(pacs_r) ? pacs_r : [pacs_r];
            pacs = _.compact(array.map(parseObjectID));
            if (pacs.length !== array.length) {
                throw errors.create(status.BAD_REQUEST, {
                    debug: "Invalid PAC id",
                });
            }
        }

        // Parse date of birth
        let dob: undefined|Date;
        if (dob_n) {
            dob = moment(dob_n | 0).toDate();
        }

        // Search patients
        const patients = await Patient.search(db, {
            caregivers,
            pacs,
            name,
            dob,
            address,
            phone,
            email,
            limit: limit | 0,
        });

        ctx.status = status.OK;
        ctx.body = { patients };
    };
}

interface SearchConditionsQuery {
    query?: string;
    limit?: number;
}

export function searchConditions(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db } = ctx.state as ContextState;
        const { query, limit, ...unexpected } = ctx.request.query as SearchConditionsQuery;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Ensure that we are allowed to access this route
        if (!api_user.pac && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Retrieve the full list of conditions from the database
        const document = await Document.get<Document.Conditions>(db, fixtures.conditions);
        const conditions = document.value.conditions || [];

        // Get the list of conditions that match
        const matches = !query
            ? conditions
            : conditions.filter((c) => c.toLowerCase().indexOf(query.toLowerCase()) >= 0);

        ctx.status = status.OK;
        ctx.body = { conditions: matches.slice(0, limit) };
    };
}

// Get the user's care circle
export function getCareCircle(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.query;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Fetch the patient
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Get all contacts for the patient
        const contacts = patient.contacts();
        const caregivers = await patient.caregiver_contacts(db);

        // Get all providers
        const pac = await PAC.fetch(db, { _id: patient.pac_id });
        const providers = pac ? [pac] : [];

        ctx.status = status.OK;
        ctx.body = { contacts, caregivers, providers };
    };
}

interface InviteCareGiverBody {
    email_address?: string;
    full_name?: string;
    relationship?: string;
}

export function inviteCareGiver(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id } = ctx.state as ContextState;
        const { email_address, full_name, relationship, ...unexpected } = ctx.request.body as InviteCareGiverBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Fetch patient
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!email_address || !full_name || !assert.email(email_address)) {
            throw errors.create(status.BAD_REQUEST, "InvalidParams");
        }

        let new_user = false;

        // Get a user object for this email address
        let user = await User.get(db, { username: email_address });
        if (!user) {
            new_user = true;
            user = await User.create(db, { username: email_address });
        }

        // Get a caregiver object for this email address
        let caregiver = await CareGiver.fetch(db, { user_id: user._id });
        if (!caregiver) {
            caregiver = await CareGiver.create(db, { user_id: user._id, full_name });
        }

        // Do not send the invitation if the caregiver is already in the patient's carecircle
        if (patient.in_carecircle(caregiver)) {
            throw errors.create(status.CONFLICT, "AlreadyInCareCircle");
        }

        // Add a pending invitation to join the patient's care circle
        caregiver.add_invite(patient, relationship);
        await caregiver.save(db);

        // Link the user to the dashboard
        let link = Dashboard.profile();

        // Require the user to setup the account if new
        if (new_user) {
            const token = await user.issueToken({ once: true });
            await user.save(db);
            link = Dashboard.setup(user._id, { token, next: link });
        }

        // Send email invitation
        await Email.send({
            to: email_address,
            from: email.from,
            content: {
                template: "invite_caregiver",
                context: { patient, link },
            },
        });

        // Log the invitation of the caregiver
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_invite_caregiver",
            title: "Invited CareGiver",
            author: api_user.author_for(patient),
            meta: { caregiver_id: caregiver._id },
        });

        ctx.status = status.NO_CONTENT;
    };
}

export function acceptInvitation(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Fetch patient
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Only the caregiver can accept an invitation
        const caregiver = api_user.caregiver;
        if (!caregiver) {
            throw errors.create(status.FORBIDDEN);
        }

        // Find the invitation to this patient's carecircle
        const invite = caregiver.remove_invite(patient);
        if (!invite) {
            throw errors.create(status.NOT_FOUND, "NoSuchInvite");
        }

        // Add the caregiver to the patient's care circle
        patient.add_caregiver({
            _id: caregiver._id,
            full_name: caregiver.full_name,
            relationship: invite.relationship,
        });

        // Save changes
        await Promise.all([
            patient.save(db),
            caregiver.save(db),
        ]);

        // Log the acceptance of the invite
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_add_caregiver",
            title: "Accepted Invite",
            author: api_user.author_for(patient),
            meta: { caregiver_id: caregiver._id },
        });

        ctx.status = status.NO_CONTENT;
    };
}

interface AddCareGiverBody {
    primary?: boolean;
    relationship?: string;
    read_discharge_docs?: boolean;
    notes?: Variadic<Note.JSON>;
}

export function updateCareGiver(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, caregiver_id } = ctx.state as ContextState;
        const {
            primary,
            relationship,
            notes: notesJSON,
            read_discharge_docs,
            ...unexpected,
        } = ctx.request.body as AddCareGiverBody;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        // Fetch patient
        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Fetch caregiver
        const caregiver = await CareGiver.fetch(db, { _id: caregiver_id });
        if (!caregiver) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Parse notes
        const notes = undef.map(notesJSON, Note.fromJSON);

        // Add the caregiver to the care circle
        const index = patient.caregiver_index(caregiver);
        patient.add_caregiver({
            ...caregiver.reference(),
            notes,
            relationship,
            read_discharge_docs,
        }, primary);
        await patient.save(db);

        // Log addition into care circle
        if (index == null) {
            await ActivityLog.create(db, {
                patient_id: patient._id,
                type: "carecircle_add_caregiver",
                title: "Added to Care Circle",
                author: api_user.author_for(patient),
                meta: { caregiver_id },
            });
        }

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}

export function removeCareGiver(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, caregiver_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // We need to unassign all tasks assigned to this caregiver
        const tasklists = await TaskList.fetchAll(db, patient);
        await Promise.all(
            tasklists.map((task) => task.unassign(db, caregiver_id)));

        // Remove caregiver from the patient
        patient.remove_caregiver(caregiver_id);
        await patient.save(db);

        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_remove_caregiver",
            title: "Removed Care Giver",
            author: api_user.author_for(patient),
            meta: { caregiver_id },
        });

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}

interface AddContactBody {
    full_name?: string;
    relationship?: string;
    photo?: Media.ReferenceJSON;
    addresses?: Variadic<IContact.PhysicalAddress>;
    phones?: Variadic<IContact.PhoneNumber>;
    emails?: Variadic<IContact.EmailAddress>;
    notes?: Variadic<Note.JSON>;
}

export function updateContact(): Router.IMiddleware {
    return async (ctx) => {
        try {
            const { api_user, db, patient_id, contact_id } = ctx.state as ContextState;
            const {
                full_name,
                relationship,
                photo: photoJSON,
                addresses: addressesJSON,
                phones: phonesJSON,
                emails: emailsJSON,
                notes: notesJSON,
                ...unexpected,
            } = ctx.request.body as AddContactBody;

            // Error when there are unexpected properties
            assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

            // Get patient to update
            const patient = await Patient.fetch(db, { _id: patient_id });
            if (!patient) {
                throw errors.create(status.NOT_FOUND);
            }

            // Ensure that we are allowed to access this route
            if (!api_user.caregiver_to(patient)
                && !api_user.pac_to(patient)
                && !api_user.sv_admin()) {
                throw errors.create(status.FORBIDDEN);
            }

            if (!full_name) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: "Missing required params",
                });
            }

            const notes = undef.map(notesJSON, Note.fromJSON);
            const addresses = undef.apply(addressesJSON, toArray);
            const phones = undef.apply(phonesJSON, toArray);
            const emails = undef.apply(emailsJSON, toArray);
            const photo = undef.apply(photoJSON, Media.Reference.fromJSON);

            const index = patient.contact_index(contact_id);
            const contact = patient.add_contact({
                _id: contact_id,
                full_name,
                relationship,
                photo,
                addresses,
                phones,
                emails,
                notes,
            });

            await patient.save(db);

            // Log the addition of the contact
            if (index == null) {
                await ActivityLog.create(db, {
                    patient_id: patient._id,
                    type: "carecircle_add_contact",
                    title: "Contact Added",
                    author: api_user.author_for(patient),
                    meta: contact,
                });
            }

            ctx.status = status.CREATED;
            ctx.body = patient.contact(contact_id);

        } catch (error) {
            if (error instanceof assert.AssertionError) {
                throw errors.create(status.BAD_REQUEST, "InvalidParams", {
                    debug: error.message,
                });
            }

            throw error;
        }
    };
}

export function removeContact(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, contact_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        const contact = patient.contact(contact_id);
        if (!contact) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we have access to this route
        if (!api_user.caregiver_to(patient)
            && !api_user.pac_to(patient)
            && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        // Remove contact from carecircle
        patient.remove_contact(contact);
        await patient.save(db);
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_remove_contact",
            title: "Removed Contact",
            author: api_user.author_for(patient),
            meta: contact,
        });

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}

export function updatePAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, pac_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        const pac = await PAC.fetch(db, { _id: pac_id });
        if (!pac) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        patient.pac_id = pac._id;
        await patient.save(db);
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_add_pac",
            title: "Assigned PAC",
            author: api_user.author_for(patient),
            meta: pac,
        });

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}

export function removePAC(): Router.IMiddleware {
    return async (ctx) => {
        const { api_user, db, patient_id, pac_id } = ctx.state as ContextState;
        const { ...unexpected } = ctx.request.body;

        // Error when there are unexpected properties
        assertEmpty(unexpected, status.BAD_REQUEST, "InvalidParams");

        const patient = await Patient.fetch(db, { _id: patient_id });
        if (!patient) {
            throw errors.create(status.NOT_FOUND);
        }

        // Ensure that we are allowed to access this route
        if (!api_user.pac_to(patient) && !api_user.sv_admin()) {
            throw errors.create(status.FORBIDDEN);
        }

        if (!patient.pac_id || !pac_id.equals(patient.pac_id)) {
            throw errors.create(status.CONFLICT, {
                debug: "Patient pac doesn't match specified",
            });
        }

        patient.pac_id = null;
        await patient.save(db);
        await ActivityLog.create(db, {
            patient_id: patient._id,
            type: "carecircle_remove_pac",
            title: "Unassigned PAC",
            author: api_user.author_for(patient),
            meta: {},
        });

        ctx.status = status.NO_CONTENT;
        ctx.body = undefined;
    };
}
