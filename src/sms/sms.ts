import { SNS, Topic } from "../notifications";
import { Template } from "../templating";


export namespace SMS {

    export type Message = string | Template;

    export interface Template {
        template: string;
        context: object;
    }

    export interface SendOptions {
        to: string | string[];
        message: Message;
    }

}


export class SMS {

    // Render a template
    static render(template: SMS.Template) {
        return Template.render(template.template, template.context);
    }

    static subscriber(number: string): Topic.Subscriber {
        return { type: "sms", endpoint: number };
    }

    // Send a text message to zero or more recipients
    static async send(options: SMS.SendOptions) {
        let message = options.message;

        // Render a template if needed to create a message
        if (typeof message !== "string") {
            message = SMS.render(message);
        }

        // Create a topic and subscribe all of the recipients
        const topic = await Topic.create(SNS);
        const to = Array.isArray(options.to) ? options.to : [options.to];
        await topic.subscribe(...to.map(SMS.subscriber));

        try {
            return await topic.publish({ message });
        } finally {
            // Always delete the topic afterwards
            await topic.delete();
        }
    }
}
