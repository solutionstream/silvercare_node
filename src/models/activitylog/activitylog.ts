import * as database from "../../database";
import * as errors from "../../errors";
import "../../time";
import { assert, Omit, StringMap } from "../../utils";
import { CareGiver } from "../caregiver";
import { Media } from "../medialibrary";
import { IPAC } from "../pac";
import { CareCircle, IMedication, IPatient, Patient } from "../patient";
import { ITaskID, ITaskList, Task as TaskInstance } from "../tasklist";


// Logs all activity
export class ActivityLog {

    static async setupCollection(db: database.Db) {
        await ActivityLog.collection(db).createIndexes([
            { key: { "patient_id": 1 } },
            { key: { "author._id": 1 } },
            { key: { "created": 1 } },
            { key: { "title": "text", "meta.body": "text" } },
        ]);
    }

    // Create a new activity object from a partial object
    // Applies default values where possible
    static factory(options: Activity.FactoryOptions): Activity {
        const now = Date.current();
        const activity: Activity = {
            _id: options._id || database.id(),
            patient_id: options.patient_id,
            type: options.type as any,
            title: options.title,
            author: options.author,
            meta: options.meta,
            created: options.created || now,
            updated: options.updated || now,
        };

        return activity;
    }

    // Insert an activity into the database
    static async insert(db: database.Db, activity: Activity) {
        this.validate(activity);
        const result = await ActivityLog.collection(db).insertOne(activity);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return activity;
    }

    // Create an activity in the database
    static async create(db: database.Db, options: Activity.FactoryOptions) {
        const activity = ActivityLog.factory(options);
        return ActivityLog.insert(db, activity);
    }

    // Update an existing activity in the activity log
    static async update(db: database.Db, activity: Activity) {
        this.validate(activity);

        const result = await ActivityLog.collection(db).findOneAndReplace(
            { "_id": activity._id },
            { ...activity },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        return this.factory(result.value);
    }

    // Adds a journal entry
    static async journal(db: database.Db, options: Activity.JournalOptions) {
        return ActivityLog.create(db, { ...options, type: Activity.Type.journal });
    }

    // Search activities
    static async search(db: database.Db, options: Activity.SearchOptions) {
        const { patient_id, start, until, limit, type, term, author_id } = options;

        // Always limit search by patient_id and date range
        const query: any = {
            patient_id,
            created: { "$gte": start },
        };

        // Constrain by until date if provided
        if (until) {
            query.created.$lte = until;
        }

        // Limit search by activity type
        if (type) {
            query.type = type;
        }

        // Limit search by caregiver
        if (author_id) {
            query["author._id"] = author_id;
        }

        // Perform full-text search
        if (term) {
            query.$text = { "$search": term };
        }

        // Search for the journals
        let cursor = ActivityLog.collection(db)
            .find(query)
            .sort({ created: 1 });

        // Apply limit if specified
        if (limit) { cursor = cursor.limit(limit); }

        // Get and validate the activites
        const documents: Activity[] = await cursor.toArray();
        const activites = documents.map((doc) => ActivityLog.factory(doc));
        return activites;
    }

    // Check whether a string is one of the activity types
    static is_type(str: string): str is Activity.Type {
        return Object.keys(Activity.Type).indexOf(str) >= 0;
    }

    // Validate an activityfeed
    static validate(activity: Activity) {
        assert.instanceOf(activity._id, database.ObjectID);
        assert.instanceOf(activity.patient_id, database.ObjectID);
        assert.string(activity.title, { min: 1 });
        assert.date(activity.created);
        assert.object(activity.meta);
        Activity.Author.validate(activity.author);
    }

    private static collection(db: database.Db) {
        return db.collection("activitylog");
    }

    private constructor() { }

}

export namespace Activity {

    export type Type = keyof typeof Type;
    export const Type = StringMap(
        "journal",
        "pers_ping",
        "media_create",
        "media_update",
        "media_delete",
        "medication_create",
        "medication_update",
        "medication_delete",
        "medication_reschedule",
        "patient_create",
        "patient_update",
        "carecircle_invite_caregiver",
        "carecircle_add_caregiver",
        "carecircle_remove_caregiver",
        "carecircle_add_contact",
        "carecircle_remove_contact",
        "carecircle_add_pac",
        "carecircle_remove_pac",
        "tasklist_create",
        "tasklist_update",
        "tasklist_assign",
        "tasklist_unassign",
        "tasklist_reschedule",
        "tasklist_delete",
        "task_cancel",
        "task_complete",
        "task_assign",
        "task_unassign",
        "task_overdue",
        "task_update",
        "other");

    export type FactoryOptions = Partial<IActivityAuto> & IActivity;
    export type JournalOptions = Omit<IActivityType<"journal", Activity.Meta.Journal>, "type">;

    export interface SearchOptions {
        patient_id: database.ObjectID;
        start: Date;
        until?: Date;
        limit?: number;
        type?: Type;
        term?: string;
        author_id?: database.ObjectID;
    }

    export interface IAuthor {
        _id: database.ObjectID;
        type: Author.Type;
        full_name: string;
    }

    export class Author implements IAuthor {

        static validate(object: IAuthor) {
            assert.ok(object);
            assert.instanceOf(object._id, database.ObjectID);
            assert.oneOf(object.type, Object.keys(Author.Type));
            assert.string(object.full_name);
        }

        static factory(object: IAuthor|Patient|CareGiver.Reference) {
            const id = object._id;
            const full_name = object.full_name;

            if (object instanceof Patient) {
                return new Author(id, "patient", full_name);
            }

            if (object instanceof CareGiver.Reference) {
                return new Author(id, "caregiver", full_name);
            }

            return new Author(object._id, object.type, object.full_name);
        }

        static fromJSON(json: Author.JSON): Author {
            const _id = database.parseObjectID(json._id);
            if (!_id) { throw new Error("InvalidObjectID"); }

            const type = json.type;
            if (type !== "patient" && type !== "caregiver") { throw new Error("InvalidType"); }

            const author = Author.factory({ _id, type, full_name: json.full_name });
            Author.validate(author);
            return author;
        }

        constructor(
            public readonly _id: database.ObjectID,
            public readonly type: Author.Type,
            public readonly full_name: string,
        ) { }

        // Create a plain JSON representation of a caregiver reference
        toJSON(): Author.JSON {
            return {
                _id: this._id.toHexString(),
                type: this.type,
                full_name: this.full_name,
            };
        }
    }

    export namespace Author {
        export const Type = StringMap("patient", "caregiver", "admin", "system");
        export type Type = keyof typeof Type;

        export interface JSON {
            _id: string;
            type: string;
            full_name: string;
        }
    }

    export namespace Meta {
        export interface Empty {}

        export interface Journal {
            body: string;
        }

        export interface Media extends Media.File {
            user_id: database.ObjectID;
        }

        export interface CareGiver {
            caregiver_id: database.ObjectID;
        }

        export interface Contact extends CareCircle.Miscellaneous {}
        export interface Medication extends IMedication {}
        export interface PAC extends IPAC {}
        export interface Patient extends IPatient {}
        export interface Task extends TaskInstance {}
        export interface TaskID extends ITaskID {}
        export interface Tasklist extends ITaskList {}
    }
}

// Properties that are set automatically
interface IActivityAuto {
    _id: database.ObjectID;
    created: Date;
    updated: Date;
}

// Creates a activity type
interface IActivityType<Type extends Activity.Type, Meta> {
    patient_id: database.ObjectID;
    type: Type;
    title: string;
    author: Activity.IAuthor;
    meta: Meta;
}

// All of the activity types in a union
export type IActivity =
      IActivityType<"pers_ping", Activity.Meta.Empty>
    | IActivityType<"journal", Activity.Meta.Journal>
    | IActivityType<"media_create", Activity.Meta.Media>
    | IActivityType<"media_update", Activity.Meta.Media>
    | IActivityType<"media_delete", Activity.Meta.Media>
    | IActivityType<"medication_create", Activity.Meta.Medication>
    | IActivityType<"medication_update", Activity.Meta.Medication>
    | IActivityType<"medication_delete", Activity.Meta.Medication>
    | IActivityType<"medication_reschedule", Activity.Meta.Medication>
    | IActivityType<"patient_create", Activity.Meta.Patient>
    | IActivityType<"patient_update", Activity.Meta.Patient>
    | IActivityType<"carecircle_invite_caregiver", Activity.Meta.CareGiver>
    | IActivityType<"carecircle_add_caregiver", Activity.Meta.CareGiver>
    | IActivityType<"carecircle_remove_caregiver", Activity.Meta.CareGiver>
    | IActivityType<"carecircle_add_contact", Activity.Meta.Contact>
    | IActivityType<"carecircle_remove_contact", Activity.Meta.Contact>
    | IActivityType<"carecircle_add_pac", Activity.Meta.PAC>
    | IActivityType<"carecircle_remove_pac", Activity.Meta.Empty>
    | IActivityType<"tasklist_create", Activity.Meta.Tasklist>
    | IActivityType<"tasklist_update", Activity.Meta.Tasklist>
    | IActivityType<"tasklist_assign", Activity.Meta.Tasklist>
    | IActivityType<"tasklist_unassign", Activity.Meta.Tasklist>
    | IActivityType<"tasklist_reschedule", Activity.Meta.Tasklist>
    | IActivityType<"tasklist_delete", Activity.Meta.Tasklist>
    | IActivityType<"task_cancel", Activity.Meta.Task>
    | IActivityType<"task_complete", Activity.Meta.Task>
    | IActivityType<"task_assign", Activity.Meta.Task>
    | IActivityType<"task_unassign", Activity.Meta.Task>
    | IActivityType<"task_overdue", Activity.Meta.Task>
    | IActivityType<"task_update", Activity.Meta.Task>
    | IActivityType<"other", { [key: string]: any }>;

// Fat type with the activity union and the auto properties
export type Activity = IActivityAuto & IActivity;
