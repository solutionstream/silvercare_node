import * as bcrypt from "bcrypt";
import * as _ from "lodash";
import * as moment from "moment";
import {auth} from "../../config";
import { session } from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import { JWT } from "../../jwt";
import { StringMap, updateFrom } from "../../utils";


export interface IUser {
    _id: database.ObjectID;
    version: string;
    roles: User.Role[];
    tokens: string[];
    username: string;
    password: null | string;
    failed_logins: number;
    locked_until: null | Date;
    read_tacos: boolean;
}

export class Password {

    // Generate a unique salt to hash a password
    static async generateSalt() {
        return bcrypt.genSalt(auth.bcrypt_rounds);
    }

    // Hash a plaintext password and return the ciphertext
    static async hash(password: string, salt: string) {
        return bcrypt.hash(password, salt);
    }

    // Compare a plaintext password to a stored ciphertext
    static async compare(plaintext: string, ciphertext: string) {
        if (!plaintext || !ciphertext) { return false; }
        return bcrypt.compare(plaintext, ciphertext);
    }

    // Encrypt a password string.
    // Will not encrypt an already encrypted password.
    static async encrypt(password: string) {
        if (this.isCiphertext(password)) {
            return password;
        }

        // Hash a plaintext password string
        const salt = await this.generateSalt();
        const ciphertext = await this.hash(password, salt);
        return ciphertext;
    }

    // Determine whether a string has been previously encrypted
    static isCiphertext(ciphertext: string) {
        try {
            return bcrypt.getRounds(ciphertext) > 0;
        } catch (e) {
            return false;
        }
    }
}

export class User implements IUser {

    // Perform setup for collection
    // This should be called before the database is written to
    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndexes([
            { key: { "username": 1 }, unique: true },
            { key: { "username": "text" }, name: "user_fts" },
        ]);
    }

    // Create a User document, applying defaults where possible
    static factory(options: User.FactoryOptions) {
        return new User(options);
    }

    static async insert(db: database.Db, user: User) {
        // Ensure the user's password is encrypted
        if (user.password) {
            user.password = await Password.encrypt(user.password);
        }

        User.validate(user);
        const result = await this.collection(db).insertOne(user);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return user;
    }

    // Create a new user in the database
    static async create(db: database.Db, options: User.FactoryOptions) {
        const user = this.factory(options);
        return this.insert(db, user);
    }

    // Get the user record
    // If an authorization token is provided it must belong to the user
    static async get(db: database.Db, query: any) {
        const user = await this.collection(db).findOne(query);
        if (!user) { return undefined; }
        return this.factory(user);
    }

    // Search users
    static async search(db: database.Db, options: User.SearchOptions) {
        const { username, limit } = options;

        // Turn username search into list of exact phrases
        // This allows first searching a domain, then narrowing by username e.g. "silvercare.com mmouse"
        const parts = username.split(" ").map((part) => `"${part}"`);
        const phrases = parts.join(" ");

        const query: any = { "$text": { "$search": phrases } };
        const documents = await User.collection(db).find(query).limit(limit).toArray();
        const users = documents.map((doc) => this.factory(doc));
        return users;
    }

    // Retrieve the user record by providing username and password
    static async login(db: database.Db, username: string, password: string) {
        const user = await this.get(db, { username });
        if (!user) { throw new errors.NoSuchUser(); }

        try {
            const lockedUntil = user.accountLockedUntil();

            // Throw if account is locked
            if (lockedUntil) {
                throw new errors.AccountLocked(lockedUntil);
            }

            // Require that the provided password be either a token or the user's plaintext password
            if (!_.includes(user.tokens, password) && !(await Password.compare(password, user.password!))) {
                throw new errors.WrongPassword();
            }

            // Reset the account lock on successful login
            if (user.locked_until || user.failed_logins) {
                user.resetAccountLock();
                return user.save(db);
            }

            return user;
        } catch (error) {
            try {
                user.incrementFailedLogins();
                user.revokeToken(password);
                await user.save(db);
            } catch (e) { /* Intentionally empty */ }

            if (error instanceof errors.AccountLocked
            || error instanceof errors.WrongPassword) {
                throw error;
            }

            throw new errors.NotAuthorized("AuthenticationFailed");
        }
    }

    static validate(user: IUser) {
        const errorMsg = "InvalidUser";

        database.assert(!!user, errorMsg);
        database.assert(user._id instanceof database.ObjectID, errorMsg);
        database.assert(Array.isArray(user.roles), errorMsg);
        database.assert(_.difference(user.roles, Object.keys(User.Role)).length === 0, errorMsg);
        database.assert(typeof user.username === "string", errorMsg);
        database.assert(user.username.length > 0, errorMsg);
        database.assert(typeof user.failed_logins === "number", errorMsg);
        database.assert(user.failed_logins >= 0, errorMsg);
        database.assert(typeof user.read_tacos === "boolean", errorMsg);

        if (user.password !== null) {
            database.assert(typeof user.password === "string", errorMsg);
            database.assert(user.password.length > 0, errorMsg);
            database.assert(Password.isCiphertext(user.password), errorMsg);
        }

        if (user.locked_until !== null) {
            database.assert(user.locked_until instanceof Date, errorMsg);
        }
    }

    private static collection(db: database.Db) {
        return db.collection("user");
    }

    _id: database.ObjectID;
    version: string;
    roles: User.Role[];
    tokens: string[];
    username: string;
    password: null | string;
    failed_logins: number;
    locked_until: null | Date;
    read_tacos: boolean;

    private constructor(options: User.FactoryOptions) {
        this.update({
            _id: options._id || new database.ObjectID(),
            version: options.version || database.version(),
            roles: options.roles || [],
            tokens: options.tokens || [],
            username: options.username,
            password: options.password || null,
            failed_logins: options.failed_logins || 0,
            locked_until: options.locked_until || null,
            read_tacos: options.read_tacos || false,
        });
    }

    update(user: Partial<IUser>) {
        updateFrom("_id", this, user);
        updateFrom("version", this, user);
        updateFrom("roles", this, user);
        updateFrom("tokens", this, user);
        updateFrom("username", this, user);
        updateFrom("password", this, user);
        updateFrom("failed_logins", this, user);
        updateFrom("locked_until", this, user);
        updateFrom("read_tacos", this, user);
    }

    // Sets the entire user record into the database
    // The modified object is returned
    async save(db: database.Db) {

        // Ensure that the password is encrypted
        if (this.password) {
            this.password = await Password.encrypt(this.password);
        }

        User.validate(this);

        // Write user into database
        const result = await User.collection(db).findOneAndReplace(
            { "_id": this._id, "version": this.version },
            { ...(this as IUser), "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.update(result.value);
        return this;
    }

    // Detemine whether the account is currently locked
    // Will only return a lock date if it is in the future
    accountLockedUntil() {
        if (!this.locked_until) { return undefined; }
        if (this.locked_until.getTime() <= Date.current().getTime()) { return; }
        return this.locked_until;
    }

    // Reset the user account lock
    resetAccountLock() {
        this.locked_until = null;
        this.failed_logins = 0;
    }

    // Increment the number of failed login attempts
    // Sets the account lockout time if the number of attempts exceeds the configured max
    incrementFailedLogins() {
        this.failed_logins += 1;

        // If we have failed to login too many times lock the account
        if (this.failed_logins > auth.failed_logins_max) {
            this.locked_until = moment().add({ minutes: auth.account_lockout_minutes }).toDate();
        }
    }

    // Issue an authorizatio token for the user
    async issueToken(options: User.IssueTokenOptions) {
        const token_id = options.id || database.version();
        const expiration = options.expires != null ? JWT.timestamp(options.expires) : undefined;

        const token = await JWT.sign(session.secret, {
            jti: token_id,
            iss: session.issuer,
            sub: this.username,
            aud: "*",
            exp: expiration,
            once: options.once,
        });

        this.tokens.push(token_id);
        return token;
    }

    // Revoke an authorization token for this user
    async revokeToken(token: string) {
        _.pull(this.tokens, token);
    }

    // Determine whether the user object has one of the desired roles
    hasRole(...roles: User.Role[]) {
        if (roles.length === 0) {
            return true;
        } else {
            return !!_.intersection(this.roles, roles).length;
        }
    }

}

export namespace User {
    export const Role = StringMap("caregiver", "pac_admin", "sv_admin", "sv_super");
    export type Role = keyof typeof Role;

    export type FactoryOptions = Pick<IUser, "username"> & Partial<IUser>;
    export interface SearchOptions {
        limit: number;
        username: string;
    }

    export interface IssueTokenOptions {
        expires?: number;
        id?: string;
        once?: boolean;
    }
}
