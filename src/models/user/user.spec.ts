import "mocha";
import * as moment from "moment";
import * as config from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import {expect, sinon} from "../../testing";
import {Password, User} from "../user";


describe("User", async () => {
    let db: database.Db;
    let existing_user: User;
    let sandbox: sinon.SinonSandbox;
    const existing_username = "test@example.com";
    const existing_password = "password";
    const existing_token = "E0D28589-C411-4445-AB0C-43D6CEFA1395";

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        sandbox = sinon.sandbox.create();

        existing_user = User.factory({
            roles: [],
            tokens: [existing_token],
            username: existing_username,
            password: await Password.encrypt(existing_password),
        });

        await database.setupCollections(db);
        await database.fixture(db, { user: [existing_user] });
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });


    it("should salt and hash password", async () => {
        const salt = "salt";
        const password = "password1234";
        const ciphertext = "ciphertext";

        const genSalt = sandbox.stub(Password, "generateSalt").returns(salt);
        const hashPass = sandbox.stub(Password, "hash").returns(ciphertext);

        const encrypted = await Password.encrypt(password);
        expect(encrypted).to.equal(ciphertext);
        expect(genSalt).to.have.callCount(1);
        expect(hashPass).to.have.been.calledWith(password, salt);
    });

    it("should not return plaintext password", async () => {
        const password = "password1234";
        const encrypted = await Password.encrypt(password);
        expect(password).to.not.equal(encrypted);
    });

    it("should not re-hash stored password", async () => {
        const ciphertext = await Password.encrypt("password1234");
        const ciphertext2 = await Password.encrypt(ciphertext);
        expect(ciphertext).to.equal(ciphertext2);
    });

    it("should compare plaintext to ciphertext", async () => {
        const plaintext = "password1234";
        const ciphertext = await Password.encrypt(plaintext);

        expect(await Password.compare(plaintext, ciphertext)).to.equal(true);
        expect(await Password.compare("notmypassword", ciphertext)).to.equal(false);
    });

    it("should create user without password", async () => {
        const user_id = new database.ObjectID();
        const username = "mmouse@example.com";
        await User.create(db, { _id: user_id, username });

        const user = await db.collection("user").findOne({ _id: user_id });
        expect(user).to.deep.equal({
            _id: user._id,
            version: user.version,
            roles: [],
            tokens: [],
            username,
            password: null,
            failed_logins: 0,
            locked_until: null,
            read_tacos: false,
        });
    });

    it("should encrypt password on create", async () => {
        const user_id = new database.ObjectID();
        const username = "mmouse@example.com";
        const ciphertext = await Password.encrypt("password");
        const encrypt = sandbox.stub(Password, "encrypt").returns(ciphertext);

        // Create a user with credentials
        await User.create(db, { _id: user_id, username, password: "password" });
        expect(encrypt.callCount).to.equal(1);
        expect(encrypt.calledWith("password"));

        const user = await db.collection("user").findOne({ _id: user_id });
        expect(user).to.deep.equal({
            _id: user_id,
            version: user.version,
            roles: [],
            tokens: [],
            username,
            password: ciphertext,
            failed_logins: 0,
            locked_until: null,
            read_tacos: false,
        });
    });

    it("should encrypt password on update", async () => {
        const ciphertext = await Password.encrypt("password");
        const encrypt = sandbox.stub(Password, "encrypt").returns(ciphertext);

        existing_user.update({ ...existing_user, password: "password" });
        await existing_user.save(db);

        expect(encrypt).to.have.callCount(1);
        expect(encrypt).to.have.been.calledWith("password");

        const user = await db.collection("user").findOne({ _id: existing_user._id });
        expect(user).to.deep.equal({
            _id: existing_user._id,
            version: user.version,
            tokens: [existing_token],
            roles: [],
            username: existing_username,
            password: ciphertext,
            failed_logins: 0,
            locked_until: null,
            read_tacos: false,
        });
    });

    it("should not create user with duplicate username", async () => {
        const user_id = new database.ObjectID();

        try {
            await User.create(db, { _id: user_id, username: existing_username });
            expect.fail("Should not create new user with reserved username");
        } catch (e) {
            expect(e).to.be.instanceof(database.MongoError);
            expect(e.message).to.match(/duplicate key error/);

            const result = await db.collection("user").findOne({ _id: user_id });
            expect(result).to.equal(null);
        }
    });

    it("should login user with matching username and password", async () => {
        const user = await User.login(db, existing_username, existing_password);
        expect(user).to.deep.equal(existing_user);
    });

    it("should login user with matching username and token", async () => {
        const user = await User.login(db, existing_username, existing_token);
        expect(user).to.deep.equal(existing_user);
    });

    it("should not login user with incorrect username or password", async () => {
        try {
            await User.login(db, "incorrect", existing_password);
            expect.fail("Should not login user");
        } catch (e) {
            expect(e).to.be.instanceof(errors.NoSuchUser);
            expect(e.message).to.match(/NoSuchUser/);
        }

        try {
            const result = await User.login(db, existing_username, "incorrect");
            expect.fail(`Unexpected result: ${result}`);
        } catch (e) {
            expect(e).to.be.instanceof(errors.WrongPassword);
            expect(e.message).to.match(/WrongPassword/);
        }
    });

    it("should not login user without password", async () => {
        try {
            const password: null | string = null;
            existing_user.update({ ...existing_user, password: null });
            await existing_user.save(db);
            await User.login(db, existing_username, password!);
        } catch (e) {
            expect(e).to.be.instanceof(errors.WrongPassword);
            expect(e.message).to.match(/WrongPassword/);
        }

        try {
            existing_user.update({ ...existing_user, password: "" });
            await existing_user.save(db);
            await User.login(db, existing_username, "");
        } catch (e) {
            expect(e).to.be.instanceof(errors.InvalidDocument);
            expect(e.message).to.match(/InvalidUser/);
        }
    });

    it("should check for locked account on login", async () => {
        const accountLockedUntil = sandbox.stub(User.prototype, "accountLockedUntil");
        await User.login(db, existing_username, existing_password);
        expect(accountLockedUntil).to.have.callCount(1);
    });

    it("should not login user with locked account", async () => {
        const futureDate = moment().add({ minutes: 10 }).toDate();
        const accountLockedUntil = sandbox.stub(User.prototype, "accountLockedUntil").returns(futureDate);

        try {
            await User.login(db, existing_username, existing_password);
            expect.fail("Should throw error");
        } catch (error) {
            expect(accountLockedUntil).to.have.callCount(1);
            expect(error).to.be.instanceof(errors.AccountLocked);
            expect(error.message).to.match(/AccountLocked/);
            expect(error.until).to.equal(futureDate);
        }
    });

    it("should call increment failed logins on failed login", async () => {
        const incrementFailedLogins = sandbox.stub(User.prototype, "incrementFailedLogins");

        try {
            await User.login(db, existing_username, "incorrect");
            expect.fail("should throw error");
        } catch (_) {
            expect(incrementFailedLogins).to.have.callCount(1);
        }

        const futureDate = moment().add({ minutes: 10 }).toDate();
        const accountLockedUntil = sandbox.stub(User.prototype, "accountLockedUntil").returns(futureDate);

        try {
            await User.login(db, existing_username, existing_password);
            expect.fail("should throw error");
        } catch (_) {
            expect(incrementFailedLogins).to.have.callCount(2);
            expect(accountLockedUntil).to.have.callCount(1);
        }
    });

    it("should track failed logins and lock account when exceeded", async () => {
        config.auth.failed_logins_max = 2;

        try {
            await User.login(db, existing_username, "incorrect");
            expect.fail("should throw error");
        } catch (_) {
            const user = await db.collection("user").findOne({ _id: existing_user._id });
            expect(user.failed_logins).to.equal(1);
            expect(user.locked_until).to.equal(null);
        }

        try {
            await User.login(db, existing_username, "incorrect");
            expect.fail("should throw error");
        } catch (_) {
            const user = await db.collection("user").findOne({ _id: existing_user._id });
            expect(user.failed_logins).to.equal(2);
            expect(user.locked_until).to.equal(null);
        }

        try {
            await User.login(db, existing_username, "incorrect");
            expect.fail("should throw error");
        } catch (_) {
            const user = await db.collection("user").findOne({ _id: existing_user._id });
            expect(user.failed_logins).to.equal(3);
            expect(user.locked_until).to.not.equal(null);
        }
    });

    it("should unlock account after locktime has expired", async () => {
        // Create a user account that was previously locked
        const username = "locked@example.com";
        const password = "letmein";
        const locked_until = moment().subtract({ minutes: 1 }).toDate();
        const failed_logins = 2;
        const locked_user = await User.create(db, { username, password, locked_until, failed_logins });

        expect(locked_user.failed_logins).to.equal(failed_logins);
        expect(locked_user.locked_until).to.equal(locked_until);

        // Login as the user whose account was previously locked
        await User.login(db, username, password);
        const unlocked_user = await db.collection("user").findOne({ username });

        expect(unlocked_user.failed_logins).to.equal(0);
        expect(unlocked_user.locked_until).to.equal(null);
    });

    it("should test user roles", async () => {
        existing_user.roles = [];
        expect(existing_user.hasRole()).to.equal(true);
        expect(existing_user.hasRole("caregiver")).to.equal(false);
        expect(existing_user.hasRole("pac_admin")).to.equal(false);
        expect(existing_user.hasRole("sv_admin")).to.equal(false);
        expect(existing_user.hasRole("sv_super")).to.equal(false);

        existing_user.roles = ["caregiver", "pac_admin", "sv_admin", "sv_super"];
        expect(existing_user.hasRole()).to.equal(true);
        expect(existing_user.hasRole("caregiver")).to.equal(true);
        expect(existing_user.hasRole("pac_admin")).to.equal(true);
        expect(existing_user.hasRole("sv_admin")).to.equal(true);
        expect(existing_user.hasRole("sv_super")).to.equal(true);
    });

    it("should validate user");
});
