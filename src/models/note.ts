import { assert } from "../utils";


export class Note {
    static validate(note: Note) {
        assert.string(note.author, { min: 1 });
        assert.date(note.date);
        assert.string(note.content, { min: 1 });
    }

    static fromJSON(json: Note.JSON) {
        const note: Note = {
            author: json.author,
            content: json.content,
            date: new Date(json.date),
        };

        return note;
    }

    author: string;
    date: Date;
    content: string;
}

export namespace Note {
    export interface JSON {
        author: string;
        date: number;
        content: string;
    }
}
