import "mocha";
import * as config from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import {expect, sinon} from "../../testing";
import {Unsafe} from "../../utils";
import {Device, DeviceList, IDeviceList} from "../devicelist";


describe("DeviceListModel", async () => {
    let db: database.Db;
    let devicelist: DeviceList;
    let user_id: database.ObjectID;
    let sandbox: sinon.SinonSandbox;

    // Valid mobile device for testing
    const mobile: Device.Mobile = {
        _id: new database.ObjectID(),
        created: new Date(),
        type: "ios",
        version: { major: 0, minor: 0, patch: 0 },
        push_token: "0df980dfdf098df",
    };

    // Valid SMS device for testing
    const sms: Device.SMS = {
        _id: new database.ObjectID(),
        created: new Date(),
        type: "sms",
        phone_number: "+15556667777",
    };

    // Valid email device for testing
    const email: Device.Email = {
        _id: new database.ObjectID(),
        created: new Date(),
        type: "email",
        email_address: "mmouse@aol.com",
    };

    // Valid PERS device for testing
    const pers: Device.PERS = {
        _id: new database.ObjectID(),
        created: new Date(),
        type: "pers",
        identifier: "asdf098098sdf098asdf",
    };

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        sandbox = sinon.sandbox.create();

        user_id = new database.ObjectID();
        devicelist = DeviceList.factory({
            _id: new database.ObjectID(),
            user_id,
            version: database.version(),
            devices: [mobile, sms, email, pers],
        });

        await db.dropDatabase();
        await database.setupCollections(db);
        await database.fixture(db, { devicelist: [devicelist] });
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });


    it("should validate devicelist", async () => {

        // Execution needs to be deferred in order to test throwing of errors
        // This small wrapper returns a thunk that is ready to perform validation.
        function validate(override: Unsafe<IDeviceList>) {
            const newlist: IDeviceList = Object.assign({}, devicelist, override);
            return () => DeviceList.validate(newlist);
        }

        expect(validate({ devices: [] })).to.not.throw();
        expect(validate({ devices: [mobile, sms, email, pers] })).to.not.throw();

        // Validate device list _id

        expect(validate({ _id: null })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);
        expect(validate({ _id: undefined })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);
        expect(validate({ _id: 100 })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);

        // validate device list

        expect(validate({ devices: null })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);
        expect(validate({ devices: undefined })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);
        expect(validate({ devices: {} })).to.throw(errors.InvalidDocument, /InvalidDeviceList/);

    });

    it("should call validateDevice for every device in list", async () => {
        const validateDevice = sandbox.stub(DeviceList, "validateDevice");
        DeviceList.validate(devicelist);

        expect(validateDevice).to.have.callCount(devicelist.devices.length);
        expect(validateDevice).to.have.been.calledWith(mobile);
        expect(validateDevice).to.have.been.calledWith(sms);
        expect(validateDevice).to.have.been.calledWith(email);
        expect(validateDevice).to.have.been.calledWith(pers);
    });

    it("should validate devices", async () => {

        // Execution needs to be deffered in order to test throwing of errors
        // This type-safe wrapper returns a thunk that is ready to perform validation.
        function validate<T extends Device>(device: T, override?: Unsafe<T>) {
            const newdevice: any = Object.assign({}, device, override);
            return () => DeviceList.validateDevice(newdevice);
        }

        expect(validate(mobile)).to.not.throw();
        expect(validate(sms)).to.not.throw();
        expect(validate(email)).to.not.throw();
        expect(validate(pers)).to.not.throw();

        // validate what is common among devices

        expect(validate(mobile, { _id: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms   , { _id: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email , { _id: 100 })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(pers  , { type: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { type: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms   , { type: "invalid" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(email , { created: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers  , { created: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { created: "yesterday" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        // validate mobile devices
        expect(validate(mobile, { type: "sms" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { type: "email" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { type: "pers" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(mobile, { version: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { version: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { version: 100 })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { version: "" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(mobile, { push_token: null })).to.not.throw();
        expect(validate(mobile, { push_token: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { push_token: 321654987 })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(mobile, { push_token: "" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        // Validate sms devices

        expect(validate(sms, { type: "ios" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { type: "android" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { type: "email" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { type: "pers" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(sms, { phone_number: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { phone_number: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { phone_number: 55512345678 })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(sms, { phone_number: "" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        // validate email devices

        expect(validate(email, { type: "ios" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { type: "android" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { type: "sms" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { type: "pers" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(email, { email_address: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { email_address: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { email_address: {} })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(email, { email_address: "" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        // Validate pers devices

        expect(validate(pers, { type: "ios" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers, { type: "android" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers, { type: "sms" })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers, { type: "email" })).to.throw(errors.InvalidDocument, /InvalidDevice/);

        expect(validate(pers, { identifier: null })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers, { identifier: undefined })).to.throw(errors.InvalidDocument, /InvalidDevice/);
        expect(validate(pers, { identifier: 987654321 })).to.throw(errors.InvalidDocument, /InvalidDevice/);

    });

    it("should validate devices on save", async () => {
        const validate = sandbox.stub(DeviceList, "validate");
        await DeviceList.save(db, devicelist);

        expect(validate.callCount).to.equal(1);
        expect(validate).to.have.been.calledWith(devicelist);
    });

    it("should create empty devicelist", async () => {
        const user_id = new database.ObjectID();
        const expected = DeviceList.factory({ user_id });
        await DeviceList.create(db, expected);

        const actual = await db.collection("devicelist").findOne({ _id: expected._id });
        expect(actual).to.deep.equal(expected);
        expect(actual).to.shallow.equal({
            _id: expected._id,
            devices: [],
            user_id,
        });
    });

    it("should create non-empty devicelist", async () => {
        const user_id = new database.ObjectID();
        const mobile_copy = { ...mobile, _id: new database.ObjectID() };
        const sms_copy = { ...sms, _id: new database.ObjectID() };
        const email_copy = { ...email, _id: new database.ObjectID() };
        const pers_copy = { ...pers, _id: new database.ObjectID() };

        const expected = DeviceList.factory({
            user_id,
            devices: [
                mobile_copy,
                sms_copy,
                email_copy,
                pers_copy,
            ],
        });
        await DeviceList.create(db, expected);

        const actual = await db.collection("devicelist").findOne({ _id: expected._id });
        expect(actual).to.deep.equal(expected);
        expect(actual).to.shallow.equal({
            _id: expected._id,
            devices: [ mobile_copy, sms_copy, email_copy, pers_copy ],
            user_id,
        });
    });

    it("should fail to create when devicelist is invalid", async () => {
        const expected = DeviceList.factory({ user_id });
        const validate = sandbox.stub(DeviceList, "validate").throws();

        try {
            await DeviceList.save(db, expected);
            expect.fail("Should throw");
        } catch (e) { /* Intentionally Empty */ }

        const actual = await db.collection("devicelist").findOne({ _id: expected._id });
        expect(actual).to.equal(null);
        expect(validate).to.have.callCount(1);
    });

    it("should retrieve devicelist by user id", async () => {
        const actual = await DeviceList.get(db, user_id);
        expect(actual).to.deep.equal(devicelist);
    });

    it("should add a device", async () => {
        const newDevice: Device = {
            _id: new database.ObjectID(),
            type: "ios",
            created: new Date(),
            version: { major: 0, minor: 0, patch: 0 },
            push_token: "098sdf098asdfasdfasdf",
        };

        await devicelist.insert(db, newDevice);
        const newDevicelist = await db.collection("devicelist").findOne({ _id: devicelist._id });
        expect(newDevicelist).to.shallow.equal({
            ...devicelist,
            devices: [ mobile, sms, email, pers, newDevice ],
        });
    });

    it("should update a device", async () => {
        const mobile_copy = { ...mobile, push_token: "asdf09asdf098asdf09asf" };
        await devicelist.insert(db, mobile_copy);

        const actual = await db.collection("devicelist").findOne({ _id: devicelist._id });
        expect(actual).to.deep.equal({
            ...devicelist,
            devices: [ mobile_copy, sms, email, pers ],
        });
    });

    it("should remove a device", async () => {
        await devicelist.remove(db, mobile);

        const actual = await db.collection("devicelist").findOne({ _id: devicelist._id });
        expect(actual).to.deep.equal({
            ...devicelist,
            devices: [ sms, email, pers ],
        });
    });
});
