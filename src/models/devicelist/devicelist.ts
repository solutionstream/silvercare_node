import * as _ from "lodash";
import * as config from "../../config";
import * as database from "../../database";
import { Email } from "../../emails";
import * as errors from "../../errors";
import { IUser } from "../../models";
import { PushNotification } from "../../notifications";
import { PERS } from "../../pers";
import { SMS } from "../../sms";
import { Dictionary, exhaustiveCheck, jsonify, StringMap } from "../../utils";


export namespace Device {

    export const Type = StringMap("ios", "android", "sms", "email", "pers");
    export type Type = keyof typeof Type;

    // Base requirements for a device
    // Not exposed outside of this file
    interface Base {
        _id: database.ObjectID;
        type: Device.Type;
        created: Date;
    }

    // Device definition for mobile devices
    export interface Mobile extends Base {
        type: typeof Device.Type.ios | typeof Device.Type.android;
        version: { major: number, minor: number, patch: number };
        push_token: null|string;
    }

    // Device definition for SMS only devices
    export interface SMS extends Base {
        type: typeof Device.Type.sms;
        phone_number: string;
    }

    // Device definition for Email only devices
    export interface Email extends Base {
        type: typeof Device.Type.email;
        email_address: string;
    }

    // Device definition for PERS devices
    export interface PERS extends Base {
        type: typeof Device.Type.pers;
        identifier: string;
    }
}

// Define generic Device type as a union of specific device types
// Device can refer to any valid device type (only) making it great for heterogenous collection.
export type Device = Device.Mobile | Device.SMS | Device.Email | Device.PERS;

// A devicelist simply holds a collection of devices
export interface IDeviceList {
    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    devices: Device[];
}


export class DeviceList implements IDeviceList {

    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndexes([
            { key: { "user_id": 1 }, unique: true },
            { key: { "devices._id": 1 }, unique: true, sparse: true },
        ]);
    }

    // Create a new devicelist from a partial object
    // Applies default values where possible.
    static factory(options: DeviceList.FactoryOptions): DeviceList {
        return new DeviceList(options);
    }

    // Create one devicelist from multiple
    static merge(devicelists: DeviceList[]): DeviceList {
        const devicelist = DeviceList.factory({
            user_id: undefined!, // Prevent from being written to DB
            devices: _.flatMap(devicelists, (list) => list.devices),
        });

        return devicelist;
    }

    // Get a devicelist with a given id
    static async get(db: database.Db, user: IUser|database.ObjectID) {
        const user_id = database.toID(user);

        // Get an existing devicelist
        const devicelist = await this.fetch(db, { user_id });
        if (devicelist) { return devicelist; }

        // Create a new devicelist
        return await this.create(db, { user_id });
    }

    // Get all devicelists matching an id
    static async getAll(db: database.Db, users: Array<IUser|database.ObjectID>) {
        const ids = users.map(database.toID);
        return this.fetchMany(db, { "user_id": { "$in": ids }});
    }

    // Retrieve the device list by its id
    static async fetch(db: database.Db, query: object) {
        const devicelist = await this.collection(db).findOne(query);
        if (!devicelist) { return undefined; }
        return this.factory(devicelist);
    }

    static async fetchMany(db: database.Db, query: object) {
        const documents = await this.collection(db).find(query).toArray();
        const devicelists = documents.map((doc) => this.factory(doc));
        return devicelists;
    }

    // Create a devicelist in the database
    static async create(db: database.Db, devicelist: DeviceList.FactoryOptions) {
        const document = this.factory(devicelist);
        const result = await this.collection(db).insert(document);
        database.assert(result.result.ok === 1, new errors.InsertFailed());
        return this.factory(document);
    }

    // Insert or replace the document into the collection
    static async save(db: database.Db, devicelist: IDeviceList) {
        const document = { ...devicelist };
        this.validate(document);

        // Replace the entire document when we make a change
        const result = await this.collection(db).findOneAndUpdate(
            { "_id": devicelist._id, "version": devicelist.version },
            { ...devicelist, "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());
        return this.factory(result.value);
    }

    // Type guard: determine whether the device is a mobile device
    static isMobile(device: Device): device is Device.Mobile {
        return device.type === Device.Type.ios || device.type === Device.Type.android;
    }

    // Type guard: determine whether the device is an sms device
    static isSMS(device: Device): device is Device.SMS {
        return device.type === Device.Type.sms;
    }

    // Type guard: determine whether the device is an email device
    static isEmail(device: Device): device is Device.Email {
        return device.type === Device.Type.email;
    }

    // Type guard: determine whether the device is a PERS device
    static isPERS(device: Device): device is Device.PERS {
        return device.type === Device.Type.pers;
    }

    // Validate a device list
    static validate(object: IDeviceList) {
        const errorMsg = "InvalidDeviceList";

        database.assert(!!object, errorMsg);
        database.assert(object._id instanceof database.ObjectID, errorMsg);
        database.assert(typeof object.version === "string", errorMsg);
        database.assert(object.version.length > 0, errorMsg);
        database.assert(Array.isArray(object.devices), errorMsg);

        // Validate each of the subdocuments
        object.devices.forEach(this.validateDevice);
    }

    // Validate an individual device
    static validateDevice(object: Device) {
        const errorMsg = "InvalidDevice";

        database.assert(object._id instanceof database.ObjectID, errorMsg);
        database.assert(Object.keys(Device.Type).indexOf(object.type) >= 0, errorMsg);
        database.assert(object.created instanceof Date, errorMsg);

        switch (object.type) {
        case "ios":
        case "android":
            database.assert(object.version != null, errorMsg);
            database.assert(typeof object.version === "object", errorMsg);
            database.assert(typeof object.version.major === "number", errorMsg);
            database.assert(object.version.major >= 0, errorMsg);
            database.assert(typeof object.version.minor === "number", errorMsg);
            database.assert(object.version.minor >= 0, errorMsg);
            database.assert(typeof object.version.patch === "number", errorMsg);
            database.assert(object.version.patch >= 0, errorMsg);

            if (object.push_token !== null) {
                database.assert(typeof object.push_token === "string", errorMsg);
                database.assert(object.push_token.length > 0, errorMsg);
            }
            break;

        case "sms":
            database.assert(typeof object.phone_number === "string", errorMsg);
            database.assert(object.phone_number.length > 0, errorMsg);
            break;

        case "email":
            database.assert(typeof object.email_address === "string", errorMsg);
            database.assert(object.email_address.length > 0, errorMsg);
            break;

        case "pers":
            database.assert(typeof object.identifier === "string", errorMsg);
            database.assert(object.identifier.length > 0, errorMsg);
            break;

        default:
            return exhaustiveCheck(object);
        }
    }

    private static collection(db: database.Db) {
        return db.collection("devicelist");
    }

    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    devices: Device[];

    private constructor(devicelist: DeviceList.FactoryOptions) {
        this.updateFromDB({
            _id: devicelist._id || new database.ObjectID(),
            user_id: devicelist.user_id,
            version: devicelist.version || database.version(),
            devices: devicelist.devices || [],
        });
    }

    // Update the current value from database
    updateFromDB(devicelist: IDeviceList) {
        this._id = devicelist._id;
        this.user_id = devicelist.user_id;
        this.version = devicelist.version;
        this.devices = devicelist.devices;
    }

    // Determine whether the devicelist contains a particular device (by id)
    contains(device: Device|database.ObjectID) {
        return this.indexOf(device) >= 0;
    }

    // Refresh the data from the database
    async refresh(db: database.Db) {
        const devicelist = await DeviceList.fetch(db, { _id: this._id });
        this.updateFromDB(devicelist!);
    }

    // Insert a device into the devicelist
    // Will replace existing device if _id matches
    async insert(db: database.Db, device: Device) {
        const index = this.indexOf(device);

        if (index >= 0) {
            this.devices[index] = device;
        } else {
            this.devices.push(device);
        }

        const devicelist = await DeviceList.save(db, this);
        this.updateFromDB(devicelist);
        return this;
    }

    // Remove a device from the devicelist if it exists
    async remove(db: database.Db, device: Device|database.ObjectID) {
        const index = this.indexOf(device);
        if (index === -1) { return this; }

        this.devices.splice(index, 1);
        const devicelist = await DeviceList.save(db, this);
        this.updateFromDB(devicelist);
        return this;
    }

    // Send a push notification to all mobile devices in this list
    async push(options: DeviceList.PushOptions) {
        const push_tokens: string[] = [];

        // Collect push tokens
        for (const device of this.devices) {
            if (!DeviceList.isMobile(device)) { continue; }
            if (!PushNotification.validToken(device.push_token)) { continue; }
            push_tokens.push(device.push_token);
        }

        // Do nothing if there are no tokens
        if (!push_tokens.length) { return; }

        // Send push notifications
        await PushNotification.send({
            tokens: push_tokens,
            title: options.title,
            body: options.body,
            data: jsonify(options.data || {}),
        });
    }

    // Send SMS to recipients
    async sms(options: DeviceList.SMSOptions) {
        const phone_numbers: string[] = [];

        // Gather phone numbers
        for (const device of this.devices) {
            if (!DeviceList.isSMS(device)) { continue; }
            phone_numbers.push(device.phone_number);
        }

        // Do nothing if there are no phone numbers
        if (!phone_numbers.length) { return; }

        // Send SMS to recipients
        await SMS.send({
            to: phone_numbers,
            message: options.message,
        });
    }

    // Send email to recipients
    async email(options: DeviceList.EmailOptions) {
        const email_addresses: string[] = [];

        // Gather email addresses
        for (const device of this.devices) {
            if (!DeviceList.isEmail(device)) { continue; }
            email_addresses.push(device.email_address);
        }

        // Do nothing if there are no email addresses
        if (!email_addresses.length) { return; }

        // Send email to all recipients
        await Email.send({
            from: config.email.from,
            bcc: email_addresses,
            content: options.message,
        });
    }

    async pers(options: DeviceList.PERSOptions) {
        const { message } = options;
        const imeis: string[] = [];

        // Gather IMEI numbers
        for (const device of this.devices) {
            if (!DeviceList.isPERS(device)) { continue; }
            imeis.push(device.identifier);
        }

        // Do nothing if there are no pers
        if (!imeis.length) { return; }

        // Send the alert to recepients
        const promises = imeis.map(
            (imei) => PERS.alertevent({ imei, message }));
        await Promise.all(promises);
    }

    // Get the index of device in the devices list
    protected indexOf(device: Device|database.ObjectID) {
        const device_id = database.toID(device);
        return _.findIndex(this.devices, (d) => device_id.equals(d._id));
    }
}

export namespace DeviceList {
    export type FactoryOptions = Pick<IDeviceList, "user_id"> & Partial<IDeviceList>;

    export interface PushOptions {
        title: string;
        body: string;
        data?: Dictionary<any>;
    }

    export interface SMSOptions {
        message: SMS.Message;
    }

    export interface EmailOptions {
        message: Email.Content;
    }

    export interface PERSOptions {
        message: PERS.Message;
    }
}
