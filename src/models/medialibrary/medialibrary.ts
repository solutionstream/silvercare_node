import * as _ from "lodash";
import * as config from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import { Bucket } from "../../media";
import { assert, defaultTo, Omit, Require, StringMap } from "../../utils";


export namespace Media {

    export const Storage = StringMap("s3");
    export type Storage = keyof typeof Storage;

    export class File {

        static matches_tag(file: File, tags: string|string[]) {
            const tags_r = Array.isArray(tags) ? tags : [tags];
            return _.intersection(file.tags, tags_r).length > 0;
        }

        static matches_extension(file: File, extensions: string|string[]) {
            const extensions_r = Array.isArray(extensions) ? extensions : [extensions];
            const extension = this.extension(file.filename);
            return _.includes(extensions_r, extension);
        }

        // Correctly get the extension of a file
        static extension(filename: string) {
            const [basename, ...extensions] = filename.split(".");

            // Handle filenames that start with "."
            if (!basename) {
                extensions.shift();
            }

            // Join all remaining extensions
            const extension = extensions.join(".");
            return extension ? "." + extension : "";
        }

        static validate(object: File) {
            assert.ok(object);
            assert.instanceOf(object._id, database.ObjectID);

            // Validate file path attributes
            assert.string(object.basedir, { pattern: File.basedirRE });
            assert.string(object.filename, { pattern: File.filenameRE });
            assert.equals(object.filepath, `${object.basedir}/${object.filename}`);

            assert.string(object.original_filename, { pattern: File.filenameRE });
            assert.string(object.mimetype, { pattern: File.mimetypeRE });
            assert.number(object.size, { min: 0 });
            assert.oneOf(object.storage, Object.keys(Storage));
            assert.date(object.created);
            assert.date(object.updated);
            assert.array(object.tags, assert.string);
        }

        static factory(user_id: database.ObjectID, object: File.FactoryOptions) {
            const now = Date.current();
            const file_id = object._id || database.id();
            const extension = this.extension(object.original_filename);
            const filename = `${file_id}${extension}`;
            const basedir = `medialibrary/${user_id}`;

            const file: File = {
                _id: file_id,
                basedir,
                filename,
                filepath: `${basedir}/${filename}`,
                original_filename: object.original_filename,
                mimetype: object.mimetype,
                size: object.size,
                storage: object.storage || "s3",
                created: object.created || now,
                updated: object.updated || now,
                tags: object.tags || [],
            };

            return file;
        }

        _id: database.ObjectID;
        basedir: string;
        filename: string;
        filepath: string;
        original_filename: string;
        size: number;
        mimetype: string;
        storage: Media.Storage;
        created: Date;
        updated: Date;
        tags: string[];
    }

    export namespace File {
        export const basedirRE = /^medialibrary\/[^/]+$/;
        export const filenameRE = /^[^\/]+$/;
        export const extensionRE = /^($|(\.[^\.]+)+)$/;
        export const mimetypeRE = /^[^\/]+\/[^\/]+$/;

        export type FileLite = Omit<File, "filepath" | "filename" | "basedir">;
        export type FactoryOptions = Require<FileLite, "original_filename" | "mimetype" | "size">;
    }

    // Enough information to reference a file in a media library
    export interface IReference {
        user: database.ObjectID;
        file: database.ObjectID;
    }

    // JSON version of a media reference
    // Used by the API to give and receive reference data
    export type ReferenceJSON = Record<keyof IReference, string>;

    // Media reference object
    export class Reference implements IReference {

        // Validate a reference
        static validate(object: IReference) {
            assert.ok(object != null);
            assert.instanceOf(object.user, database.ObjectID);
            assert.instanceOf(object.file, database.ObjectID);
        }

        // Create a media reference
        static factory(user_id: database.ObjectID, file: database.ObjectID|File) {
            return new Media.Reference(user_id, database.toID(file));
        }

        // Create a reference object from reference JSON
        static fromJSON(json: ReferenceJSON): Reference {
            const user = database.parseObjectID(json.user);
            const file = database.parseObjectID(json.file);

            assert.instanceOf(user, database.ObjectID, "InvalidMediaReference");
            assert.instanceOf(file, database.ObjectID, "InvalidMediaReference");
            return new Reference(user!, file!);
        }

        constructor(
            public readonly user: database.ObjectID,
            public readonly file: database.ObjectID,
        ) {}

        // Create a JSON representation of a reference
        toJSON(): ReferenceJSON {
            return {
                user: this.user.toHexString(),
                file: this.file.toHexString(),
            };
        }
    }


}

export interface IMediaLibrary {
    _id: database.ObjectID;
    version: string;
    user_id: database.ObjectID;
    files: Media.File[];
    pending: Media.File[];
}


export class MediaLibrary implements IMediaLibrary {

    static async setupCollection(_db: database.Db) {
        // Intentionally empty
    }

    static collection(db: database.Db) {
        return db.collection("medialibrary");
    }

    static factory(options: MediaLibrary.FactoryOptions) {
        return new MediaLibrary(options);
    }

    static validate(object: IMediaLibrary) {
        assert.ok(object);
        assert.instanceOf(object._id, database.ObjectID);
        assert.string(object.version, { min: 1 });
        assert.array(object.files, Media.File.validate);
        assert.array(object.pending, Media.File.validate);
    }

    // Insert medialibrary
    static async insert(db: database.Db, document: IMediaLibrary) {
        const medialibrary = this.factory(document);
        this.validate(medialibrary);

        const result = await this.collection(db).insertOne(medialibrary);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return medialibrary;
    }

    // Create a new media list in the database
    static async create(db: database.Db, options: MediaLibrary.FactoryOptions) {
        const medialibrary = this.factory(options);
        return this.insert(db, medialibrary);
    }

    // Get a medialibrary
    static async fetch(db: database.Db, query: MediaLibrary.FetchOptions) {
        const document = await this.collection(db).findOne(query);
        if (!document) { return undefined; }

        const medialibrary = this.factory(document);
        return medialibrary;
    }

    _id: database.ObjectID;
    version: string;
    user_id: database.ObjectID;
    files: Media.File[];
    pending: Media.File[];

    // Initialize from MediaLibrary object
    private constructor(options: MediaLibrary.FactoryOptions) {
        this.update({
            _id: options._id || new database.ObjectID(),
            version: options.version || database.version(),
            user_id: options.user_id,
            files: options.files || [],
            pending: options.pending || [],
        });
    }

    // Write document back to database
    async save(db: database.Db) {
        MediaLibrary.validate(this);

        // Write medialibrary to database
        const result = await MediaLibrary.collection(db).findOneAndReplace(
            { "_id": this._id, "version": this.version },
            { ...(this as MediaLibrary), "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.update(result.value);
        return this;
    }

    // Pull fresh data from DB
    async refresh(db: database.Db) {
        const medialibrary = await MediaLibrary.fetch(db, { _id: this._id });
        this.update(medialibrary!);
    }

    space_files() {
        return _.sum(_.filter(this.files, "size"));
    }

    space_pending() {
        return _.sum(_.filter(this.pending, "size"));
    }

    space_total() {
        return this.space_files() + this.space_pending();
    }

    check_quotas(size: number) {
        // Enforce per file quotas (if enabled)
        if (config.media.per_file_size) {
            if (size > config.media.per_file_size) {
                return false;
            }
        }

        // Enforce per user quotas (if enabled)
        if (config.media.per_user_size) {
            if ((this.space_total() + size) > config.media.per_user_size) {
                return false;
            }
        }

        return true;
    }

    pending_index(file: database.ObjectID|Media.File) {
        const file_id = database.toID(file);
        const index = _.findIndex(this.pending, (file) => file._id.equals(file_id));
        return index >= 0 ? index : undefined;
    }

    // Add a pending file to the library
    add_pending(file: Media.File) {
        const index = this.pending_index(file);
        if (index == null) {
            this.pending.push(file);
        } else {
            this.pending[index] = file;
        }
    }

    // Remove pending file from library
    remove_pending(file: database.ObjectID|Media.File) {
        const index = this.pending_index(file);
        if (index == null) { return undefined; }
        const [removed] = this.pending.splice(index, 1);
        return removed;
    }

    file_index(file: database.ObjectID|Media.File) {
        const file_id = database.toID(file);
        const index = _.findIndex(this.files, (file) => file._id.equals(file_id));
        return index >= 0 ? index : undefined;
    }

    file(file: database.ObjectID|Media.File) {
        const index = this.file_index(file);
        if (index == null) { return undefined; }
        return this.files[index];
    }

    // Add a confirmed file
    add_file(file: Media.File) {
        const index = this.file_index(file);
        if (index == null) {
            this.files.push(file);
        } else {
            this.files[index] = file;
        }
    }

    // Remove a confirmed file
    remove_file(file: database.ObjectID|Media.File) {
        const index = this.file_index(file);
        if (index == null) { return undefined; }
        const [removed] = this.files.splice(index, 1);
        return removed;
    }

    // Get the profile photo for this medialibrary
    profile_photo(): undefined|Media.File {
        return this.files.find(
            (file) => file.tags.indexOf("profile") >= 0);
    }

    // Get the file reference for the user's profile photo
    profile_reference(user_id: database.ObjectID): undefined|Media.Reference {
        const profile = this.profile_photo();
        if (!profile) { return undefined; }
        return Media.Reference.factory(user_id, profile);
    }

    // Verify that a media file matches the file in s3
    verify(file: Media.File, meta: Bucket.HeadReceipt) {
        return file.size === meta.ContentLength
            && file.mimetype === meta.ContentType;
    }

    // Update document values
    update(object: Partial<IMediaLibrary>) {
        this._id = defaultTo(object._id, this._id);
        this.version = defaultTo(object.version, this.version);
        this.user_id = defaultTo(object.user_id, this.user_id);
        this.files = defaultTo(object.files, this.files);
        this.pending = defaultTo(object.pending, this.pending);
    }
}

export namespace MediaLibrary {
    export type FactoryOptions = Require<IMediaLibrary, "user_id">;
    export type FetchOptions = Partial<Pick<IMediaLibrary, "user_id"|"_id">>;
}

