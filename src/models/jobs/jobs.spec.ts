import "mocha";
import * as moment from "moment";
import * as config from "../../config";
import * as database from "../../database";
import { IDateTrigger, IJob, IScheduledJob } from "../../jobs";
import { expect, sinon } from "../../testing";
import { Job } from "./";


describe("Jobs", async () => {
    let db: database.Db;
    let sandbox: sinon.SinonSandbox;
    let today: Date;
    let tomorrow: Date;
    let trigger: IDateTrigger;
    let tasklist_id: database.ObjectID;
    let patient_id: database.ObjectID;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        today = moment().toDate();
        tomorrow = moment(today).add({ d: 1 }).toDate();
        tasklist_id = database.id();
        patient_id = database.id();
        trigger = { type: "date", date: today };

        sandbox = sinon.sandbox.create();
        await db.dropDatabase();
        await database.setupCollections(db);
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });


    // Create a job instance
    async function createJob(job: IScheduledJob, options?: Partial<IJob>) {
        return await Job.create(db, {
            ...options,
            ...job,
        });
    }

    it("should create pending job", async () => {
        sandbox.stub(Date, "current").returns(today);

        const expected = await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });
        const actual = await db.collection("jobs").findOne({ _id: expected._id });

        expect(actual).to.deep.equal({
            _id: actual._id,
            type: "remind_due",
            status: "pending",
            trigger: { type: "date", date: today },
            tasklist_id,
            patient_id,
            created: today,
            updated: today,
            last_error: null,
            last_completed: null,
            estimated_completion: null,
            lockedBy: null,
            attempts: 0,
        });
    });

    it("should dequeue pending job", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });

        const updated = new Date();
        sandbox.stub(Date, "current").returns(updated);

        const jobs = await Job.dequeue(db, {
            type: "remind_due",
            date: today,
            limit: 10,
            attempts: 0,
        });

        expect(jobs.length).to.equal(1);
        expect(jobs[0]).to.shallow.equal({
            type: "remind_due",
            status: "processing",
            updated,
            estimated_completion: moment(updated).add({ s: 60 }).toDate(),
            lockedBy: jobs[0].lockedBy,
            attempts: 0,
        });
    });

    it("should dequeue pending job by type", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });
        await createJob({ type: "overdue_count", tasklist_id, patient_id, trigger, most_recent: null });

        const jobs = await Job.dequeue(db, {
            date: today,
            limit: 10,
            attempts: 0,
            type: "remind_due",
        });

        expect(jobs.length).to.equal(1);
        expect(jobs[0]).to.shallow.equal({
            type: "remind_due",
            status: "processing",
        });
    });

    it("should not dequeue job with too many attempts", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger }, { attempts: 2 });

        const jobs = await Job.dequeue(db, {
            date: today,
            limit: 10,
            attempts: 1,
            type: "remind_due",
        });

        expect(jobs.length).to.equal(0);
    });

    it("should mark job as completed", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });
        const jobs = await Job.dequeue(db, {
            date: today,
            limit: 1,
            attempts: 0,
            type: "remind_due",
        });

        const job = await Job.completed(db, jobs[0]);
        expect(jobs.length).to.equal(1);
        expect(job).to.shallow.equal({
            type: "remind_due",
            status: "completed",
            estimated_completion: null,
            lockedBy: null,
            attempts: 0,
        });
    });

    it("should mark job as failed", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });
        const jobs = await Job.dequeue(db, {
            date: today,
            limit: 1,
            attempts: 0,
            type: "remind_due",
        });

        const job = await Job.failed(db, jobs[0], new Error());
        expect(jobs.length).to.equal(1);
        expect(job).to.shallow.equal({
            type: "remind_due",
            status: "pending",
            estimated_completion: null,
            lockedBy: null,
            attempts: 1,
        });
    });

    it("should reset idle jobs", async () => {
        await createJob({ type: "remind_due", tasklist_id, patient_id, trigger });

        // Dequeue the job and mark it as taking 0 seconds to complete
        // This lets the message be over the idle threshold for our test
        const jobs = await Job.dequeue(db, {
            date: today,
            limit: 1,
            attempts: 0,
            timeout: 0,
            type: "remind_due",
        });

        expect(jobs.length).to.equal(1);
        expect(jobs[0]).to.shallow.equal({
            type: "remind_due",
            status: "processing",
        });

        // Reset the idle jobs
        await Job.resetIdle(db, Date.now());

        // Get the job from the database and check its data
        const job = await db.collection("jobs").findOne({ _id: jobs[0]._id });
        expect(job).to.shallow.equal({
            type: "remind_due",
            status: "pending",
            estimated_completion: null,
            lockedBy: null,
            attempts: 1,
        });
    });
});
