import * as _ from "lodash";
import * as moment from "moment";
import * as database from "../../database";
import * as errors from "../../errors";
import { IJob, IScheduledJob, ITrigger, ScheduledJob } from "../../jobs";
import { exhaustiveCheck } from "../../utils";


export class Job {

    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndexes([
            { key: { "type": 1 }, unique: false },
            { key: { "trigger.type": 1, "trigger.date": 1 } },
        ]);
    }

    // Create a new scheduled job object
    static factory(options: Job.FactoryOptions): ScheduledJob {
        const now = Date.current();
        const job: ScheduledJob = {
            ...options,
            _id: options._id || new database.ObjectID(),
            status: options.status || "pending",
            created: options.created || now,
            updated: options.updated || now,
            last_error: options.last_error || null,
            last_completed: options.last_completed || null,
            estimated_completion: options.estimated_completion || null,
            lockedBy: options.lockedBy || null,
            attempts: options.attempts || 0,
        };

        return job;
    }

    // Create a job in the database
    static async create(db: database.Db, options: Job.FactoryOptions) {
        const job = this.factory(options);
        return this.insert(db, job);
    }

    // Insert a new job into the database
    static async insert(db: database.Db, job: ScheduledJob) {
        this.validate(job);
        const result = await this.collection(db).insertOne(job);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return job;
    }

    // Search for jobs in the database
    static async dequeue(db: database.Db, options: Job.SearchOptions) {
        // Unique id to identify jobs affected by this operation
        const id = new database.ObjectID();

        // Filter to eligible items
        const query: any = {
            "status": IJob.Status.pending,
            "trigger.type": ITrigger.Type.date, // Only date currently supported
            "trigger.date": { "$lte": moment(options.date).toDate() },
        };

        // Limit query by number of failed attempts
        if (options.attempts != null) {
            query.attempts = { "$lte": options.attempts };
        }

        // Limit query by job type if specified
        if (options.type != null) {
            const type_array = Array.isArray(options.type) ? options.type : [options.type];
            query.type = { "$in": type_array };
        }

        // Get up to `limit` jobs whose scheduled time has passed
        const cursor = this.collection(db).find(query).project({ _id: 1 }).limit(options.limit);
        const matches: ScheduledJob[] = await cursor.toArray();

        // Every job should have an estimated completion date
        // This allows us to know when a job is stuck or abandoned
        const current_date = Date.current();
        const est_timeout = options.timeout != null ? options.timeout : 60;
        const estimated_completion = moment(current_date).add({ s: est_timeout }).toDate();

        await this.collection(db).updateMany(
            { "status": IJob.Status.pending, "_id": { "$in": _.map(matches, "_id") } },
            {
                "$set": {
                    // Change status to processing
                    "status": IJob.Status.processing,
                    // Identifier that is added on status change
                    // This lets us know whether it was us who changed the status
                    "lockedBy": id,
                    // All changes must update the current date
                    "updated": current_date,
                    // Each caller estimates how long the job should be "locked" for
                    // If a job is found in "processing" for longer than this it will be
                    // considered stuck or abandoned.
                    "estimated_completion": estimated_completion,
                },
            });

        // Fetch all currently processing jobs by the current id
        const documents = await this.collection(db).find(
            { "status": IJob.Status.processing, "lockedBy": id }).toArray();
        const jobs = documents.map((doc) => this.factory(doc));
        return jobs;
    }

    // Set any jobs in the processing state that are older than threshold back to pending
    static async resetIdle(db: database.Db, threshold: number|Date) {
        const threshold_date = moment(threshold).toDate();
        const current_date = Date.current();
        await this.collection(db).updateMany(
            { "status": IJob.Status.processing, "estimated_completion": { "$lte": threshold_date }},
            {
                "$inc": { "attempts": 1 },
                "$set": {
                    "status": IJob.Status.pending,
                    "lockedBy": null,
                    "estimated_completion": null,
                    "updated": current_date,
                    "last_error": null,
                },
             });
    }

    // Mark the job as completed
    static async completed(db: database.Db, job: ScheduledJob) {
        const current_date = Date.current();

        // Mark job as completed
        const updated_job = _.merge({}, job, {
            status: IJob.Status.completed,
            estimated_completion: null,
            lockedBy: null,
            updated: current_date,
            last_completed: current_date,
            last_error: null,
        });

        const result = await this.collection(db).findOneAndUpdate(
            { "_id": job._id, "lockedBy": job.lockedBy, "status": IJob.Status.processing },
            { ...updated_job },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());
        return this.factory(result.value);
    }

    // Complete the job but schedule it to run again
    // We do this for jobs that are recurring
    static async reschedule(db: database.Db, job: ScheduledJob, date: number|Date) {
        const current_date = Date.current();

        // Reschedule the job
        const updated_job = _.merge({}, job, {
            status: IJob.Status.pending,
            estimated_completion: null,
            lockedBy: null,
            updated: current_date,
            last_error: null,
            last_completed: current_date,
            trigger: { date: moment(date).toDate() },
        });

        // Update job fields in the database
        const result = await this.collection(db).findOneAndUpdate(
            { "_id": job._id, "lockedBy": job.lockedBy, "status": IJob.Status.processing },
            { ...updated_job },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());
        return this.factory(result.value);
    }

    // Fail the job for whatever reason
    static async failed(db: database.Db, job: ScheduledJob, error: Error) {
        const current_date = Date.current();
        const result = await this.collection(db).findOneAndUpdate(
            { "_id": job._id, "lockedBy": job.lockedBy, "status": IJob.Status.processing },
            {
                "$inc": { "attempts": 1 },
                "$set": {
                    "status": IJob.Status.pending,
                    "updated": current_date,
                    "lockedBy": null,
                    "estimated_completion": null,
                    "last_error": { message: error.message, stack: error.stack, name: error.name },
                },
            },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());
        return this.factory(result.value);
    }

    // Validate a scheduled job to ensure it is correct
    static validate(job: ScheduledJob) {
        database.assert(job._id instanceof database.ObjectID);
        database.assert(this.is_type(job.type));
        database.assert(this.is_status(job.status));
        database.assert(job.created instanceof Date);
        database.assert(job.updated instanceof Date);
        database.assert(typeof job.attempts === "number");
        database.assert(job.attempts >= 0);

        if (job.last_completed !== null) {
            database.assert(job.last_completed instanceof Date);
        }

        if (job.last_error !== null) {
            database.assert(typeof job.last_error.message === "string");
            database.assert(typeof job.last_error.name === "string");
            database.assert(typeof job.last_error.stack === "string");
        }

        if (job.estimated_completion !== null) {
            database.assert(job.estimated_completion instanceof Date);
        }

        if (job.lockedBy !== null) {
            database.assert(job.lockedBy instanceof database.ObjectID);
        }

        // Only date triggers are currently supported
        database.assert(job.trigger.type === "date");
        database.assert(job.trigger.date instanceof Date);

        // Job specific validation
        switch (job.type) {
            case "alert_overdue":
                database.assert(job.tasklist_id instanceof database.ObjectID);
                database.assert(job.patient_id instanceof database.ObjectID);
                database.assert(job.task_date instanceof Date);
                database.assert(typeof job.task_position === "number");
                database.assert(job.task_position > 0);
                break;

            case "remind_due":
                database.assert(job.tasklist_id instanceof database.ObjectID);
                database.assert(job.patient_id instanceof database.ObjectID);
                break;

            case "overdue_count":
                database.assert(job.tasklist_id instanceof database.ObjectID);
                database.assert(job.patient_id instanceof database.ObjectID);
                if (job.most_recent !== null) {
                    database.assert(job.most_recent instanceof Date);
                }
                break;

            case "alert_reassign":
                database.assert(job.tasklist_id instanceof database.ObjectID);
                database.assert(job.patient_id instanceof database.ObjectID);
                database.assert(Array.isArray(job.reasons));

                for (const reason of job.reasons) {
                    switch (reason.type) {
                    case "default":
                        if (reason.previous !== null) {
                            database.assert(reason.previous instanceof database.ObjectID);
                        }

                        if (reason.current !== null) {
                            database.assert(reason.current instanceof database.ObjectID);
                        }
                        break;

                    case "instance":
                        database.assert(reason.task_date instanceof Date);
                        database.assert(typeof reason.task_position === "number");
                        database.assert(reason.task_position > 0);

                        if (reason.previous !== null) {
                            database.assert(reason.previous instanceof database.ObjectID);
                        }

                        if (reason.current !== null) {
                            database.assert(reason.current instanceof database.ObjectID);
                        }
                        break;

                    default:
                        exhaustiveCheck(reason);
                    }
                }
                break;

            case "reset_password":
            case "invite_pac":
                database.assert(job.user_id instanceof database.ObjectID);
                break;

            default:
                exhaustiveCheck(job);
        }
    }

    // Determine whether a string is a valid status
    static is_status(status: string): status is IJob.Status {
        return _.includes(Object.keys(IJob.Status), status);
    }

    // Determine whether a string is a job type
    static is_type(type: string): type is IJob.Type {
        return _.includes(Object.keys(IJob.Type), type);
    }

    // Determine the collection we are operating on
    static collection(db: database.Db) {
        return db.collection("jobs");
    }

    protected constructor() {}
}

export namespace Job {
    export type FactoryOptions = IScheduledJob & Partial<IJob>;
    export interface SearchOptions {
        date: number|Date;
        type: IJob.Type | IJob.Type[];
        limit: number;
        attempts?: number;
        timeout?: number; // seconds
    }
}
