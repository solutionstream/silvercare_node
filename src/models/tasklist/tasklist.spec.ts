import "mocha";
import * as moment from "moment";
import { CareGiver } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Schedule } from "../../scheduling";
import { expect, sinon } from "../../testing";
import { ITask, ITaskShared, Task, TaskList} from "./";


describe("Tasklist", () => {
    let db: database.Db;
    let sandbox: sinon.SinonSandbox;
    let now: Date;
    let counter: number;
    let patient_id: database.ObjectID;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        now = moment().toDate();
        counter = 0;
        patient_id = new database.ObjectID();

        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);

        await database.setupCollections(db);
    });

    afterEach(async () => {
        sandbox.restore();
        await database.drop(db);
    });


    function createCaregiverRef() {
        return new CareGiver.Reference(database.id(), `[${counter++}] CareGiver`);
    }

    interface CreateTaskList extends Partial<ITaskShared>, Partial<ITask> {
        schedule: Schedule;
        patient_id?: database.ObjectID;
        medication_id?: database.ObjectID;
        overdue_count?: number;
    }

    // Create a tasklist object
    async function createTasklist(options: CreateTaskList) {
        return await TaskList.create(db, {
            patient_id: options.patient_id || patient_id,
            medication_id: options.medication_id,
            overdue_count: options.overdue_count,
            schedule: options.schedule,
            shared: {
                title: options.title || `[${counter++}] Title`,
                type: options.type || "other",
                description: options.description || `[${counter++}] Description`,
                notes: options.notes || [],
                attachments: options.attachments || [],
            },
            defaults: {
                caregiver: options.caregiver || null,
            },
        });
    }

    describe("assignee", () => {
        it("should apply default assignee", async () => {
            const schedule = Schedule.once(now);
            const caregiver = createCaregiverRef();
            const tasklist = await createTasklist({ schedule, caregiver });

            const [task] = tasklist.all();
            expect(task.caregiver).to.deep.equal(caregiver);
        });

        it("should apply instance assignee", async () => {
            const schedule = Schedule.daily({ start: now, until: now, instances: 2 });
            const tasklist = await createTasklist({ schedule });

            // Demonstrate that the caregiver is null
            const [first, second] = tasklist.all();
            expect(first.caregiver).to.equal(null);
            expect(second.caregiver).to.equal(null);

            // Set individual asignees on instances
            const caregiver1 = createCaregiverRef();
            const caregiver2 = createCaregiverRef();
            await tasklist.updateOne(db, first, { caregiver: caregiver1 });
            await tasklist.updateOne(db, second, { caregiver: caregiver2 });

            const [task1, task2] = tasklist.all();
            expect(task1.caregiver).to.deep.equal(caregiver1);
            expect(task2.caregiver).to.deep.equal(caregiver2);
        });

        it("should apply instance assignee over default assignee", async () => {
            const schedule = Schedule.daily({ start: now, until: now, instances: 2 });
            const tasklist = await createTasklist({ schedule });

            // Set caregiver specifically on second instance
            const [first] = tasklist.all();
            const caregiver1 = createCaregiverRef();
            await tasklist.updateOne(db, first, { caregiver: caregiver1 });

            // Set default caregiver for all tasks
            const caregiver2 = createCaregiverRef();
            await tasklist.updateAll(db, { caregiver: caregiver2 });

            // Default caregiver does not override instance caregiver
            const [task1, task2] = tasklist.all();
            expect(task1.caregiver).to.deep.equal(caregiver1);
            expect(task2.caregiver).to.deep.equal(caregiver2);
        });

        it("should remove only default asignee", async () => {
            let first: Task;
            let second: Task;
            let third: Task;
            let fourth: Task;

            // Create tasklist with infinite schedule
            const schedule = Schedule.daily({ start: now, instances: 2 });
            const caregiver1 = createCaregiverRef();
            const tasklist = await createTasklist({ schedule, caregiver: caregiver1 });

            // Create second schedule for a single event tomorrow
            const tomorrow = moment(now).add({ d: 1 }).toDate();
            const two_days = moment(now).add({ d: 2 }).toDate();
            const schedule_t = Schedule.daily({ start: tomorrow, until: two_days });
            await tasklist.updateSchedule(db, schedule_t);

            // Change the assignee on one of the two schedules
            [first, , third] = tasklist.all();
            const caregiver2 = createCaregiverRef();
            await tasklist.updateOne(db, first, { caregiver: caregiver2 });
            await tasklist.updateOne(db, third, { caregiver: caregiver2 });

            // Verify that caregiver assignments are as expected
            [first, second, third, fourth] = tasklist.all();
            expect(first.caregiver).to.deep.equal(caregiver2);
            expect(second.caregiver).to.deep.equal(caregiver1);
            expect(third.caregiver).to.deep.equal(caregiver2);
            expect(fourth.caregiver).to.deep.equal(caregiver1);

            // Unsassign default caregiver
            await tasklist.unassign(db, caregiver1);
            [first, second, third, fourth] = tasklist.all();
            expect(first.caregiver).to.deep.equal(caregiver2);
            expect(second.caregiver).to.equal(null);
            expect(third.caregiver).to.deep.equal(caregiver2);
            expect(fourth.caregiver).to.equal(null);
        });

        it("should remove only instance assignee", async () => {
            let first: Task, second: Task, third: Task, fourth: Task;

            // Create tasklist with infinite schedule
            const schedule = Schedule.daily({ start: now, instances: 2 });
            const caregiver1 = createCaregiverRef();
            const tasklist = await createTasklist({ schedule, caregiver: caregiver1 });

            // Create second schedule for a single event tomorrow
            const tomorrow = moment(now).add({ d: 1 }).toDate();
            const two_days = moment(now).add({ d: 2 }).toDate();
            const schedule_t = Schedule.daily({ start: tomorrow, until: two_days });
            await tasklist.updateSchedule(db, schedule_t);

            // Change the assignee on one of the two schedules
            [, second, , fourth] = tasklist.all();
            const caregiver2 = createCaregiverRef();
            await tasklist.updateOne(db, second, { caregiver: caregiver2 });
            await tasklist.updateOne(db, fourth, { caregiver: caregiver2 });

            // Verify that caregiver assignments are as expected
            [first, second, third, fourth] = tasklist.all();
            expect(first.caregiver).to.deep.equal(caregiver1);
            expect(second.caregiver).to.deep.equal(caregiver2);
            expect(third.caregiver).to.deep.equal(caregiver1);
            expect(fourth.caregiver).to.deep.equal(caregiver2);

            // Unsassign default caregiver
            await tasklist.unassign(db, caregiver2);
            [first, second, third, fourth] = tasklist.all();
            expect(first.caregiver).to.deep.equal(caregiver1);
            expect(second.caregiver).to.equal(null);
            expect(third.caregiver).to.deep.equal(caregiver1);
            expect(fourth.caregiver).to.equal(null);
        });
    });

    describe("scheduling", () => {
        it("should create new sequence when schedule change?");
        it("should never overlap schedules");
        it("should only have one exception object per task");
        it("should calculate due date for task"); // all schedules, including times_by_minutes
        it("should calculate due date for relative schedule"); // weekly, daily, &c
        it("should allow overlapping schedules?");
        it("should archive completed tasks"); // compact
        it("should determine whether a date falls within the tasklist range");
        it("should retrieve the schedule for a given date");
        it("should determine whether the tasklist contains a certain date");
        it("should fail to update schedule if new schedule overlaps current");
        it("should complete active schedule just before start of new one");
        it("should only create new sequence for schedule with date range");
    });

    describe("completion and overdue", () => {
        it("should track overdue count");
        it("should archive completed tasks");
        it("should schedule overdue counter on insert");
        it("should schedule overdue counter on create");
        it("should reset overdue count when task instance is completed");
        it("should not reset overdue count when completing already completed task");
    });

    describe("validation", () => {
        it("should validate tasklist");
        it("should validate tasklist on insert");
        it("should validate tasklist on create");
        it("should validate tasklist on find");
        it("should validate tasklist on fetch"); // one and all
    });

    describe("medication", () => {
        it("should create tasklist from medication");
        it("should not allow changing medication type");
        it("should update when medication change happens");
        it("should update medication when tasklist changes");
    });

    describe("update", () => {
        it("should not duplicate tasklists");
        it("should update data for every instance");
        it("should fail to save if db version is different");
        it("should update version on save");
        it("should update from DB");
    });

    describe("get tasks", () => {
        it("should get tasks in date range");
        it("should get all tasks in the tasklist"); // dangerous
        it("should get the next task in the tasklist (following date)");
        it("should get the first task in the tasklist");
        it("should get the task by task id");
        it("should get the task by actual date and position");
        it("should get the task by original date and position");
    });
});
