import * as status from "http-status";
import * as _ from "lodash";
import * as moment from "moment";
import * as config from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import { Agenda, IAlertReassignJob } from "../../jobs";
import { Event, EventSequence, Schedule } from "../../scheduling";
import { assert, StringMap } from "../../utils";
import { CareGiver } from "../caregiver";
import { Media } from "../medialibrary";
import { Note } from "../note";
import { Medication, Patient } from "../patient";


// These are shared among all tasks
export interface ITaskShared {
    title: string;
    type: Task.Type;
    attachments: Media.Reference[];
    description: null | string;
    notes: Note[];
}


// A tasklist is an event list, with a little extra special data
export interface ITaskList {
    // The id for the entire event list
    // All instances will refer back to this id
    _id: database.ObjectID;

    // The id for the patient owning this list
    patient_id: database.ObjectID;

    // The id for the associated medication
    // This is only set if there is a matching medication
    medication_id?: database.ObjectID;

    // Unique token for a database writer
    version: string;

    // The template used to create instances
    // This provides the default values for per-task fields
    // These values can be overwritten by a particular event list
    defaults: ITask;

    // The data shared across all task instances
    // This provides a global shared state across tasks
    shared: ITaskShared;

    // Array of all sequences in this list
    // Each sequence has a different schedule
    sequences: Array<Event.Schedule<ITask>>;

    // The number of tasks that are considered overdue
    // This number resets when a task is completed.
    overdue_count: number;
}


// The following fields can be changed per task:
export interface ITask {
    caregiver: null | CareGiver.IReference;
}


export namespace TaskList {
    export interface FactoryOptions {
        patient_id: database.ObjectID;
        medication_id?: database.ObjectID;
        _id?: database.ObjectID;
        version?: string;
        schedule: Schedule;
        defaults?: Partial<ITask>;
        shared: Pick<ITaskShared, "title"|"type"> & Partial<ITaskShared>;
        overdue_count?: number;
    }

    export interface UpdateOneOptions extends Partial<ITask> {
        cancelled?: boolean;
        completed?: boolean;
        date?: Date;
    }

    export interface UpdateAllOptions extends Partial<ITaskShared> {
        caregiver?: null | CareGiver.IReference;
    }
}


export class TaskList {

    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndexes([
            { key: { "patient_id": 1 }, unique: false },
            { key: { "medication_id": 1 }, unique: true, sparse: true },
        ]);
    }

    static fromMedication(patient: Patient|database.ObjectID, medication: Medication) {
        return this.factory({
            patient_id: database.toID(patient),
            medication_id: medication._id,
            schedule: medication.schedule,
            shared: {
                type: "medication",
                title: medication.name,
                description: medication.instructions,
                notes: medication.notes,
                attachments: medication.attachments,
            },
            defaults: {
                caregiver: null,
            },
        });
    }

    // Create a new task list
    // For convenience it provides default values where possible
    static factory(options: TaskList.FactoryOptions) {
        options.shared = options.shared || { title: null!, type: null! };
        options.defaults = options.defaults || {};

        // Apply defaults to shared values
        const shared: ITaskShared = {
            title: options.shared.title,
            type: options.shared.type,
            description: options.shared.description || null,
            notes: options.shared.notes || [],
            attachments: options.shared.attachments || [],
        };

        // Apply defaults to default values
        const defaults: ITask = {
            caregiver: options.defaults.caregiver || null,
        };

        // Create a new tasklist with a single schedule
        return new TaskList({
            _id: options._id || new database.ObjectID(),
            patient_id: options.patient_id,
            medication_id: options.medication_id,
            version: options.version || database.version(),
            overdue_count: options.overdue_count || 0,
            defaults,
            shared,
            sequences: [
                {
                    _id: new database.ObjectID(),
                    schedule: options.schedule,
                    exceptions: [],
                },
            ],
        });
    }

    static async insert(db: database.Db, tasklist: TaskList) {
        this.validate(tasklist);

        // Schedule an overdue counter for this task
        // This handles tracking the number of tasks that are overdue
        await Agenda.overdue_counter(db, {
            patient: tasklist.patient_id,
            tasklist: tasklist._id,
            date: Date.current(),
        });

        // Schedule reminders for this task
        // This handles sending notifications when the task is about to be due
        await Agenda.remind_due(db, {
            patient: tasklist.patient_id,
            tasklist: tasklist._id,
            date: Date.current(),
        });

        // Insert tasklist into database
        const result = await this.collection(db).insertOne(tasklist.toPOJO());
        database.assert(result.insertedCount === 1, new errors.InsertFailed());

        return tasklist;
    }

    // Insert a tasklist into the database
    static async create(db: database.Db, options: TaskList.FactoryOptions) {
        const tasklist = this.factory(options);
        return await this.insert(db, tasklist);
    }

    static async find(db: database.Db, query: any) {
        const document = await this.collection(db).findOne(query);
        if (!document) { return undefined; }

        const tasklist = new TaskList(document);
        return tasklist;
    }

    // Fetch taskslists for the patient
    static async fetch(db: database.Db, patient: Patient | database.ObjectID, tasklist?: TaskList | database.ObjectID) {
        if (tasklist) {
            const list = await this.fetchOne(db, patient, tasklist);
            if (!list) { return []; }
            return [list];
        } else {
            return await this.fetchAll(db, patient);
        }
    }

    // Get the tasklist from the database
    static async fetchOne(db: database.Db, patient: Patient|database.ObjectID, tasklist_id: TaskList|database.ObjectID) {
        return this.find(db, {
            _id: database.toID(tasklist_id),
            patient_id: database.toID(patient),
        });
    }

    // Get every tasklist from the database
    static async fetchAll(db: database.Db, patient: Patient|database.ObjectID) {
        const documents = await this.collection(db).find(
            { patient_id: database.toID(patient) }).toArray();

        const tasklists = documents.map((doc) => new TaskList(doc));
        return tasklists;
    }

    // Validate a tasklist
    static validate(task: TaskList) {
        assert.ok(task, "InvalidTask");
        assert.instanceOf(task._id, database.ObjectID, "InvalidID");
        assert.instanceOf(task.patient_id, database.ObjectID, "InvalidPatient");
        assert.string(task.version, { min: 1 }, "InvalidVersion");

        // Only medication tasks can have a medication id
        if (task.shared.type === "medication") {
            assert.instanceOf(task.medication_id, database.ObjectID, "MedicationRequired");
        } else {
            assert.ok(typeof task.medication_id === "undefined", "MedicationNotAllowed");
        }

        this.validateDefaults(task.defaults);
        this.validateShared(task.shared);
        this.validateSequences(task.sequences);
    }

    static validateDefaults(defaults: ITask) {
        assert.ok(defaults, "InvalidDefaults");

        if (defaults.caregiver !== null) {
            CareGiver.Reference.validate(defaults.caregiver);
        }
    }

    static validateShared(shared: ITaskShared) {
        assert.ok(shared, "InvalidShared");
        assert.string(shared.title, { min: 1 }, "InvalidTitle");
        assert.ok(Task.is_type(shared.type), "InvalidType");

        // If there is a description it cannot be empty
        if (shared.description !== null) {
            assert.string(shared.description, { min: 1 }, "InvalidDescription");
        }

        this.validateNotes(shared.notes);
        this.validateAttachments(shared.attachments);
    }

    static validateNotes(notes: Note[]) {
        assert.array(notes, Note.validate);
    }

    static validateAttachments(attachments: Media.Reference[]) {
        assert.array(attachments, Media.Reference.validate);
    }

    static validateSequences(sequences: Array<EventSequence<ITask>>) {
        // If all dates are sorted it means:
        // - sequence dates do not overlap between sequences
        // - sequences are in chronological order
        const dates = _.flatMap(sequences, (seq) => [seq.schedule.start, seq.schedule.until]);
        const sorted = _.sortBy(dates, (d) => d.getTime());
        assert(_.isEqual(dates, sorted), "InvalidSequenceOrder");

        // Validate each of the sequences
        for (const sequence of sequences) {
            assert.instanceOf(sequence._id, database.ObjectID, "InvalidSequence");
            this.validateDefaults(sequence.template);
            this.validateSchedule(sequence.schedule);
            this.validateExceptions(sequence.exceptions);
        }
    }

    static validateSchedule(schedule: Schedule) {
        Schedule.validate(schedule);
    }

    static validateExceptions(exceptions: ReadonlyArray<Event.Exception<ITask>>) {
        assert.array(exceptions, (exception) => {
            assert.date(exception.original_date);
            assert.number(exception.position, { min: 1 });

            if (exception.exception_date) {
                assert.date(exception.exception_date);
            }

            if (exception.cancelled) {
                assert.ok(exception.cancelled === true || exception.cancelled === false);
            }
        });
    }

    private static collection(db: database.Db) {
        return db.collection("tasklist");
    }

    _id: database.ObjectID;
    patient_id: database.ObjectID;
    medication_id?: database.ObjectID;
    version: string;
    defaults: ITask;
    shared: ITaskShared;
    sequences: Array<EventSequence<ITask>>;
    overdue_count: number;

    get start() {
        const start_dates = _.map(this.sequences, (seq) => seq.schedule.start);
        return _.minBy(start_dates, (date) => date.getTime());
    }

    get until() {
        const until_dates = _.map(this.sequences, (seq) => seq.schedule.until);
        return _.maxBy(until_dates, (date) => date.getTime());
    }

    // Assumes an already valid tasklist object
    protected constructor(tasklist: ITaskList) {
        this._id = tasklist._id || new database.ObjectID();
        this.patient_id = tasklist.patient_id;
        this.version = tasklist.version;
        this.defaults = tasklist.defaults;
        this.shared = tasklist.shared;
        this.overdue_count = tasklist.overdue_count;
        this.sequences = tasklist.sequences.map(
            (seq) => EventSequence.create(seq, this.defaults));

        if (tasklist.medication_id) {
            this.medication_id = tasklist.medication_id;
        }
    }

    // Writes the tasklist back into the database
    // This will fail if the current instance is not at the latest version
    async save(db: database.Db) {

        // Ensure that new tasklist is valid
        TaskList.validate(this);

        // Write tasklist to database
        const result = await TaskList.collection(db).findOneAndReplace(
            { "_id": this._id, "patient_id": this.patient_id, "version": this.version },
            { ...this.toPOJO(), "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.updateFromDB(result.value);
        return this;
    }

    async refresh(db: database.Db) {
        const result = await TaskList.collection(db).findOne({ _id: this._id, patient_id: this.patient_id });
        database.assert(result != null, new errors.UpdateFailed());
        this.updateFromDB(result);
        return this;
    }

    // Update a single task in the tasklist
    async updateOne(db: database.Db, task: Task, object: Partial<TaskList.UpdateOneOptions>) {
        const { sequence_id } = task.address;
        const exception = task.processChanges(object);
        const alert_reasons: IAlertReassignJob.Reason[] = [];

        // Task is already at the desired state
        // Nothing to do here
        if (!exception) {
            return task;
        }

        // Find the sequence matching the task
        const sequence = this.sequences.find((seq) => seq._id.equals(sequence_id));
        if (!sequence) {
            throw new errors.UpdateFailed();
        }

        // Reset overdue if we are marking the task as completed
        if (object.completed && !task.completed) {
            this.overdue_count = 0;
        }

        if (exception.overrides && exception.overrides.caregiver) {
            const { caregiver: previous } = task;
            const { caregiver: current } = exception.overrides;
            alert_reasons.push({
                type: "instance",
                task_date: exception.original_date,
                task_position: exception.position,
                previous: previous ? previous._id : null,
                current: current ? current._id : null,
            });
        }

        // Add the exception to the correct sequence in the database
        // Save the tasklist into the database
        sequence.addException(exception);
        await this.save(db);

        // Alert of reassignments
        if (alert_reasons.length) {
            await Agenda.alert_reassign(db, {
                patient: this.patient_id,
                tasklist: this._id,
                reasons: alert_reasons,
            });
        }

        // Return the updated task
        return this.instance(task.address);
    }

    // Update the entire task sequence
    async updateAll(db: database.Db, options: Partial<TaskList.UpdateAllOptions>) {
        const shared: Partial<ITaskShared> = {};
        const defaults: Partial<ITask> = {};
        const alert_reasons: IAlertReassignJob.Reason[] = [];

        // Change caregiver assigned to this task
        if (typeof options.caregiver !== "undefined" && !_.isEqual(options.caregiver, this.defaults.caregiver)) {
            defaults.caregiver = options.caregiver;

            const { caregiver: previous } = this.defaults;
            const { caregiver: current } = options;
            alert_reasons.push({
                type: "default",
                previous: previous ? previous._id : null,
                current: current ? current._id : null,
            });
        }

        // Set the sequence title
        if (typeof options.title !== "undefined" && options.title.length > 0) {
            shared.title = options.title;
        }

        // Handle task change if there is one
        if (typeof options.type !== "undefined" && options.type !== this.shared.type) {
            if (!Task.is_type(options.type)) {
                throw errors.create(status.BAD_REQUEST, "InvalidType", {
                    debug: `${options.type} is not a valid task type`,
                });
            }

            if (options.type === "medication" || this.shared.type === "medication") {
                throw errors.create(status.BAD_REQUEST, "InvalidType", {
                    debug: "Cannot change the type of a medication task.",
                });
            }

            shared.type = options.type;
        }

        // Set the task description if one was provided
        if (typeof options.description !== "undefined" && options.description !== this.shared.description) {
            shared.description = options.description;
        }

        // There is no reconcilliation of notes, it is assumed we are doing a replace
        if (typeof options.notes !== "undefined") {
            shared.notes = options.notes;
        }

        // Replace the list of attachments
        if (typeof options.attachments !== "undefined") {
            shared.attachments = options.attachments;
        }

        // Update shared and defaults
        this.shared = _.merge(this.shared, shared);
        this.defaults = _.merge(this.defaults, defaults);
        await this.save(db);

        // Schedule a notification for the assignment change
        if (alert_reasons.length) {
            await Agenda.alert_reassign(db, {
                patient: this.patient_id,
                tasklist: this._id,
                reasons: alert_reasons,
            });
        }

        return this;
    }

    // Update the schedule for the task sequence
    async updateSchedule(db: database.Db, schedule: Schedule) {
        // TODO: is it necessary to reschedule jobs when schedule changes?

        // Find the active sequence
        const index = _.findIndex(this.sequences, (seq) => seq.contains(schedule.start));
        const active: undefined|EventSequence<ITask> = this.sequences[index];

        // Remove all sequences that appear after the current one
        // This assumes that sequences don't overlap and are added in chronological order
        this.sequences.slice(index + 1);

        // If there is an active sequence then make it end before the new schedule
        if (active) {
            // The new schedule MUST have a start date greater than the active one
            if (!moment(schedule.start).isAfter(active.schedule.start)) {
                throw new errors.UpdateFailed("New schedule must start after existing schedule");
            }

            // End the active schedule just before the start of the new one
            active.setSchedule(_.merge(active.schedule, {
                until: moment(schedule.start).subtract({ ms: 1 }).toDate(),
            }));
        }

        // Create a new sequence if:
        // - There is no active sequence
        // - The new sequence designates a future date range
        if (!active ||
            (moment(schedule.until).isAfter(schedule.start) &&
             moment(schedule.until).isAfter(active.schedule.until))
        ) {
            const new_sequence = EventSequence.create<ITask>({
                _id: new database.ObjectID(),
                schedule,
                exceptions: [],
            }, this.defaults);

            this.sequences.push(new_sequence);
        }

        // Now commit stuff to the database
        await this.save(db);
        return this;
    }

    // Remove caregiver from all tasks to which they are assigned
    async unassign(db: database.Db, caregiver: database.ObjectID|CareGiver.Reference) {
        const caregiver_id = database.toID(caregiver);
        const { caregiver: default_caregiver } = this.defaults;
        const alert_reasons: IAlertReassignJob.Reason[] = [];

        // If this caregiver is the default caregiver:
        // We need to unset default caregiver and notify about this fact
        if (default_caregiver && default_caregiver._id.equals(caregiver_id)) {
            this.defaults.caregiver = null;

            // Notify of reasignment of the default caregiver
            alert_reasons.push({
                type: "default",
                previous: default_caregiver._id,
                current: null,
            });
        }

        // Unassign incomplete instances assigned to caregiver
        for (const sequence of this.sequences) {
            for (const exception of sequence.exceptions) {
                if (exception.completed || exception.cancelled) { continue; }
                if (!exception.overrides || !exception.overrides.caregiver) { continue; }
                if (!exception.overrides.caregiver._id.equals(caregiver_id)) { continue; }

                exception.overrides.caregiver = null;
                sequence.addException(exception);

                // Notify of reassignment of this instance
                alert_reasons.push({
                    type: "instance",
                    task_date: exception.original_date,
                    task_position: exception.position,
                    previous: caregiver_id,
                    current: null,
                });
            }
        }

        // Schedule an alert if there have been reassignments
        if (alert_reasons.length) {
            await Agenda.alert_reassign(db, {
                tasklist: this._id,
                patient: this.patient_id,
                reasons: alert_reasons,
            });
        }

        await this.save(db);
    }

    // Determine whether a date is between two others
    is_between(date: number|Date, start: number|Date, until: number|Date) {
        return moment(date).isBetween(start, until, "minute", "[]");
    }

    // Determine whether this tasklist is scheduled within a date range
    in_range(start: number|Date, until: number|Date) {
        const sequence = this.sequences.find((seq) =>
            this.is_between(start, seq.schedule.start, seq.schedule.until)
            || this.is_between(until, seq.schedule.start, seq.schedule.until)
            || this.is_between(seq.schedule.start, start, until)
            || this.is_between(seq.schedule.until, start, until));
        return sequence != null;
    }

    // Get the active schedule for a particular date
    schedule(date: number|Date): undefined|Schedule {
        const sequence = this.sequences.find((seq) => seq.contains(date));
        return sequence ? sequence.schedule : undefined;
    }

    // Determine whether this tasklist contains tasks at the requested dates
    contains(date: number|Date) {
        return this.schedule(date) != null;
    }

    // Create a task from an event instance
    task(instance: Event.Instance<ITask>) {
        return new Task({
            tasklist_id: this._id,
            ...instance.address,
        }, instance);
    }

    // Return all tasks in a date range
    between(start: number|Date, until: number|Date): Task[] {
        const instances = _.flatMap(this.sequences, (seq) => seq.between(start, until));
        return instances.map((inst) => this.task(inst));
    }

    // Return all tasks in the tasklist
    all(): Task[] {
        const instances = _.flatMap(this.sequences, (seq) => seq.all());
        return instances.map((inst) => this.task(inst));
    }

    // Get the task that follows a given date
    next(date: Date): undefined|Task {
        // Get the last index whose start date is greater than our current date
        // We need to do it this way because it is possible have gaps in schedules
        let index = _.findLastIndex(this.sequences, (seq) => seq.schedule.start.getTime() <= date.getTime());

        // If nothing was found then handle the case where a date is before the first schedule
        if (index === -1) {
            index = _.findLastIndex(this.sequences, (seq) => date.getTime() <= seq.schedule.start.getTime());
        }

        // If there is no index then there are no tasks after the current date
        if (index === -1) { return undefined; }

        // Get two schedules (in case the first one ends right at the given date)
        const [seq_1, seq_2] = this.sequences.slice(index, index + 2);

        // Attempt to get the next task from the sequence
        const instance_1 = seq_1.next(date);
        if (instance_1) { return this.task(instance_1); }

        // The first sequence had no next instance
        // This can happen if the sequence ends at or before our desired date
        // To compensate for this we will check the next sequence if there is one
        if (!seq_2) { return undefined; }
        const instance_2 = seq_2.next(date);
        if (instance_2) { return this.task(instance_2); }

        // We were unable to find a task following our date
        return undefined;
    }

    // Return the first instance in this tasklist
    first(): undefined | Task {
        const instance = this.sequences[0].first();
        if (!instance) { return undefined; }
        return this.task(instance);
    }

    // Get the instance matching the given id
    instance(id: ITaskID): undefined | Task {
        return this.instanceAt(id.original_date, id.position);
    }

    // Get the instance at a date and position
    instanceAt(date: number|Date, position: number): undefined | Task {
        // Find the sequence containing a given date
        const sequence = this.sequences.find((seq) => seq.contains(date));
        if (!sequence) { return undefined; }

        // Get the instance from the sequence
        const instance = sequence.at(date, position);
        if (!instance) { return undefined; }

        // Return a new task object
        return new Task({
            tasklist_id: this._id,
            sequence_id: sequence._id,
            original_date: moment(date).toDate(),
            position,
        }, instance);
    }

    toPOJO() {
        const tasklist: ITaskList = {
            _id: this._id,
            patient_id: this.patient_id,
            version: this.version,
            defaults: this.defaults,
            shared: this.shared,
            overdue_count: this.overdue_count,
            sequences: this.sequences.map((seq) => seq.toPOJO()),
        };

        if (this.medication_id) {
            tasklist.medication_id = this.medication_id;
        }

        return tasklist;
    }

    // Update this tasklist instance with data from the database
    protected updateFromDB(tasklist: ITaskList) {
        this._id = tasklist._id; // This shouldn't change but you never know...
        this.patient_id = tasklist.patient_id;
        this.version = tasklist.version;
        this.defaults = tasklist.defaults;
        this.shared = tasklist.shared;
        this.overdue_count = tasklist.overdue_count;
        this.sequences = _.map(tasklist.sequences,
            (seq) => EventSequence.create(seq, tasklist.defaults));

        if (tasklist.medication_id) {
            this.medication_id = tasklist.medication_id;
        }
    }

}

// A unique identifier to a specific task
export interface ITaskID extends Event.Address {
    tasklist_id: database.ObjectID;
}

export interface TaskIDJSON {
    tasklist_id: string;
    sequence_id: string;
    original_date: number;
    position: number;
}

export class TaskID implements ITaskID {

    // Parse the task ID from a json object
    static fromJSON(json: TaskIDJSON): undefined | TaskID {
        const tasklist_id = database.parseObjectID(json.tasklist_id);
        const sequence_id = database.parseObjectID(json.sequence_id);
        if (!tasklist_id || !sequence_id) { return undefined; }

        return new TaskID({
            tasklist_id,
            sequence_id,
            original_date: new Date(json.original_date),
            position: json.position,
        });
    }

    static factory(id: ITaskID) {
        return new TaskID(id);
    }

    readonly tasklist_id: database.ObjectID;
    readonly sequence_id: database.ObjectID;
    readonly original_date: Date;
    readonly position: number;

    protected constructor(id: ITaskID) {
        this.tasklist_id = id.tasklist_id;
        this.sequence_id = id.sequence_id;
        this.original_date = id.original_date;
        this.position = id.position;
    }

    toJSON(): TaskIDJSON {
        return {
            tasklist_id: this.tasklist_id.toHexString(),
            sequence_id: this.sequence_id.toHexString(),
            original_date: this.original_date.getTime(),
            position: this.position,
        };
    }
}

// A single task ocurrence
export class Task implements Event.Instance<ITask> {

    static is_type(type?: string): type is Task.Type {
        return _.includes(Task.Type, type);
    }

    address: TaskID;
    date: Date;
    due: Date;
    position: number;
    out_of: number;
    caregiver: null | CareGiver.IReference;
    completed: boolean;

    constructor(_id: ITaskID, instance: ITask & Event.Base & Event.Status) {
        this.address = TaskID.factory(_id);
        this.date = instance.date;
        this.due = instance.due;
        this.position = instance.position;
        this.out_of = instance.out_of;
        this.caregiver = instance.caregiver;
        this.completed = instance.completed;
    }

    // Determine whether the task is overdue by a given date
    // Defaults to checking against current date
    overdue(now: Date = new Date()) {
        return !this.completed && moment(this.due)
            .add({ m: config.task.overdue_after_mins })
            .isBefore(now);
    }

    // Determine what would need to change in order to get the task into the desired state
    // This only accounts for task-level changes
    processChanges(options: Partial<TaskList.UpdateOneOptions>): undefined | Event.Exception<ITask> {
        const exception: Event.Exception<ITask> = { original_date: this.date, position: this.position, overrides: {} };
        const exception_original = _.merge({}, exception);

        // Toggle task completion
        if (options.completed != null) {
            exception.completed = options.completed;
        }

        // Cancel or reschedule task
        if (options.cancelled) {
            exception.cancelled = true;
        } else if (options.date && !moment(options.date).isSame(this.date, "minute")) {
            exception.exception_date = options.date;
        }

        // Change caregiver assigned to this task
        if (typeof options.caregiver !== "undefined" && !_.isEqual(options.caregiver, this.caregiver)) {
            _.merge(exception.overrides, { caregiver: options.caregiver });
        }

        // Only return the exception if we've done something to it
        return _.isEqual(exception, exception_original) ? undefined : exception;
    }
}

export namespace Task {
    export const Type = StringMap("appointment", "medication", "other");
    export type Type = keyof typeof Type;
}
