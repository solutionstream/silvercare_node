import { IContact, Media } from "../";
import * as database from "../../database";
import * as errors from "../../errors";
import { assert, Require, updateFrom } from "../../utils";


export interface IPAC extends IContact {
    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    full_name: string;
    photo: Media.Reference | null;
    theme: PAC.Theme;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];
}


export class PAC implements IPAC {

    static async setupCollection(db: database.Db) {
        await PAC.collection(db).createIndexes([
            { key: { "user_id": 1 }, unique: true },
            { key: {
                "full_name": "text",
                "addresses.address": "text",
                "phones.number": "text",
                "emails.email": "text",
            }, name: "pac_fts" },
        ]);
    }

    static factory(options: PAC.FactoryOptions) {
        const pac: IPAC = {
            _id: options._id || database.id(),
            user_id: options.user_id,
            version: options.version || database.version(),
            full_name: options.full_name,
            photo: options.photo || null,
            theme: options.theme || PAC.Theme.default,
            addresses: options.addresses || [],
            phones: options.phones || [],
            emails: options.emails || [],
        };

        return new PAC(pac);
    }

    static validate(pac: IPAC) {
        assert.ok(pac);
        assert.instanceOf(pac._id, database.ObjectID);
        assert.instanceOf(pac.user_id, database.ObjectID);
        assert.string(pac.version, { min: 1 });
        assert.string(pac.full_name, { min: 1 });

        if (pac.photo !== null) {
            Media.Reference.validate(pac.photo);
        }

        PAC.Theme.validate(pac.theme);
        assert.array(pac.addresses, IContact.PhysicalAddress.validate);
        assert.array(pac.phones, IContact.PhoneNumber.validate);
        assert.array(pac.emails, IContact.EmailAddress.validate);
    }

    // Insert PAC into database
    static async insert(db: database.Db, pac: IPAC) {
        PAC.validate(pac);

        // Insert document into database
        const result = await PAC.collection(db).insertOne(pac);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return PAC.factory(pac);
    }

    // Create pac in the database
    static async create(db: database.Db, options: PAC.FactoryOptions) {
        const pac = PAC.factory(options);
        return await PAC.insert(db, pac);
    }

    // Get a single pac from the database
    static async fetch(db: database.Db, query: any) {
        const document = await PAC.collection(db).findOne(query);
        if (!document) { return undefined; }

        const pac = PAC.factory(document);
        return pac;
    }

    // Search for PACs
    static async search(db: database.Db, options: PAC.SearchOptions) {
        const query: any = {};

        // FTS for string fields
        if (options.full_name || options.address || options.phone || options.email) {
            const phrases: string[] = [];

            if (options.full_name) { phrases.push(`"${options.full_name}"`); }
            if (options.address) { phrases.push(`"${options.address}"`); }
            if (options.phone) { phrases.push(`"${options.phone}"`); }
            if (options.email) { phrases.push(`"${options.email}"`); }

            query.$text = { "$search": phrases.join(" ") };
        }

        const documents = await PAC.collection(db).find(query).limit(options.limit).toArray();
        const pacs = documents.map((doc) => this.factory(doc));
        return pacs;
    }

    static collection(db: database.Db) {
        return db.collection("pac");
    }

    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    full_name: string;
    photo: Media.Reference | null;
    theme: PAC.Theme;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];

    protected constructor(pac: IPAC) {
        this.update(pac);
    }

    update(pac: Partial<IPAC>) {
        updateFrom("_id", this, pac);
        updateFrom("user_id", this, pac);
        updateFrom("version", this, pac);
        updateFrom("full_name", this, pac);
        updateFrom("photo", this, pac);
        updateFrom("theme", this, pac);
        updateFrom("addresses", this, pac);
        updateFrom("phones", this, pac);
        updateFrom("emails", this, pac);
    }

    // Save the current model into the database
    async save(db: database.Db) {
        PAC.validate(this);

        const result = await PAC.collection(db).findOneAndReplace(
            { "_id": this._id, "version": this.version },
            { ...(this as PAC), "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.update(result.value);
        return this;
    }

    // Update model to latest document
    async refresh(db: database.Db) {
        const document = await PAC.fetch(db, { _id: this._id });
        database.assert(document != null, new errors.UpdateFailed());
        this.update(document!);
        return this;
    }
}

export namespace PAC {
    export type FactoryOptions = Require<IPAC, "full_name"|"user_id">;
    export interface SearchOptions {
        limit: number;
        full_name?: string;
        address?: string;
        phone?: string;
        email?: string;
    }

    export class Theme {
        static default: Theme = {
            icon: null,
            primary_color: null,
        };

        static fromJSON(json: Theme.JSON) {
            const { icon: iconJSON, primary_color } = json;
            let icon: null | Media.Reference = null;

            if (iconJSON != null) {
                icon = Media.Reference.fromJSON(iconJSON);
            }

            return { icon, primary_color };
        }

        static validate(theme: Theme) {
            if (theme.primary_color !== null) {
                assert.string(theme.primary_color, { pattern: Theme.colorRE });
            }

            if (theme.icon !== null) {
                Media.Reference.validate(theme.icon);
            }
        }

        icon: null | Media.Reference;
        primary_color: null | string;
    }

    export namespace Theme {
        export const colorRE = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;

        export interface JSON {
            icon: null | Media.ReferenceJSON;
            primary_color: null | string;
        }
    }
}
