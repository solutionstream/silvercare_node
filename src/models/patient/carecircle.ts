import * as database from "../../database";
import { assert, Optional, Require, StringMap } from "../../utils";
import { CareGiver as _CareGiver } from "../caregiver";
import { Media } from "../medialibrary";
import { Note } from "../note";
import { Notification } from "../patient";


// The minimum information required to be a "contact"
export interface IContact {
    _id: database.ObjectID;
    full_name: string;
    photo: Media.Reference | null;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];
}

export namespace IContact {

    export class PhysicalAddress {

        static fromJSON(json: PhysicalAddress.JSON): PhysicalAddress {
            const { type, address } = json;
            if (!PhysicalAddress.is_type(type)) { throw new Error("InvalidType"); }
            return { type, address };
        }

        static is_type(type: string): type is PhysicalAddress.Type {
            return Object.keys(PhysicalAddress.Type).indexOf(type) >= 0;
        }

        static validate(object: PhysicalAddress) {
            assert.string(object.address, { min: 1 });
            assert.string(object.type, { min: 1 }, "Contact:InvalidAddress");
            assert.oneOf(object.type, Object.keys(IContact.PhysicalAddress.Type), "Contact:InvalidAddress");
        }

        type: PhysicalAddress.Type;
        address: string;
    }

    export namespace PhysicalAddress {
        export const Type = StringMap("home", "work", "other");
        export type Type = keyof typeof Type;
        export type JSON = Record<string, keyof PhysicalAddress>;
    }

    export class PhoneNumber {

        static fromJSON(json: PhoneNumber.JSON): PhoneNumber {
            const { type, number } = json;
            if (!PhoneNumber.is_type(type)) { throw new Error("InvalidType"); }
            return { type, number };
        }

        static is_type(type: string): type is PhoneNumber.Type {
            return Object.keys(PhoneNumber.Type).indexOf(type) >= 0;
        }

        static validate(object: PhoneNumber) {
            assert.phone(object.number, "Contact:InvalidPhone");
            assert.string(object.type, { min: 1 }, "Contact:InvalidPhone");
            assert.oneOf(object.type, Object.keys(IContact.PhoneNumber.Type), "Contact:InvalidPhone");
        }

        type: PhoneNumber.Type;
        number: string;
    }

    export namespace PhoneNumber {
        export const Type = StringMap("home", "mobile", "work");
        export type Type = keyof typeof Type;
        export type JSON = Record<string, keyof PhoneNumber>;
    }

    export class EmailAddress {

        static fromJSON(json: EmailAddress.JSON) {
            const { type, email } = json;
            if (!EmailAddress.is_type(type)) { throw new Error("InvalidType"); }
            return { type, email };
        }

        static is_type(type: string): type is EmailAddress.Type {
            return Object.keys(EmailAddress.Type).indexOf(type) >= 0;
        }

        static validate(object: EmailAddress) {
            assert.email(object.email, "Contact:InvalidEmail");
            assert.string(object.type, { min: 1 }, "Contact:InvalidEmail");
            assert.oneOf(object.type, Object.keys(IContact.EmailAddress.Type), "Contact:InvalidEmail");
        }

        type: EmailAddress.Type;
        email: string;
    }

    export namespace EmailAddress {
        export const Type = StringMap("home", "work");
        export type Type = "home" | "work";
        export type JSON = Record<string, keyof EmailAddress>;
    }

}


export namespace CareCircle {

    // Information about contacts that is specific to
    // this carecircle.  This is patient specific information.
    export interface Specific {
        relationship: string;
        notes: Note[];
    }

    // Manually entered contacts
    export interface IMiscellaneous extends IContact, Specific {}
    export class Miscellaneous implements IMiscellaneous {

        static validate(object: IMiscellaneous) {
            assert.instanceOf(object._id, database.ObjectID, "InvalidID");
            assert.string(object.full_name, { min: 1 }, "InvalidFullName");
            assert.string(object.relationship, undefined, "InvalidRelationship");

            if (object.photo !== null) {
                Media.Reference.validate(object.photo);
            }

            assert.array(object.addresses, IContact.PhysicalAddress.validate, "InvalidAddress");
            assert.array(object.emails, IContact.EmailAddress.validate, "InvalidEmail");
            assert.array(object.phones, IContact.PhoneNumber.validate, "InvalidPhone");
            assert.array(object.notes, Note.validate, "InvalidNote");
        }

        static factory(object: Require<IMiscellaneous, "full_name"|"relationship">) {
            return new Miscellaneous(
                object._id || new database.ObjectID(),
                object.full_name,
                object.photo || null,
                object.addresses || [],
                object.phones || [],
                object.emails || [],
                object.relationship,
                object.notes || []);
        }

        constructor(
            public _id: database.ObjectID,
            public full_name: string,
            public photo: Media.Reference | null,
            public addresses: IContact.PhysicalAddress[],
            public phones: IContact.PhoneNumber[],
            public emails: IContact.EmailAddress[],
            public relationship: string,
            public notes: Note[]) {}
    }

    // Associates the patient with a PAC
    export interface PAC extends Specific {}

    // Associates the patient with a caregiver
    export interface ICareGiverSpecific extends Specific {
        read_discharge_docs: boolean;
        notifications: Partial<Notification.ISettings>;
    }

    export type ICareGiver = _CareGiver.IReference & ICareGiverSpecific;

    export class CareGiver implements ICareGiver {

        static validate(object: ICareGiver) {
            assert.ok(object, "InvalidCareGiver");
            assert.instanceOf(object._id, database.ObjectID, "InvalidID");
            assert.string(object.full_name, { min: 1 }, "InvalidName");
            assert.string(object.relationship, undefined, "InvalidRelationship");
            assert.array(object.notes, Note.validate, "Invalid Note");
            assert.boolean(object.read_discharge_docs);

            // Validate notification settings
            const notifications = Notification.Settings.factory(object.notifications);
            Notification.Settings.validate(notifications);
        }

        static factory(object: Optional<ICareGiver, "relationship"|"notes"|"notifications"|"read_discharge_docs">) {
            const { _id, full_name, relationship, notes, notifications, read_discharge_docs } = object;
            const _relationship = relationship || "";
            const _notes = notes || [];
            const _discharge = read_discharge_docs != null ? read_discharge_docs : false;
            const _notifications = notifications || {};

            return new CareGiver(_id, full_name, _relationship, _notes, _discharge, _notifications);
        }

        constructor(
            public _id: database.ObjectID,
            public full_name: string,
            public relationship: string,
            public notes: Note[],
            public read_discharge_docs: boolean,
            public notifications: Partial<Notification.ISettings>) { }

        reference() {
            return _CareGiver.Reference.factory(this);
        }
    }

    // A contact is any of the above contact types
    export type Contact = CareGiver | PAC | Miscellaneous;

}
