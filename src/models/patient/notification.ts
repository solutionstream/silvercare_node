import { defaultTo, Dictionary } from "../../utils";

export namespace Notification {

    export interface ITaskSettings {
        enabled?: boolean;
        remind_due?: boolean;
        assignee_change?: boolean;
        overdue_count?: number;
    }

    export interface ISettings {
        enabled: boolean;
        tasks: Dictionary<ITaskSettings>;
    }

    export class Settings implements ISettings {

        static validate(_object: ISettings) {
            // TODO
        }

        static factory(opts: Partial<ISettings>) {
            const { enabled, tasks } = opts;
            const settings: ISettings = {
                enabled: defaultTo<boolean>(enabled, true),
                tasks: defaultTo(tasks, {}),
            };

            return new Settings(settings);
        }

        enabled: boolean;
        tasks: Dictionary<ITaskSettings>;

        constructor(settings: ISettings) {
            const { enabled, tasks } = settings;
            this.enabled = (enabled != null) ? enabled : true;
            this.tasks = (tasks != null) ? tasks : {};
        }

        // Determine whether notifications for this task type are enabled
        task_enabled(type: string) {
            const task = this.tasks[type];
            if (task == null) { return true; }
            return (task.enabled != null) ? task.enabled : true;
        }

        // Determine whether notifications are enabled for this overdue count
        task_overdue(type: string, overdue: number) {
            const task = this.tasks[type];
            if (task == null) { return true; }

            const { overdue_count } = task;
            return overdue_count != null
                ? overdue >= overdue_count
                : true;
        }

        // Determine whether reminders are enabled for this task type
        task_due(type: string) {
            const task = this.tasks[type];
            if (task == null) { return true; }

            const { remind_due } = task;
            return remind_due != null ? remind_due : true;
        }

        // Determine whether alerts are enabled for this task type
        task_assign(type: string) {
            const task = this.tasks[type];
            if (task == null) { return true; }

            const { assignee_change } = task;
            return assignee_change != null ? assignee_change : true;
        }
    }

}
