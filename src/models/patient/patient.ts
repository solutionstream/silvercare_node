import * as _ from "lodash";
import * as database from "../../database";
import * as errors from "../../errors";
import { Schedule } from "../../scheduling";
import { assert, defaultTo, Omit, Require, updateFrom } from "../../utils";
import { CareGiver } from "../caregiver";
import { Media } from "../medialibrary";
import { Note } from "../note";
import { CareCircle, IContact } from "./carecircle";


export interface IPatient {
    _id: database.ObjectID;
    user_id: database.ObjectID;
    medications: Medication[];
    primary_caregiver: null | database.ObjectID;
    carecircle: Patient.CareCircle;
    version: string;
    pac_id: null | database.ObjectID;
    date_of_birth: null | Date;
    full_name: string;
    preferred_name: string;
    gender: string;
    blood_type: string;
    faith: string;
    instructions: string;
    conditions: string[];
    allergies: string[];
    photo: Media.Reference | null;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];
}


export class Patient implements IPatient {

    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndexes([
            { key: { "user_id": 1 }, unique: true },
            { key: { "pac_id": 1 } },
            { key: { "carecircle.caregivers._id": 1 } },
            { key: { "carecircle.contacts._id": 1 } },
            { key: { "medications._id": 1 } },
            { key: {
                "full_name": "text",
                "preferred_name": "text",
                "addresses.address": "text",
                "phones.number": "text",
                "emails.email": "text",
            }, name: "patient_fts" },
        ]);
    }

    // Create an instance of Patient applying defaults where possible
    static factory(options: Patient.FactoryOptions) {
        return new Patient(options);
    }

    // Create a patient in the database
    static async create(db: database.Db, options: Patient.FactoryOptions) {
        const patient = this.factory(options);
        return await this.insert(db, patient);
    }

    // Insert a patient object into the databae
    static async insert(db: database.Db, patient: IPatient) {
        // Add the user as their own caregiver
        const caregiver = CareGiver.factory(patient);
        patient.carecircle.caregivers.unshift(
            CareCircle.CareGiver.factory(caregiver));

        // Ensure that the records are valid
        Patient.validate(patient);
        CareGiver.validate(caregiver);

        // Insert patient and caregiver records
        const [result] = await Promise.all([
            this.collection(db).insertOne(patient),
            CareGiver.insert(db, caregiver),
        ]);

        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return new Patient(patient);
    }

    // Get a patient object from the database
    static async fetch(db: database.Db, query: any) {
        const document = await this.collection(db).findOne(query);
        if (!document) { return; }

        const patient = Patient.factory(document);
        return patient;
    }

    // Search for patients
    static async search(db: database.Db, options: Patient.SearchOptions) {
        const query: any = {};

        if (options.caregivers) {
            query["carecircle.caregivers._id"] = { "$in": options.caregivers };
        }

        if (options.pacs) {
            query.pac_id = { "$in": options.pacs };
        }

        if (options.dob) {
            query.date_of_birth = options.dob;
        }

        if (options.name || options.address || options.phone || options.email) {
            const strings: string[] = [];

            if (options.name) { strings.push(`"${options.name}"`); }
            if (options.address) { strings.push(`"${options.address}"`); }
            if (options.phone) { strings.push(`"${options.phone}"`); }
            if (options.email) { strings.push(`"${options.email}"`); }

            query.$text = { "$search": strings.join(" ") };
        }

        const documents = await this.collection(db).find(query).limit(options.limit).toArray();
        const patients = documents.map((doc) => this.factory(doc));
        return patients;
    }

    // Validate a patient object
    static validate(patient: IPatient) {
        assert.ok(patient != null, "InvalidPatient");
        assert.instanceOf(patient._id, database.ObjectID, "InvalidPatientID");
        assert.instanceOf(patient.user_id, database.ObjectID, "InvalidUserID");

        if (patient.pac_id !== null) {
            assert.instanceOf(patient.pac_id, database.ObjectID, "InvalidPACID");
        }

        if (patient.primary_caregiver !== null) {
            assert.instanceOf(patient.primary_caregiver, database.ObjectID, "InvalidPrimaryCaregiver");
        }

        assert.string(patient.version, { min: 1 });
        assert.string(patient.full_name, { min: 1 });
        assert.string(patient.preferred_name);
        assert.string(patient.gender);
        assert.string(patient.blood_type);
        assert.string(patient.faith);
        assert.string(patient.instructions);

        if (patient.date_of_birth !== null) {
            assert.date(patient.date_of_birth);
        }

        if (patient.photo !== null) {
            Media.Reference.validate(patient.photo);
        }

        assert.array(patient.conditions, assert.string);
        assert.array(patient.allergies, assert.string);
        assert.array(patient.medications, Medication.validate);
        assert.array(patient.carecircle.caregivers, CareCircle.CareGiver.validate);
        assert.array(patient.carecircle.contacts, CareCircle.Miscellaneous.validate);
        assert.array(patient.addresses, IContact.PhysicalAddress.validate);
        assert.array(patient.phones, IContact.PhoneNumber.validate);
        assert.array(patient.emails, IContact.EmailAddress.validate);
    }

    private static collection(db: database.Db) {
        return db.collection("patient");
    }

    _id: database.ObjectID;
    user_id: database.ObjectID;
    medications: Medication[];
    primary_caregiver: null | database.ObjectID;
    carecircle: Patient.CareCircle;
    version: string;
    pac_id: null | database.ObjectID;
    date_of_birth: null | Date;
    full_name: string;
    preferred_name: string;
    gender: string;
    blood_type: string;
    faith: string;
    instructions: string;
    conditions: string[];
    allergies: string[];
    photo: Media.Reference | null;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];

    protected constructor(options: Patient.FactoryOptions) {
        this.update({
            _id: options._id || new database.ObjectID(),
            user_id: options.user_id,
            medications: options.medications || [],
            primary_caregiver: options.primary_caregiver || null,
            carecircle: options.carecircle || { caregivers: [], contacts: [] },
            version: options.version || database.version(),
            pac_id: options.pac_id || null,
            full_name: options.full_name || "",
            preferred_name: options.preferred_name || "",
            date_of_birth: options.date_of_birth || null,
            gender: options.gender || "",
            blood_type: options.blood_type || "",
            faith: options.faith || "",
            instructions: options.instructions || "",
            conditions: options.conditions || [],
            allergies: options.allergies || [],
            photo: options.photo || null,
            addresses: options.addresses || [],
            phones: options.phones || [],
            emails: options.emails || [],
        });
    }

    // Update the current instance with data from the database
    update(this: IPatient, patient: Partial<IPatient>) {
        updateFrom("_id", this, patient);
        updateFrom("user_id", this, patient);

        updateFrom("medications", this, patient);
        this.medications = this.medications.map(Medication.factory);

        // Care Circle
        updateFrom("primary_caregiver", this, patient);
        updateFrom("carecircle", this, patient);
        this.carecircle.caregivers = this.carecircle.caregivers.map(CareCircle.CareGiver.factory);
        this.carecircle.contacts = this.carecircle.contacts.map(CareCircle.Miscellaneous.factory);

        updateFrom("version", this, patient);
        updateFrom("pac_id", this, patient);
        updateFrom("full_name", this, patient);
        updateFrom("preferred_name", this, patient);
        updateFrom("date_of_birth", this, patient);
        updateFrom("gender", this, patient);
        updateFrom("blood_type", this, patient);
        updateFrom("faith", this, patient);
        updateFrom("instructions", this, patient);
        updateFrom("conditions", this, patient);
        updateFrom("allergies", this, patient);
        updateFrom("photo", this, patient);
        updateFrom("addresses", this, patient);
        updateFrom("phones", this, patient);
        updateFrom("emails", this, patient);
    }

    // Save the current patient instance into the database
    async save(db: database.Db) {
        Patient.validate(this);

        // Write patient to database
        const result = await Patient.collection(db).findOneAndReplace(
            { "_id": this._id, "version": this.version },
            { ...(this as Patient), "version": database.version() },
            { returnOriginal: false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.update(result.value);
        return this;
    }

    // Pull the latest version of the database
    async refresh(db: database.Db) {
        const result = await Patient.collection(db).findOne({ _id: this._id });
        database.assert(result != null, new errors.UpdateFailed());
        this.update(result);
        return this;
    }

    // Add a medication to the patient
    async add_medication(db: database.Db, options: Medication.CreateOptions) {
        const medication = Medication.factory(options);
        this.medications.push(medication);

        const result = await this.save(db);
        this.update(result);
        return this;
    }

    async remove_medication(db: database.Db, options: database.ObjectID|Medication) {
        const index = this.medication_index(options);
        const [medication] = this.medications.splice(index, 1);
        if (!medication) {
            throw new Error("NoSuchMedication");
        }

        // Save patient without medication
        await this.save(db);
    }

    // Update a medication
    async update_medication(db: database.Db, options: Medication.UpdateOptions) {
        const medication = _.find(this.medications, (med) => med._id.equals(options._id));
        if (!medication) {
            throw new Error("NoSuchMedication");
        }

        // Update medication
        medication.update(options);
        await this.save(db);
    }

    async update_medication_schedule(db: database.Db, options: Medication.UpdateScheduleOptions) {
        const medication = _.find(this.medications, (med) => med._id.equals(options._id));
        if (!medication) {
            throw new Error("NoSuchMedication");
        }

        // Update medication
        medication.update(options);
        await this.save(db);
    }

    medication(medication: database.ObjectID|Medication) {
        const index = this.medication_index(medication);
        if (index === -1) { return undefined; }
        return this.medications[index];
    }

    medication_index(medication: database.ObjectID|Medication) {
        const medication_id = database.toID(medication);
        return _.findIndex(this.medications, (med) => med._id.equals(medication_id));
    }

    // Determine the index of a contact
    contact_index(contact: database.ObjectID|IContact): undefined|number {
        const contact_id = database.toID(contact);
        const index = _.findIndex(this.carecircle.contacts, (ref) => ref._id.equals(contact_id));
        return index >= 0 ? index : undefined;
    }

    contact(contact: database.ObjectID) {
        const index = this.contact_index(contact);
        if (index == null) { return undefined; }
        return this.carecircle.contacts[index];
    }

    contacts() {
        return this.carecircle.contacts;
    }

    // Add a contact to the carecircle
    add_contact(contact_t: Require<CareCircle.Miscellaneous, "_id"|"full_name">) {
        const contact_s = this.contact(contact_t._id);
        const contact = _.merge({
            relationship: "",
            photo: null,
            addresses: [],
            phones: [],
            emails: [],
            notes: [],
        }, contact_s, contact_t);

        // replace the contact object in the carecircle
        const index = this.contact_index(contact);
        if (index == null) {
            this.carecircle.contacts.push(contact);
        } else {
            this.carecircle.contacts[index] = contact;
        }

        return contact;
    }

    // Remove contact from care circle
    remove_contact(contact: database.ObjectID|CareCircle.Miscellaneous) {
        const index = this.contact_index(contact);
        if (index == null) { return; }
        this.carecircle.contacts.splice(index, 1);
    }

    // Determine the index of the caregiver
    caregiver_index(caregiver: CareGiver.IReference|database.ObjectID): undefined|number {
        const caregiver_id = database.toID(caregiver);
        const index = _.findIndex(this.carecircle.caregivers, (ref) => ref._id.equals(caregiver_id));
        return index >= 0 ? index : undefined;
    }

    // Get caregiver data stored in patient record
    caregiver_specific(caregiver: CareGiver.IReference|database.ObjectID): undefined|CareCircle.CareGiver {
        const index = this.caregiver_index(caregiver);
        if (index == null) { return undefined; }
        return this.carecircle.caregivers[index];
    }

    // Get a caregiver contact object
    async caregiver_contact(db: database.Db, caregiver_t: CareGiver.Reference|database.ObjectID) {
        // Get patient specific data
        const specific = this.caregiver_specific(caregiver_t);
        if (!specific) { return undefined; }

        // Get caregiver data from database
        const caregiver_id = database.toID(caregiver_t);
        const caregiver = await CareGiver.fetch(db, { _id: caregiver_id });
        if (!caregiver) { return undefined; }

        // Combine caregiver and patient data
        return new CareGiver.Contact(caregiver, specific);
    }

    // Get all caregivers as contact objects
    async caregiver_contacts(db: database.Db) {
        const caregivers = await CareGiver.getAll(db, this.carecircle.caregivers);
        return _.map(caregivers, (cg) => {
            const specific = this.caregiver_specific(cg._id);
            return new CareGiver.Contact(cg, specific!);
        });
    }

    // Determine whether a caregiver is in a patient's care circle
    in_carecircle(caregiver: CareGiver.IReference|database.ObjectID): boolean {
        return this.caregiver_index(caregiver) != null;
    }

    // Add caregiver data to carecircle
    add_caregiver(caregiver_t: Require<CareCircle.ICareGiver, "_id"|"full_name">, primary?: boolean) {
        const caregiver_s = this.caregiver_specific(caregiver_t);
        const caregiver_m = _.merge({
            relationship: "",
            notes: [],
            read_discharge_docs: false,
            notifications: {},
        }, caregiver_s, caregiver_t);
        const caregiver = CareCircle.CareGiver.factory(caregiver_m);

        // Replace the caregiver object in the carecircle
        const index = this.caregiver_index(caregiver);
        if (index == null) {
            this.carecircle.caregivers.push(caregiver);
        } else {
            this.carecircle.caregivers[index] = caregiver;
        }

        // Set/unset primary caregiver
        if (typeof primary !== "undefined") {
            if (primary) {
                this.primary_caregiver = caregiver._id;
            } else if (this.is_primary(caregiver)) {
                this.primary_caregiver = null;
            }
        }
    }

    // Remove caregiver from the carecircle
    remove_caregiver(caregiver: database.ObjectID|CareGiver.IReference) {
        const index = this.caregiver_index(caregiver);
        if (index == null) { return; }
        const [removed] = this.carecircle.caregivers.splice(index, 1);

        if (this.is_primary(caregiver)) {
            this.primary_caregiver = null;
        }

        return removed;
    }

    // Determine whether a caregiver is the primary
    is_primary(caregiver: database.ObjectID|CareGiver.IReference) {
        const caregiver_id = database.toID(caregiver);
        return this.primary_caregiver && this.primary_caregiver.equals(caregiver_id);
    }
}

export interface IMedication {
    _id: database.ObjectID;
    pillpack_id: null | database.ObjectID;
    name: string;
    dosage: string;
    rx_num: string;
    next_refill: null | Date;
    pharmacy: string;
    prescriber: string;
    purpose: string;
    appearance: string;
    instructions: string;
    schedule: Schedule;
    photo: null | Media.Reference;
    notes: Note[];
    attachments: Media.Reference[];
}

export class Medication implements IMedication {

    static validate(medication: Medication) {
        assert.ok(medication);
        assert.instanceOf(medication._id, database.ObjectID);
        assert.string(medication.name, { min: 1 });
        assert.string(medication.dosage, { min: 1 });
        assert.string(medication.rx_num);
        assert.string(medication.pharmacy);
        assert.string(medication.prescriber);
        assert.string(medication.purpose);
        assert.string(medication.appearance);
        assert.string(medication.instructions);
        Schedule.validate(medication.schedule);

        if (medication.next_refill !== null) {
            assert.date(medication.next_refill);
        }

        if (medication.pillpack_id !== null) {
            assert.instanceOf(medication.pillpack_id, database.ObjectID);
        }

        if (medication.photo !== null) {
            Media.Reference.validate(medication.photo);
        }

        assert.array(medication.notes, Note.validate);
        assert.array(medication.attachments, Media.Reference.validate);
    }

    static factory(options: Medication.CreateOptions) {
        return new Medication(options);
    }

    _id: database.ObjectID;
    pillpack_id: null | database.ObjectID;
    name: string;
    dosage: string;
    rx_num: string;
    next_refill: null | Date;
    pharmacy: string;
    prescriber: string;
    purpose: string;
    appearance: string;
    instructions: string;
    schedule: Schedule;
    photo: null | Media.Reference;
    notes: Note[];
    attachments: Media.Reference[];

    constructor(options: Medication.CreateOptions) {
        this._id = options._id || new database.ObjectID();
        this.pillpack_id = options.pillpack_id || null;
        this.name = options.name;
        this.dosage = options.dosage;
        this.rx_num = options.rx_num || "";
        this.next_refill = options.next_refill || null;
        this.pharmacy = options.pharmacy || "";
        this.prescriber = options.prescriber || "";
        this.purpose = options.purpose || "";
        this.appearance = options.appearance || "";
        this.instructions = options.instructions || "";
        this.schedule = options.schedule;
        this.photo = options.photo || null;
        this.notes = options.notes || [];
        this.attachments = options.attachments || [];
    }

    // Update the instance
    update(options: Partial<Medication>) {
        this._id = defaultTo(options._id, this._id);
        this.pillpack_id = defaultTo(options.pillpack_id, this.pillpack_id);
        this.name = defaultTo(options.name, this.name);
        this.dosage = defaultTo(options.dosage, this.dosage);
        this.rx_num = defaultTo(options.rx_num, this.rx_num);
        this.next_refill = defaultTo(options.next_refill, this.next_refill);
        this.pharmacy = defaultTo(options.pharmacy, this.pharmacy);
        this.prescriber = defaultTo(options.prescriber, this.prescriber);
        this.purpose = defaultTo(options.purpose, this.purpose);
        this.appearance = defaultTo(options.appearance, this.appearance);
        this.instructions = defaultTo(options.instructions, this.instructions);
        this.schedule = defaultTo(options.schedule, this.schedule);
        this.photo = defaultTo(options.photo, this.photo);
        this.notes = defaultTo(options.notes, this.notes);
        this.attachments = defaultTo(options.attachments, this.attachments);
    }

}

export namespace Medication {
    export type CreateOptions = Require<IMedication, "_id" | "name" | "dosage" | "schedule">;
    export type UpdateOptions = Require<Omit<IMedication, "schedule">, "_id">;
    export type UpdateScheduleOptions = Pick<IMedication, "_id" | "schedule">;
}

export namespace Patient {
    export type FactoryOptions = Require<IPatient, "user_id"|"full_name">;

    export interface SearchOptions {
        limit: number;
        caregivers?: database.ObjectID[];
        pacs?: database.ObjectID[];
        dob?: Date;
        name?: string;
        address?: string;
        phone?: string;
        email?: string;
    }

    export interface CareCircle {
        caregivers: CareCircle.CareGiver[];
        contacts: CareCircle.Miscellaneous[];
    }
}
