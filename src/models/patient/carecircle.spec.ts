// import "mocha";
// import * as database from "../../database";
// import { expect, sinon } from "../../testing";
 // import { assert, Unsafe } from "../../utils";
// import { Contact, IContact } from "../carecircle";


// describe("Contact", async () => {
//     let existing_contact: Contact;
//     let user_id: database.ObjectID;
//     let sandbox: sinon.SinonSandbox;

//     beforeEach(async () => {
//         sandbox = sinon.sandbox.create();
//         user_id = database.id();
//         existing_contact = createContact({ user_id });
//     });

//     afterEach(async () => {
//         sandbox.restore();
//     });

//     // Create a contact object
//     // Allow overriding of any properties
//     function createContact(object: Unsafe<IContact> = {}) {
//         return Contact.factory({
//             user_id,
//             full_name: "Mickey Mouse",
//             addresses: [{
//                 type: "home",
//                 address: "123 Fake St. Cityville DSY 99999",
//             }],
//             phones: [{
//                 type: "mobile",
//                 number: "+15559876543",
//             }],
//             emails: [{
//                 type: "home",
//                 email: "mmouse@aol.com",
//             }],
//             ...object,
//         });
//     }

//     it("should validate contact", async () => {

//         function validate(override: Unsafe<Contact>) {
//             const newcontact = Object.assign({}, existing_contact, override);
//             return Contact.validate.bind(Contact, newcontact);
//         }

//         expect(validate({})).to.not.throw();
//         expect(validate({ addresses: [] })).to.not.throw();
//         expect(validate({ phones: [] })).to.not.throw();
//         expect(validate({ emails: [] })).to.not.throw();

//         expect(validate({ _id: null })).to.throw(assert.AssertionError);
//         expect(validate({ _id: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ _id: 123123098 })).to.throw(assert.AssertionError);

//         expect(validate({ user_id: null })).to.throw(assert.AssertionError);
//         expect(validate({ user_id: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ user_id: 123123098 })).to.throw(assert.AssertionError);

//         expect(validate({ version: null })).to.throw(assert.AssertionError);
//         expect(validate({ version: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ version: 12345679 })).to.throw(assert.AssertionError);

//         expect(validate({ full_name: null })).to.throw(assert.AssertionError);
//         expect(validate({ full_name: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ full_name: 987654321 })).to.throw(assert.AssertionError);
//         expect(validate({ full_name: "" })).to.throw(assert.AssertionError);

//         expect(validate({ photo: null })).to.not.throw(assert.AssertionError);
//         expect(validate({ photo: { user: database.id(), file: database.id() } })).to.not.throw(assert.AssertionError);
//         expect(validate({ photo: {} })).to.throw(assert.AssertionError);
//         expect(validate({ photo: undefined })).to.throw(assert.AssertionError);

//         expect(validate({ addresses: null })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: {} })).to.throw(assert.AssertionError);

//         expect(validate({ phones: null })).to.throw(assert.AssertionError);
//         expect(validate({ phones: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ phones: {} })).to.throw(assert.AssertionError);

//         expect(validate({ emails: null })).to.throw(assert.AssertionError);
//         expect(validate({ emails: undefined })).to.throw(assert.AssertionError);
//         expect(validate({ emails: {} })).to.throw(assert.AssertionError);

//         const address: Contact.PhysicalAddress = { type: "home", address: "123 Fake St." };
//         expect(validate({ addresses: [address] })).to.not.throw();
//         expect(validate({ addresses: [{}] })).to.throw(assert.AssertionError);

//         expect(validate({ addresses: [{ ...address, type: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, type: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, type: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, type: "invalid" }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, type: "" }] })).to.throw(assert.AssertionError);

//         expect(validate({ addresses: [{ ...address, address: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, address: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, address: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ addresses: [{ ...address, address: "" }] })).to.throw(assert.AssertionError);

//         const number: Contact.PhoneNumber = { type: "home", number: "+15556667777" };
//         expect(validate({ phones: [number] })).to.not.throw();
//         expect(validate({ phones: [{}] })).to.throw(assert.AssertionError);

//         expect(validate({ phones: [{ ...number, type: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, type: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, type: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, type: "invalid" }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, type: "" }] })).to.throw(assert.AssertionError);

//         expect(validate({ phones: [{ ...number, number: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, number: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, number: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, number: "invalid" }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...number, number: "" }] })).to.throw(assert.AssertionError);

//         const email: Contact.EmailAddress = { type: "home", email: "mmouse@aol.com" };
//         expect(validate({ emails: [email] })).to.not.throw();
//         expect(validate({ emails: [{}] })).to.throw(assert.AssertionError);

//         expect(validate({ emails: [{ email, type: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ emails: [{ email, type: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ emails: [{ email, type: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ emails: [{ email, type: "invalid" }] })).to.throw(assert.AssertionError);
//         expect(validate({ emails: [{ email, type: "" }] })).to.throw(assert.AssertionError);

//         expect(validate({ phones: [{ ...email, email: null }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...email, email: undefined }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...email, email: 987654321 }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...email, email: "invalid" }] })).to.throw(assert.AssertionError);
//         expect(validate({ phones: [{ ...email, email: "" }] })).to.throw(assert.AssertionError);
//     });
// });
