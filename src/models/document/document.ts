import * as database from "../../database";
import * as errors from "../../errors";
import { Require, updateFrom } from "../../utils";


export class Document<T extends object> {

    // Setup indexes on the collection
    static async setupCollection(db: database.Db) {
        await Document.collection(db).createIndexes([
            { key: { "name": 1 }, unique: true },
        ]);
    }

    // Create a document instance
    static factory<T extends object>(options: Document.FactoryOptions<T>) {
        return new Document<T>(options);
    }

    // Get the collection by name
    static async get<T extends object>(db: database.Db, name: string) {
        // Get the document by name
        const document = await Document.collection(db).findOne({ name });
        if (document) { return this.factory<T>(document); }

        // Create the document if it is not found
        const new_document = this.factory<T>({ name });
        await Document.collection(db).insertOne(new_document);
        return new_document;
    }

    // Determine the documents collection
    static collection(db: database.Db) {
        return db.collection("documents");
    }

    name: string;
    version: string;
    created: Date;
    updated: Date;
    value: T;

    protected constructor(options: Document.FactoryOptions<T>) {
        this.update({
            name: options.name,
            version: options.version || database.version(),
            created: options.created || Date.current(),
            updated: options.updated || Date.current(),
            value: options.value || {} as T,
        });
    }

    // Update document values
    update(document: Partial<Document<T>>) {
        updateFrom("name", this, document);
        updateFrom("version", this, document);
        updateFrom("created", this, document);
        updateFrom("updated", this, document);
        updateFrom("value", this, document);
    }

    // Writes all fields into the database
    async save(this: Document<T>, db: database.Db) {
        return this.execute(db, { "$set": { ...this } });
    }

    // Replaces the document by executing an arbitrary query
    async execute(this: Document<T>, db: database.Db, query: any) {
        // Set values to update on the query
        query.$set = query.$set || {};
        query.$set.updated = Date.current();
        query.$set.version = database.version();

        // Replace the entire document when we make a change
        const result = await Document.collection(db).findOneAndReplace(
            { "name": this.name, "version": this.version },
            { ...query },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());
        return this.update(result.value);
    }
}


export namespace Document {
    export type FactoryOptions<T extends object> = Require<Document<T>, "name">;

    export interface Conditions {
        conditions: string[];
    }

}
