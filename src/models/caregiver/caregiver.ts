import * as database from "../../database";
import * as errors from "../../errors";
import { assert, defaultTo, Omit, Require } from "../../utils";
import { Media } from "../medialibrary";
import { Note } from "../note";
import { CareCircle, IContact, Notification, Patient } from "../patient";


export interface ICareGiver extends IContact {
    _id: database.ObjectID;
    user_id: database.ObjectID;
    pac_id: null | database.ObjectID;
    version: string;
    full_name: string;
    invites: CareGiver.Invite[];
    photo: Media.Reference | null;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];
}

export class CareGiver implements ICareGiver {

    // Perform setup for collection
    // This should be called before the database is written to
    static async setupCollection(db: database.Db) {
        await this.collection(db).createIndex("user_id", { unique: true });
    }

    // Create a caregiver object from a partial object
    // Apply defaults where possible.
    static factory(options: CareGiver.FactoryOptions) {
        return new CareGiver(options);
    }

    // Create a caregiver document in the database
    static async create(db: database.Db, options: CareGiver.FactoryOptions) {
        const caregiver = this.factory(options);
        return await this.insert(db, caregiver);
    }

    // Insert a caregiver object as a new document
    static async insert(db: database.Db, caregiver: CareGiver) {
        this.validate(caregiver);

        // Insert the documents into the database
        const result = await this.collection(db).insertOne(caregiver);
        database.assert(result.insertedCount === 1, new errors.InsertFailed());
        return caregiver;
    }

    // Get CareGivers from ids or references
    static async getAll(db: database.Db, caregivers: Array<CareGiver.IReference|database.ObjectID>) {
        const ids = caregivers.map(database.toID);
        return this.fetchMany(db, { "_id": { "$in": ids }});
    }

    // Retrieve a caregiver document
    static async fetch(db: database.Db, query: any) {
        const document: ICareGiver = await this.collection(db).findOne(query);
        if (!document) { return; }

        const caregiver = this.factory(document);
        return caregiver;
    }

    static async fetchMany(db: database.Db, query: any) {
        const documents = await this.collection(db).find(query).toArray();
        const caregivers = documents.map((doc) => this.factory(doc));
        return caregivers;
    }

    static validate(caregiver: CareGiver) {
        assert.ok(caregiver != null);
        assert.instanceOf(caregiver._id, database.ObjectID);
        assert.instanceOf(caregiver.user_id, database.ObjectID);
        assert.string(caregiver.version, { min: 1 });
        assert.string(caregiver.full_name, { min: 1 });

        if (caregiver.pac_id !== null) {
            assert.instanceOf(caregiver.pac_id, database.ObjectID);
        }

        if (caregiver.photo !== null) {
            Media.Reference.validate(caregiver.photo);
        }

        assert.array(caregiver.invites, CareGiver.Invite.validate);
        assert.array(caregiver.addresses, IContact.PhysicalAddress.validate);
        assert.array(caregiver.emails, IContact.EmailAddress.validate);
        assert.array(caregiver.phones, IContact.PhoneNumber.validate);
    }

    private static collection(db: database.Db) {
        return db.collection("caregiver");
    }

    _id: database.ObjectID;
    user_id: database.ObjectID;
    version: string;
    pac_id: null | database.ObjectID;
    full_name: string;
    invites: CareGiver.Invite[];
    photo: null | Media.Reference;
    addresses: IContact.PhysicalAddress[];
    phones: IContact.PhoneNumber[];
    emails: IContact.EmailAddress[];

    protected constructor(options: CareGiver.FactoryOptions) {
        this.update({
            _id: options._id || database.id(),
            user_id: options.user_id,
            version: options.version || database.version(),
            pac_id: options.pac_id || null,
            full_name: options.full_name,
            invites: options.invites || [],
            photo: options.photo || null,
            addresses: options.addresses || [],
            phones: options.phones || [],
            emails: options.emails || [],
        });
    }

    async save(db: database.Db) {
        CareGiver.validate(this);

        // Write to database
        const result = await CareGiver.collection(db).findOneAndReplace(
            { "_id": this._id, "version": this.version },
            { ...(this as CareGiver), "version": database.version() },
            { "returnOriginal": false });

        database.assert(result.ok === 1, new errors.UpdateFailed());
        database.assert(result.value != null, new errors.UpdateFailed());

        this.update(result.value);
        return this;
    }

    update(options: Partial<ICareGiver>) {
        this._id = defaultTo(options._id, this._id);
        this.user_id = defaultTo(options.user_id, this.user_id);
        this.version = defaultTo(options.version, this.version);
        this.pac_id = defaultTo(options.pac_id, this.pac_id);
        this.full_name = defaultTo(options.full_name, this.full_name);
        this.invites = defaultTo(options.invites, this.invites);
        this.photo = defaultTo(options.photo, this.photo);
        this.addresses = defaultTo(options.addresses, this.addresses);
        this.phones = defaultTo(options.phones, this.phones);
        this.emails = defaultTo(options.emails, this.emails);
    }

    // Find the index of the invite matching the patient
    invite_index(patient: Patient|database.ObjectID) {
        const patient_id = database.toID(patient);
        const index = this.invites.findIndex((invite) => invite._id.equals(patient_id));
        return index >= 0 ? index : undefined;
    }

    // Add an invitation to join a patient's carecircle
    add_invite(patient: Patient, relationship?: string) {
        const index = this.invite_index(patient);
        const invite = CareGiver.Invite.factory({
            _id: patient._id,
            full_name: patient.full_name || "",
            relationship: relationship || "",
        });

        if (index == null) { this.invites.push(invite); }
        else { this.invites[index] = invite; }

        return invite;
    }

    // Remove an invitation
    remove_invite(patient: Patient|database.ObjectID) {
        const index = this.invite_index(patient);
        if (index == null) { return undefined; }
        const [removed] = this.invites.splice(index, 1);
        return removed;
    }

    // Get the caregiver contact
    contact(specific?: Partial<CareCircle.ICareGiverSpecific>): CareGiver.Contact {
        const specific_o = specific || {};
        return new CareGiver.Contact(this, {
            relationship: specific_o.relationship || "",
            notes: specific_o.notes || [],
            read_discharge_docs: specific_o.read_discharge_docs || false,
            notifications: specific_o.notifications || {},
        });
    }

    // Get notification settings for patient
    notification_settings(patient: Patient) {
        const specific = patient.caregiver_specific(this);
        const notifications = specific ? specific.notifications : {};
        return Notification.Settings.factory(notifications);
    }

    // Get the caregiver reference
    reference(): CareGiver.Reference {
        return new CareGiver.Reference(this._id, this.full_name);
    }

}

export namespace CareGiver {

    // Options that can be used to create a new caregiver
    export type FactoryOptions = Require<ICareGiver, "user_id" | "full_name">;

    export interface IInvite {
        _id: database.ObjectID;
        full_name: string;
        relationship: string;
        created: Date;
    }

    export class Invite implements IInvite {
        static validate(object: IInvite) {
            assert.ok(object);
            assert.instanceOf(object._id, database.ObjectID);
            assert.string(object.full_name, { min: 1 });
            assert.string(object.relationship);
            assert.date(object.created);
        }

        static factory(object: Invite.FactoryOptions) {
            return new Invite(object._id, object.full_name || "", object.relationship || "", Date.current());
        }

        constructor(
            public readonly _id: database.ObjectID,
            public readonly full_name: string,
            public readonly relationship: string,
            public readonly created: Date) {}
    }

    export namespace Invite {
        export type FactoryOptions = Omit<IInvite, "created">;
    }

    export interface IReference {
        _id: database.ObjectID;
        full_name: string;
    }

    // CareGiver JSON reference
    // Used by API to send and receive caregiver references
    export type ReferenceJSON = Record<keyof IReference, string>;

    // A small reference to a CareGiver object
    // This object cannot be modified
    export class Reference implements IReference {

        static validate(object: IReference) {
            assert.ok(object);
            assert.instanceOf(object._id, database.ObjectID);
            assert.string(object.full_name, { min: 1 });
        }

        static factory(object: IReference) {
            return new Reference(object._id, object.full_name);
        }

        static fromJSON(json: ReferenceJSON): Reference {
            const _id = database.parseObjectID(json._id);
            if (!_id) { throw new Error("InvalidObjectID"); }
            return new Reference(_id, json.full_name);
        }

        constructor(
            public readonly _id: database.ObjectID,
            public readonly full_name: string,
        ) { }

        // Create a plain JSON representation of a caregiver reference
        toJSON(): ReferenceJSON {
            return {
                _id: this._id.toHexString(),
                full_name: this.full_name,
            };
        }

    }

    // Create a contact object from a caregiver and carecircle specific data
    export class Contact implements IContact, CareCircle.ICareGiver {
        static validate(object: Contact) {
            assert.instanceOf(object._id, database.ObjectID, "InvalidID");
            assert.string(object.full_name, { min: 1 }, "InvalidFullName");
            assert.string(object.relationship, { min: 1 }, "InvalidRelationship");
            assert.boolean(object.read_discharge_docs, "InvalidReadDischargeDocs");

            if (object.photo !== null) {
                Media.Reference.validate(object.photo);
            }

            assert.array(object.addresses, IContact.PhysicalAddress.validate, "InvalidAddress");
            assert.array(object.emails, IContact.EmailAddress.validate, "InvalidEmail");
            assert.array(object.phones, IContact.PhoneNumber.validate, "InvalidPhone");
            assert.array(object.notes, Note.validate, "InvalidNote");
            assert.array(object.notifications, Notification.Settings.validate, "InvalidNotifications");
        }

        _id: database.ObjectID;
        full_name: string;
        photo: Media.Reference | null;
        addresses: IContact.PhysicalAddress[];
        phones: IContact.PhoneNumber[];
        emails: IContact.EmailAddress[];
        relationship: string;
        notes: Note[];
        read_discharge_docs: boolean;
        notifications: Notification.ISettings;

        constructor(caregiver: CareGiver, specific: CareCircle.ICareGiverSpecific) {
            this._id = caregiver._id;
            this.full_name = caregiver.full_name;
            this.photo = caregiver.photo;
            this.addresses = caregiver.addresses;
            this.phones = caregiver.phones;
            this.emails = caregiver.emails;
            this.relationship = specific.relationship;
            this.notes = specific.notes;
            this.read_discharge_docs = specific.read_discharge_docs;
            this.notifications = Notification.Settings.factory(specific.notifications);
        }

        reference(): CareGiver.Reference {
            return new CareGiver.Reference(this._id, this.full_name);
        }
    }
}
