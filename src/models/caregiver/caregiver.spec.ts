import "mocha";
import * as config from "../../config";
import * as database from "../../database";
import * as errors from "../../errors";
import { expect, sinon } from "../../testing";
import { Unsafe } from "../../utils";
import { CareGiver, ICareGiver } from "../caregiver";
import { DeviceList, IDeviceList } from "../devicelist";
import { IUser, User } from "../user";


describe("CareGiverModel", async () => {
    let db: database.Db;
    let user_id: database.ObjectID;
    let existing_user: IUser;
    let existing_caregiver: CareGiver;
    let existing_devicelist: IDeviceList;
    let sandbox: sinon.SinonSandbox;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        user_id = new database.ObjectID();
        existing_user = User.factory({ username: "test" });
        existing_devicelist = DeviceList.factory({ user_id, devices: [] });
        existing_caregiver = CareGiver.factory({
            user_id: existing_user._id,
            full_name: "Mickey Mouse",
        });

        await db.dropDatabase();
        await database.setupCollections(db);
        await database.fixture(db, {
            user: [existing_user],
            devicelist: [existing_devicelist],
            caregiver: [existing_caregiver],
        });

        sandbox = sinon.sandbox.create();
    });

    afterEach(async () => {
        await db.dropDatabase();
        sandbox.restore();
    });

    it("should find caregiver by id", async () => {
        const expected = await CareGiver.fetch(db, { _id: existing_caregiver._id });
        const actual = await db.collection("caregiver").findOne({ _id: existing_caregiver._id });
        expect(actual).to.deep.equal(expected);
        expect(actual).to.deep.equal(existing_caregiver);
    });

    it("should find caregiver by user id", async () => {
        const expected = await CareGiver.fetch(db, { user_id: existing_user._id });
        const actual = await db.collection("caregiver").findOne({ user_id: existing_user._id });
        expect(actual).to.deep.equal(expected);
        expect(actual).to.deep.equal(existing_caregiver);
    });

    it.skip("should validate caregiver", async () => {

        // Execution needs to be deferred in order to test throwing errors
        function validate(override: Unsafe<ICareGiver>) {
            const caregiver = CareGiver.factory({ ...existing_caregiver, ...override });
            return () => CareGiver.validate(caregiver);
        }

        expect(validate({})).to.not.throw();

        expect(validate({ _id: null })).to.throw(errors.InvalidDocument);
        expect(validate({ _id: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ _id: 1000 })).to.throw(errors.InvalidDocument);

        expect(validate({ user_id: null })).to.throw(errors.InvalidDocument);
        expect(validate({ user_id: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ user_id: 1000 })).to.throw(errors.InvalidDocument);

        expect(validate({ addresses: null })).to.throw(errors.InvalidDocument);
        expect(validate({ addresses: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ addresses: {} })).to.throw(errors.InvalidDocument);

        expect(validate({ phones: null })).to.throw(errors.InvalidDocument);
        expect(validate({ phones: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ phones: {} })).to.throw(errors.InvalidDocument);

        expect(validate({ emails: null })).to.throw(errors.InvalidDocument);
        expect(validate({ emails: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ emails: {} })).to.throw(errors.InvalidDocument);

        expect(validate({ full_name: null })).to.not.throw();
        expect(validate({ full_name: "name" })).to.not.throw();
        expect(validate({ full_name: undefined })).to.throw(errors.InvalidDocument);
        expect(validate({ full_name: "" })).to.throw(errors.InvalidDocument);
        expect(validate({ full_name: {} })).to.throw(errors.InvalidDocument);

        expect.fail("Not implemented");
    });

    it("should call validate on create", async () => {
        const validate = sandbox.stub(CareGiver, "validate");
        const caregiver = await CareGiver.create(db, { user_id, full_name: "Mickey Mouse" });
        expect(validate.callCount).to.equal(1);
        expect(validate.calledWith(caregiver)).to.equal(true);
    });

});
