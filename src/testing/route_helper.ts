import * as Koa from "koa";
import * as compose from "koa-compose";
import * as Router from "koa-router";
import * as _ from "lodash";
import * as http from "node-mocks-http";
import {output} from "../middleware";
import * as utils from "../utils";


export class RouteHelper {

    // Create a mock request object
    static createRequest(options?: http.RequestOptions): http.MockRequest {
        const _request: any = http.createRequest(options);
        _request.socket = { ..._request.socket, encrypted: false };
        return _request;
    }

    // Create a mock response object
    static createResponse(options?: http.ResponseOptions): http.MockResponse {
        return http.createResponse(options);
    }

    // Create a mock response object
    static createContext(req: http.MockRequest, res: http.MockResponse) {
        const context = new Koa().createContext(req as any, res as any);
        context.request.body = {};
        context.params = {};
        return context;
    }

    constructor(public defaults: any = {}) {}

    headers(object: object, merge: boolean = false) {
        if (!merge) {
            this.set("request.headers", object);
        } else {
            this.merge("request.headers", object);
        }
    }

    // Set data into state
    state(object: object, merge: boolean = false) {
        if (!merge) {
            this.set("state", object);
        } else {
            this.merge("state", object);
        }
    }

    // Set the query string
    query(object: object, merge: boolean = false) {
        if (!merge) {
            this.set("request.query", this.json(object));
        } else {
            this.merge("request.query", this.json(object));
        }
    }

    // Set the request body
    body(object: object, merge: boolean = false) {
        if (!merge) {
            this.set("request.body", this.json(object));
        } else {
            this.merge("request.json", this.json(object));
        }
    }

    // Ensure that we are dealing with JSON the way the client would set it
    json(object: object) {
        return utils.jsonify(object);
    }

    // Set the value at path
    set(path: string, object: object) {
        this.defaults = _.set(this.defaults, path, object);
    }

    // Set a value on the context
    merge(object: object): void;
    merge(path: string, object: object): void;
    merge(first: object|string, second?: object): void {
        let toMerge: object;
        if (typeof first === "string") {
            toMerge = _.set({}, first, second);
        } else {
            toMerge = first;
        }

        this.defaults = _.merge(this.defaults, toMerge);
    }

    // Run the middleware and pass its output through our custom error function
    // This allows us to test the context as it will be output to the client
    async run(middleware: Koa.Middleware, options: http.RequestOptions = {}) {
        const _req = RouteHelper.createRequest(options);
        const _res = RouteHelper.createResponse();
        const context = RouteHelper.createContext(_req, _res);
        context.request = _.merge(context.request, this.defaults.request);
        context.state = this.defaults.state || {};

        try {
            await middleware(context, this.next.returns());
            await output()(context, this.next.returns());
        } catch (err) {
            await output()(context, this.next.throws(err));
        }

        return context;
    }

    // POST to a certain path in the router
    async post(path: string, router: Router) {
        const middleware = compose([ router.routes(), router.allowedMethods() ]);
        return await this.run(middleware, { method: "POST", path });
    }

    // PUT to a certain path in the router
    async put(path: string, router: Router) {
        const middleware = compose([router.routes(), router.allowedMethods()]);
        return await this.run(middleware, { method: "PUT", path });
    }

    async get(path: string, router: Router) {
        const middleware = compose([router.routes(), router.allowedMethods()]);
        return await this.run(middleware, { method: "GET", path });
    }

    async delete(path: string, router: Router) {
        const middleware = compose([ router.routes(), router.allowedMethods() ]);
        return await this.run(middleware, { method: "DELETE", path });
    }

    // Get a new "next" promise
    get next(): NextPromise<any> {
        return new NextPromise<any>();
    }

}


type Rejecter = (reason?: any) => void;
type Resolver = (value?: {} | PromiseLike<{}>) => void;


// Promise with easy resolution/rejection handling
export class NextPromise<T> extends Promise<T> {

    private resolve: Resolver;
    private reject: Rejecter;

    constructor() {
        let _resolve: Resolver|undefined;
        let _reject: Rejecter|undefined;

        super((resolve, reject) => {
            _resolve = resolve;
            _reject = reject;
        });

        this.resolve = _resolve!;
        this.reject = _reject!;
    }

    // Reject the promise
    throws(reason?: any): () => Promise<T> {
        this.reject(reason);
        return () => this;
    }

    // Resolve the promise
    returns(): () => Promise<T> {
        this.resolve();
        return () => this;
    }

}
