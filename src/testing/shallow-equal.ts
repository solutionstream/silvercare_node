

export function shallow(expect: any, actual: any, path: string) {

    // null value
    if (expect === null) {
        if (!(actual === null)) {
            throw new Error(`Expected to have null but got "${actual}" at path "${path}".`);
        }

        return true;
    }

    // undefined expected value
    if (typeof expect === "undefined") {
        if (typeof actual !== "undefined") {
            throw new Error(`Expected to have undefined but got "${actual}" at path "${path}".`);
        }

        return true;
    }

    // scalar description
    if (/boolean|number|string/.test(typeof expect)) {
        if (expect !== actual) {
            throw new Error(`Expected to have "${expect}" but got "${actual}" at path "${path}".`);
        }

        return true;
    }

    // dates
    if (expect instanceof Date) {
        if (actual instanceof Date) {
            if (expect.getTime() !== actual.getTime()) {
                throw new Error(`Expected to have date "${expect.toISOString()}" but got "${actual.toISOString()}" at path "${path}".`);
            }
        } else {
            throw new Error(`Expected to have date "${expect.toISOString()}" but got "${actual}" at path "${path}".`);
        }
    }

    if (actual === null) {
        throw new Error(`Expected to have an array/object but got null at path "${path}".`);
    }

    // array/object description
    for (const prop in expect) {
        if (!expect.hasOwnProperty(prop)) { continue; }
        if (typeof actual[prop] === "undefined" && typeof expect[prop] !== "undefined") {
            throw new Error(`Expected "${prop}" field to be defined at path "${path}".`);
        }

        shallow(expect[prop], actual[prop], path + (path === "/" ? "" : "/") + prop);
    }

    return true;
}
