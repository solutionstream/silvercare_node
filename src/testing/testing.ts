import * as chai from "chai";
import * as status from "http-status";
import * as sinon from "sinon";
import * as sinon_chai from "sinon-chai";
import { jsonify, Omit } from "../utils";
import { shallow } from "./shallow-equal";


chai.use(sinon_chai);

// Overwrite equality to do some custom stuff
chai.use((chai, utils) => {

    // Add the word JSON to chai's vocabulary
    chai.Assertion.addProperty("json", function(this: any) {
        utils.flag(this, "json", true);
    });

    // Add the word JSON to chai's vocabulary
    chai.Assertion.addProperty("shallow", function(this: any) {
        utils.flag(this, "shallow", true);
    });

    // Overwrite "equal" to handle our new vocabulary
    chai.Assertion.overwriteMethod("equal", (_super: any) => {
        return function jsonEqual(this: any, expect: any) {
            if (utils.flag(this, "json")) {
                if (utils.flag(this, "shallow")) {
                    new chai.Assertion(this._obj).to.shallow.equal(jsonify(expect));
                } else {
                    new chai.Assertion(this._obj).to.deep.equal(jsonify(expect));
                }
            } else if (utils.flag(this, "shallow")) {
                try {
                    shallow(expect, this._obj, "/");
                } catch (error) {
                    this.assert(false, error.message, undefined, expect, this._obj);
                }
            } else {
                _super.apply(this, arguments);
            }
        };
    });
});

// Stupid hack to get typeof Equal function (not exported)
const __equal = chai.expect("").equal;

// Add our new stuff to the chai interface in typescript
type Equal = typeof __equal;
interface HasEqual { equal: Equal; }

declare global {
    export namespace Chai {
        export interface Assert {
            json: HasEqual;
            shallow: {
                equal: Equal,
                json: HasEqual,
            };
        }

        export interface Assertion {
            json: HasEqual;
            shallow: {
                equal: Equal,
                json: HasEqual,
            };
        }
    }
}


export const expect = chai.expect;
export {sinon, status};

export type Stubbed<T, U extends keyof T = keyof T> = Omit<T, U> & Record<U, sinon.SinonStub>;
