import { email, super_user } from "../config";
import * as database from "../database";
import { Email } from "../emails";
import { User } from "../models";
import { Dashboard } from "../utils";


// Add/Remove conditions from the database
export async function create_super_user(db: database.Db) {
    const { username } = super_user;

    // Ensure that the super user account exists
    let user = await User.get(db, { username });
    if (!user) {
        user = await User.create(db, {
            username: super_user.username,
            roles: ["sv_admin", "sv_super"],
            read_tacos: true,
        });
    }

    // If the account has a password then no setup is needed
    if (user.password) { return; }

    // Issue a token to allow the user to complete account creation
    const token = await user.issueToken({});
    await user.save(db);

    // Quick and dirty email message
    const link = Dashboard.setup(user._id, { token });
    const text = `Click the following link to finish setup: ${link}`;
    const subject = "Complete Account Setup";

    await Email.send({
        to: username,
        from: email.from,
        content: { subject, text },
    });
}
