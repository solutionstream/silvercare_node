import { fixtures } from "../config";
import * as database from "../database";
import { Document } from "../models";


interface Conditions {
    conditions: string[];
}

// This is the list of conditions that will be added
const conditions_add: string[] = [
    "Alzheimer’s Disease",
    "Arthritis",
    "Chronic Obstructive Pulmonary Disease (COPD)",
    "Coronary Heart Disease",
    "Dementia",
    "Depression",
    "Diabetes",
    "Heart Failure",
    "High Cholesterol",
    "Hypertension",
    "Kidney Disease",
];

// This is the list of conditions that will be removed
const conditions_remove: string[] = [
    "condition 0",
    "condition 1",
    "condition 2",
    "condition 3",
    "condition 4",
    "condition 5",
    "condition 6",
    "condition 7",
    "condition 8",
    "condition 9",
];


// Add/Remove conditions from the database
export async function conditions(db: database.Db) {
    const document = await Document.get<Conditions>(db, fixtures.conditions);
    await document.execute(db, { "$pullAll": { "value.conditions": conditions_remove } });
    await document.execute(db, { "$addToSet": { "value.conditions": { "$each": conditions_add } } });
}
