import * as chalk from "chalk";
import * as debug from "debug";
import * as status from "http-status";
import {IMiddleware} from "koa-router";
import * as moment from "moment";
import {session} from "../config";
import {app} from "../config";
import * as database from "../database";
import * as errors from "../errors";
import {JWT} from "../jwt";
import {User} from "../models";
import {jsonify} from "../utils";
import {APIUser} from "./access_control";


interface ContextState {
    db: database.Db;
    user: User;
    api_user: APIUser;
}

const requests = debug("requests");
const requests_params = debug("requests:params");

export function log_requests(): IMiddleware {
    return async (ctx, next) => {
        const start = Date.current();

        try {
            await next();
        } finally {
            if (requests.enabled) {
                const duration = moment().diff(start, "ms");
                const status = ctx.response.status;
                const method = ctx.request.method;
                const path = `${ctx.request.path}?${ctx.request.querystring}`;
                let output = `[${start.toISOString()}] "${method} ${path}" ${status} (${duration}ms)`;

                if (requests_params.enabled) {
                    output = `${output}\n${JSON.stringify(ctx.request.body, null, 2)}`;
                }

                console.log(output);
            }
        }
    };
}

const log = debug("OUTPUT");

// Handles all application output, especially in error cases
export function output(): IMiddleware {
    return async (ctx, next) => {
        try {
            await next();

            // Treat 404 as an error when there is no body
            if (ctx.response.status === status.NOT_FOUND && !ctx.response.body) {
                ctx.throw(status.NOT_FOUND);
            }

            // Output JSON based on our custom JSON function
            if (typeof ctx.response.body === "object") {
                ctx.body = jsonify(ctx.response.body);
            }

        } catch (error) {
            // Log received errors
            log(chalk.red(error));

            // Handle "Error No Entry" messages
            if (error.code === "ENOENT") {
                error.status = status.NOT_FOUND;
            }

            // Default to 500 if the error status is something we don't understand
            if (typeof error.status !== "number" || !(status as any)[error.status]) {
                error.status = status.INTERNAL_SERVER_ERROR;
            }

            ctx.type = "application/json";
            ctx.status = error.status || status.INTERNAL_SERVER_ERROR;
            ctx.set(error.headers);

            // Expose all headers via CORS
            const headers = Object.keys(ctx.response.headers);
            ctx.set({ "Access-Control-Expose-Headers": headers.join(", ") });

            // Messages that bubble naturally will default to a 500 error code.
            // This will set the expose flag to false automatically.
            // Every other error was made intentionally so we expect that error message is okay.
            const message = error.expose ? error.message : error.name;
            if (app.debug) {
                ctx.body = jsonify({ error: message, debug: error.debug, ...error.body });
            } else {
                ctx.body = jsonify({ error: message, ...error.body });
            }
        }
    };
}


// Connect to the mongo database
export function mongo(db: database.Db): IMiddleware {
    return async (ctx, next) => {
        ctx.state.db = db;
        await next();
    };
}


// Authenticate the user request and add a `user` property on the app context
// This function expects a database to already be connected
export function authenticate(): IMiddleware {
    return async (ctx, next) => {
        try {
            // Get the authorization header
            const auth_header = ctx.get("Authorization");
            database.assert(!!auth_header, new errors.InvalidHeader());

            // Split the header
            const [type, token] = auth_header.split(" ");
            database.assert(!!type, new errors.InvalidHeader());
            database.assert(!!token, new errors.InvalidHeader());

            // Authenticate with username and password
            if (type.toLowerCase() === "basic") {
                const [username, password] = Buffer.from(token, "base64").toString().split(":");
                database.assert(!!username, new errors.NoSuchUser());
                database.assert(!!password, new errors.WrongPassword());
                ctx.state.user = await User.login(ctx.state.db, username, password);
            }

            // Authenticate using a web token
            else if (type.toLowerCase() === "bearer") {
                const payload = await JWT.verify(session.secret, token, { issuer: session.issuer, ignoreExpiration: true });
                const user = await User.login(ctx.state.db, payload.sub, payload.jti!);

                // Revoke the token if it has expired
                if (JWT.expired(payload)) {
                    await user.revokeToken(payload.jti!);
                    await user.save(ctx.state.db);
                    throw new JWT.TokenExpiredError("token revoked", payload.exp!);
                }

                // Revoke the token if it is meant for single use
                // This is not an error, but rather allows us to restrict token lifecycle
                if (payload.once) {
                    await user.revokeToken(payload.jti!);
                    await user.save(ctx.state.db);
                }

                ctx.state.user = user;
            }

            // Unknown header type
            else {
                throw errors.create(status.UNAUTHORIZED, "InvalidHeader");
            }

        } catch (error) {
            const headers = { "WWW-Authenticate": 'Basic realm="SilverCare"' };
            let locked_until: null | Date = null;

            // Specify time that the account is locked until
            if (error instanceof errors.AccountLocked) {
                locked_until = error.until;
            }

            // Passthrough for known errors
            if (error instanceof errors.AccountLocked
             || error instanceof errors.NoSuchUser
             || error instanceof errors.WrongPassword
             || error instanceof errors.NotAuthorized
             || error instanceof JWT.Error
             ) {
                ctx.throw(status.UNAUTHORIZED, error.name, {
                    headers,
                    debug: error.message,
                    body: { locked_until },
                });
            }

            // Generic Error
            ctx.throw(status.UNAUTHORIZED, status[401], {
                headers,
                debug: error.message,
                body: { locked_until },
            });
        }

        await next();
    };
}

// Initialize an APIUser object in order to enforce access controls
export function api_user(): IMiddleware {
    return async (ctx, next) => {
        const { db, user } = ctx.state as ContextState;
        ctx.state.api_user = await APIUser.get(db, user);
        await next();
    };
}

// Require the user to have a certain role in order to access a route.
// This expects a database and user object to have already been set.
export function restrict(...roles: User.Role[]): IMiddleware {
    return async (ctx, next) => {
        const { user } = ctx.state as ContextState;
        if (!user.hasRole(...roles)) {
            ctx.throw(status.FORBIDDEN, "MissingRole", {
                debug: `Missing required role: ${roles}`,
            });
        }

        await next();
    };
}
