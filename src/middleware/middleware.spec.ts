import "koa";
import "mocha";
import * as moment from "moment";
import * as config from "../config";
import * as database from "../database";
import { JWT } from "../jwt";
import { User } from "../models";
import { expect, RouteHelper, sinon, status } from "../testing";
import * as middleware from "./middleware";


describe("middleware", () => {
    let db: database.Db;
    let existing_user: User;
    let helper: RouteHelper;
    let sandbox: sinon.SinonSandbox;
    const username = "test@example.com";
    const password = "secret";

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        await db.dropDatabase();
        await database.setupCollections(db);
        existing_user = await User.create(db, { username, password });
        helper = new RouteHelper();

        sandbox = sinon.sandbox.create();
        sandbox.spy(JWT, "verify");
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });


    describe("mongo", async () => {
        it("should assign database to context", async () => {
            const context = await helper.run(middleware.mongo(db));
            expect(context.state.db).to.equal(db);
        });

        it("should throw when invalid database object provided");
        it("should throw when database is not connected");
    });

    describe("authenticate", async () => {

        // Header on unsuccessful authentication
        const www_authenticate = { "WWW-Authenticate": 'Basic realm="SilverCare"' };

        // Create a basic auth header
        function basic_auth(user: string, pass: string) {
            const credentials = new Buffer(`${user}:${pass}`).toString("base64");
            return `Basic ${credentials}`;
        }

        // Create a token header
        async function token_auth(user: string, secret: string, expired: boolean = false) {
            const expires = expired ? moment().add({ h: -1 }).toDate() : moment().add({ h: 1 }).toDate();
            const token = await JWT.sign(secret, {
                jti: database.version(),
                iss: config.session.issuer,
                sub: user,
                aud: "*",
                exp: Math.floor(expires.getTime() / 1000),
            });
            return `Bearer ${token}`;
        }

        beforeEach(async () => {
            helper.state({ db });
        });

        it("should throw when no header is provided", async () => {
            helper.headers({});

            const context = await helper.run(middleware.authenticate());
            expect(context.status).to.equal(status.UNAUTHORIZED);
            expect(context.response.headers).to.shallow.equal(www_authenticate);
            expect(context.body).to.json.equal({
                error: "InvalidHeader",
                locked_until: null,
            });
        });

        it("should throw when invalid header is provided", async () => {
            const authorization = "321324687987987654321";
            helper.headers({ authorization });

            const context = await helper.run(middleware.authenticate());
            expect(context.status).to.equal(status.UNAUTHORIZED);
            expect(context.response.headers).to.shallow.equal(www_authenticate);
            expect(context.body).to.json.equal({
                error: "InvalidHeader",
                locked_until: null,
            });
        });

        it("should throw when the account is locked", async () => {
            const authorization = basic_auth(username, password);
            helper.headers({ authorization });

            // Lock the user account
            existing_user.locked_until = moment().add({ minutes: 1 }).toDate();
            await existing_user.save(db);

            const context = await helper.run(middleware.authenticate());
            expect(context.status).to.equal(status.UNAUTHORIZED);
            expect(context.response.headers).to.shallow.equal(www_authenticate);
            expect(context.body).to.json.equal({
                error: "AccountLocked",
                locked_until: existing_user.locked_until,
            });
        });

        it("should login when account is unlocked", async () => {
            const authorization = basic_auth(username, password);
            helper.headers({ authorization });

            // Mark the account as previously locked
            existing_user.locked_until = moment().subtract({ minutes: 1 }).toDate();
            await existing_user.save(db);

            const context = await helper.run(middleware.authenticate());
            const user = await db.collection("user").findOne({ _id: existing_user._id });
            expect(context.state.user).to.deep.equal(user);
        });

        describe("basic auth", () => {
            it("should throw when user is empty", async () => {
                const authorization = basic_auth("", password);
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "NoSuchUser",
                    locked_until: null,
                });
            });

            it("should throw when password is empty", async () => {
                const authorization = basic_auth(username, "");
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "WrongPassword",
                    locked_until: null,
                });
            });

            it("should throw when there is no such user", async () => {
                const authorization = basic_auth("unknown", password);
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "NoSuchUser",
                    locked_until: null,
                });
            });

            it("should throw when the password is wrong", async () => {
                const authorization = basic_auth(username, "incorrect");
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "WrongPassword",
                    locked_until: null,
                });
            });

            it("should set user on context when logged in", async () => {
                const authorization = basic_auth(username, password);
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                const user = await db.collection("user").findOne({ _id: existing_user._id });
                expect(context.state.user).to.deep.equal(user);
            });

        });

        describe("token auth", () => {
            it("should throw when token cannot be verified", async () => {
                const authorization = await token_auth(username, "wrong key");
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "JsonWebTokenError",
                    locked_until: null,
                });
            });

            it("should throw when token contains empty user", async () => {
                const authorization = await token_auth("", config.session.secret);
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "NoSuchUser",
                    locked_until: null,
                });
            });

            it("should throw when token is not part of user object", async () => {
                const authorization = await token_auth(username, config.session.secret);
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "WrongPassword",
                    locked_until: null,
                });
            });

            it("should throw when token is expired", async () => {
                const token = await existing_user.issueToken({ expires: -9999 });
                existing_user.save(db);

                const authorization = `Bearer ${token}`;
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.status).to.equal(status.UNAUTHORIZED);
                expect(context.response.headers).to.shallow.equal(www_authenticate);
                expect(context.body).to.json.equal({
                    error: "TokenExpiredError",
                    locked_until: null,
                });
            });

            it("should authenticate successfully with token", async () => {
                const token = await existing_user.issueToken({ expires: 60 });
                await existing_user.save(db);

                const authorization = `Bearer ${token}`;
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                const user = await db.collection("user").findOne({ _id: existing_user._id });
                expect(context.state.user).to.deep.equal(user);
            });

            it("should revoke single use token", async () => {
                const jti = "5C1C2402-8B6C-4E0B-A8C3-2DF4581CD9F3";
                const token = await existing_user.issueToken({ expires: 60, once: true, id: jti });
                await existing_user.save(db);

                const authorization = `Bearer ${token}`;
                helper.headers({ authorization });

                const context = await helper.run(middleware.authenticate());
                expect(context.state.user.tokens).to.not.contain(jti);
            });
        });
    });

    describe("restrict", async () => {

        beforeEach(async () => {
            helper.state({ user: existing_user });
        });

        it("should not throw if no roles specified", async () => {
            helper.set("state.user.roles", []);

            const context = await helper.run(middleware.restrict(/* no roles */));
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.equal(undefined);
        });

        it("should not throw if user has matching role", async () => {
            helper.set("state.user.roles", ["caregiver"]);

            const context = await helper.run(middleware.restrict("caregiver", "sv_admin"));
            expect(context.status).to.equal(status.OK);
            expect(context.body).to.equal(undefined);
        });

        it("should throw if user does not have matching role", async () => {
            helper.set("state.user.roles", ["caregiver"]);

            const context = await helper.run(middleware.restrict("sv_admin"));
            expect(context.status).to.equal(status.FORBIDDEN);
            expect(context.body).to.json.equal({
                error: "MissingRole",
            });
        });
    });

});
