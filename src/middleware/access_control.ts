import * as database from "../database";
import { Activity, CareGiver, PAC, Patient, User } from "../models";
import { Omit } from "../utils";


export interface IAPIUser {
    user: User;
    patient?: Patient;
    caregiver?: CareGiver;
    pac?: PAC;
}

export type IAPIUserMethods = Omit<APIUser, keyof IAPIUser>;

export class APIUser {

    // Create an instance
    static factory(user: IAPIUser) {
        return new APIUser(
            user.user,
            user.patient,
            user.caregiver,
            user.pac);
    }

    // Create an instance by fetching records from the database
    static async get(db: database.Db, user: User) {
        const [caregiver, pac, patient] = await Promise.all([
            CareGiver.fetch(db, { user_id: user._id }),
            PAC.fetch(db, { user_id: user._id }),
            Patient.fetch(db, { user_id: user._id }),
        ]);

        return APIUser.factory({ user, caregiver, pac, patient });
    }

    constructor(
        public readonly user: User,
        public readonly patient?: Patient,
        public readonly caregiver?: CareGiver,
        public readonly pac?: PAC) { }

    // Determine whether this user is a super admin
    sv_super(): boolean {
        return this.user.hasRole("sv_super");
    }

    // Determine whether this user is an admin
    // This is anyone with sv_admin or sv_super permissions
    sv_admin(): boolean {
        return this.user.hasRole("sv_admin")
            || this.sv_super();
    }

    // Determine whether this user is the user
    user_self(user?: User|database.ObjectID): boolean {
        if (!user) { return false; }
        const user_id = database.toID(user);
        return this.user._id.equals(user_id);
    }

    // Determine whether this user is the admin of a PAC
    admin_of(pac?: PAC|database.ObjectID) {
        if (!pac) { return false; }
        const pac_id = database.toID(pac);
        return this.user._id.equals(pac_id);
    }

    // Determine whether this user is a particular patient
    patient_self(patient?: Patient|database.ObjectID): boolean {
        if (!patient || !this.patient) { return false; }

        const patient_id = database.toID(patient);
        return this.patient._id.equals(patient_id);
    }

    // Determine whether this user is a patient of a PAC or CareGiver
    patient_of(provider?: Patient|CareGiver|PAC): boolean {
        if (!provider || !this.patient) { return false; }

        if (provider instanceof Patient) {
            return this.patient_self(provider);
        }

        if (provider instanceof CareGiver) {
            return this.patient.in_carecircle(provider);
        }

        if (provider instanceof PAC) {
            return !!this.patient.pac_id
                && this.patient.pac_id.equals(provider._id);
        }

        return false;
    }

    // Determine whether this user is a particular caregiver
    caregiver_self(caregiver?: CareGiver|database.ObjectID): boolean {
        if (!caregiver || !this.caregiver) { return false; }

        const caregiver_id = database.toID(caregiver);
        return this.caregiver._id.equals(caregiver_id);
    }

    // Determine whether this user is a caregiver of a particular patient
    // A patient is always considered their own caregiver
    caregiver_to(patient?: Patient): boolean {
        if (this.patient_self(patient)) { return true; }
        if (!patient || !this.caregiver) { return false; }
        return patient.in_carecircle(this.caregiver);
    }

    // Determine whether this user is a caregiver for a particular PAC
    caregiver_of(pac?: PAC|database.ObjectID): boolean {
        if (!pac || !this.caregiver || !this.caregiver.pac_id) { return false; }

        const pac_id = database.toID(pac);
        return this.caregiver.pac_id.equals(pac_id);
    }

    // Determine whether this user is a particular pac
    pac_self(pac?: PAC|database.ObjectID): boolean {
        if (!pac || !this.pac) { return false; }

        const pac_id = database.toID(pac);
        return this.pac._id.equals(pac_id);
    }

    // Determine whether this user is a PAC admin of a PAC associated with a patient or caregiver
    pac_to(sub?: Patient|CareGiver): boolean {
        if (!sub || !sub.pac_id || !this.pac) { return false; }
        return sub.pac_id.equals(this.pac._id);
    }

    // Create an Author object based on relationship to the patient
    author_for(patient: Patient): Activity.Author {
        if (this.patient_self(patient)) {
            return Activity.Author.factory(this.patient!);
        }

        if (this.caregiver_to(patient)) {
            return Activity.Author.factory(this.caregiver!.reference());
        }

        if (this.pac_to(patient)) {
            return Activity.Author.factory({
                _id: this.user._id,
                type: "admin",
                full_name: "PAC Administrator",
            });
        }

        if (this.sv_admin()) {
            return Activity.Author.factory({
                _id: this.user._id,
                type: "admin",
                full_name: "Administrator",
            });
        }

        throw new Error("Cannot author for patient");
    }
}
