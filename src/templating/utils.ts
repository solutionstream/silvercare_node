import * as fs from "fs";
import * as path from "path";

// Read directory of files
// istanbul ignore next: utility wrapper
export async function readdir(directory: string) {
    return new Promise<string[]>((resolve, reject) => {
        fs.readdir(directory, (error, files) => {
            if (error) { return reject(error); }
            resolve(files.map((file) =>
                path.join(directory, file)));
        });
    });
}

// Get the contents of a file
// istanbul ignore next: utility wrapper
export async function readfile(file: string) {
    return new Promise<string>((resolve, reject) => {
        fs.readFile(file, (error, buffer) => {
            if (error) { return reject(error); }
            return resolve(buffer.toString());
        });
    });
}

// Determine whether a file is a template
// istanbul ignore next: utility wrapper
export function is_template(file: string) {
    return path.extname(file) === ".mustache";
}
