import * as _ from "lodash";
import * as mustache from "mustache";
import * as path from "path";
import { Dictionary } from "../utils";
import { is_template, readdir, readfile } from "./utils";

export class TemplateDirectory {

    // Default template directory
    static default_dir = `${__dirname}/templates`;

    // Easily get the default template directory
    static default(template: string) {
        return new TemplateDirectory(`${this.default_dir}/${template}`);
    }

    // Render an array of templates
    static async render(files: TemplateFile[], context: object): Promise<string[]> {
        return Promise.all(
            _.map(files, (file) => file.render(context)));
    }

    // Render a map of template and return a map of results
    static async renderMap(files: Dictionary<TemplateFile>, context: object): Promise<Dictionary<string>> {
        const names = _.map(files, (file) => file.name);
        const rendered = await Promise.all(
            _.map(files, (file) => file.render(context)));
        return _.zipObject(names, rendered);
    }

    constructor(public directory: string) { }

    // Get list of file templates from disk
    async files(): Promise<TemplateFile[]> {
        const files = await readdir(this.directory);
        const templates = _.filter(files, is_template);
        return _.map(templates, (tpl) => new TemplateFile(tpl));
    }

    // Get template map where key is the file name (without extension)
    async templates(): Promise<Dictionary<TemplateFile>> {
        const files = await this.files();
        const names = _.map(files, (file) => file.name);
        return _.zipObject(names, files);
    }

    // Get string map where the key is the template name and the content
    // is the template rendered with the provided context
    async render(context: object): Promise<Dictionary<string>> {
        const files = await this.templates();
        return TemplateDirectory.renderMap(files, context);
    }

}

export class TemplateFile {

    public path: string;
    public dir: string;
    public name: string;
    public ext: string;

    constructor(file: string) {
        this.path = file;
        this.dir = path.dirname(file);
        this.ext = path.extname(file);
        this.name = path.basename(file, this.ext);
    }

    async template(): Promise<string> {
        return readfile(this.path);
    }

    async render(context: object): Promise<string> {
        const template = await this.template();
        return Template.render(template, context);
    }
}

export class Template {
    static render(template: string, context: object): string {
        return mustache.render(template, context);
    }
}
