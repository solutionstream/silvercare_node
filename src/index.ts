import * as dotenv from "dotenv";
import * as cors from "kcors";
import * as Koa from "koa";
import * as bodyparser from "koa-bodyparser";
import * as config from "./config";
import * as database from "./database";
import * as middleware from "./middleware";
import { noauth, router} from "./routes";


(async () => {

    // Load configuration (.env) into environment
    dotenv.config();

    // Only connect to the database once and reuse the connection
    const db = await database.connect(config.db.app);
    await database.migrate(db);
    await database.setupCollections(db);

    const app = new Koa();

    app
    .use(middleware.log_requests())
    .use(middleware.output())
    .use(cors())
    .use(bodyparser())
    .use(middleware.mongo(db));

    // Routes that run before authentication is performed
    app
    .use(noauth.routes())
    .use(noauth.allowedMethods());

    // Routes that require authentication
    app
    .use(middleware.authenticate())
    .use(middleware.api_user())
    .use(router.routes())
    .use(router.allowedMethods());

    // Start server
    app.listen(8000, () => console.log(`Listening...`));
})();
