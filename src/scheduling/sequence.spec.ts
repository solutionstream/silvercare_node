import "mocha";
import * as moment from "moment";
import * as database from "../database";
import { expect } from "../testing";
import { Event } from "./event";
import { Schedule } from "./schedule";
import { EventSequence } from "./sequence";


describe("EventSequence", async () => {
    let template: TestEvent;

    beforeEach(async () => {
        template = { description: "description", assignee: "me" };
    });

    function createSequence(schedule: Schedule, template: TestEvent) {
        return EventSequence.create<TestEvent>({
            _id: new database.ObjectID(),
            schedule,
            exceptions: [],
        }, template);
    }

    function createInstance(sequence: EventSequence<TestEvent>, options: TestEvent & Event.Base & { original_date?: Date }): Event.Instance<TestEvent> {
        const { description, assignee, position, date, original_date, out_of } = options;
        return sequence.apply({
            original_date: original_date || date,
            exception_date: date,
            position,
            overrides: { description, assignee },
        }, { position, out_of, date: original_date || date })!;
    }

    describe("due date", async () => {
        // Arbitrary moment in time to schedule events
        const start = moment("2017-11-02T10:11:12.789").toDate();
        // Arbitrary moment in time later than start of events
        const check = moment("2017-11-02T11:12:13.456").toDate();
        // End of the current hour
        const end_of_hour = moment("2017-11-02T11:59:59.999").toDate();
        // End of the following hour
        const end_of_two_hours = moment("2017-11-02T12:59:59.999").toDate();
        // End of the current day
        const end_of_day = moment("2017-11-02T23:59:59.999").toDate();
        // End of the following day
        const end_of_two_days = moment("2017-11-03T23:59:59.999").toDate();
        // End of the current week (Sunday)
        const end_of_week = moment("2017-11-05T23:59:59.999").toDate();
        // End of the following week (Sunday)
        const end_of_two_weeks = moment("2017-11-12T23:59:59.999").toDate();
        // End of the current month (last day of month)
        const end_of_month = moment("2017-11-30T23:59:59.999").toDate();
        // End of the following month (last day of month)
        const end_of_two_months = moment("2017-12-31T23:59:59.999").toDate();

        it("should be due on scheduled date", async () => {
            // Event scheduled only once at specific time
            const once_1 = EventSequence.due_date(check, Schedule.once(start));
            expect(once_1).to.deep.equal(check);

            // Event scheduled daily at specific time
            const daily_1 = EventSequence.due_date(check, Schedule.daily({ start }));
            expect(daily_1).to.deep.equal(check);

            // Event scheduled every 2 days at 10 am
            const daily_2 = EventSequence.due_date(check, Schedule.daily({ start, interval: 2 }));
            expect(daily_2).to.deep.equal(check);

            // Event scheduled once a week at specific time
            const weekly_1 = EventSequence.due_date(check, Schedule.weekly({ start }));
            expect(weekly_1).to.deep.equal(check);

            // Event scheduled for every 2 weeks at specific time
            const weekly_2 = EventSequence.due_date(check, Schedule.weekly({ start, interval: 2 }));
            expect(weekly_2).to.deep.equal(check);

            // Event scheduled once a month at a specfic time
            const monthly_1 = EventSequence.due_date(check, Schedule.monthly({ start }));
            expect(monthly_1).to.deep.equal(check);

            // Event scheduled for every 2 months at specific time
            const monthly_2 = EventSequence.due_date(check, Schedule.monthly({ start, interval: 2 }));
            expect(monthly_2).to.deep.equal(check);
        });

        it("should be due at end of period", async () => {
            // Event scheduled every hour
            const hourly_1 = EventSequence.due_date(check, Schedule.hourly(start, 1));
            expect(hourly_1).to.deep.equal(end_of_hour);

            // Event scheduled every 2 hours
            const hourly_2 = EventSequence.due_date(check, Schedule.hourly(start, 2));
            expect(hourly_2).to.deep.equal(end_of_two_hours);

            // Event scheduled multiple times a day
            const daily_1 = EventSequence.due_date(check, Schedule.daily({ start, relative: true }));
            expect(daily_1).to.deep.equal(end_of_day);

            // Event scheduled once every 2 days
            const daily_2 = EventSequence.due_date(check, Schedule.daily({ start, interval: 2, relative: true }));
            expect(daily_2).to.deep.equal(end_of_two_days);

            // Event scheduled once a week without specific time
            const weekly_1 = EventSequence.due_date(check, Schedule.weekly({ start, relative: true }));
            expect(weekly_1).to.deep.equal(end_of_week);

            // Event scheduled for every 2 weeks without specific time
            const weekly_2 = EventSequence.due_date(check, Schedule.weekly({ start, interval: 2, relative: true }));
            expect(weekly_2).to.deep.equal(end_of_two_weeks);

            // Event scheduled once a month without specific time
            const monthly_1 = EventSequence.due_date(check, Schedule.monthly({ start, relative: true }));
            expect(monthly_1).to.deep.equal(end_of_month);

            // Event schedule every 2 months without specific time
            const monthly_2 = EventSequence.due_date(check, Schedule.monthly({ start, interval: 2, relative: true }));
            expect(monthly_2).to.deep.equal(end_of_two_months);
        });
    });

    it("should include last event in daily range", async () => {
        const start = moment("2017-11-01T10:00:00").toDate();
        const until = moment("2017-11-02T10:00:00").toDate();
        const schedule = Schedule.daily({ start, until });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances.length).to.equal(2);
    });

    it("should include last event in weekly range", async () => {
        const start = moment("2017-11-01T10:00:00").toDate();
        const until = moment("2017-11-08T10:00:00").toDate();
        const schedule = Schedule.weekly({ start, until });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances.length).to.equal(2);
    });

    it("should include last event in monthly range", async () => {
        const start = moment("2017-11-01T10:00:00").toDate();
        const until = moment("2017-12-01T10:00:00").toDate();
        const schedule = Schedule.monthly({ start, until });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances.length).to.equal(2);
    });

    it("should work across daylight savings time", async () => {
        const start = moment("2017-11-04T10:00:00-06:00").toDate();
        const until = moment("2017-11-05T10:00:00-07:00").toDate();
        const schedule = Schedule.daily({ start, until });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances).to.deep.equal([
            // BEFORE DST
            createInstance(sequence, {
                date: moment.utc("2017-11-04T16:00:00").toDate(),
                position: 1,
                out_of: 1,
                ...template,
            }),
            // AFTER DST
            createInstance(sequence, {
                date: moment.utc("2017-11-05T17:00:00").toDate(),
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);
    });

    it("should know when it is done");
    it("should truncate past data");

    it("should expand instances for one-off", async () => {
        const date = moment("2017-11-01T10:00:00").toDate();
        const schedule = Schedule.once(date);
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances).to.deep.equal([
            createInstance(sequence, {
                date,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);
    });

    it("should expand by times of day", async () => {
        const today = moment("2017-11-01T00:00:00").toDate();
        const ten_fifteen = moment("2017-11-01T10:15:00").toDate();
        const two_thirty = moment("2017-11-01T14:30:00").toDate();
        const six_oclock = moment("2017-11-01T18:00:00").toDate();
        const tomorrow = moment("2017-11-02T00:00:00").toDate();
        const times_in_minutes = [
            moment(ten_fifteen).diff(today, "minutes"),
            moment(two_thirty).diff(today, "minutes"),
            moment(six_oclock).diff(today, "minutes"),
        ];

        const schedule = Schedule.daily({ start: today, until: tomorrow, times_in_minutes });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances).to.deep.equal([
            createInstance(sequence, {
                date: ten_fifteen,
                position: 1,
                out_of: 1,
                ...template,
            }),
            createInstance(sequence, {
                date: two_thirty,
                position: 1,
                out_of: 1,
                ...template,
            }),
            createInstance(sequence, {
                date: six_oclock,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);
    });

    it("should expand instances for X per day", async () => {
        const today = moment("2017-11-01T10:00:00").toDate();
        const tomorrow = moment("2017-11-02T10:00:00").toDate();
        const schedule = Schedule.daily({ start: today, until: tomorrow, instances: 2 });
        const sequence = createSequence(schedule, template);

        const instances = sequence.all();
        expect(instances).to.deep.equal([
            // TODAY
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 2,
                ...template,
            }),
            createInstance(sequence, {
                date: today,
                position: 2,
                out_of: 2,
                ...template,
            }),

            // TOMORROW
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 2,
                ...template,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 2,
                out_of: 2,
                ...template,
            }),
        ]);
    });

    it("should cancel a single instance", async () => {
        const today = moment("2017-11-01T10:00:00").toDate();
        const tomorrow = moment("2017-11-02T10:00:00").toDate();
        const schedule = Schedule.daily({ start: today, until: tomorrow });
        const sequence = createSequence(schedule, template);

        // Calculate the events in the sequence
        const original = sequence.all();
        expect(original).to.deep.equal([
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 1,
                ...template,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);

        // Cancel one of the events
        sequence.addException({
            original_date: today,
            position: 1,
            cancelled: true,
        });

        // Recalculate the events in the sequence
        const modified = sequence.all();
        expect(modified).to.deep.equal([
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);
    });

    it("should cancel a date range");

    it("should reschedule single instance", async () => {
        const today = moment("2017-11-01T10:00:00").toDate();
        const tomorrow = moment("2017-11-02T10:00:00").toDate();
        const two_days = moment("2017-11-03T00:00:00").toDate();
        const schedule = Schedule.daily({ start: today, until: tomorrow, instances: 2 });
        const sequence = createSequence(schedule, template);

        // Calculate the events in the sequence
        const original = sequence.all();
        expect(original).to.deep.equal([
            // TODAY
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 2,
                ...template,
            }),
            createInstance(sequence, {
                date: today,
                position: 2,
                out_of: 2,
                ...template,
            }),

            // TOMORROW
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 2,
                ...template,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 2,
                out_of: 2,
                ...template,
            }),
        ]);

        // Schedule the event into the future
        // This tests the returned order of events regardless of the order of exceptions
        sequence.addException({
            original_date: today,
            position: 2,
            exception_date: two_days,
        });

        // Recalculate the sequence
        const modified = sequence.all();
        expect(modified).to.deep.equal([
            // TODAY
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 2,
                ...template,
            }),
            // TOMORROW
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 2,
                ...template,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 2,
                out_of: 2,
                ...template,
            }),
            // TWO DAYS
            createInstance(sequence, {
                original_date: today,
                date: two_days,
                position: 2,
                out_of: 2,
                ...template,
            }),
        ]);
    });

    it("should reschedule a date range");

    it("should change event field for single instance", async () => {
        const today = moment("2017-11-01T10:00:00").toDate();
        const tomorrow = moment("2017-11-02T10:00:00").toDate();
        const schedule = Schedule.daily({ start: today, until: tomorrow });
        const sequence = createSequence(schedule, template);

        const original = sequence.all();
        expect(original).to.deep.equal([
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 1,
                ...template,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);

        // Reassign one event
        const assignee = "not me";
        sequence.addException({
            original_date: today,
            position: 1,
            overrides: { assignee },
        });

        const modified = sequence.all();
        expect(modified).to.deep.equal([
            createInstance(sequence, {
                date: today,
                position: 1,
                out_of: 1,
                ...template,
                assignee,
            }),
            createInstance(sequence, {
                date: tomorrow,
                position: 1,
                out_of: 1,
                ...template,
            }),
        ]);
    });

    it("should change event field for date range");

    it("should reschedule positional instance (x out of y)");
    it("should reassign positional instance (x out of y");
    it("should change event field for positional instance (x out of y)");

});


// Generic event for testing event sequence
interface TestEvent {
    description: string;
    assignee: string;
}
