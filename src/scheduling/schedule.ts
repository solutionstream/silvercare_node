import * as _ from "lodash";
import * as moment from "moment";
import * as rrule from "rrule";
import { assert, exhaustiveCheck, StringMap } from "../utils";


// This is the max date according to the spec:
// http://www.ecma-international.org/ecma-262/5.1/#sec-15.9.1.1
const DATE_MAX = new Date(8640000000000000);


interface ScheduleOptions {
    instances?: number;
    relative?: boolean;
    interval?: number;
    start: Date;
    until?: Date;
    position_in_set?: number;
    days_of_month?: number[];
    days_of_week?: Schedule.Weekday[];
    times_in_minutes?: number[];
}

export interface ISchedule extends ScheduleOptions {
    // Denotes the period on which the rule is evaluated.
    // A match could occur zero or more times during the period.
    frequency: Schedule.Frequency;

    // Determine whether the schedule is relative to the period
    relative: boolean;

    // Number of instances to create per match.
    // This allows X daily|weekly|monthly type events.
    // e.g., 2 DAILY, 1 WEEKLY, 10 MONTHLY
    instances: number;

    // The interval between each period iteration.
    // e.g., every 8 hours, every 2 weeks, etc.
    interval: number;

    // The date of the first ocurrence.
    start: Date;

    // The date at which to stop recurring.
    // May or may not be an event ocurrence.
    until: Date;

    // Only instances occurring on these days will be considered.
    // The 1st, 3rd, and 7th
    days_of_month?: number[];

    // Only instances ocurring on these days will be considered.
    // e.g., every thursday, weekdays only
    days_of_week?: Schedule.Weekday[];

    // A single task can be scheduled multiple times per day
    // For example a medication that should be taken at 10:30, 2:00, and 7:15
    // These times are represented by minutes since the start of day
    times_in_minutes?: number[];
}

export interface IScheduleJSON {
    frequency: Schedule.Frequency;
    instances: number;
    relative: boolean;
    interval: number;
    start: number;
    until: number;
    days_of_month?: number[];
    days_of_week?: Schedule.Weekday[];
    times_in_minutes?: number[];
}


export class Schedule implements ISchedule {

    // Convert a frequency string to an rrule frequency
    static frequency(str: string): rrule.Frequency {
        switch (str.toLowerCase()) {
            case "hourly": return rrule.HOURLY;
            case "daily": return rrule.DAILY;
            case "weekly": return rrule.WEEKLY;
            case "monthly": return rrule.MONTHLY;
            default: throw new Error(`Invalid frequency: ${str}`);
        }
    }

    // Convert a weekday string to an rrule.Weekday
    static weekday(str: string): rrule.Weekday {
        switch (str.toLowerCase()) {
            case "mon": return rrule.MO;
            case "tue": return rrule.TU;
            case "wed": return rrule.WE;
            case "thu": return rrule.TH;
            case "fri": return rrule.FR;
            case "sat": return rrule.SA;
            case "sun": return rrule.SU;
            default: throw new Error(`Invalid weekday: ${str}`);
        }
    }

    // Convert a schedule object into RRule.Options
    static rruleOptions(schedule: Schedule): rrule.Options {
        const options: RRule.Options = { ...rrule.DEFAULT_OPTIONS };
        options.freq = Schedule.frequency(schedule.frequency);
        options.interval = schedule.interval;
        options.dtstart = schedule.start;
        options.until = schedule.until;

        if (schedule.days_of_month != null) {
            options.bymonthday = schedule.days_of_month;
        }

        if (schedule.days_of_week != null) {
            options.byweekday = _.map(schedule.days_of_week, (v) => Schedule.weekday(v));
        }

        return options;
    }

    static rrule(schedule: Schedule, cache: boolean): rrule.RRuleSet {
        const ruleset = new rrule.RRuleSet(!cache);
        const options = Schedule.rruleOptions(schedule);

        if (!schedule.times_in_minutes) {
            ruleset.rrule(new rrule.RRule(options));
        } else {
            options.dtstart = moment(options.dtstart).set({ h: 0, m: 0, s: 0, ms: 0 }).toDate();
            for (const minutes of schedule.times_in_minutes) {
                const opts_copy = { ...options };
                opts_copy.dtstart = moment(opts_copy.dtstart).add({ minutes }).toDate();
                ruleset.rrule(new rrule.RRule(opts_copy));
            }
        }

        return ruleset;
    }

    // Convert a json schedule to a proper schedule
    static fromJSON(json: IScheduleJSON): Schedule {
        return Schedule.factory(json.frequency, {
            instances: json.instances,
            interval: json.interval,
            relative: json.relative,
            start: new Date(json.start),
            until: json.until ? new Date(json.until) : undefined,
            days_of_month: json.days_of_month,
            days_of_week: json.days_of_week,
            times_in_minutes: json.times_in_minutes,
        });
    }

    // Validate the schedule
    static validate(schedule: Schedule) {
        assert.ok(schedule, "InvalidSchedule");
        assert.oneOf(schedule.frequency, Object.keys(Schedule.Frequency), "InvalidFrequency");
        assert.number(schedule.interval, { min: 1 }, "InvalidInterval");
        assert.date(schedule.start, "InvalidStart");
        assert.date(schedule.until, "InvalidUntil");
        assert.ok(schedule.start.getTime() <= schedule.until.getTime(), "InvalidDateRange");

        if (schedule.instances != null) {
            assert.number(schedule.instances, { min: 1 }, "InvalidInstances");
        }

        if (schedule.days_of_month) {
            assert.array(schedule.days_of_month, (d) => {
                assert.number(d, { min: 1, max: 31 });
            }, "InvalidDaysOfMonth");
        }

        if (schedule.days_of_week) {
            assert.array(schedule.days_of_week, (d) => {
                assert.oneOf(d, Object.keys(Schedule.Weekday));
            }, "InvalidDaysOfWeek");
        }

        if (schedule.times_in_minutes) {
            assert.array(schedule.times_in_minutes, (t) => {
                assert.number(t, { min: 0, max: 1440 });
            }, "InvalidTimesInMinutes");
        }
    }

    // Get the boundaries of the current period
    static period(date: Date, frequency: Schedule.Frequency): { start: Date, until: Date } {
        const start = Schedule.start(date, frequency);
        const until = Schedule.until(date, frequency);
        return { start, until };
    }

    // Get the boundaries of the previous period
    static last_period(date: Date, frequency: Schedule.Frequency): { start: Date, until: Date } {
        const current = Schedule.period(date, frequency);
        const end_of_last = moment(current.start).add({ ms: -1 }).toDate();
        return Schedule.period(end_of_last, frequency);
    }

    // Get the boundaries of the next period
    static next_period(date: Date, frequency: Schedule.Frequency): { start: Date, until: Date } {
        const current = Schedule.period(date, frequency);
        const start_of_next = moment(current.until).add({ ms: 1 }).toDate();
        return Schedule.period(start_of_next, frequency);
    }

    // Clamp the date to align with start of frequency period
    // e.g., start of week is Monday, and start of month is first day of the month
    static start(date: Date, frequency: Schedule.Frequency): Date {
        if (date.getTime() === DATE_MAX.getTime()) {
            return date;
        }

        switch (frequency) {
            case "hourly":
                return moment(date).startOf("hour").toDate();

            case "daily":
                return moment(date).startOf("day").toDate();

            case "weekly":
                return moment(date).startOf("isoWeek").toDate();

            case "monthly":
                return moment(date).startOf("month").toDate();

            default:
                return exhaustiveCheck(frequency);
        }
    }

    // Clamp the date to align with the end of the frequence period
    static until(date: Date, frequency: Schedule.Frequency): Date {
        if (date.getTime() === DATE_MAX.getTime()) {
            return date;
        }

        switch (frequency) {
            case "hourly":
                return moment(date).endOf("hour").toDate();

            case "daily":
                return moment(date).endOf("day").toDate();

            case "weekly":
                return moment(date).endOf("isoWeek").toDate();

            case "monthly":
                return moment(date).endOf("month").toDate();

            default:
                return exhaustiveCheck(frequency);
        }
    }

    // Apply default options
    static factory(freq: Schedule.Frequency, options: ScheduleOptions): Schedule {
        return new Schedule(freq, options);
    }

    // Create a schedule that ocurrs only once at a specified time/date
    static once(date: Date): Schedule {
        return Schedule.factory("daily", { start: date, until: date });
    }

    // Create a schedule that ocurrs every X hours
    static hourly(start: Date, interval: number = 1) {
        return Schedule.factory("hourly", {
            start,
            interval,
            relative: true,
        });
    }

    // Create a schedule that ocurrs daily
    static daily(options: ScheduleOptions): Schedule {
        return Schedule.factory("daily", options);
    }

    // Create a schedule that ocurrs weekly
    static weekly(options: ScheduleOptions): Schedule {
        return Schedule.factory("weekly", options);
    }

    // Create a schedule that ocurrs monthly
    static monthly(options: ScheduleOptions): Schedule {
        return Schedule.factory("monthly", options);
    }

    frequency: Schedule.Frequency;
    interval: number;
    start: Date;
    until: Date;
    relative: boolean;
    instances: number;
    days_of_month?: number[];
    days_of_week?: Schedule.Weekday[];
    times_in_minutes?: number[];

    protected constructor(freq: Schedule.Frequency, options: ScheduleOptions) {
        this.frequency = freq;
        this.interval = options.interval || 1;
        this.instances = options.instances || 1;
        this.start = options.start;
        this.until = options.until || DATE_MAX;
        this.relative = options.relative || false;

        // If the schedule is relative to the period then create implicit end date
        if (this.relative) {
            this.until = Schedule.until(this.until, freq);
        }

        if (options.days_of_month) {
            this.days_of_month = options.days_of_month;
        }

        if (options.days_of_week) {
            this.days_of_week = options.days_of_week;
        }

        if (options.times_in_minutes) {
            this.times_in_minutes = options.times_in_minutes;
        }
    }
}

export namespace Schedule {
    export const Frequency = StringMap("hourly", "daily", "weekly", "monthly");
    export type Frequency = keyof typeof Frequency;

    export const Weekday = StringMap("mon", "tue", "wed", "thu", "fri", "sat", "sun");
    export type Weekday = keyof typeof Weekday;
}
