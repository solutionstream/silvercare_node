import * as _ from "lodash";
import * as moment from "moment";
import * as rrule from "rrule";
import * as database from "../database";
import { exhaustiveCheck } from "../utils";
import { Event } from "./event";
import { Schedule } from "./schedule";


// An event sequence calculates event instances that have yet to happen
// It does not store instances but rather calculates them as needed from a set of rules
export class EventSequence<T> {

    // Creates a new sequence of T instances
    static create<T>(list: Event.Schedule<T>, template: T) {
        return new EventSequence<T>(list, template);
    }

    // Calculate the date that a task would be due
    static due_date(date: Date, schedule: Schedule): Date {

        // Schedules that are not relative do not offset the hour
        if (!schedule.relative) {
            return date;
        }

        // The task is due at the end of the period in which it occurs
        switch (schedule.frequency) {
            case "hourly":
                return moment(date)
                    .startOf("hour")
                    .add({ h: schedule.interval, ms: -1 })
                    .toDate();

            case "daily":
                return moment(date)
                    .startOf("day")
                    .add({ d: schedule.interval, ms: -1 })
                    .toDate();

            case "weekly":
                return moment(date)
                    .startOf("isoWeek")
                    .add({ w: schedule.interval, ms: -1 })
                    .toDate();

            case "monthly":
                return moment(date)
                    .startOf("month")
                    .add({ M: schedule.interval, ms: -1 })
                    .toDate();

            default:
                return exhaustiveCheck(schedule.frequency);
        }
    }

    protected _sequence: Event.Schedule<T>;
    protected _ruleset: rrule.RRule;
    protected _template: T;

    // Prevent external mutation of list data
    get _id(): Readonly<database.ObjectID> { return this._sequence._id; }
    get template(): Readonly<T> { return this._template; }
    get schedule(): Readonly<Schedule> { return this._sequence.schedule; }
    get exceptions(): Array<Event.Exception<T>> { return this._sequence.exceptions; }

    // An EventSequence is a superset of Event.Sequence<T>
    protected constructor(sequence: Event.Schedule<T>, template: T) {
        this._sequence = sequence;
        this._template = template;
        this._ruleset = Schedule.rrule(this.schedule, true);
    }

    // Determine whether the given date falls within this schedule
    contains(date: number|Date) {
        return moment(date).isBetween(
            this.schedule.start,
            this.schedule.until,
            "minute",
            "[]");
    }

    // Replace the sequence template
    setTemplate(template: T) {
        this._template = template;
    }

    // Replace the sequence schedule
    setSchedule(schedule: Schedule) {

        // If the schedule has changed then remove any unneeded exceptions
        if (!_.isEqual(schedule, this._sequence.schedule)) {
            const exceptions = this.exceptions.filter((ex) => {
                return moment(ex.original_date).isBetween(
                    schedule.start,
                    schedule.until,
                    "minute",
                    "[]");
            });
            this.setExceptions(exceptions);
        }

        this._sequence.schedule = schedule;
    }

    // Replace the list of exceptions
    setExceptions(exceptions: Array<Event.Exception<T>>) {
        this._sequence.exceptions = exceptions;
    }

    // Add an exception to the sequence
    addException(exception: Event.Exception<T>) {

        // Try to update an existing exception with the data for a new one
        const existing_exception = this.findException(exception.original_date, exception.position);
        if (existing_exception) {
            return _.merge(existing_exception, exception);
        }

        // Insert the exception
        this._sequence.exceptions.push(exception);
        return exception;
    }

    // Trace the date back through exceptions to get the original date
    original_date(date: number|Date): Date {
        const date_moment = moment(date);
        const exception = _.find(this.exceptions, (ex) => date_moment.isSame(ex.exception_date));
        return exception ? exception.original_date : date_moment.toDate();
    }

    // Check if a date is in the rules sequence
    in_sequence(date: number|Date): boolean {
        const start_date = moment(date).toDate();
        const next = this._ruleset.after(start_date, true);
        if (!next) { return false; }
        return next.getTime() === start_date.getTime();
    }

    // Get the next instance in this sequence
    next(date: Date, include: boolean = false): undefined | Event.Instance<T> {
        const ocurrence = this._ruleset.after(date, include);
        if (!ocurrence) { return undefined; }
        const instances = this.expand([ocurrence]);
        return instances[0];
    }

    // Get the first event instance in this sequence
    first(): undefined | Event.Instance<T> {
        return this.next(this.schedule.start, true);
    }

    // Get the instance at date and position
    at(date: number|Date, position: number): undefined | Event.Instance<T> {
        const original_date = this.original_date(date);

        // Return nothing if the original date is not in the sequence
        if (!this.in_sequence(original_date)) {
            return;
        }

        // Expand instances and apply all exceptions
        const instances = this.expand([original_date]);
        return instances.find((inst) => inst.position === position);
    }

    // Get all events between two dates
    between(start: number|Date, until: number|Date): Array<Event.Instance<T>> {
        const start_date = moment(start).toDate();
        const until_date = moment(until).toDate();
        const ocurrences = this._ruleset.between(start_date, until_date, true);
        return this.expand(ocurrences);
    }

    // Get all events in the sequence
    // This is mostly useful for unit testing, it is dangerous to call in production
    all(iterator?: (date: Date, index?: number) => void): Array<Event.Instance<T>> {
        const ocurrences = this._ruleset.all(iterator);
        return this.expand(ocurrences);
    }

    // Return a simple object to be inserted into mongo
    toPOJO(): Event.Schedule<T> {
        return {
            _id: this._id,
            schedule: this.schedule,
            exceptions: this.exceptions,
        };
    }

    // Output the schedule plus the id
    outputSchedule() {
        return {
            _id: this._id,
            ...this.schedule,
        };
    }

    // Create an event instance by applying exceptions to a base event
    // The exceptions are not checked for relevance to the instance, this is assumed to have already been done
    // Will return undefined if the instance has been cancelled
    apply(exception: undefined | Event.Exception<T>, base: Event.Base): undefined | Event.Instance<T> {
        // Merge our template and event base
        // This is essentially an instance without an address
        const partial: T & Event.Base & Event.Status = _.merge({}, this.template, base, {
            completed: false,
            due: base.date,
        });

        // Apply overrides from exception if we have one
        if (exception) {
            // Return nothing if the instance is cancelled
            if (exception.cancelled) { return undefined; }

            // Apply date and status overrides
            partial.date = exception.exception_date || partial.date;
            partial.completed = exception.completed || false;

            // Apply type specific overrides
            _.merge(partial, exception.overrides);
        }

        // Slap an address on our partial instance to create a full instance
        const instance: Event.Instance<T> = _.merge(partial, {
            due: EventSequence.due_date(partial.date, this.schedule),
            address: {
                // The id of the sequence (this) that created the instance
                sequence_id: this._id,

                // The original date before applying exceptions
                original_date: base.date,

                // The original position before applying exceptions
                position: base.position,
            },
        });

        return instance;
    }

    // Create the instances corresponding to an array of dates
    protected expand(dates: Date[]): Array<Event.Instance<T>> {

        // Expands an array of dates into an array of instances
        // There can be multiple instances on any given date
        const instances = _.flatMap(dates, (date) => {
            // Some schedules repeat the task X times per date
            // So we will repeat the creation process X times.
            const instance_count = this.schedule.instances || 1;
            const instances = _.times(instance_count, (x) => {
                const position = x + 1; // Position is 1-indexed
                const exception = this.findException(date, position);
                return this.apply(exception, {
                    date,
                    position,
                    out_of: instance_count,
                });
            });

            // Cleanup cancelled instances
            return _.compact(instances);
        });

        // Exceptions can modify dates so it is important for us to sort the instances
        // This is a stable sort so the order of repeated instances should be unaffected
        return _.sortBy(instances, (instance) => instance.date.getTime());
    }

    // Find the override for a specific instance
    protected findException(date: Date, position: number): undefined|Event.Exception<T> {
        const needle = moment(date);
        return _.find(this.exceptions, (ex) => {
            return needle.isSame(ex.original_date, "minute") && ex.position === position;
        });
    }
}
