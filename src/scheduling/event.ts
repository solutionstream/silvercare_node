import * as database from "../database";
import { ISchedule } from "./schedule";


export namespace Event {

    // Uniquely identifies a single instance
    // Serves as an "address" rather than an ID
    export interface Address {

        // The id of the series schedule used to generate an instance
        sequence_id: database.ObjectID;

        // The specific date within the series schedule
        original_date: Date;

        // The position within the instance date
        position: number;

    }

    // Base interface for an event instance
    export interface Base {

        // A schedule may have X instances per date
        // So we need to provide a X outof Y position for each one
        position: number;
        out_of: number;

        // The date that an instance is to ocurr
        date: Date;

    }

    // Instance statuses
    export interface Status {
        completed: boolean;
        due: Date;
    }

    // An instance is a single ocurrance in a series of events
    // An instance is the result of applying all scheduling and exceptions
    // The instance extends Base with all of the instance specific properties.
    export type Instance<T> = T & Base & Status & { address: Event.Address };

    // An exception overrides the values of an event instance
    // This allows us to reassign, reschedule, or cancel instances
    export interface Exception<T> {

        // The date that the original event was scheduled to happen
        original_date: Date;

        // So instance 1 of 2 can be moved without affecting the second
        position: number;

        // The date that the event is now scheduled to happen
        // If not provided it defaults to the original date of the event
        exception_date?: Date;

        // Express that the event has been cancelled
        // If not provided it is treated as false
        cancelled?: boolean;

        // Express whether the event has been completed
        // If not provided it is treated as false
        completed?: boolean;

        // Any field in T can be overriden here
        // If not provided then the instance defaults are used
        overrides?: Partial<T>;

    }

    // An event sequence contains enough information to calculate a series of instances.
    export interface Schedule<T> {

        // Unique id for this schedule
        // We need this to identify the specific instances this schedule generates
        _id: database.ObjectID;

        // The recurrence schedule for this event sequence
        // Instances are created based on these rules
        schedule: ISchedule;

        // Exceptions overriding future instances
        exceptions: Array<Event.Exception<T>>;
    }

    // An event sequence contains all of the sequence schedules
    // The series is the result of calculating all instances for all schedules
    export interface Series<T> {

        // The id for the entire series
        _id: database.ObjectID;

        // An array of series schedules
        // The series uses these to calculate all instances
        schedules: Array<Event.Schedule<T>>;

    }

}
