import "mocha";
import * as moment from "moment";
import { Schedule } from "../scheduling";
import { expect } from "../testing";
import { jsonify } from "../utils";


describe("Schedule", async () => {

    it("should parse schedule from JSON", async () => {
        const schedule = Schedule.factory("hourly", {
            instances: 1,
            relative: false,
            interval: 1,
            start: new Date(),
            until: new Date(),
            position_in_set: -1,
            days_of_month: [1, 2, 3, 4, 5, 6, 7],
            days_of_week: ["mon", "tue", "wed", "thu", "fri"],
            times_in_minutes: [600, 900, 1200],
        });

        const schedule_json = jsonify(schedule);
        const schedule_parsed = Schedule.fromJSON(schedule_json);
        expect(schedule_parsed).to.deep.equal(schedule);
    });

    // Test period methods for hourly period
    testPeriod("hourly", {
        start_of_last: moment("2017-11-01T10:00:00.000").toDate(),
        end_of_last: moment("2017-11-01T10:59:59.999").toDate(),
        start_of_current: moment("2017-11-01T11:00:00.000").toDate(),
        middle_of_current: moment("2017-11-01T11:30:30.500").toDate(),
        end_of_current: moment("2017-11-01T11:59:59.999").toDate(),
        start_of_next: moment("2017-11-01T12:00:00.000").toDate(),
        end_of_next: moment("2017-11-01T12:59:59.999").toDate(),
    });

    // Test period methods for daily period
    testPeriod("daily", {
        start_of_last: moment("2017-10-31T00:00:00.000").toDate(),
        end_of_last: moment("2017-10-31T23:59:59.999").toDate(),
        start_of_current: moment("2017-11-01T00:00:00.000").toDate(),
        middle_of_current: moment("2017-11-01T12:00:00.500").toDate(),
        end_of_current: moment("2017-11-01T23:59:59.999").toDate(),
        start_of_next: moment("2017-11-02T00:00:00.000").toDate(),
        end_of_next: moment("2017-11-02T23:59:59.999").toDate(),
    });

    // Test period methods for weekly period
    testPeriod("weekly", {
        start_of_last: moment("2017-10-23T00:00:00.000").toDate(),
        end_of_last: moment("2017-10-29T23:59:59.999").toDate(),
        start_of_current: moment("2017-10-30T00:00:00.000").toDate(),
        middle_of_current: moment("2017-11-02T12:00:00.500").toDate(),
        end_of_current: moment("2017-11-05T23:59:59.999").toDate(),
        start_of_next: moment("2017-11-06T00:00:00.000").toDate(),
        end_of_next: moment("2017-11-12T23:59:59.999").toDate(),
    });

    // Test period methods for monthly period
    testPeriod("monthly", {
        start_of_last: moment("2017-10-01T00:00:00.000").toDate(),
        end_of_last: moment("2017-10-31T23:59:59.999").toDate(),
        start_of_current: moment("2017-11-01T00:00:00.000").toDate(),
        middle_of_current: moment("2017-11-15T00:00:00.000").toDate(),
        end_of_current: moment("2017-11-30T23:59:59.999").toDate(),
        start_of_next: moment("2017-12-01T00:00:00.000").toDate(),
        end_of_next: moment("2017-12-31T23:59:59.999").toDate(),
    });

    it("should create one-time schedule");
    it("should create hourly schedule"); // relative
    it("should create daily schedule");
    it("should create weekly schedule");
    it("should create monthly schedule");
});

interface PeriodDates {
    start_of_last: Date;
    end_of_last: Date;
    start_of_current: Date;
    middle_of_current: Date;
    end_of_current: Date;
    start_of_next: Date;
    end_of_next: Date;
}

// Create a test suite for period frequency
function testPeriod(frequency: Schedule.Frequency, dates: PeriodDates) {
    describe(frequency, async () => {
        const {
            start_of_last,
            end_of_last,
            start_of_current,
            middle_of_current,
            end_of_current,
            start_of_next,
            end_of_next,
        } = dates;

        const current_period = { start: start_of_current, until: end_of_current };
        const last_period = { start: start_of_last, until: end_of_last };
        const next_period = { start: start_of_next, until: end_of_next };

        function startOf(date: Date, freq: Schedule.Frequency) {
            return expect(Schedule.start(date, freq));
        }

        function endOf(date: Date, freq: Schedule.Frequency) {
            return expect(Schedule.until(date, freq));
        }

        function periodOf(date: Date, freq: Schedule.Frequency) {
            return expect(Schedule.period(date, freq));
        }

        function lastPeriodOf(date: Date, freq: Schedule.Frequency) {
            return expect(Schedule.last_period(date, freq));
        }

        function nextPeriodOf(date: Date, freq: Schedule.Frequency) {
            return expect(Schedule.next_period(date, freq));
        }


        it("should calculate start period", async () => {
            startOf(start_of_current, frequency).deep.equals(start_of_current);
            startOf(middle_of_current, frequency).deep.equals(start_of_current);
            startOf(end_of_current, frequency).deep.equals(start_of_current);
            startOf(end_of_last, frequency).deep.equals(start_of_last);
            startOf(start_of_next, frequency).deep.equals(start_of_next);
        });

        it("should calculate end of period", async () => {
            endOf(start_of_current, frequency).deep.equals(end_of_current);
            endOf(middle_of_current, frequency).deep.equals(end_of_current);
            endOf(end_of_current, frequency).deep.equals(end_of_current);
            endOf(end_of_last, frequency).deep.equals(end_of_last);
            endOf(start_of_next, frequency).deep.equals(end_of_next);
        });

        it("should calculate period", async () => {
            periodOf(start_of_current, frequency).deep.equals(current_period);
            periodOf(middle_of_current, frequency).deep.equals(current_period);
            periodOf(end_of_current, frequency).deep.equals(current_period);
            periodOf(end_of_last, frequency).deep.equals(last_period);
            periodOf(start_of_next, frequency).deep.equals(next_period);
        });

        it("should calculate previous period", async () => {
            lastPeriodOf(start_of_current, frequency).deep.equals(last_period);
            lastPeriodOf(middle_of_current, frequency).deep.equals(last_period);
            lastPeriodOf(end_of_current, frequency).deep.equals(last_period);
            lastPeriodOf(start_of_next, frequency).deep.equals(current_period);
        });

        it("should calculate next period", async () => {
            nextPeriodOf(start_of_current, frequency).deep.equals(next_period);
            nextPeriodOf(middle_of_current, frequency).deep.equals(next_period);
            nextPeriodOf(end_of_current, frequency).deep.equals(next_period);
            nextPeriodOf(end_of_last, frequency).deep.equals(current_period);
        });
    });
}
