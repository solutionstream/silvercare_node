import * as AWS from "aws-sdk";
import * as _ from "lodash";


export class Bucket {

    public url: BucketURL;

    constructor(
        public readonly $s3: AWS.S3,
        public readonly bucket: string) {
        this.url = new BucketURL($s3, bucket);
    }

    // Get metadata for file
    async head(options: Bucket.HeadOptions) {
        const params: AWS.S3.HeadObjectRequest = {
            Bucket: this.bucket,
            Key: options.path,
        };

        const output = await this.$s3.headObject(params).promise();
        const receipt: Bucket.HeadReceipt = {
            LastModified: new Date(output.LastModified!),
            ContentType: output.ContentType!,
            ContentLength: output.ContentLength!,
            ETag: output.ETag!,
            Metadata: output.Metadata!,
        };

        return receipt;
    }

    // Get object from bucket
    async get(options: Bucket.GetOptions) {
        const params: AWS.S3.GetObjectRequest = {
            Bucket: this.bucket,
            Key: options.path,
        };

        return this.$s3.getObject(params).promise();
    }

    // Put an object into the bucket
    async put(options: Bucket.PutOptions) {
        const params: AWS.S3.PutObjectRequest = {
            Bucket: this.bucket,
            Key: options.path,
            Body: options.content,
            ContentType: options.mimetype,
            Metadata: options.meta,
        };

        return this.$s3.putObject(params).promise();
    }

    // Delete a file from the bucket
    async delete(options: Bucket.DeleteOptions) {
        const params: AWS.S3.DeleteObjectRequest = {
            Bucket: this.bucket,
            Key: options.path,
        };

        return this.$s3.deleteObject(params).promise();
    }
}


export class BucketURL {

    defaultTTL = 60;

    constructor(
        public readonly $s3: AWS.S3,
        public readonly bucket: string) { }

    // Signed URL for getting file metadata
    head(options: BucketURL.HeadOptions) {
        return this.url("headObject", {
            Bucket: this.bucket,
            Key: options.filename,
            Expires: options.ttl || this.defaultTTL,
        });
    }

    // Signed URL for getting file content
    get(options: BucketURL.GetOptions) {
        return this.url("getObject", {
            Bucket: this.bucket,
            Key: options.filename,
            Expires: options.ttl || this.defaultTTL,
        });
    }

    // Signed URL for putting file in bucket
    put(options: BucketURL.PutOptions) {
        return this.url("putObject", {
            Bucket: this.bucket,
            Key: options.filename,
            Expires: options.ttl || this.defaultTTL,
            ContentType: options.mimetype,
            ContentMD5: options.md5sum,
            Metadata: options.meta,
        });
    }

    post(options: BucketURL.PostOptions) {
        const { filename, mimetype, ttl, meta: meta_o } = options;
        const meta = _.mapKeys(meta_o!, (_, key) => `x-amz-meta-${key}`);

        return this.$s3.createPresignedPost({
            Bucket: this.bucket,
            Expires: ttl || this.defaultTTL,
            Fields: {
                "key": filename,
                "Content-Type": mimetype,
                ...meta,
            },
        });
    }

    // Signed URL for deleting file from bucket
    delete(options: BucketURL.DeleteOptions) {
        return this.$s3.getSignedUrl("deleteObject", {
            Bucket: this.bucket,
            Key: options.filename,
            Expires: options.ttl || this.defaultTTL,
        });
    }

    // Signed URL for performing the corresponding operation
    url(operation: string, params: object) {
        return this.$s3.getSignedUrl(operation, params);
    }
}

export namespace Bucket {
    interface Options {
        path: string;
    }

    export interface GetOptions extends Options {}
    export interface HeadOptions extends Options {}
    export interface DeleteOptions extends Options {}
    export interface PutOptions extends Options {
        content: Buffer;
        mimetype: string;
        meta?: { [key: string]: string };
    }

    export interface HeadReceipt {
        LastModified: Date;
        ContentType: string;
        ContentLength: number;
        ETag: string;
        Metadata: { [key: string]: string };
    }
}

export namespace BucketURL {
    interface Options {
        filename: string;
        ttl?: number;
    }

    export interface GetOptions extends Options {}
    export interface HeadOptions extends Options {}
    export interface DeleteOptions extends Options { }
    export interface PutOptions extends Options {
        mimetype: string;
        md5sum: string;
        meta?: { [key: string]: string };
    }

    export interface PostOptions extends Options {
        mimetype: string;
        meta?: { [key: string]: string };
    }
}
