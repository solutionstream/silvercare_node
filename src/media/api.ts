import * as AWS from "aws-sdk";
import * as configuration from "../config";


const { aws: { s3: config } } = configuration;
export const S3 = new AWS.S3({
    accessKeyId: config.access,
    secretAccessKey: config.secret,
    region: config.region,
});
