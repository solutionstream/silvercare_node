import * as node_assert from "assert";


export const email_re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
export const phone_re = /^\+[0-9]{1,3}[0-9]{4,14}(?:x.+)?$/;

// Throw an AssertionError
function fail(message?: string) {
    node_assert.fail(null, null, message || `Assertion failed`, "fail");
}

// Call a function:
// Throw an assertion error if the function throws an error
// Return the result of calling the function otherwise
function test<T>(f: () => T, message?: string): T {
    try {
        return f();
    } catch (e) {
        throw assert.fail(message || "" + e);
    }
}

// Test that a value is truthy
function truthy(o?: any, message?: string) {
    if (!!o) { return o; }
    throw assert.fail(message);
}

// Generic assertion
function ok(o?: any, message?: string) {
    assert.test(() => truthy(o), message);
}

// Apply a function to determine whether the condition passes
// Fails if predicate returns false
function apply(a: any, f: (a: any) => any, message?: string) {
    assert.test(() => truthy(f(a)), message);
}

// Ensure that value is an array
function array(arr: any, fn?: (a: any) => any,  message?: string) {
    ok(Array.isArray(arr), message || `${arr} is not a valid array`);

    // Apply function to all elements
    if (fn) {
        for (const elem of arr) {
            assert.test(() => fn(elem), message);
        }
    }
}

// Ensure value is boolean
function boolean(b: any, message?: string) {
    return ok(b === true || b === false, message || `${b} is not a boolean`);
}

// Ensure that value is a date
function date(d: any, message?: string) {
    return ok(Object.prototype.toString.call(d) === "[object Date]", message || `${d} is not a date`);
}

// Test that two things are equal
function equals(a?: any, b?: any, message?: string) {
    assert.ok(a === b, message);
}

// Test that string is an email address
function email(a?: any, message?: string) {
    string(a, { pattern: email_re }, message || `${a} is not an email address`);
}

// Ensure that value is a number
function number(n?: any, opts?: { min?: number, max?: number }, message?: string) {
    ok(typeof n === "number", message || `${n} is not a valid number`);
    ok(n === n, message || `${n} is not a valid number`); // NaN

    if (opts && opts.min != null) {
        ok(n >= opts.min, message || `${n} should be greater than ${opts.min}`);
    }

    if (opts && opts.max != null) {
        ok(n <= opts.max, message || `${n} should be less than ${opts.max}`);
    }
}

function string(s?: any, opts?: { min?: number, max?: number, pattern?: RegExp }, message?: string) {
    ok(typeof s === "string", message || `${s} is not a valid string`);
    if (!opts) { return; }

    if (opts.min) {
        ok(s.length >= opts.min, message || `string length should be greater than ${opts.min}`);
    }

    if (opts.max) {
        ok(s.length <= opts.max, message || `string length should be less than ${opts.max}`);
    }

    if (opts.pattern) {
        ok(s.match(opts.pattern) != null, message || `string should match pattern ${opts.pattern}`);
    }
}

function object(o?: any, message?: string) {
    ok(Object(o) === o, message || `${o} is not an object`);
}

function oneOf(t: any, array: any[], message?: string) {
    ok(array.indexOf(t) !== -1, message || `${t} should be one of ${array}`);
}

function phone(p?: any, message?: string) {
    string(p, { pattern: phone_re }, message || `${p} is not a phone number`);
}

function instanceOf(i: any, t: any, message?: string) {
    ok(i instanceof t, message || `${i} is not an instance of ${t}`);
}

// Add properties to our assert function
export const assert = Object.assign(ok, {
    AssertionError: node_assert.AssertionError,
    array,
    apply,
    boolean,
    date,
    email,
    equals,
    fail,
    instanceOf,
    number,
    object,
    ok,
    oneOf,
    phone,
    string,
    test,
});
