import * as _ from "lodash";
import * as qs from "querystring";
import { dashboard } from "../config";
import { ObjectID } from "../database";

// Shadow the built in URL type
// This avoids programming errors that I've already committed
type URL = Dashboard.URL;


// Simple class for creating dashboard urls
export class Dashboard {

    // Builds a URL from path and query object
    static url(path: string, params?: object) {
        return new Dashboard.URL(path, params);
    }

    // Link to login page
    static login(params?: { next?: URL }) {
        return Dashboard.url("/login", params);
    }

    // Link to logout page
    static logout() {
        return Dashboard.url("/logout");
    }

    // Link to profile page
    static profile() {
        return Dashboard.url("/profile");
    }

    // LInk to user page
    static user(user_id: ObjectID) {
        return Dashboard.url(`/user/${user_id}`);
    }

    // Link to account setup page
    static setup(user_id: ObjectID, params?: { next?: URL, token?: string }) {
        return Dashboard.url(`/user/${user_id}/setup`, params);
    }

    // Link to patient page
    static patient(user_id: ObjectID|"new") {
        return Dashboard.url(`/user/${user_id}/patient`);
    }

    // Link to caregiver page
    static caregiver(user_id: ObjectID|"new") {
        return Dashboard.url(`/user/${user_id}/caregiver`);
    }

    // Link to pac page
    static pac(user_id: ObjectID|"new") {
        return Dashboard.url(`/user/${user_id}/pac`);
    }
}

export namespace Dashboard {
    export class URL {

        // Creates an absolute path
        // Optionally adds domain name to the URL
        static absolute_path(path: string, fqd: boolean): string {
            return fqd ? dashboard.base_url + path : path;
        }

        // Creates a querystring
        // Recursively flattens nested Dashboard.URL objects
        static querystring(params: object) {
            const flattened = _.mapValues(params, (param) => {
                return param instanceof Dashboard.URL
                    ? param.stringify(false) // Create relative URL
                    : param.toString();      // Rely on string representation
            });

            return qs.stringify(flattened);
        }

        constructor(
            public path: string,
            public params?: object) { }

        // Creates a url string.  Optionally adds domain name to the url.
        stringify(fqd: boolean = true): string {
            const { path, params } = this;
            const url = URL.absolute_path(path, fqd);
            return params
                ? `${url}?${URL.querystring(params)}`
                : url;
        }

        // Creates an absolute url string.
        toString() {
            return this.stringify(true);
        }
    }
}
