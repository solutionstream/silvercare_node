import * as moment from "moment";

// Maps a union of strings e.g., 'a'|'b' into a map of {a: 'a', b: 'b'}
export type StringMap<T extends string> = {[K in T]: K};

// Convert a list of strings ['a', 'b'] into a map of {a: 'a', b: 'b'}
export function StringMap<T extends string>(...keys: T[]): StringMap<T> {
    return keys.reduce((acc, key) => {
        acc[key] = key;
        return acc;
    }, Object.create(null));
}

// Force exhaustiveness checking
export function exhaustiveCheck(object: never): never {
    throw new Error(`NotExhaustive: ${object}`);
}

// Get all methods of an object
export function methods(object: any) {
    const result: any = {};
    const keys = Object.getOwnPropertyNames(object)
        .filter((key) => typeof object[key] === "function" && key !== "constructor");

    for (const key of keys) {
        result[key] = object[key];
    }

    return result;
}

// Set a value with default
export function defaultTo<T>(i: undefined|T, d: T): T {
    return (typeof i === "undefined") ? d : i;
}

// Overwrite an object's property
export function updateFrom<T, K extends keyof T>(key: K, i: T, j: undefined|Partial<T>) {
    i[key] = defaultTo(j && j[key], i[key]);
}

// Return an array from an object that may have one or many
export function toArray<T>(i: T|T[]): T[] {
    return Array.isArray(i) ? i : [i];
}

// Create a delay before continuing
export async function delay(ms: number) {
    return new Promise<void>((resolve) => {
        setTimeout(resolve, ms);
    });
}

// Life a function to operate on undefined values
export function undefWrap<T, U>(fn: (o: U) => T): (o: undefined|U) => undefined|T {
    return (o: undefined|U): undefined|T => {
        if (typeof o === "undefined") { return undefined; }
        return fn(o);
    };
}

export function undefApply<T, U>(o: undefined|T, fn: (o: T) => U): undefined|U {
    return undefWrap(fn)(o);
}

export function undefMap<T, U, TT= T[]>(o: undefined|T|TT, fn: (o: T) => U): undefined|U[] {
    const o_r = undefApply(o, toArray);
    return undefApply(o_r, (o) => o.map(fn));
}

export const undef = {
    wrap: undefWrap,
    apply: undefApply,
    map: undefMap,
};

// Parse a date
export function parseDate(d: any) {
    const date_m = moment(d);
    if (!date_m.isValid()) { return undefined; }
    return date_m.toDate();
}

// Customize json parsing
export function parseJSON(string: string) {
    return JSON.parse(string);
}

// Create a regular expression for parsing ISO8601 dates
const date_re = /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})\.(\d{3})Z/;

// Customize JSON stringify
export function stringifyJSON(object: object) {
    return JSON.stringify(object, (_key, value) => {

        // Return timestamp for date strings
        if (typeof value === "string" && value.match(date_re)) {
            return new Date(value).getTime();
        }

        return value;
    });
}

// Convert any object into the JSON object that would be parsed
// This is useful mostly for unit testing
export function jsonify(object: object) {
    return JSON.parse(stringifyJSON(object));
}
