import { assert } from "./";


// A Backoff strategy is simply a number iterator
export interface BackOffStrategy {
    next(): number;
    reset(): void;
}

export interface IExponentialStrategy {
    factor: number;
    attempts: number;
}

// Implements an exponential backoff strategy
export class ExponentialStrategy implements IExponentialStrategy, BackOffStrategy {
    constructor(
        public factor: number = 2,
        public attempts: number = 0) {
        assert.number(factor, { min: 1 });
    }

    next() {
        return Math.pow(++this.attempts, this.factor);
    }

    reset() {
        this.attempts = 0;
    }
}


// Helper for implementing backoff
export class BackOff {

    // Create an exponential backoff
    static exponential(options: Partial<BackOff.Options & IExponentialStrategy> = {}) {
        const { factor, attempts, ...opts} = options;
        const strategy = new ExponentialStrategy(factor, attempts);
        return new BackOff({ ...opts, strategy });
    }

    multiplier: number;
    minDelay: number;
    maxDelay: number;
    randomness: number;
    strategy: BackOffStrategy;

    constructor(options: BackOff.Options) {
        this.multiplier = options.multiplier || 1;
        this.minDelay = options.minDelay || 1 * this.multiplier;
        this.maxDelay = options.maxDelay || Infinity;
        this.randomness = options.randomness || 0;
        this.strategy = options.strategy;

        assert.number(this.multiplier, { min: 1 });
        assert.number(this.minDelay, { min: 1 });
        assert.number(this.maxDelay, { min: 1 });
        assert.number(this.randomness, { min: 0, max: 1 });
        assert.ok(this.minDelay <= this.maxDelay, "maxDelay must be greater than minDelay");
    }

    // Calculate the next delay
    next(): number {
        const delay = this.strategy.next();
        const randomSeed = 1 + Math.random() * this.randomness;
        const randomDelay = Math.round(randomSeed * (delay * this.multiplier));
        return Math.min(Math.max(randomDelay, this.minDelay), this.maxDelay);
    }

    // Start calculating numbers from the beginning
    reset() {
        this.strategy.reset();
    }

    // Delay for the duration of the next delay
    async wait() {
        return new Promise<void>((resolve) => {
            setTimeout(resolve, this.next());
        });
    }
}


export namespace BackOff {
    export interface Options {
        minDelay?: number;
        maxDelay?: number;
        randomness?: number;
        multiplier?: number;
        strategy: BackOffStrategy;
    }
}
