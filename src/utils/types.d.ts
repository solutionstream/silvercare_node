// Express the nullability of T
export type Nullable<T> = null | T;

// Express something that can be one or many
export type Variadic<T> = T|T[];

// Remove all type safety from type T
// Keep the typesafe list of keys
export type Unsafe<T> = {
    [P in keyof T]?: any
};


// https://github.com/Microsoft/TypeScript/issues/12215
interface Obj<T> { [k: string]: T; }
type SafeObj<O extends { [k: string]: any }, Name extends string, Param extends string> = O & Obj<{[K in Name]: Param }>;
type SwitchObj<Param extends string, Name extends string, O extends Obj<any>> = SafeObj<O, Name, Param>[Param];
type Not<T extends string> = SwitchObj<T, "InvalidNotParam", {
    "1": "0";
    "0": "1";
}>;
type UnionHas<Union extends string, K extends string> = ({[S in Union]: "1" } & { [k: string]: "0" })[K];
type Obj2Keys<T> = {[K in keyof T]: K } & { [k: string]: never };
type Omit_<T extends { [s: string]: any }, K extends keyof T> =
    {[P2 in keyof T]: { 1: Obj2Keys<T>[P2], 0: never }[Not<UnionHas<K, P2>>]};

export type Omit<T extends { [s: string]: any }
    , K extends keyof T
    , T1 extends Omit_<T, K>= Omit_<T, K>
    , T1K extends keyof Pick<T1, keyof T>= keyof Pick<T1, keyof T>> =
    {[P1 in T1[T1K]]: T[P1]};


// Turn only some of the properties into optional values
export type Optional<T, U extends keyof T,
    T1 extends Omit<T, U> & Partial<Pick<T, U>>= Omit<T, U> & Partial<Pick<T, U>>> =
    {[P in keyof T1]: T1[P]};

// Make all properties optional except the ones that are specified
export type Require<T, U extends keyof T,
    T1 extends Partial<Omit<T, U>> & Pick<T, U>= Partial<Omit<T, U>> & Pick<T, U>> =
    {[P in keyof T1]: T1[P]};

export interface Dictionary<T> {
    [key: string]: T;
}
