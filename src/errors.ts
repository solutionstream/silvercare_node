import * as errors from "http-errors";
import {MongoError} from "mongodb";


// Database Operations

export class InsertFailed extends MongoError {
    constructor(message?: string) { super(message || "InsertFailed"); }
}

export class UpdateFailed extends MongoError {
    constructor(message?: string) { super(message || "UpdateFailed"); }
}

export class ReplaceFailed extends MongoError {
    constructor(message?: string) { super(message || "ReplaceFailed"); }
}

export class DeleteFailed extends MongoError {
    constructor(message?: string) { super(message || "DeleteFailed"); }
}


// Validation Errors

export class InvalidDocument extends MongoError {
    constructor(message?: string) { super(message || "InvalidDocument"); }
}


// Authentication Errors

export const create = errors;
export const HttpError = errors.HttpError;

export class NotAuthorized extends Error {
    name = "NotAuthorized";
    constructor(message?: string) { super(message || "NotAuthorized"); }
}

export class InvalidHeader extends NotAuthorized {
    name = "InvalidHeader";
    constructor() { super("InvalidHeader"); }
}

export class NoSuchUser extends NotAuthorized {
    name = "NoSuchUser";
    constructor() { super("NoSuchUser"); }
}

export class WrongPassword extends NotAuthorized {
    name = "WrongPassword";
    constructor() { super("WrongPassword"); }
}

export class AccountLocked extends NotAuthorized {
    name = "AccountLocked";
    constructor(public until: Date) { super("AccountLocked"); }
}

