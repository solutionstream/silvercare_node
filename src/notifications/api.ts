import * as AWS from "aws-sdk";
import * as configuration from "../config";


const { aws: { sns: config } } = configuration;
export const SNS = new AWS.SNS({
    region: config.region,
    accessKeyId: config.access,
    secretAccessKey: config.secret,
});
