declare namespace Expo {
    type HttpAgent = object;
    type PushToken = string;

    interface Options {
        httpAgent?: HttpAgent;
    }

    interface PushMessage {
        to: PushToken;
        data?: object;
        title?: string;
        body?: string;
        sound?: "default" | null;
        ttl?: number;
        expiration?: number;
        priority?: "default" | "normal" | "high";
        badge?: number;
    }

    interface PushReceipt {
        status: "ok" | "error";
        details?: {
            error?: "DeviceNotRegistered" | "MessageTooBig" | "MessageRateExceeded",
        };
    }
}

declare class Expo {
    static pushNotificationChunkSizeLimit: number;
    static isExpoPushToken(token: string): boolean;

    constructor(options?: Expo.Options);
    chunkPushNotifications(messages: Expo.PushMessage[]): Expo.PushMessage[][];
    sendPushNotificationAsync(message: Expo.PushMessage): Promise<Expo.PushReceipt>;
    sendPushNotificationsAsync(messages: Expo.PushMessage[]): Promise<Expo.PushReceipt[]>;
}

declare module "exponent-server-sdk" {
    export = Expo;
}
