import * as Expo from "exponent-server-sdk";
import * as _ from "lodash";
import { assert, jsonify } from "../utils";


// Everything that can be configured about push notifications
export interface IPushNotification {
    token: string;
    data: object;
    title?: string;
    body?: string;
    sound?: "default" | null;
    ttl?: number;
    expiration?: number;
    priority?: "default" | "normal" | "high";
    badge?: number;
}

// Simple wrapper for sending push notifications
export class PushNotification {

    static factory(opts: IPushNotification) {
        return new PushNotification(opts);
    }

    static validate(notification: PushNotification) {
        assert(this.validToken(notification.token));
        assert.object(notification.data);

        if (notification.title !== undefined) {
            assert.string(notification.title, { min: 0 });
        }

        if (notification.body !== undefined) {
            assert.string(notification.body, { min: 0 });
        }

        if (notification.sound !== null) {
            assert(notification.sound === "default");
        }

        if (notification.ttl !== undefined) {
            assert.number(notification.ttl, { min: 0 });
        }

        if (notification.expiration !== undefined) {
            assert.number(notification.expiration, { min: 0 });
        }

        if (notification.priority !== undefined) {
            assert.oneOf(notification.priority, ["default", "normal", "high"]);
        }

        if (notification.badge !== undefined) {
            assert.number(notification.badge, { min: 0 });
        }
    }

    static validToken(token: string | null | undefined): token is string {
        if (token == null) { return false; }
        return Expo.isExpoPushToken(token);
    }

    // Send push notifications
    static async send(options: PushNotification.Options): Promise<Expo.PushReceipt[]> {
        const expo = new Expo();

        // Create push notification objects
        const { tokens, title, body, data } = options;
        const notifications = tokens.map((token) =>
            PushNotification.factory({ token, title, body, data }));

        // Nothing to send
        if (!notifications.length) { return []; }

        // Transform and chunk the messages
        const messages = _.map(notifications, (n) => n.pushMessage());
        const messageChunks = expo.chunkPushNotifications(messages);

        // Send all of the message chunks in parallel
        const promises = _.map(messageChunks, (chunk) => expo.sendPushNotificationsAsync(chunk));
        const receiptChunks = await Promise.all(promises);

        // Collect all of the receipts
        return _.flatten(receiptChunks);
    }

    token: string;
    data: PushNotification.Data;
    title?: string;
    body?: string;
    sound: "default" | null;
    ttl?: number;
    expiration?: number;
    priority?: "default" | "normal" | "high";
    badge?: number;

    protected constructor(opts: IPushNotification) {
        this.token = opts.token;
        this.data = jsonify(opts.data);
        this.title = opts.title;
        this.body = opts.body;
        this.ttl = opts.ttl;
        this.expiration = opts.expiration;
        this.priority = opts.priority;
        this.badge = opts.badge;
    }

    // Create an expo message from this notification
    pushMessage(): Expo.PushMessage {
        return {
            to: this.token,
            title: this.title,
            body: this.body,
            sound: this.sound,
            ttl: this.ttl,
            expiration: this.expiration,
            priority: this.priority,
            badge: this.badge,
            data: {
                title: this.title,
                body: this.body,
                sound: this.sound,
                ttl: this.ttl,
                expiration: this.expiration,
                priority: this.priority,
                badge: this.badge,
                ...this.data,
            },
        };
    }
}

export namespace PushNotification {

    export type Datum = string | number | Data | null | string[] | number[] | Data[];
    export interface Data {
        [key: string]: Datum;
    }

    export interface Options {
        tokens: string[];
        title: string;
        body: string;
        data: Data;
    }

}
