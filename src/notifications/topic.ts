import * as AWS from "aws-sdk";
import * as uuid from "uuid";


export namespace Topic {
    export interface Subscriber {
        type: "sms";
        endpoint: string;
    }

    export interface Message {
        message: string;
    }
}

export class Topic {

    // Create a topic in AWS
    static async create($sns: AWS.SNS) {
        const name = uuid.v4();
        const topic = await $sns.createTopic({ Name: name }).promise();
        return new Topic($sns, topic.TopicArn!);
    }

    constructor(
        public $sns: AWS.SNS,
        public arn: string) { }

    // Subscribe to topic
    async subscribe(...subscribers: Topic.Subscriber[]) {
        const promises = subscribers.map((sub) => {
            return this.$sns.subscribe({
                TopicArn: this.arn,
                Protocol: sub.type,
                Endpoint: sub.endpoint,
            }).promise();
        });

        return await Promise.all(promises);
    }

    // Publish message to topic
    async publish(message: Topic.Message) {
        return this.$sns.publish({
            TopicArn: this.arn,
            Message: message.message,
            MessageStructure: "string",
        }).promise();
    }

    // Delete the topic
    async delete() {
        return await this.$sns.deleteTopic({
            TopicArn: this.arn,
        }).promise();
    }

}
