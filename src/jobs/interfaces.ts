import * as database from "../database";
import { StringMap } from "../utils";


// A trigger is the condition that must be met for a job to start
export interface ITrigger {
    type: ITrigger.Type;
}

export namespace ITrigger {
    export const Type = StringMap("date");
    export type Type = keyof typeof Type;
}

// Start a job when a certain date has passed
export interface IDateTrigger extends ITrigger {
    type: typeof ITrigger.Type.date;
    date: Date;
}

// A Job is a unit of work to be performed
export interface IJob {
    _id: database.ObjectID;
    status: IJob.Status;
    created: Date;
    updated: Date;
    last_error: null|{ message: string, stack: string, name: string };
    last_completed: null | Date;
    estimated_completion: null | Date;
    trigger: ITrigger;
    lockedBy: null | database.ObjectID;
    attempts: number;
}

export namespace IJob {
    export const Type = StringMap("remind_due", "alert_overdue", "overdue_count", "alert_reassign", "reset_password", "invite_pac");
    export type Type = keyof typeof Type;

    export const Status = StringMap("pending", "processing", "completed");
    export type Status = keyof typeof Status;
}

// Send reminder (proactive alert) about a task list
export interface IOverdueCountJob {
    type: typeof IJob.Type.overdue_count;
    trigger: IDateTrigger;
    tasklist_id: database.ObjectID;
    patient_id: database.ObjectID;
    most_recent: null|Date;
}

// Send reminder (proactive alert) about a task list
export interface IRemindDueJob {
    type: typeof IJob.Type.remind_due;
    trigger: IDateTrigger;
    tasklist_id: database.ObjectID;
    patient_id: database.ObjectID;
}

// Send a password reset email
export interface IResetPasswordJob {
    type: typeof IJob.Type.reset_password;
    trigger: IDateTrigger;
    user_id: database.ObjectID;
}

export interface IInvitePACJob {
    type: typeof IJob.Type.invite_pac;
    trigger: IDateTrigger;
    user_id: database.ObjectID;
}

// Send alert if task is overdue
export interface IAlertOverdueJob {
    type: typeof IJob.Type.alert_overdue;
    trigger: IDateTrigger;
    tasklist_id: database.ObjectID;
    patient_id: database.ObjectID;
    task_date: Date;
    task_position: number;
}

// Send an alert that the task as been reassigned
export interface IAlertReassignJob {
    type: typeof IJob.Type.alert_reassign;
    trigger: IDateTrigger;
    tasklist_id: database.ObjectID;
    patient_id: database.ObjectID;
    reasons: IAlertReassignJob.Reason[];
}

export namespace IAlertReassignJob {

    export const Type = StringMap("default", "instance");
    export type Type = keyof typeof Type;

    // Default assignee change
    export interface DefaultReason {
        type: typeof Type.default;
        previous: null | database.ObjectID;
        current: null | database.ObjectID;
    }

    // Instance assignee change
    export interface InstanceReason {
        type: typeof Type.instance;
        task_date: Date;
        task_position: number;
        previous: null | database.ObjectID;
        current: null | database.ObjectID;
    }

    // Unified reason type
    export type Reason = DefaultReason | InstanceReason;
}

export type IScheduledJob = IOverdueCountJob | IRemindDueJob | IResetPasswordJob | IInvitePACJob | IAlertOverdueJob | IAlertReassignJob;
export type ScheduledJob = IScheduledJob & IJob;
