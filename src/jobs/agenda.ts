import * as moment from "moment";
import * as database from "../database";
import { Job, Patient, TaskList } from "../models";
import { IAlertReassignJob, IJob } from "./interfaces";


// Helper class for easily scheduling jobs
export class Agenda {

    // Schedule a reminder for a tasklist
    static async remind_due(db: database.Db, options: Agenda.TaskListOptions) {
        return await Job.create(db, {
            type: IJob.Type.remind_due,
            tasklist_id: database.toID(options.tasklist),
            patient_id: database.toID(options.patient),
            most_recent: null,
            trigger: {
                type: "date",
                date: moment(options.date).toDate(),
            },
        });
    }

    // Schedule a job to maintain an overdue counter on a tasklist
    static async overdue_counter(db: database.Db, options: Agenda.TaskListOptions) {
        return await Job.create(db, {
            type: IJob.Type.overdue_count,
            tasklist_id: database.toID(options.tasklist),
            patient_id: database.toID(options.patient),
            most_recent: null,
            trigger: {
                type: "date",
                date: moment(options.date).toDate(),
            },
        });
    }

    // Schedule alert for overdue
    static async alert_overdue(db: database.Db, options: Agenda.AlertOverdueOptions) {
        return await Job.create(db, {
            type: IJob.Type.alert_overdue,
            tasklist_id: database.toID(options.tasklist),
            patient_id: database.toID(options.patient),
            task_date: options.task_date,
            task_position: options.task_position,
            trigger: { type: "date", date: Date.current() },
        });
    }

    // Schedule alert for task reassignment
    static async alert_reassign(db: database.Db, options: Agenda.TaskReassignOptions) {
        return await Job.create(db, {
            type: IJob.Type.alert_reassign,
            tasklist_id: database.toID(options.tasklist),
            patient_id: database.toID(options.patient),
            reasons: options.reasons,
            trigger: { type: "date", date: Date.current() },
        });
    }

    // Schedule a password reset email
    static async reset_password(db: database.Db, options: Agenda.ResetPasswordOptions) {
        return Job.create(db, {
            type: IJob.Type.reset_password,
            trigger: { type: "date", date: Date.current() },
            user_id: options.user_id,
        });
    }

    // Schedule a PAC invite email
    static async invite_pac(db: database.Db, options: Agenda.InvitePACOptions) {
        return Job.create(db, {
            type: IJob.Type.invite_pac,
            trigger: { type: "date", date: Date.current() },
            user_id: options.user_id,
        });
    }

    protected constructor() {}
}


export namespace Agenda {
    interface Schedule {
        date: number|Date;
    }


    export interface TaskListOptions extends Schedule {
        tasklist: TaskList|database.ObjectID;
        patient: Patient|database.ObjectID;
    }

    export interface AlertOverdueOptions {
        tasklist: TaskList|database.ObjectID;
        patient: Patient|database.ObjectID;
        task_date: Date;
        task_position: number;
    }

    export interface TaskReassignOptions {
        tasklist: TaskList | database.ObjectID;
        patient: Patient | database.ObjectID;
        reasons: IAlertReassignJob.Reason[];
    }

    export interface ResetPasswordOptions {
        user_id: database.ObjectID;
    }

    export interface InvitePACOptions {
        user_id: database.ObjectID;
    }
}
