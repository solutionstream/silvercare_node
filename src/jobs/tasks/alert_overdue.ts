import * as chalk from "chalk";
import * as debug from "debug";
import { IJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { CareGiver, DeviceList, Job, Patient, TaskList } from "../../models";
import { PERS } from "../../pers";
import { TemplateDirectory } from "../../templating";
import { BackOff, Dictionary } from "../../utils";


const log = debug("alert_overdue");

export class AlertOverdue {

    constructor(
        public db: database.Db) { }

    // Choose a template
    choose(template: Dictionary<string>, type: "subject" | "sms" | "email", tasklist: TaskList) {
        return template[`${tasklist.shared.type}_${type}`];
    }

    // Run the task using the specified time as "now"
    async run(backoff: BackOff, now: Date = new Date()) {
        log(chalk.cyan(`Looking for jobs ready by: ${now}`));
        const { db } = this;

        // Get a single job from the queue
        const [job] = await Job.dequeue(db, {
            type: IJob.Type.alert_overdue,
            date: now,
            limit: 1,
            timeout: 60,
        });

        // Stop if no jobs were dequeued
        if (!job || job.type !== IJob.Type.alert_overdue) {
            log(chalk.yellow(`No suitable job found: ${job}`));
            return;
        }

        try {
            // If a job was found then reset the backoff timer
            log(chalk.cyan(`[${job._id}] Started job`));
            backoff.reset();

            // Fetch the associated tasklist and patient
            const [tasklist, patient] = await Promise.all([
                TaskList.fetchOne(db, job.patient_id, job.tasklist_id),
                Patient.fetch(db, { _id: job.patient_id }),
            ]);

            // If there is no matching job then mark it as completed
            // The task will stop running as there is nothing it can do in the future
            if (!tasklist || !patient) {
                log(chalk.yellow(`[${job._id}] Tasklist or patient not found`));
                await Job.completed(db, job);
                return;
            }

            // Get the task that is supposed to be overdue
            const task = tasklist.instanceAt(job.task_date, job.task_position);

            // If there is no task at date and position then do nothing
            if (!task) {
                log(chalk.yellow(`[${job._id}] Task not found date: ${job.task_date} position: ${job.task_position}`));
                await Job.completed(db, job);
                return;
            }

            // Task must be overdue for an alert to happen
            if (!task.overdue(now)) {
                log(chalk.yellow(`[${job._id}] Task is not overdue: ${task.address}`));
                await Job.completed(db, job);
                return;
            }

            // Caregivers to notify
            let caregivers: CareGiver[] = [];

            if (task.caregiver) {
                const caregiver = await CareGiver.fetch(db, { "_id": task.caregiver._id });
                if (caregiver) { caregivers = [caregiver]; }
            } else {
                // When there is no assignee we notify all caregivers
                caregivers = await CareGiver.getAll(db, patient.carecircle.caregivers);
            }

            // Get notification settings for each patient
            caregivers = caregivers.filter((caregiver) => {
                const settings = caregiver.notification_settings(patient);
                return settings.enabled
                    && settings.task_enabled(tasklist.shared.type)
                    && settings.task_overdue(tasklist.shared.type, tasklist.overdue_count);
            });

            // Get the device lists for all of the caregivers who will receive this notification
            const devicelists = await DeviceList.getAll(db, caregivers.map((caregiver) => caregiver.user_id));
            const devicelist = DeviceList.merge(devicelists);

            // Render templates in order to determine notifications
            const directory = TemplateDirectory.default("task_overdue");
            const template = await directory.render({ tasklist, patient });

            // Send push notification to mobile devices
            const push_notifications = devicelist.push({
                title: this.choose(template, "subject", tasklist),
                body: tasklist.shared.title,
                data: {
                    type: "task_overdue",
                    address: task.address,
                },
            });

            // Send SMS notification to phone numbers
            const sms_notifications = devicelist.sms({
                message: this.choose(template, "sms", tasklist),
            });

            // Send email notificaiton to email addresses
            const email_notifications = devicelist.email({
                message: {
                    html: this.choose(template, "email", tasklist),
                    subject: this.choose(template, "subject", tasklist),
                },
            });

            // Send notification to PERS devices
            const pers_notifications = devicelist.pers({
                message: {
                    vibrate: true,
                    action: PERS.Message.GENERIC,
                },
            });

            // Wait for all notifications to complete
            await Promise.all([
                push_notifications,
                sms_notifications,
                email_notifications,
                pers_notifications,
            ]);

            // We are done!
            await Job.completed(db, job);

        } catch (error) {

            log(chalk.red(error));
            await Job.failed(db, job, error);
            throw error;

        }

    }

}

/* istanbul ignore if */
if (require.main === module) {
    (async () => {
        const backoff = BackOff.exponential({ multiplier: 100, maxDelay: 60000 });
        const db = await database.connect(config.db.app);
        const job = new AlertOverdue(db);

        while (true) {
            await backoff.wait();
            await job.run(backoff);
        }
    })()
    .then(() => process.exit())
    .catch((error) => { log(error); process.exit(1); });
}
