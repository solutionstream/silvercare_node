import * as chalk from "chalk";
import * as debug from "debug";
import * as moment from "moment";
import { IJob, IRemindDueJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { CareGiver, DeviceList, Job, Patient, Task, TaskList } from "../../models";
import { PERS } from "../../pers";
import { TemplateDirectory } from "../../templating";
import { BackOff, Dictionary } from "../../utils";


const log = debug("remind_due");

export class RemindDue {

    constructor(
        public db: database.Db) { }

    // Schedule the job to run again
    async schedule(db: database.Db, job: IRemindDueJob & IJob, next_run: undefined | Date) {
        if (next_run) {
            await Job.reschedule(db, job, next_run);
            log(chalk.green(`Scheduled job to run again: ${next_run}`));
        } else {
            log(chalk.yellow(`Tasklist does not need to run anymore`));
            await Job.completed(db, job);
        }
    }

    // Determine when to next run this job
    next_run(task: Task, tasklist: TaskList): undefined|Date {
        const next = tasklist.next(task.date);
        if (!next) { return undefined; }

        // Run within the reminder period
        return moment(next.due)
            .add({ m: -config.task.remind_before_mins })
            .toDate();
    }

    // Create display for date
    format_date(date: Date) {
        const day = moment(date).format("MMM D");
        const time = moment(date).format("h:mma");
        return `${day} at ${time}`;
    }

    // Choose a template
    choose(template: Dictionary<string>, type: "subject"|"sms"|"email", tasklist: TaskList) {
        return template[`${tasklist.shared.type}_${type}`];
    }

    // Run the task using the specified time as "now"
    async run(backoff: BackOff, now: Date = new Date()) {
        log(chalk.cyan(`Looking for jobs ready by: ${now}`));
        const { db } = this;

        // Get a single job from the queue
        const [job] = await Job.dequeue(db, {
            type: IJob.Type.remind_due,
            date: now,
            limit: 1,
            timeout: 60,
        });

        // Stop if no jobs were dequeued
        if (!job || job.type !== IJob.Type.remind_due) {
            log(chalk.yellow(`No suitable job found: ${job}`));
            return;
        }

        try {
            // If a job was found then reset the backoff timer
            log(chalk.cyan(`[${job._id}] Started job`));
            backoff.reset();

            // Fetch the associated tasklist and patient
            const [tasklist, patient] = await Promise.all([
                TaskList.fetchOne(db, job.patient_id, job.tasklist_id),
                Patient.fetch(db, { _id: job.patient_id }),
            ]);

            // If there is no matching job then mark it as completed
            // The task will stop running as there is nothing it can do in the future
            if (!tasklist || !patient) {
                log(chalk.yellow(`[${job._id}] Tasklist or patient not found`));
                await this.schedule(db, job, undefined);
                return;
            }

            // Get the next task in the tasklist
            const task = tasklist.next(now);

            // If there is no next task then the tasklist has ended
            if (!task) {
                log(chalk.yellow(`[${job._id}] No upcoming tasks`));
                await this.schedule(db, job, undefined);
                return;
            }

            // Calculate the next time this task should run
            const next_run = this.next_run(task, tasklist);

            // If task has already been completed then we don't send the reminder
            if (task.completed) {
                log(chalk.yellow(`[${job._id}] Task is already completed`));
                await this.schedule(db, job, next_run);
                return;
            }

            // Determine if we are within the reminder window for the task
            // const remind_start = moment(task.due).add({ m: -config.task.remind_before_mins });
            // if (!moment(now).isSameOrAfter(remind_start) || !moment(now).isBefore(task.due)) {
            //     log(chalk.yellow(`[${job._id}] We are not within the notification period`));
            //     await this.schedule(db, job, next_run);
            //     return;
            // }

            // Caregivers to notify
            let caregivers: CareGiver[] = [];

            if (task.caregiver) {
                const caregiver = await CareGiver.fetch(db, { "_id": task.caregiver._id });
                if (caregiver) { caregivers = [caregiver]; }
            } else {
                // When there is no assignee we notify all caregivers
                caregivers = await CareGiver.getAll(db, patient.carecircle.caregivers);
            }

            // Get notification settings for each patient
            caregivers = caregivers.filter((caregiver) => {
                const settings = caregiver.notification_settings(patient);
                return settings.enabled
                    && settings.task_enabled(tasklist.shared.type)
                    && settings.task_due(tasklist.shared.type);
            });

            // Get the device lists for all of the caregivers who will receive this notification
            const devicelists = await DeviceList.getAll(db, caregivers.map((caregiver) => caregiver.user_id));
            const devicelist = DeviceList.merge(devicelists);

            // Render templates in order to determine notifications
            const directory = TemplateDirectory.default("remind_due");
            const template = await directory.render({
                tasklist,
                patient,
                tasklist_due: this.format_date(task.due),
            });

            // Send push notification to mobile devices
            const push_notifications = devicelist.push({
                title: this.choose(template, "subject", tasklist),
                body: tasklist.shared.title,
                data: {
                    type: "remind_due",
                    address: task.address,
                },
            });

            // Send SMS notification to phone numbers
            const sms_notifications = devicelist.sms({
                message: this.choose(template, "sms", tasklist),
            });

            // Send email notificaiton to email addresses
            const email_notifications = devicelist.email({
                message: {
                    html: this.choose(template, "email", tasklist),
                    subject: this.choose(template, "subject", tasklist),
                },
            });

            // Send notification to PERS devices
            const pers_notifications = devicelist.pers({
                message: {
                    vibrate: true,
                    action: tasklist.shared.type === "medication"
                        ? PERS.Message.TAKE_MEDICATION
                        : PERS.Message.GENERIC,
                },
            });

            // Wait for all notifications to complete
            await Promise.all([
                push_notifications,
                sms_notifications,
                email_notifications,
                pers_notifications,
            ]);

            // We are done!
            await this.schedule(db, job, next_run);

        } catch (error) {

            log(chalk.red(error));
            await Job.failed(db, job, error);
            throw error;

        }

    }

}

/* istanbul ignore if */
if (require.main === module) {
    (async () => {
        const backoff = BackOff.exponential({ multiplier: 100, maxDelay: 60000 });
        const db = await database.connect(config.db.app);
        const job = new RemindDue(db);

        while (true) {
            await backoff.wait();
            await job.run(backoff);
        }
    })()
    .then(() => process.exit())
    .catch((error) => { log(error); process.exit(1); });
}
