import "mocha";
import * as moment from "moment";
import { IAlertOverdueJob, IJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Email } from "../../emails";
import { CareGiver, Device, DeviceList, Job, Patient, Task, TaskList } from "../../models";
import { PushNotification } from "../../notifications";
import { PERS } from "../../pers";
import { Schedule } from "../../scheduling";
import { SMS } from "../../sms";
import { TemplateDirectory } from "../../templating";
import { expect, sinon } from "../../testing";
import { BackOff, jsonify } from "../../utils";
import { AlertOverdue } from "./alert_overdue";


describe("AlertOverdue", async () => {
    let db: database.Db;
    let sandbox: sinon.SinonSandbox;
    let render: sinon.SinonSpy;
    let now: Date;
    let start: Date;
    let task: AlertOverdue;
    let backoff: BackOff;
    let patient: Patient;
    let caregiver_1: CareGiver;
    let caregiver_2: CareGiver;
    let patient_devices: DeviceList;
    let caregiver_1_devices: DeviceList;
    let caregiver_2_devices: DeviceList;
    let ios_device: Device.Mobile;
    let android_device: Device.Mobile;
    let sms_device: Device.SMS;
    let email_device: Device.Email;
    let pers_device: Device.PERS;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        start = moment().toDate();
        now = moment(start).add({ m: 90 }).toDate();
        task = new AlertOverdue(db);
        backoff = BackOff.exponential();

        // Stubs and spies
        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);
        sandbox.stub(Email, "send");
        sandbox.stub(SMS, "send");
        sandbox.stub(PERS, "alertevent");
        sandbox.stub(PushNotification, "send");
        sandbox.spy(Job, "completed");
        sandbox.spy(Job, "failed");
        sandbox.spy(Job, "dequeue");
        sandbox.spy(backoff, "next");
        sandbox.spy(backoff, "reset");
        render = sandbox.spy(TemplateDirectory.prototype, "render");

        // Database fixtures
        ios_device = {
            _id: database.id(),
            type: "ios",
            created: now,
            version: { major: 0, minor: 0, patch: 0 },
            push_token: "ExponentPushToken[ios]",
        };

        android_device = {
            _id: database.id(),
            type: "ios",
            created: now,
            version: { major: 0, minor: 0, patch: 0 },
            push_token: "ExponentPushToken[android]",
        };

        sms_device = {
            _id: database.id(),
            type: "sms",
            created: now,
            phone_number: "+18015551234",
        };

        email_device = {
            _id: database.id(),
            type: "email",
            created: now,
            email_address: "1@example.com",
        };

        pers_device = {
            _id: database.id(),
            type: "pers",
            created: now,
            identifier: "asdf",
        };

        // Create a caregiver with iOS and email devices
        caregiver_1 = CareGiver.factory({ user_id: database.id(), full_name: "1" });
        caregiver_1_devices = DeviceList.factory({
            user_id: caregiver_1.user_id,
            devices: [ ios_device, email_device ],
        });

        // Create a caregiver with android and SMS devices
        caregiver_2 = CareGiver.factory({ user_id: database.id(), full_name: "2" });
        caregiver_2_devices = DeviceList.factory({
            user_id: caregiver_2.user_id,
            devices: [ android_device, sms_device ],
        });

        // Create patient and devicelist
        patient = Patient.factory({
            _id: database.id(),
            user_id: database.id(),
            full_name: "Patient",
            carecircle: {
                contacts: [],
                caregivers: [ caregiver_1.contact(), caregiver_2.contact() ],
            },
        });
        patient_devices = DeviceList.factory({
            user_id: patient.user_id,
            devices: [ pers_device ],
        });

        // Setup and insert data
        await database.setupCollections(db);
        await Promise.all([
            Patient.insert(db, patient),
            CareGiver.insert(db, caregiver_1),
            CareGiver.insert(db, caregiver_2),
            DeviceList.create(db, patient_devices),
            DeviceList.create(db, caregiver_1_devices),
            DeviceList.create(db, caregiver_2_devices),
        ]);
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    interface CreateJob {
        trigger: Date;
        tasklist: TaskList;
        task?: Task;
        patient?: Patient|database.ObjectID;
    }

    // Create a job object
    function createJob(opts: CreateJob): IAlertOverdueJob & IJob {
        const task = opts.task || opts.tasklist.first();
        if (!task) { throw new Error("No valid task"); }

        const job = Job.factory({
            type: "alert_overdue",
            trigger: { type: "date", date: opts.trigger },
            tasklist_id: database.toID(opts.tasklist),
            patient_id: database.toID(opts.patient || opts.tasklist.patient_id),
            task_date: task.address.original_date,
            task_position: task.address.position,
        });

        if (job.type !== "alert_overdue") {
            throw new Error("Shut up typescript");
        }

        return job;
    }

    interface CreateTasklist {
        type?: Task.Type;
        schedule: Schedule;
        patient: Patient|database.ObjectID;
        overdue_count?: number;
    }

    // Create a tasklist object
    function createTasklist(opts: CreateTasklist): TaskList {
        return TaskList.factory({
            patient_id: database.toID(opts.patient),
            schedule: opts.schedule,
            shared: { title: "Test", type: opts.type || "other" },
            medication_id: opts.type === "medication" ? database.id() : undefined,
            overdue_count: opts.overdue_count || 0,
        });
    }

    interface ScheduleJob {
        type?: Task.Type;
        schedule: Schedule;
        patient: Patient|database.ObjectID;
        overdue_count?: number;
    }

    // Create an overdue tasklist
    async function scheduleJob(opts: ScheduleJob) {
        const tasklist = createTasklist(opts);
        const job = createJob({
            trigger: now,
            tasklist,
            patient: tasklist.patient_id });

        await Promise.all([
            TaskList.insert(db, tasklist),
            Job.insert(db, job),
        ]);

        return { job, tasklist };
    }

    it("should do nothing if no job is found", async () => {
        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(0);
        expect(backoff.reset).to.have.callCount(0);
    });

    it("should reset backoff object if job is found", async () => {
        await scheduleJob({
            patient,
            schedule: Schedule.once(start),
        });

        await task.run(backoff, now);
        expect(backoff.reset).to.have.callCount(1);
    });

    it("should complete job if patient is not found", async () => {
        await scheduleJob({
            patient: database.id(),
            schedule: Schedule.once(start),
        });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
    });

    it("should complete job if tasklist not found", async () => {
        const tasklist = createTasklist({ patient, schedule: Schedule.once(start) });
        const job = createJob({ trigger: now, tasklist });
        await Job.insert(db, job);

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
    });

    it("should complete job if task is not found", async () => {
        const tasklist = createTasklist({ patient, schedule: Schedule.once(start) });
        const job = createJob({ tasklist, trigger: now });

        await Promise.all([
            TaskList.insert(db, tasklist),
            Job.insert(db, job),
        ]);

        // Cancel the task before running the job
        await tasklist.updateOne(db, tasklist.first()!, { cancelled: true });

        // Run the job with a cancelled task
        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
    });

    it("should complete job if task is not overdue", async () => {
        await scheduleJob({ patient, schedule: Schedule.once(now) });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
    });

    // check notification content by type
    function should_notify(type: Task.Type) {
        return async () => {
            const { tasklist } = await scheduleJob({ type, patient, schedule: Schedule.once(start) });

            await task.run(backoff, now);
            const template = await render.returnValues[0];
            expect(Job.completed).to.have.callCount(1);

            // Verify that push notifications are sent
            expect(PushNotification.send).to.have.callCount(1);
            expect(PushNotification.send).to.have.been.calledWith({
                tokens: [ios_device.push_token, android_device.push_token],
                title: template[`${type}_subject`],
                body: tasklist.shared.title,
                data: jsonify({
                    type: "task_overdue",
                    address: tasklist.first()!.address,
                }),
            });

            // Verify that SMs notifications are sent
            expect(SMS.send).to.have.callCount(1);
            expect(SMS.send).to.have.been.calledWith({
                to: [sms_device.phone_number],
                message: template[`${type}_sms`],
            });

            // Verify that email notifications are sent
            expect(Email.send).to.have.callCount(1);
            expect(Email.send).to.have.been.calledWith({
                from: "info@silvervue.com",
                bcc: [email_device.email_address],
                content: {
                    html: template[`${type}_email`],
                    subject: template[`${type}_subject`],
                },
            });

            expect(PERS.alertevent).to.have.callCount(1);
            expect(PERS.alertevent).to.have.been.calledWith({
                imei: pers_device.identifier,
                message: {
                    action: "PLAY1",
                    vibrate: true,
                },
            });
        };
    }

    it("should notify job if 'appointment' task is overdue", should_notify("appointment"));

    it("should notify job if 'medication' task is overdue", should_notify("medication"));

    it("should notify job if 'other' task is overdue", should_notify("other"));
});
