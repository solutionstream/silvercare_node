import "mocha";
import * as moment from "moment";
import { IJob, IOverdueCountJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Job, TaskList } from "../../models";
import { Schedule } from "../../scheduling";
import { expect, sinon } from "../../testing";
import { BackOff } from "../../utils";
import { OverdueCount } from "./overdue_count";


describe("OverdueCount", async () => {
    let db: database.Db;
    let backoff: BackOff;
    let task: OverdueCount;
    let patient_id: database.ObjectID;
    let sandbox: sinon.SinonSandbox;
    let reschedule: sinon.SinonSpy;
    let completed: sinon.SinonSpy;
    let failed: sinon.SinonSpy;
    let current_date: sinon.SinonStub;

    const start = moment("2017-11-01T10:00:00.000").toDate();
    const now = moment("2017-11-01T11:30:30.500").toDate();

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        patient_id = new database.ObjectID();
        task = new OverdueCount(db);
        backoff = BackOff.exponential();

        sandbox = sinon.sandbox.create();
        reschedule = sandbox.spy(Job, "reschedule");
        completed = sandbox.spy(Job, "completed");
        failed = sandbox.spy(Job, "failed");
        current_date = sandbox.stub(Date, "current").returns(now);

        await db.dropDatabase();
        await database.setupCollections(db);
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    interface CreateTasklist {
        schedule: Schedule;
        patient_id?: database.ObjectID;
    }

    interface CreateJob {
        trigger: Date;
        tasklist: database.ObjectID|TaskList;
        patient_id?: database.ObjectID;
        most_recent?: Date;
    }

    // Create a tasklist object
    function createTasklist(opts: CreateTasklist): TaskList {
        return TaskList.factory({
            patient_id: opts.patient_id || patient_id,
            schedule: opts.schedule,
            shared: { title: "Test", type: "other" },
        });
    }

    // Create a job
    function createJob(opts: CreateJob): IOverdueCountJob & IJob {
        const job = Job.factory({
            type: "overdue_count",
            trigger: { type: "date", date: opts.trigger },
            tasklist_id: database.toID(opts.tasklist),
            patient_id: opts.patient_id || patient_id,
            most_recent: opts.most_recent || null,
        });

        // This is here to please typescript
        if (job.type !== "overdue_count") {
            throw new Error("Wrong job type");
        }

        return job;
    }

    it("should do nothing if no job is found", async () => {
        await task.run(backoff);

        expect(reschedule).to.have.callCount(0);
        expect(completed).to.have.callCount(0);
        expect(failed).to.have.callCount(0);
    });

    it("should search from start of tasklist", async () => {
        const trigger = moment("2017-11-01T11:00:00.000").toDate();

        // Create a simple tasklist object
        const schedule = Schedule.once(start);
        const tasklist = createTasklist({ schedule });

        // Create a job attached to this tasklist
        const job = createJob({ trigger, tasklist });

        const start_date = task.search_start(job, tasklist);
        expect(start_date).to.deep.equal(start);
    });

    it("should search from most_recent date", async () => {
        const trigger = moment("2017-11-01T11:00:00.000").toDate();

        // Create a simple tasklist object
        const schedule = Schedule.once(start);
        const tasklist = createTasklist({ schedule });

        // Create job with most_recent date
        const most_recent = moment("2017-11-01T05:00:00.000").toDate();
        const job = createJob({ trigger, tasklist, most_recent });

        const start_date = task.search_start(job, tasklist);
        const expected_date = moment(most_recent).add({ ms: 1 }).toDate();
        expect(start_date).to.deep.equal(expected_date);
    });

    it("should search until now if there is no schedule", async () => {
        // Create tasklist with absolute schedule
        const schedule = Schedule.once(start);
        const tasklist = createTasklist({ schedule });

        const until_date = task.search_until(now, tasklist);
        expect(tasklist.schedule(now)).to.equal(undefined);
        expect(until_date).to.deep.equal(now);
    });

    it("should search until now if schedule is absolute", async () => {
        // Create tasklist with absolute schedule
        const schedule = Schedule.once(start);
        const tasklist = createTasklist({ schedule });

        const start_date = task.search_until(start, tasklist);
        expect(tasklist.schedule(start)).to.not.equal(undefined);
        expect(start_date).to.deep.equal(start);
    });

    it("should search last period if schedule is relative", async () => {
        // Create tasklist with relative schedule
        const schedule = Schedule.hourly(start);
        const tasklist = createTasklist({ schedule });

        const start_date = task.search_until(now, tasklist);
        const expected_date = moment("2017-11-01T10:59:59.999").toDate();
        expect(tasklist.schedule(now)).to.not.equal(undefined);
        expect(start_date).to.deep.equal(expected_date);
    });

    it("should update tasklist overdue_count", async () => {
        // Create a tasklist in the database
        // This also creates a scheduled job
        const tasklist = await TaskList.create(db, {
            patient_id,
            schedule: Schedule.once(start),
            shared: { title: "Test", type: "other" },
            overdue_count: 1, // Already overdue
        });

        // Run the worker
        await task.run(backoff, now);

        // Ensure that tasklist overdue count is updated
        await tasklist.refresh(db);
        expect(tasklist.overdue_count).to.equal(2);
    });

    it("should not count completed tasks as overdue");

    it("should reschedule to after next task", async () => {
        // Create a tasklist in the database
        // This also creates a scheduled job
        await TaskList.create(db, {
            patient_id,
            schedule: Schedule.hourly(start),
            shared: { title: "Test", type: "other" },
        });

        // Run the worker
        await task.run(backoff, now);

        // Determine when the next check should be
        const {start: start_next} = Schedule.next_period(now, "hourly");
        const next_check = moment(start_next).add({ m: 60, ms: 1 }).toDate();

        // Verify the job after the worker completes
        const job = await reschedule.firstCall.returnValue;
        expect(job).to.shallow.equal({
            most_recent: start,
            status: "pending",
            trigger: { date: next_check },
            last_completed: now,
        });
    });

    it("should reschedule in the event of an error", async () => {
        // Ensure there is an error
        sandbox.stub(TaskList, "fetchOne").throws();

        // Create a tasklist in the database
        // This also creates a scheduled job
        await TaskList.create(db, {
            patient_id,
            schedule: Schedule.once(start),
            shared: { title: "Test", type: "other" },
        });

        // Run the worker
        try {
            await task.run(backoff, now);
            expect.fail("should throw");
        } catch (e) { /*Intentionally empty */ }

        // Verify the job after the worker completes
        const job = await failed.firstCall.returnValue;
        expect(job).to.shallow.equal({
            most_recent: null,
            status: "pending",
            trigger: { date: now },
            last_completed: null,
            attempts: 1,
        });

    });

    it("should not reschedule job if tasklist not found", async () => {
        // Create a job for a non-existent task
        const job = createJob({
            trigger: start,
            tasklist: new database.ObjectID(),
        });
        await Job.insert(db, job);

        // Run the worker
        await task.run(backoff, now);

        // Verify the job after the worker completes
        const result = await completed.firstCall.returnValue;
        expect(result).to.shallow.equal({
            most_recent: null,
            status: "completed",
            trigger: { date: start },
        });
    });

    it("should not reschedule if there will be no more tasks", async () => {
        // Create a tasklist in the database
        // This also creates a scheduled job
        await TaskList.create(db, {
            patient_id,
            schedule: Schedule.once(start),
            shared: { title: "Test", type: "other" },
            overdue_count: 1, // Already overdue
        });

        // Run the worker
        await task.run(backoff, now);

        // Verify the job after the worker completes
        const job = await completed.firstCall.returnValue;
        expect(job).to.shallow.equal({
            most_recent: start,
            status: "completed",
            trigger: { date: now },
            last_completed: now,
        });
    });

    it("should schedule alert if tasklist has overdue tasks", async () => {
        // Create a tasklist in the database
        // This also creates a scheduled job
        const tasklist = await TaskList.create(db, {
            patient_id,
            schedule: Schedule.once(start),
            shared: { title: "Test", type: "other" },
        });

        // Run the worker
        await task.run(backoff, now);

        // Ensure that tasklist overdue count is updated
        await tasklist.refresh(db);
        expect(tasklist.overdue_count).to.equal(1);

        // Ensure that it creates an overdue alert
        const job = await Job.collection(db).findOne({
            type: IJob.Type.alert_overdue,
            tasklist_id: tasklist._id,
        });
        expect(job).to.shallow.equal({
            type: IJob.Type.alert_overdue,
            tasklist_id: tasklist._id,
            status: "pending",
            trigger: { date: now },
        });
    });
});
