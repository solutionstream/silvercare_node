import "mocha";
import * as moment from "moment";
import { IAlertReassignJob, IJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Email } from "../../emails";
import { CareGiver, Device, DeviceList, Job, Patient, TaskList } from "../../models";
import { PushNotification } from "../../notifications";
import { PERS } from "../../pers";
import { Schedule } from "../../scheduling";
import { SMS } from "../../sms";
import { TemplateDirectory } from "../../templating";
import { expect, sinon } from "../../testing";
import { BackOff, Dictionary, jsonify } from "../../utils";
import { AlertReassign } from "./alert_reassign";


describe("AlertReassign", () => {
    let db: database.Db;
    let sandbox: sinon.SinonSandbox;
    let now: Date;
    let task: AlertReassign;
    let backoff: BackOff;
    let template: Dictionary<string>;
    let default_assign: IAlertReassignJob.DefaultReason;
    let default_unassign: IAlertReassignJob.DefaultReason;
    let instance_assign: IAlertReassignJob.InstanceReason;
    let instance_unassign: IAlertReassignJob.InstanceReason;
    let schedule: Schedule;
    let patient: Patient;
    let caregiver_1: CareGiver;
    let caregiver_2: CareGiver;
    let patient_devices: DeviceList;
    let caregiver_1_devices: DeviceList;
    let caregiver_2_devices: DeviceList;
    let ios_device: Device.Mobile;
    let android_device: Device.Mobile;
    let sms_device: Device.SMS;
    let email_device: Device.Email;
    let pers_device: Device.PERS;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        now = moment().toDate();
        task = new AlertReassign(db);
        backoff = BackOff.exponential();
        schedule = Schedule.once(now);

        // Stubs and spies
        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);
        sandbox.stub(Email, "send");
        sandbox.stub(SMS, "send");
        sandbox.stub(PERS, "alertevent");
        sandbox.stub(PushNotification, "send");
        sandbox.spy(Job, "completed");
        sandbox.spy(Job, "failed");
        sandbox.spy(Job, "dequeue");
        sandbox.spy(backoff, "next");
        sandbox.spy(backoff, "reset");

        // Stub template output
        template = {
            email: "email_template",
            push: "push_template",
            sms: "sms_template",
            subject: "subject_template",
        };
        sandbox.stub(TemplateDirectory.prototype, "render").returns(template);

        // Database fixtures
        ios_device = {
            _id: database.id(),
            type: "ios",
            created: now,
            version: { major: 0, minor: 0, patch: 0 },
            push_token: "ExponentPushToken[ios]",
        };

        android_device = {
            _id: database.id(),
            type: "android",
            created: now,
            version: { major: 0, minor: 0, patch: 0 },
            push_token: "ExponentPushToken[android]",
        };

        sms_device = {
            _id: database.id(),
            type: "sms",
            created: now,
            phone_number: "+18015551234",
        };

        email_device = {
            _id: database.id(),
            type: "email",
            created: now,
            email_address: "1@example.com",
        };

        pers_device = {
            _id: database.id(),
            type: "pers",
            created: now,
            identifier: "asdf",
        };

        // Create a caregiver with iOS and email devices
        caregiver_1 = CareGiver.factory({ user_id: database.id(), full_name: "1" });
        caregiver_1_devices = DeviceList.factory({
            user_id: caregiver_1.user_id,
            devices: [ios_device, email_device],
        });

        // Create a caregiver with android and SMS devices
        caregiver_2 = CareGiver.factory({ user_id: database.id(), full_name: "2" });
        caregiver_2_devices = DeviceList.factory({
            user_id: caregiver_2.user_id,
            devices: [android_device, sms_device],
        });

        // Create patient and devicelist
        patient = Patient.factory({
            _id: database.id(),
            user_id: database.id(),
            full_name: "Patient",
            carecircle: {
                contacts: [],
                caregivers: [caregiver_1.contact(), caregiver_2.contact()],
            },
        });
        patient_devices = DeviceList.factory({
            user_id: patient.user_id,
            devices: [pers_device],
        });

        // Assign reasons
        default_assign = { type: "default", previous: null, current: caregiver_1._id };
        instance_assign = { type: "instance", previous: null, current: caregiver_2._id, task_date: now, task_position: 1 };
        default_unassign = { type: "default", previous: null, current: null };
        instance_unassign = { type: "instance", previous: null, current: null, task_date: now, task_position: 1 };

        // Setup and insert data
        await database.setupCollections(db);
        await Promise.all([
            Patient.insert(db, patient),
            CareGiver.insert(db, caregiver_1),
            CareGiver.insert(db, caregiver_2),
            DeviceList.create(db, patient_devices),
            DeviceList.create(db, caregiver_1_devices),
            DeviceList.create(db, caregiver_2_devices),
        ]);
    });

    afterEach(async () => {
        sandbox.restore();
        await db.dropDatabase();
    });

    interface CreateJob {
        trigger: Date;
        tasklist: TaskList;
        reasons: IAlertReassignJob.Reason[];
        patient?: Patient | database.ObjectID;
    }

    // Create a job object
    function createJob(opts: CreateJob): IAlertReassignJob & IJob {
        const job = Job.factory({
            type: "alert_reassign",
            trigger: { type: "date", date: opts.trigger },
            tasklist_id: database.toID(opts.tasklist),
            patient_id: database.toID(opts.patient || opts.tasklist.patient_id),
            reasons: opts.reasons,
        });

        if (job.type !== "alert_reassign") {
            throw new Error("Shut up typescript");
        }

        return job;
    }

    interface CreateTasklist {
        schedule: Schedule;
        patient: Patient | database.ObjectID;
    }

    // Create a tasklist object
    function createTasklist(opts: CreateTasklist): TaskList {
        return TaskList.factory({
            patient_id: database.toID(opts.patient),
            schedule: opts.schedule,
            shared: { title: "Test", type: "other" },
        });
    }

    interface ScheduleJob {
        schedule: Schedule;
        patient: Patient | database.ObjectID;
        reasons: IAlertReassignJob.Reason[];
    }

    // Create an overdue tasklist
    async function scheduleJob(opts: ScheduleJob) {
        const tasklist = createTasklist(opts);
        const job = createJob({
            trigger: now,
            tasklist,
            patient: tasklist.patient_id,
            reasons: opts.reasons,
        });

        await Promise.all([
            TaskList.insert(db, tasklist),
            Job.insert(db, job),
        ]);

        return { job, tasklist };
    }


    it("should do nothing if no job is found", async () => {
        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(0);
        expect(backoff.reset).to.have.callCount(0);
        expect(PushNotification.send).to.have.callCount(0);
        expect(Email.send).to.have.callCount(0);
        expect(SMS.send).to.have.callCount(0);
    });

    it("should reset backoff if job is found", async () => {
        await scheduleJob({ patient, schedule, reasons: [] });

        await task.run(backoff, now);
        expect(backoff.reset).to.have.callCount(1);
    });

    it("should complete job if patient is not found", async () => {
        await scheduleJob({ patient: database.id(), schedule, reasons: [] });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
        expect(PushNotification.send).to.have.callCount(0);
        expect(Email.send).to.have.callCount(0);
        expect(SMS.send).to.have.callCount(0);
    });

    it("should complete job if tasklist is not found", async () => {
        const tasklist = createTasklist({ patient, schedule });
        const job = createJob({ tasklist, trigger: now, reasons: [] });
        await Job.insert(db, job);

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
        expect(PushNotification.send).to.have.callCount(0);
        expect(Email.send).to.have.callCount(0);
        expect(SMS.send).to.have.callCount(0);
    });

    it("should complete job if job has no reassign reason", async () => {
        await scheduleJob({ patient, schedule, reasons: [] });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
        expect(PushNotification.send).to.have.callCount(0);
        expect(Email.send).to.have.callCount(0);
        expect(SMS.send).to.have.callCount(0);
    });

    it("should notify single caregiver on default assign", async () => {
        await scheduleJob({ patient, schedule, reasons: [ default_assign ]});

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);

        expect(PushNotification.send).to.have.callCount(1);
        expect(PushNotification.send).to.have.been.calledWith({
            tokens: [ ios_device.push_token ],
            title: template.subject,
            body: template.push,
            data: {
                type: "alert_reassign",
                ...jsonify(default_assign),
            },
        });

        expect(Email.send).to.have.callCount(1);
        expect(Email.send).to.have.been.calledWith({
            from: config.email.from,
            bcc: [ email_device.email_address ],
            content: {
                html: undefined,
                subject: template.subject,
                text: template.email,
            },
        });

        expect(SMS.send).to.have.callCount(0);
    });

    it("should notify single caregiver on instance assign", async () => {
        await scheduleJob({ patient, schedule, reasons: [instance_assign] });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);

        expect(PushNotification.send).to.have.callCount(1);
        expect(PushNotification.send).to.have.been.calledWith({
            tokens: [android_device.push_token],
            title: template.subject,
            body: template.push,
            data: {
                type: "alert_reassign",
                ...jsonify(instance_assign),
            },
        });

        expect(SMS.send).to.have.callCount(1);
        expect(SMS.send).to.have.been.calledWith({
            to: [sms_device.phone_number],
            message: template.sms,
        });

        expect(Email.send).to.have.callCount(0);
    });

    it("should notify carecircle on default unsassign", async () => {
        await scheduleJob({ patient, schedule, reasons: [default_unassign] });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);

        expect(PushNotification.send).to.have.callCount(1);
        expect(PushNotification.send).to.have.been.calledWith({
            tokens: [ios_device.push_token, android_device.push_token],
            title: template.subject,
            body: template.push,
            data: {
                type: "alert_reassign",
                ...jsonify(default_unassign),
            },
        });

        expect(Email.send).to.have.callCount(1);
        expect(Email.send).to.have.been.calledWith({
            from: config.email.from,
            bcc: [email_device.email_address],
            content: {
                html: undefined,
                subject: template.subject,
                text: template.email,
            },
        });

        expect(SMS.send).to.have.callCount(1);
        expect(SMS.send).to.have.been.calledWith({
            to: [sms_device.phone_number],
            message: template.sms,
        });

        expect(PERS.alertevent).to.have.callCount(1);
        expect(PERS.alertevent).to.have.been.calledWith({
            imei: pers_device.identifier,
            message: {
                action: "PLAY1",
                vibrate: true,
            },
        });
    });

    it("should notify carecircle on instance unassign", async () => {
        await scheduleJob({ patient, schedule, reasons: [instance_unassign] });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);

        expect(PushNotification.send).to.have.callCount(1);
        expect(PushNotification.send).to.have.been.calledWith({
            tokens: [ ios_device.push_token, android_device.push_token],
            title: template.subject,
            body: template.push,
            data: {
                type: "alert_reassign",
                ...jsonify(instance_unassign),
            },
        });

        expect(SMS.send).to.have.callCount(1);
        expect(SMS.send).to.have.been.calledWith({
            to: [sms_device.phone_number],
            message: template.sms,
        });

        expect(Email.send).to.have.callCount(1);
        expect(Email.send).to.have.been.calledWith({
            from: config.email.from,
            bcc: [email_device.email_address],
            content: {
                html: undefined,
                subject: template.subject,
                text: template.email,
            },
        });

        expect(PERS.alertevent).to.have.callCount(1);
        expect(PERS.alertevent).to.have.been.calledWith({
            imei: pers_device.identifier,
            message: {
                action: "PLAY1",
                vibrate: true,
            },
        });
    });

    it("should not notify caregivers with disabled settings");
});
