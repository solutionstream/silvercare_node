import * as chalk from "chalk";
import * as debug from "debug";
import { IJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Email } from "../../emails";
import { Job, PAC, User } from "../../models";
import { TemplateDirectory } from "../../templating";
import { BackOff, Dashboard } from "../../utils";


const log = debug("invite_pac");

export class InvitePAC {

    constructor(
        public db: database.Db) { }

    async run(backoff: BackOff, now: Date = new Date()) {
        log(chalk.cyan(`Looking for jobs ready by: ${now}`));
        const { db } = this;

        // Get a single job from the queue
        const [job] = await Job.dequeue(db, {
            type: IJob.Type.invite_pac,
            date: now,
            limit: 1,
            timeout: 60,
        });

        // Stop if no jobs were dequeued
        if (!job || job.type !== IJob.Type.invite_pac) {
            log(chalk.yellow(`No suitable job found: ${job}`));
            return;
        }

        try {
            // If a job was found then reset the backoff timer
            log(chalk.cyan(`[${job._id}] Started job`));
            backoff.reset();

            // Fetch the associated user
            const [user, pac] = await Promise.all([
                User.get(db, { _id: job.user_id }),
                PAC.fetch(db, { user_id: job.user_id }),
            ]);

            // If there is no matching user then complete the job
            if (!user || !pac) {
                log(chalk.yellow(`[${job._id}] User does not exist`));
                await Job.completed(db, job);
                return;
            }

            // Issue single-use token with expiry
            const token = await user.issueToken({ expires: 6 * 60 * 60, once: true });
            await user.save(db);

            // Render templates for email content
            const directory = TemplateDirectory.default("invite_pac");
            const template = await directory.render({
                ...user,
                name: pac.full_name,
                link: Dashboard.setup(job.user_id, { token, next: Dashboard.pac(job.user_id) }),
            });

            // Create a devicelist using the user's username as an email address
            const response = await Email.send({
                from: config.email.from,
                to: [user.username],
                content: {
                    subject: template.subject,
                    text: template.text,
                },
            });

            log(chalk.green(`[${job._id}] Email sent ${response.messageId}`));
            await Job.completed(db, job);

        } catch (error) {

            log(chalk.red(error));
            await Job.failed(db, job, error);
            throw error;

        }
    }
}

/* istanbul ignore if */
if (require.main === module) {
    (async () => {
        const backoff = BackOff.exponential({ multiplier: 60000, maxDelay: 900000 });
        const db = await database.connect(config.db.app);
        const job = new InvitePAC(db);

        while (true) {
            await backoff.wait();
            await job.run(backoff);
        }
    })()
        .then(() => process.exit())
        .catch((error) => { log(chalk.red(error)); process.exit(1); });
}
