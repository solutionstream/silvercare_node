import "mocha";
import * as config from "../../config";
import * as database from "../../database";
import { Email } from "../../emails";
import { Job, PAC, User } from "../../models";
import { TemplateDirectory } from "../../templating";
import { expect, sinon } from "../../testing";
import { BackOff } from "../../utils";
import { InvitePAC } from "./invite_pac";


describe("InvitePAC", () => {
    let db: database.Db;
    let sandbox: sinon.SinonSandbox;
    let now: Date;
    let task: InvitePAC;
    let backoff: BackOff;
    let user_id: database.ObjectID;
    let subject_tpl: string;
    let content_tpl: string;

    before(async () => {
        db = await database.random(config.db.test);
    });

    after(async () => {
        await database.destroy(db);
    });

    beforeEach(async () => {
        now = new Date();
        task = new InvitePAC(db);
        backoff = BackOff.exponential();
        user_id = database.id();
        subject_tpl = "subject";
        content_tpl = "content";

        // Stubs and spies
        sandbox = sinon.sandbox.create();
        sandbox.stub(Date, "current").returns(now);
        sandbox.stub(Email, "send").returns({});
        sandbox.spy(Job, "completed");
        sandbox.spy(Job, "failed");
        sandbox.spy(Job, "dequeue");
        sandbox.spy(backoff, "next");
        sandbox.spy(backoff, "reset");
        sandbox.stub(TemplateDirectory.prototype, "render").returns({
            subject: subject_tpl,
            text: content_tpl,
        });

        // Database setup
        await database.setupCollections(db);
    });

    afterEach(async () => {
        sandbox.restore();
        await database.drop(db);
    });


    interface ScheduleJob {
        user_id: database.ObjectID;
    }

    async function scheduleJob(opts: ScheduleJob) {
        const job = await Job.create(db, {
            type: "invite_pac",
            trigger: { type: "date", date: now },
            user_id: opts.user_id,
        });

        if (job.type !== "invite_pac") {
            throw new Error("Shut up typescript");
        }

        return job;
    }

    it("should do nothing if no job is found", async () => {
        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(0);
        expect(backoff.reset).to.have.callCount(0);
        expect(Email.send).to.have.callCount(0);
    });

    it("should reset backoff object if job is found", async () => {
        await scheduleJob({ user_id });

        await task.run(backoff, now);
        expect(backoff.reset).to.have.callCount(1);
    });

    it("should complete job if user is not found", async () => {
        await scheduleJob({ user_id });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
        expect(Email.send).to.have.callCount(0);
    });

    it ("should complete job if pac is not found", async () => {
        await User.create(db, { _id: user_id, username: "Test User" });
        await scheduleJob({ user_id });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);
        expect(Email.send).to.have.callCount(0);
    });

    it("should send pac invite email", async () => {
        const user = await User.create(db, { _id: user_id, username: "mmouse@aol.com" });
        await PAC.create(db, { user_id, full_name: "Test PAC" });
        await scheduleJob({ user_id });

        await task.run(backoff, now);
        expect(Job.completed).to.have.callCount(1);

        // Verify that the template was invoked
        expect(TemplateDirectory.prototype.render).to.have.callCount(1);

        // Verify that email was sent
        expect(Email.send).to.have.callCount(1);
        expect(Email.send).to.have.been.calledWith({
            from: "info@silvervue.com",
            to: [user.username],
            content: {
                subject: subject_tpl,
                text: content_tpl,
            },
        });
    });
});
