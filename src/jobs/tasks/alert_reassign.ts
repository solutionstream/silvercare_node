import * as chalk from "chalk";
import * as debug from "debug";
import { IJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { CareGiver, DeviceList, Job, Patient, TaskList } from "../../models";
import { PERS } from "../../pers";
import { TemplateDirectory } from "../../templating";
import { BackOff } from "../../utils";


const log = debug("alert_reassign");

export class AlertReassign {

    constructor(
        public db: database.Db) { }

    async run(backoff: BackOff, now: Date = new Date()) {
        log(chalk.cyan(`Looking for jobs ready by: ${now}`));
        const { db } = this;

        // Get a single job from the queue
        const [job] = await Job.dequeue(db, {
            type: IJob.Type.alert_reassign,
            date: now,
            limit: 1,
            timeout: 60,
        });

        // Stop if no jobs were dequeued
        if (!job || job.type !== IJob.Type.alert_reassign) {
            log(chalk.yellow(`No suitable job found: ${job}`));
            return;
        }

        try {

            // If a job was found then reset the backoff timer
            log(chalk.cyan(`[${job._id}] Started job`));
            backoff.reset();

            // Fetch tasklist and patient associated with this job
            const [tasklist, patient] = await Promise.all([
                TaskList.fetchOne(db, job.patient_id, job.tasklist_id),
                Patient.fetch(db, { _id: job.patient_id }),
            ]);

            // If either is missing then there is nothing we can do
            if (!tasklist || !patient) {
                log(chalk.yellow(`[${job._id}] Tasklist or patient not found`));
                await Job.completed(db, job);
                return;
            }

            // Ensure that we have a reason for the notification
            const [reason] = job.reasons;
            if (!reason) {
                log(chalk.yellow(`[${job._id}] We don't know what changed`));
                await Job.completed(db, job);
                return;
            }

            // Caregivers to notify
            let caregivers: CareGiver[] = [];

            // Someone is assigned to the task
            if (reason.current) {
                const caregiver = await CareGiver.fetch(db, { "_id": reason.current });
                if (caregiver) { caregivers = [caregiver]; }
            }
            // No one is assigned to the task. Notify the entire carecircle
            else {
                caregivers = await CareGiver.getAll(db, patient.carecircle.caregivers);
            }

            // Get notification settings for each patient
            caregivers = caregivers.filter((caregiver) => {
                const settings = caregiver.notification_settings(patient);
                return settings.enabled
                    && settings.task_enabled(tasklist.shared.type)
                    && settings.task_assign(tasklist.shared.type);
            });

            // Get the device lists for all of the caregivers who will receive this notification
            const devicelists = await DeviceList.getAll(db, caregivers.map((caregiver) => caregiver.user_id));
            const devicelist = DeviceList.merge(devicelists);

            // Render templates in order to determine notifications
            const alert_type = reason.current ? "alert_assign" : "needs_volunteer";
            const directory = TemplateDirectory.default(alert_type);
            const template = await directory.render({ tasklist, patient });

            // Send push notification to mobile devices
            const push_notifications = devicelist.push({
                title: template.subject,
                body: template.push,
                data: {
                    type: alert_type,
                    ...reason,
                },
            });

            // Send SMS notification to phone numbers
            const sms_notifications = devicelist.sms({
                message: template.sms,
            });

            // Send email notificaiton to email addresses
            const email_notifications = devicelist.email({
                message: {
                    subject: template.subject,
                    html: template.email,
                },
            });

            // Send notification to PERS devices
            const pers_notifications = devicelist.pers({
                message: {
                    vibrate: true,
                    action: PERS.Message.GENERIC,
                },
            });

            // Wait for all notifications to complete
            await Promise.all([
                push_notifications,
                sms_notifications,
                email_notifications,
                pers_notifications,
            ]);

            await Job.completed(db, job);

        } catch (error) {

            log(chalk.red(error));
            await Job.failed(db, job, error);
            throw error;

        }
    }
}

/* istanbul ignore if */
if (require.main === module) {
    (async () => {
        const backoff = BackOff.exponential({ multiplier: 100, maxDelay: 60000 });
        const db = await database.connect(config.db.app);
        const job = new AlertReassign(db);

        while (true) {
            await backoff.wait();
            await job.run(backoff);
        }
    })()
    .then(() => process.exit())
    .catch((error) => { log(error); process.exit(1); });
}
