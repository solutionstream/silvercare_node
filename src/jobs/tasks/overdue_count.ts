import * as chalk from "chalk";
import * as debug from "debug";
import * as moment from "moment";
import { Agenda, IJob, IOverdueCountJob } from "../";
import * as config from "../../config";
import * as database from "../../database";
import { Activity, ActivityLog, Job, TaskList } from "../../models";
import { Schedule } from "../../scheduling";
import { BackOff } from "../../utils";


const log = debug("overdue_count");


export class OverdueCount {
    constructor(public db: database.Db) {}

    // Start searching from either the most recent task processed or the start of the tasklist
    search_start(job: IOverdueCountJob, tasklist: TaskList): Date {
        if (job.most_recent) {
            return moment(job.most_recent).add({ ms: 1 }).toDate();
        }

        return tasklist.start!;
    }

    // Stop searching at the end of the previous period (relative) or now (absolute)
    search_until(now: Date, tasklist: TaskList): Date {
        const schedule = tasklist.schedule(now);
        if (!schedule || !schedule.relative) {
            return now;
        }

        const period = Schedule.last_period(now, schedule.frequency);
        return period.until;
    }

    // Determine when to next run this job
    next_run(now: Date, tasklist: TaskList): undefined|Date {
        const next = tasklist.next(now);
        if (!next) { return undefined; }

        // Configure to run after the next task would expire
        return moment(next.date)
            .add({ m: config.task.overdue_after_mins, ms: 1 })
            .toDate();
    }

    async run(backoff: BackOff, now: Date = new Date()) {
        log(chalk.cyan(`Looking for jobs ready by: ${now}`));
        const { db } = this;

        // Get a single job from the queue
        const [job] = await Job.dequeue(db, {
            type: IJob.Type.overdue_count,
            date: now,
            limit: 1,
            timeout: 60,
        });

        // Stop if no jobs were dequeued
        if (!job || job.type !== IJob.Type.overdue_count) {
            log(chalk.yellow(`No suitable job found: ${job}`));
            return;
        }

        try {
            log(chalk.cyan(`Started job with id: ${job._id}`));
            backoff.reset();

            // Fetch the associated tasklist
            const tasklist = await TaskList.fetchOne(db, job.patient_id, job.tasklist_id);

            // If there is no matching job then mark it as completed
            // The task will stop running as there is nothing it can do in the future
            if (!tasklist) {
                log(chalk.yellow(`Tasklist not found: ${job.tasklist_id}`));
                await Job.completed(db, job);
                return;
            }

            // Determine the search range for our tasklist
            const search_start = this.search_start(job, tasklist);
            const search_until = this.search_until(now, tasklist);

            // Get overdue tasks from the last time this job was completed to now
            const tasks = tasklist.between(search_start, search_until);
            const overdue = tasks.filter((task) => task.overdue(now));
            log(chalk.cyan(`${overdue.length} overdue tasks between ${search_start} and ${search_until}`));

            // Add the number of overdue items to the tasklist counter
            tasklist.overdue_count += overdue.length;
            log(chalk.cyan(`Tasklist overdue count: ${tasklist.overdue_count}`));

            // Add an activity entry for each overdue task
            const activities = overdue.map((task) => ActivityLog.create(db, {
                patient_id: job.patient_id,
                type: "task_overdue",
                title: "Task Incomplete",
                author: Activity.Author.factory({
                    _id: database.id(),
                    type: "system",
                    full_name: "System",
                }),
                meta: task,
            }));

            // Wait for database writes before continuing
            await Promise.all(activities);

            // Change "most_recent" to date of newest task
            // This ensures that we do not check the same task again
            const most_recent = tasks[tasks.length - 1];
            job.most_recent = most_recent ? most_recent.date : job.most_recent;
            log(chalk.cyan(`Tasklist most recent date: ${job.most_recent}`));

            // Determine when to next check the overdue status
            const next_check = this.next_run(now, tasklist);

            if (next_check) {
                await Job.reschedule(db, job, next_check);
                log(chalk.green(`Scheduled job to run again: ${next_check}`));
            } else {
                log(chalk.yellow(`Tasklist does not need to run anymore: ${tasklist.until}`));
                await Job.completed(db, job);
            }

            // Save the tasklist
            await tasklist.save(db);
            log(chalk.green(`Updated tasklist with changes`));

            // Schedule an alert if the tasklist has overdue tasks
            if (overdue.length) {
                const latest = overdue[overdue.length - 1];
                await Agenda.alert_overdue(db, {
                    tasklist: job.tasklist_id,
                    patient: job.patient_id,
                    task_date: latest.date,
                    task_position: latest.position,
                });
            }

        } catch (error) {

            log(chalk.red(error));
            await Job.failed(db, job, error);
            throw error;

        }
    }

}

/* istanbul ignore if */
if (require.main === module) {
    (async () => {
        const backoff = BackOff.exponential({ multiplier: 100, maxDelay: 60000 });
        const db = await database.connect(config.db.app);
        const job = new OverdueCount(db);

        while (true) {
            await backoff.wait();
            await job.run(backoff);
        }

    })()
    .then(() => process.exit())
    .catch((error) => { log(error); process.exit(1); });
}
